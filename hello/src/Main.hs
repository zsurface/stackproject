-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 

-- forall a b.
{-# LANGUAGE RankNTypes #-}  
{-# LANGUAGE TypeOperators #-}
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split

import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import Control.Applicative       
import qualified Text.Regex.TDFA as TD
import qualified Data.HashMap.Strict as M
import qualified Data.Text                 as TS

       
import AronModule

       
insertAll2::[(String, [([String], Integer)])] -> HMap2 -> HMap2
insertAll2 [] m = m 
insertAll2 (x:cx) m = insertAll2 cx (insertAppend2 (fst x) (snd x) m)
          
insertAppend2::String -> [([String], Integer)] -> HMap2 -> HMap2
insertAppend2 k ls m = M.insert k (ls ++ rls) m
      where 
          rls = case M.lookup k m of
                          Nothing -> []
                          Just x -> x
          
          


-- Data HMap  = M.HashMap String [[String]]  
-- Data HMap2 = M.HashMap String [([String], Integer)]
snippetMap2::[([String], [String], Integer)] -> IORef HMap2 -> IO ()
snippetMap2 pplist ref = do
        -- let path = "/Users/cat/myfile/bitbucket/snippets/snippet_test.hs"
        -- let path = "/Users/cat/myfile/bitbucket/snippets/snippet.hs"

        -- readSnippet::FilePath->IO [([String], [String])]
        -- pplist <- readSnippet path 
        let keylist = L.map(\x -> 
                                (foldr(++) [] $ L.map(\y -> prefix y) (t1 x),
                                 (t2 x, t3 x)
                                )
                                
                            ) pplist 


        let mymap = map(\cx -> [(x, y) | x <- fst cx, y <- [snd cx]]) keylist
        let lmap = foldr(++) [] mymap
        pre $ typeOf lmap
        -- sort x of [(x, y, n)]
        let sortedList = qqsort(\x y -> f x y) lmap                                        
              where f x y = fst x > fst y
        -- convert list [(x, y)] to map
        let mmap = M.fromList lmap                                                         
        let group= L.groupBy(\x y -> f x y) sortedList                                       
              where f x y = fst x == fst y                                 
        
        --
        -- unzip::[("dog", "dogs"), ("cat", "cats")] => (["dog", "cat"], ["dogs", "cats"])
        let uzip = map(\x -> unzip x) group

        -- fix bug: unique $ snd x => remove duplicated values
        -- cause duplicated blocks: let tupleList = map(\x -> (head . fst $ x, snd x)) uzip
        -- tupleList => [("haskell", [["dog", "line1"], ["cat", "line2"]])]
        -- tupleList => [(String, [[String]])
        let tupleList = map(\x -> (head . fst $ x, unique $ snd x)) uzip
        -- pre tupleList

        modifyIORef ref (insertAll2 tupleList)
        hmap <- readIORef ref
        fw "hmap beg" 
        -- pre hmap
        fw "hmap end" 
        return () 
          where
             t1 (x, y, z) = x
             t2 (x, y, z) = y 
             t3 (x, y, z) = z

instance Num b => Num (a -> b) where
      negate      = fmap negate
      (+)         = liftA2 (+)
      (*)         = liftA2 (*)
      fromInteger = pure . fromInteger
      abs         = fmap abs
      signum      = fmap signum

(∘) = multiMat

type HMap2 = M.HashMap String [([String], Integer)]

instance Extract [a] where
    empty = [] 
    before n cx = take n cx 
    after n cx = drop n cx 
    extract (a, b) cx = take b $ drop a  cx

validateFormat::String -> Bool
validateFormat s = if len li > 1
                   then
                     if (len . sp . head) li > 2
                     then True
                     else False
                   else False
  where
    li = lines s
    sp e = splitStr ":" e


-- 2       4      3
-- 1       1      1
--(1x2) (1x2x4) (1x2x4x3)

{-|
    1 ←
    4 1 ←
    3 4 1 
    ↑ ↑ ↑
    2 3 4   
    ↓ ↓ ↓
    1 2 3
      1 2
        1

   0 1 2 3

   (0, 1) (0, 2) (0, 3)
          (1, 2) (1, 2)
                 (2, 3)

-}

per::[a] -> [(a, a)]
per [] = []
per (x:cx) = zip (repeat x) cx

pro::(Num a)=> a -> [a] -> [a]
pro a cx = map (*a) cx

main = do
  print $ extractNumFromStr " 11 " == 11
  print $ extractNumFromStr " 11ab " == 11
  print $ extractNumFromStr "cd11ab " == 11
  print $ extractNumFromStr "11" == 11
  print $ extractNumFromStr "ab" == 0 
  print $ extractStr (0, 0) "abc" == ""
  print $ extractStr (1, 1) "abc" == "b"
  print $ extractStr (1, 2) "abc" == "bc"
  print $ extractStr (1, 2) "abc"
  print $ extractStr (1, 0) "abc"

  let ls = [1, 2, 3]::[Int]
  print $ extract (1, 0) ls == [] 
  print $ extract (1, 1) ls == [2] 
  print $ extract (1, 2) ls == [2, 3] 
  fl
  print $ "0=" ++ integerToBinary 0 
  print $ "1=" ++ integerToBinary 1 
  print $ "2=" ++ integerToBinary 2 
  print $ "3=" ++ integerToBinary 3 
  print $ "4=" ++ integerToBinary 4 
  print $ "5=" ++ integerToBinary 5 
  let m = M.empty       
  let m1 = M.insert 1 "hi" m 
  let m2 = M.insert 2 "h3" m1 
  print (m2::M.HashMap Integer String)
  fl
  print $ integerToHex 0 
  print $ integerToHex 1 
  print $ integerToHex 10 
  print $ integerToHex 16 
  print $ integerToHex 17 
  print $ integerToHex 18 
  print $ integerToHex 32 
  print $ integerToHex 255 
  print $ integerToHex 256
  fl
  print $ validateFormat "a:*:dog" == False
  print $ validateFormat "a::dog, "  == False
  print $ validateFormat "a:*:dog\ncat" == True
  print $ validateFormat "a dog, cow\ntemporary"  == False
  print $ validateFormat "a:dog, cow\ntemporary"  == False
  print $ gcdList [] == 0
  print $ gcdList [2] == 2
  print $ gcdList [2, 0] == 2
  print $ gcdList [2, 3] == 1
  print $ gcdList [2, 4] == 2
  print $ gcdList [2, 4, 6] == 2
  print $ gcdList [2, 4, 5] == 1
  print $ gcdList [2, 4, 0] == 2
  print $ gcdList [8, 6, 12] == 2
  fl

  let (!) = (!!)
  let arr = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
        ]
  print $ per [1, 2]
  let ls = per [0, 1, 2]
  pp arr
  fl
  
  let pair = map(\(x, y) -> (arr ! x, arr ! y)) ls
  pp pair
  let gls = map(\(a, b) -> let a' = head a
                               b' = head b
                               g = lcm a' b' in (pro (div g a') a, pro (div g b') b)) pair
  pp gls
  pp $ [1, 2] - [3, 4]
