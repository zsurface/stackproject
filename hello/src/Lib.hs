module Lib
    ( 
     someFunc
    ) where

someFunc :: IO ()
someFunc = putStrLn "someFunc"

addOne::Int -> Int
addOne = (+1)
