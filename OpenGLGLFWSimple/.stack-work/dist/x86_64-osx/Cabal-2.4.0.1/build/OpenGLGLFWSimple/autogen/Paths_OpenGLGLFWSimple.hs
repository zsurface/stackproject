{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_OpenGLGLFWSimple (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLGLFWSimple/.stack-work/install/x86_64-osx/7552321f0f2b5683f42a0e27840a639d2ceba1ed126f081d38ee04d1e976226b/8.6.5/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLGLFWSimple/.stack-work/install/x86_64-osx/7552321f0f2b5683f42a0e27840a639d2ceba1ed126f081d38ee04d1e976226b/8.6.5/lib/x86_64-osx-ghc-8.6.5/OpenGLGLFWSimple-0.1.0.0-KHOt9iKXnZmCwlWZY9WM9S-OpenGLGLFWSimple"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLGLFWSimple/.stack-work/install/x86_64-osx/7552321f0f2b5683f42a0e27840a639d2ceba1ed126f081d38ee04d1e976226b/8.6.5/lib/x86_64-osx-ghc-8.6.5"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLGLFWSimple/.stack-work/install/x86_64-osx/7552321f0f2b5683f42a0e27840a639d2ceba1ed126f081d38ee04d1e976226b/8.6.5/share/x86_64-osx-ghc-8.6.5/OpenGLGLFWSimple-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLGLFWSimple/.stack-work/install/x86_64-osx/7552321f0f2b5683f42a0e27840a639d2ceba1ed126f081d38ee04d1e976226b/8.6.5/libexec/x86_64-osx-ghc-8.6.5/OpenGLGLFWSimple-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLGLFWSimple/.stack-work/install/x86_64-osx/7552321f0f2b5683f42a0e27840a639d2ceba1ed126f081d38ee04d1e976226b/8.6.5/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "OpenGLGLFWSimple_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "OpenGLGLFWSimple_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "OpenGLGLFWSimple_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "OpenGLGLFWSimple_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "OpenGLGLFWSimple_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "OpenGLGLFWSimple_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
