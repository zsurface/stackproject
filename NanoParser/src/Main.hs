-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
-- import Control.Monad
-- import Data.Char
-- import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
-- import qualified Data.List as L
-- import Data.List.Split
-- import Data.Time
-- import Data.Time.Clock.POSIX
-- import System.Directory
-- import System.Environment
-- import System.Exit
-- import System.FilePath.Posix
-- import System.IO
-- import System.Posix.Files
-- import System.Posix.Unistd
-- import System.Process
-- import Text.Read
-- import Text.Regex
-- import Text.Regex.Base
-- import Text.Regex.Base.RegexLike
-- import Text.Regex.Posix
-- import Data.IORef 
-- import Control.Monad (unless, when)
-- import Control.Concurrent 
-- 
-- import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

-- zo - open
-- za - close


-- main = do 
--         print "Hello World"
--         argList <- getArgs
--         pp $ length argList
--         pp "done!"
-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
--import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import qualified System.FilePath.Posix as PP
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Applicative hiding (some, many)
import Control.Concurrent 
import AronModule 

{-|
    KEY: evaluate expression, expr, parser

    data MyType a = Parser a
    parse :: String -> [(a, String)]

    record syntax is still allowed, but only for one field

    newtype State s a = State { runState :: s -> (s, a) }

    State :: (s -> (s, a)) -> State s a
    runState :: State s a -> (s -> (s, a))

    s = State s a
    runState::(State s a) -> ((State s a), a)


    runState:: s -> (s, a)
    runState s -> (s, (s, a))      # (s, a) => a
    runState::(State s a) -> (s, (s, a))  # State s a => s
-}
       
newtype Parser a = Parser {parse::String -> [(a, String)]} 

runParser::Parser a -> String -> a
runParser m s =
    case parse m s of  -- parse m => String -> [(a, String)]
                       -- (parse m) => f
                       -- f s => [(a, String)]
        [(res, [])] -> res
        [(_, _)] -> error "Parser did not consume entire stream."
        _         -> error "Parser error."


{-|


    Parser is a Monad ?


    f x = x + 1
    (\x -> x + 1)

    data Maybe a = Nothing | Just a

    fromJust::Maybe a -> a
    fromJust Nothing = error "e"
    fromJust (Just x) = x


    data Rec = Rec{name::String, age::Int}
    name::Rec -> String
    age::Rec -> Int


    data Rec = Rec{name::String}

    data Rec = Rec String

    let r = Rec "a"

    name::Rec -> String
    name = \(ds_dFp::Rec) -> case ds_dFp of
                                  Rec ds_dFq -> ds_dFq

    name = \(rec::Rec) -> case rec of
                             Rec x -> x

    newtype Parser a = Parser{parse::String -> [(a, String)]}

    parse::Parser a -> String -> [(a, String)]

    parse = Parser{parse::String -> [(a, String)]} -> String -> [(a, String)]

    parse = \(Parse f) -> s 
    parse = \(Parser f) -> f    -- f = String -> [(a, String)]



    f x = x + 1
    (\x -> x + 1)


     (\(Parser f

    Parser Char => Type constructor
    Parser {parse::String -> [(a, String)]} => Value construct

    Ex.
    data MyType a = Type1 a | StypeNo

    runParser item "m"
    (runParser item) "m"
    (runParser (Parser Char)) "m"
    (String -> Char) "m"

    \m s -> {..} == (\m -> \s -> {..}) (Parser $ \s -> case s of ...)
    a -> b -> c == \a -> \b -> c


    (\m s -> case parse m s of ...) (Parser $ \s -> case s of ...)
    (\m -> \s -> case parse m s of ...) (Parser $ \s -> case s of ...)
    (\s -> case parse (Parser $ \s -> case s of ...) of ...)

    runParser item
    = { definition of item }
    runParser (Parser $ \s -> case s of ...)
    = { definition of runParser }
    (\m s -> case parse m s of ...) (Parser $ \s -> case s of ...)
    = { substitute argument for m everywhere }
    (\s -> case parse (Parser $ \s -> case s of ...) s of ...)
    = { definition of parse }
    (\s -> case (\(Parser f) -> f) (Parser $ \s -> case s of ...) s of ...)
    = { substitute argument for f everywhere }
    (\s -> case (\s -> case s of ...) s of ...)
    = { substitute argument for s everywhere }
    (\s -> case (case s of ...) of ...)

-}
item::Parser Char 
item = Parser $ \s ->
    case s of
        [] -> []
        (c:cs) -> [(c, cs)]

-- [(a1, S),(a2, S)] => [(b1, S), (b2, S)]
--
-- bind::(Monad m) => m a -> (a -> m b) -> m b
bind::Parser a -> (a -> Parser b) -> Parser b
bind p f = Parser $ \s -> concatMap(\(a, s') -> parse (f a) s') $ parse p s

-- class Monad m where
--   return:: a -> m a
--   bind:: m a -> (a -> m b) -> m b
--
-- f x = \x -> x + 1
-- f x = \x -> (x, x)
-- f x = \x -> [(x, x)]
-- instance Monad Maybe where
unit :: a -> Parser a
unit a = Parser (\s -> [(a, s)])

--
-- newtype State s a = State{runState:: s -> (a, s)}
--
-- Parser cs = Parser (String -> [(a, String)])
-- => cs = String -> [(a, String)]
-- => cs s => [(a, String)]
-- => (a, b) <- cs s
--
-- newType Parser a = Parser {parse::String -> [(a, String)]}
-- Parser a => only has one value constructor => Parser{parse::String -> [(a, String)]}
--
{-|
   instance Functor [] where
      fmap f [] = []
      fmap f (x:cs) = f x : fmap f cs

   instance Functor Maybe where
      fmap _ Nothing = Nothing
      fmap f (Just x) = Just (f x)

   Parser::* -> *
   Maybe ::* -> *
   []    ::* -> *

   instance Functor Parser where
      fmap f (Parser cs) = Parser (\s ->  [(f a, s) | (a, s) <- cs s])

   cs == f == parse::String -> [(a, String)]
   cs s == f s == parse s

   Parser (\str -> [(f a, b) | (a, b) <- parse str])
-}
instance Functor Parser where
    fmap f (Parser cs) = Parser (\s -> [(f a, b) | (a, b) <- cs s])

{-|

    fmap f (Parser cs) = Parser (\s -> [(f a, b) | (a, b) <- cs s])
         ↑          ↑ 
         |          + -> parse::String -> [(a, String)]
         |
         + -> (a -> b)


    newtype Parser a = Parser{parse::String -> [(a, String)]}

    Parser cs1 == Parser g => \s -> (f, s1) <- g s
      cs1 is function, (+1)
    Parser cs2 == Parser h => \s -> (a, s2) <- h s
      cs2 is just value, 2

    Ex. Just (+1) <*> Just 2

    extract function from the first functor and apply inside the second functor
    (Just (+1)) <*> Just (2)
    ((+1), s1) <- cs1 s
    (a, s2)   <- cs2 s1
    [((+1) a, s2) | ...]
-}
instance Applicative Parser where
    pure = return 
    (Parser cs1) <*> (Parser cs2) = Parser (\s -> [(f a, s2) | (f, s1) <- cs1 s, (a, s2) <- cs2 s1])
{-|
                              ↑
                              |      Parser{parse::String -> [(a, String)]}
                              +----- cs2 == f == parse::String -> [(a, String)] == cs2 s1
                         ↑
                      Parser a
       ↑
   Parser  f

   Just (+1) <*> Just 2  -- Just 3
-}

instance Monad Parser where
    return = unit
    (>>=) = bind

{-|
unit:: a -> Parser a
unit a = Parser (\s -> [(a, s)])

-}

instance MonadPlus Parser where
    mzero = failure
    mplus = combine

instance Alternative Parser where
    empty = mzero
    (<|>) = option


{-|
   newtype Parser a = Parser{parse::String -> [(a, String)]}

   combine::Parser a -> Parser a -> Parser a
   combine p q = Parser(\s -> (parse p) s
                                 ↑
                               String -> [(a, String)]
   Ex.
   data Record = Record{name::String}

   f::Record -> String
   f r = name r ++ "ok"

-}
combine::Parser a -> Parser a -> Parser a
combine p q = Parser (\s -> parse p s ++ parse q s)

failure::Parser a
failure = Parser (\cs -> [])

option::Parser a -> Parser a -> Parser a 
option p q = Parser $ \s -> 
    case parse p s of
        [] -> parse q s
        res -> res

-- class Functor => Monad m where
--        fmap::(a -> b) -> [a] -> [b]
-- | one or more
some::(Alternative f)=> f a -> f [a]
some v = some_v
    where
       many_v = some_v <|> pure []
       -- (:) <$> Just 1 <*> Just [] => Just ((:) 1) <*> Just [] <|> pure [] == Just [1] | Just []

       some_v = (:) <$> v <*> many_v
       -- (:) <$> Just 1 <*> Just [2] -- => Just ((:) 1) <*>  Just [2] == Just [1,2]
       

---- | zero or more
many::(Alternative f)=> f a -> f [a]
many v = many_v
    where
       many_v = some_v <|> pure []    -- some | zero
       -- (:) <$> Just 1 <*> Just []  -- => some | zero
       
       some_v = (:) <$> v <*> many_v  -- Add one to (zero or more) => one or more
       -- (:) <$> Just 1 <*> Just [2] -- => Just [1,2]

{-|

   unit::Char -> Parser Char
   unit c = return c

   unit:: a -> Parser a
   unit a = Parser {(\s -> [(a, s)]}
-}
satisfy::(Char -> Bool) -> Parser Char
satisfy p = item `bind` \c ->
    if p c
    then unit c
    else failure 

{-|

  elem::Eq a => a -> t a -> Bool

  elem 'a' "abc" => True

  flip elem => (flip elem) "abc" 'a' => True

-}
oneOf::[Char] -> Parser Char
oneOf s = satisfy (flip elem s)  -- (flip elem s) => (flip elem "abc") => f::Char -> Bool


chainl::Parser a -> Parser (a -> a -> a) -> a -> Parser a
chainl p op a = (p `chainl1` op) <|> return a

{-|

 chainl1::Parser a -> Parser (a -> a -> a) -> Parser a
 chainl1 p op = do {
                  a <- p
                  rest a
                }
             where
               rest a = (do f <- op
                            b <- p
                            rest (f a b)) <|> return a

  (<*>):: f (a -> b) -> f a -> f b

  Just (+1) <*> Just 1 == Just 2
  Just (a -> a) <*> Just a == Just f <*> Just a == Just (f a)

-}
chainl1::Parser a -> Parser (a -> a -> a) -> Parser a
p `chainl1` op = do {a <- p; rest a}
    where rest a = (do f <- op
                       b <- p
                       rest (f a b)) <|> return a
                    

{-|
  newtype Parser a = Parser {parse::String -> [(a, String)]}

  satifies::(Char -> Bool) -> Parser Char

    isLetter :: Char -> Bool
    isDigit  :: Char -> Bool
    isHex    :: Char -> Bool

  c == => (Char -> Bool)
-}
char::Char -> Parser Char
char c = satisfy (c ==)


{-|
   satisfy :: (Char -> Bool) -> Parser Char
   satisfy f = item `bind` >>= \c -> do
            if f c
              then unit c
              else failure

   some::(Alternative f) => f a -> f [a]
   some (Parser Char) -> Parser [Char]

-}
---- read "123"::Integer => 123
natural::Parser Integer
natural = read <$> some (satisfy isDigit)  -- one or more, fmap read (Parser Char)

{-|
    char c >> string cs >> return (c:cs)

    string "abc"
       char 'a'; => Parser 'a'
       string "bc"
         char 'b'; => Parser 'b'
           string "c"
             char 'c'  => Parser 'c'
               return []
             |
           |
         |
       |
       
-}
string::String -> Parser String
string [] = return []  
string (c:cs) = do { char c;
                     string cs;
                     return (c:cs)
                   }
--
token::Parser a -> Parser a
token p = do {
  a <- p;
  spaces;
  return a}
--
reserved::String -> Parser String
reserved s = token (string s)

spaces::Parser String
spaces = many $ oneOf " \n\r"

digit::Parser Char
digit = satisfy isDigit

number::Parser Int
number = do
    s <- string "-" <|> return []
    cs <- some digit
    return $ read (s ++ cs)

parents::Parser a -> Parser a
parents m = do
    reserved "("
    n <- m
    reserved ")"
    return n

data Expr 
    = Lit Int
    | Add Expr Expr
    | Sub Expr Expr
    | Mul Expr Expr
    deriving (Show)

eval::Expr -> Int
eval (Lit a) = a 
eval (Add e1 e2) = (+) (eval e1) (eval e2)
eval (Sub e1 e2) = (-) (eval e1) (eval e2)
eval (Mul e1 e2) = (*) (eval e1) (eval e2)

int::Parser Expr 
int = do
    n <- number
    return (Lit n)

expr::Parser Expr
expr = term `chainl1` addop

term::Parser Expr
term = factor `chainl1` mulop 

factor::Parser Expr
factor = int <|> parents expr

infixOp::String -> (a -> a -> a) -> Parser (a -> a -> a)
infixOp x f = reserved x >> return f

addop::Parser (Expr -> Expr -> Expr)
addop = (infixOp "+" Add) <|> (infixOp "-" Sub)

mulop::Parser (Expr -> Expr -> Expr)
mulop = infixOp "*" Mul

myrun::String -> Expr
myrun = runParser expr

main::IO()
main = forever $ do 
        putStr ">"
        a <- getLine
        print $ eval $ myrun a
