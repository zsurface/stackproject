histogramX::FilePath -> IO()
histogramX fp = do
        nls <- readFileList fp >>= \cx -> return $ map (\x -> read x ::Int) cx
        let w = 2000
            h = 1000

        img <- withMutableImage w h (PixelRGB8 10 10 10) $ \m -> do
            ls <- randIntList 100 (1, 100)
            drawRectBar m (10, 20) (20, 30) 100 nls (PixelRGB8 0 255 0)
            -- A dark green filled triangle
            -- fillTriangle m 50 200 250 300 70 350 (PixelRGB8 0 150 50)
        writePng "/tmp/histogram.png" img
        print "gene png file => /tmp/histogram.png"