-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

htmlTable'::[[String]] -> [String] -- htmlTable [["dog"],["cat"]]  => ["<table>", "<tr><td>dog</td></tr>", "<tr><td>cat</td></tr></table>"]
htmlTable' cs = table (join rows)
    where
        rows = map(\r -> tr $ map(\x -> td x) r) cs
        td x = "<td style='background:green; font-size:24px;'>" + x + "</td>"
        tr x = ["<tr>"] + x + ["</tr>"]
        table x = ["<table>"] + x + ["</table>"]
        (+) = (++)
        
htmlTable2::[[String]] -> [(String, String)] -> [String] -- htmlTable [["dog"],["cat"]]  => ["<table>", "<tr><td>dog</td></tr>", "<tr><td>cat</td></tr></table>"]
htmlTable2 cs lt = table (join rows)
    where
        rows = map(\r -> tr $ map(\x -> td x) r) cs
        td x = "<td " + stystr' + ">" + x + "</td>"
        tr x = ["<tr>"] + x + ["</tr>"]
        table x = ["<table>"] + x + ["</table>"]
        (+) = (++)
        ltt = map(\(a, b) -> let a' = a + ":"
                                 b' = b + ";"
                                 in a' + b'
                 ) lt
        stystr = foldl (\acc b -> acc + b) [] ltt
        stystr' = "style='" + stystr + "'"
        
fromJust::Maybe [String] -> String
fromJust Nothing = ""
fromJust (Just x) = head x

-- ∆
                        
main = do 
        print "Hello World"
        ls <- readFileList "./apl.txt"
        let hex x = "&#" ++ x ++ ";"
        let unx x = "\\" ++ x
        let literal x = "&#38;#" ++ x ++ ";"
        let cap x = matchRegex (mkRegex "(x[0-9a-f]{2,4})") x
        let unls = map(\x -> cap x) ls

        -- pp $ take 4 unls
        
        let lshex = map (\x -> let x' = fromJust x in hex x') unls
        let lshex' = map (\x -> let x' = fromJust x in literal x') unls
        -- let lshex' = map (\x -> let x' = fromJust x in "") unls
        let zls = join $ zipWith(\x y -> [x,y]) lshex lshex'
        let lsls = join $ zipWith(\x y -> [x, y]) ls lshex
        let sss = map(\x -> let x' = fromJust x in unx x') unls
        fl
        pp sss
        -- pp $ take 4 zls
        -- pp $ take 4 ls
        let lss = partList 20 zls
        -- let lstr = htmlTable' lss
            
        -- background-color:#000B02;color:#A5843E
        -- KEY: text color, file:///Users/cat/myfile/bitbucket/publicfile/backgroundcolor.html
        let lstr = htmlTable2 lss [("background-color", "#000B02"), ("color", "#A5843E"), ("font-size", "24px")]
        bitbucket <- getEnv "b"
        writeFileList (bitbucket ++ "/html/apl_table.html") lstr
        pp lshex
        pp "done!"
