                        pp "list map"
                        mx <- readIORef refMap
                        let lsm = M.toList mx 
                        let sx = map (\(a, b) -> "[0,\"" ++ a ++ " " ++ b ++ "\"]") lsm
                        let str = unlines sx
                        fw "str"
                        pre str
                        fw "sx"
                        pre sx
                        pre lsm
                        send sock $ strToStrictByteString str 
                        runConn (sock, sa) f [] refMap