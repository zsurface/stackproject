-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
{-# LANGUAGE MultiWayIf #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD
import qualified Data.Set as S 
import qualified Data.Map.Strict as M


--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

import Network.Socket
import Network.Socket.ByteString
import qualified Data.ByteString           as BS  -- Strict ByteString
import Control.Arrow
import qualified Control.Monad.IO.Class as CM
import Text.Pretty.Simple
import Data.Maybe (fromJust)
import Control.Concurrent

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

-- KEY: socket hello world, networking, tcp server

fun :: String -> String
fun = toUpperStr

mainloop :: Socket -> IORef (M.Map String String) -> IO ()
mainloop sock refMap = do
        conn <- accept sock
        let ls = []
        thid <- forkIO $ do 
                         runConn conn fun ls refMap 
        pre thid
        mainloop sock refMap

--prex::(CM.MonadIO m, Show a) => a -> m ()
--prex s = pPrintOpt CheckColorTty defaultOutputOptionsDarkBg {outputOptionsCompact = True} s


trimSnd :: (String, String) -> (String, String)
trimSnd (a, b) = if len b > 1 then (a, (init . tail) b) else error "Invalid String format"


vimDictToTuple :: String -> Maybe (String, String)
vimDictToTuple s = len s > 1 ? (let x = splitWhenFirstNoRegex "," $ (init . tail .trim) s 
                                in case x of
                                     Just t -> Just $ trimSnd t 
                                     Nothing -> Nothing 
                               ) $ Nothing

toTupleList :: String -> [Maybe (String, String)]
toTupleList s = map vimDictToTuple ls 
    where
        ls = splitStr "\n" s

fuxx :: [(String, String)] -> Bool
fuxx [] = False
fuxx cx = True 

{-|
    === Vim json to a list of String

    [(1, "dog"), (2, "cat")]
    => "[1,\"dog\"]\n[2,\"cat\"]"
-}
tupleToListJson :: [(String, String)] -> (String -> String) -> String 
tupleToListJson [] f = []
tupleToListJson cx f =  unlines $ map (\(n, a) -> g n $ f a) cx
        where
            g n s = "[" ++ n ++ "," ++ (show s) ++ "]"
        

-- Check the URL and see how to decode the data from vim client to JSON
-- https://github.com/bsdshell/haskell-vim-channel/blob/master/src/Main.hs

writeToTmp :: FilePath -> [(String, String)] -> IO()
writeToTmp p ls = writeFileList p $ map snd ls 

{-|
    === Receive JSON from Vim client

    [("1", "beg"), ("2", "cmd=writetotmp"), ("3", "dog"), ("4", "cat"), ("5", "end")]
    =>
    [("2", "cmd=writetotmp"), ("3", "dog"), ("4", "cat")]

-}
receiveVimData :: [(String, String)] -> Either String [String]
receiveVimData cx = if len cx > 1 && isOk then Right $ (init . tail) ls else Left "Receive Invalid Vim Data" 
    where
        ls = map snd cx
        isOk = head ls == "beg" && last ls == "end" 

-- /Users/aaa/myfile/bitbucket/stackproject/haskell-vim-channel/src/isValidCmd-2023-02-17-13-29-10.x
isValidCmd :: String -> (Bool, String)
isValidCmd s = isOk ? (True, last ls) $ (False, []) 
    where
        ls = splitStr "=" s
        isOk = len ls == 2


{-|
    @

    raw=beg
    cmd=insert
    key=k1
    val=v1
    raw=end

    @
-}
isValidBlock::[String] -> Bool
isValidBlock  cx = bo 
    where
        ln = len cx 
        bo = if ln > 1 then let h = head cx
                                l = last cx
                                beg = last $ splitStr "=" h
                                end = last $ splitStr "=" l 
                            in beg == "beg" && end == "end"
                        else False

-- /Users/aaa/myfile/bitbucket/stackproject/haskell-vim-channel/src/strToList-2023-02-17-14-16-03.x
strToList :: String -> [String]
strToList s = filter (\x -> (len . trim) x > 0) $ splitStr "=" s

-- /Users/aaa/myfile/bitbucket/stackproject/haskell-vim-channel/src/runConn-2023-02-17-14-17-52.x
{-|
    === KEY:

    @ 
    protocol:
        pro=vim

        1=raw=beg@
        2=pro=raw@
        3=cmd=insert@
        4=key=k1@
        5=val=v1@
        6=raw=end@
        
        pro=raw@
        cmd=listkey

        pro=raw@
        cmd=deletekey@
        key=k1
        
    @
-}
runConn :: (Socket, SockAddr) -> (String -> String) -> [String] -> IORef (M.Map String String) -> IO()
runConn (sock, sa) f lt refMap = do
        clientMsg <- recv sock 4242
        fw "runConn clientMsg"
        prex clientMsg
        let bsx = strToStrictByteString "TCP Server:me"

        -- NOTE: If Vim call ch_close(g:tcpChannel)
        --       Then clientMsg is null
        --       The socket is NOT valid any more 
        unless (BS.null clientMsg) $ do
            let clientMsg' = strToStrictByteString $ let s = (f . toStr) clientMsg in s 
            fw "putStrLn clientMsg"
            let s = toStr clientMsg
            putStrLn $ toStr clientMsg
            let ls = filter (\x -> (len . trim) x > 0) $ splitStr "@" $ toStr clientMsg
            let lw = lt ++ ls
            let b = isValidBlock lw 
            if b then do
                let lz = init . tail $ lw
                let cmd = trim . last $ splitStr "=" $ head lz
                case cmd of
                    v | v == "insert" -> do 
                        pp "insert key"
                        pp "insert key val to map"
                        let ss = splitStr "=" $ head . tail $ lz
                        let tt = splitStr "=" $ last lz
                        let keyCmd = head ss
                        let keyStr = last ss 
                        let valCmd = head tt 
                        let valStr = last tt 
                        fw "ss"
                        pre ss
                        fw "tt"
                        pre tt
                        if keyCmd == "key" && valCmd == "val" then do 
                            pp "ok"
                            mref <- readIORef refMap
                            modifyIORef refMap (M.insert keyStr valStr)
                            map <- readIORef refMap
                            fw "map"
                            pre map
                            runConn (sock, sa) f [] refMap
                        else do
                            pp "kk"

                      | v == "delete" -> do
                        let ss = splitStr "=" $ head lz
                        let keyCmd = head ss
                        let keyStr = last ss
                        pp "delete key"
                        if keyCmd == "key" then do 
                            modifyIORef refMap (M.delete keyStr)
                            map <- readIORef refMap
                            fw "map"
                            pre map
                            runConn (sock, sa) f [] refMap
                        else do
                            pp $ "ERROR: Invalid format from Vim:" ++ keyCmd
                            pp "Close socket"

                      | v == "list" -> do
                        pp "list map"
                        mx <- readIORef refMap
                        let lsm = M.toList mx 
                        let sx = map (\(a, b) -> "[0,\"" ++ a ++ " " ++ b ++ "\"]") lsm
                        let str = unlines sx
                        fw "str"
                        pre str
                        fw "sx"
                        pre sx
                        pre lsm
                        send sock $ strToStrictByteString str 
                        runConn (sock, sa) f [] refMap

                      | otherwise -> do
                        pp $ "ERROR: Invalid cmd:" ++ cmd
                        pp "Close socket"

                pp "Valid Block"
                fw "lw"
                pre lw
            else do
                pp "Not a Valid Block"
                runConn (sock, sa) f lw refMap

            let lr = filter (\x -> (len . trim) x > 0) $ splitStr "@" $ toStr clientMsg
            ff "clientMsg'" clientMsg'
            prex clientMsg'
            putStrLn $ toStr clientMsg'
            fw "write clientMsg to file"
        fw "send sock and close sock" 
        -- send sock s 
        close sock


main = do 
        sock <- socket AF_INET Stream 0
        setSocketOption sock ReuseAddr 1
        -- 127.0.0.1 => little-endian architecture 0x100007f in Hex
        bind sock (SockAddrInet 4242 0x100007f)
        listen sock 2
        refMap <- newIORef M.empty 
        mainloop sock refMap

{-|
main = do
        print "ok"
        let s = "abcd\nefgh"
        let ls = map (\x ->  (integerToHex . fi . charToDecimal $ x, x) ) s
        prex ls
-}        
