isValidBlock::[String] -> Bool
isValidBlock  cx = bo 
    where
        ln = len cx 
        bo = if ln > 1 then let h = head cx
                                l = last cx
                                beg = head $ splitStr "=" h
                                end = head $ splitStr "=" l 
                            in beg == "beg" && end == "end"
                        else False