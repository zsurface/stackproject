-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
{-# LANGUAGE MultiWayIf #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD
import qualified Data.Set as S 
import qualified Data.Map.Strict as M


--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

import Network.Socket
import Network.Socket.ByteString
import qualified Data.ByteString           as BS  -- Strict ByteString
import Control.Arrow
import qualified Control.Monad.IO.Class as CM
import Text.Pretty.Simple
import Data.Maybe (fromJust)
import Control.Concurrent

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

-- KEY: socket hello world, networking, tcp server

fun :: String -> String
fun = toUpperStr

mainloop :: Socket -> IORef (M.Map String String) -> IO ()
mainloop sock refMap = do
        conn <- accept sock
        let ls = []
        thid <- forkIO $ do 
                         runConn conn fun ls refMap 
        pre thid
        mainloop sock refMap

--prex::(CM.MonadIO m, Show a) => a -> m ()
--prex s = pPrintOpt CheckColorTty defaultOutputOptionsDarkBg {outputOptionsCompact = True} s


trimSnd :: (String, String) -> (String, String)
trimSnd (a, b) = if len b > 1 then (a, (init . tail) b) else error "Invalid String format"


vimDictToTuple :: String -> Maybe (String, String)
vimDictToTuple s = len s > 1 ? (let x = splitWhenFirstNoRegex "," $ (init . tail .trim) s 
                                in case x of
                                     Just t -> Just $ trimSnd t 
                                     Nothing -> Nothing 
                               ) $ Nothing

toTupleList :: String -> [Maybe (String, String)]
toTupleList s = map vimDictToTuple ls 
    where
        ls = splitStr "\n" s

fuxx :: [(String, String)] -> Bool
fuxx [] = False
fuxx cx = True 

{-|
    === Vim json to a list of String

    [(1, "dog"), (2, "cat")]
    => "[1,\"dog\"]\n[2,\"cat\"]"
-}
tupleToListJson :: [(String, String)] -> (String -> String) -> String 
tupleToListJson [] f = []
tupleToListJson cx f =  unlines $ map (\(n, a) -> g n $ f a) cx
        where
            g n s = "[" ++ n ++ "," ++ (show s) ++ "]"
        

-- Check the URL and see how to decode the data from vim client to JSON
-- https://github.com/bsdshell/haskell-vim-channel/blob/master/src/Main.hs

writeToTmp :: FilePath -> [(String, String)] -> IO()
writeToTmp p ls = writeFileList p $ map snd ls 

{-|
    === Receive JSON from Vim client

    [("1", "beg"), ("2", "cmd=writetotmp"), ("3", "dog"), ("4", "cat"), ("5", "end")]
    =>
    [("2", "cmd=writetotmp"), ("3", "dog"), ("4", "cat")]

-}
receiveVimData :: [(String, String)] -> Either String [String]
receiveVimData cx = if len cx > 1 && isOk then Right $ (init . tail) ls else Left "Receive Invalid Vim Data" 
    where
        ls = map snd cx
        isOk = head ls == "beg" && last ls == "end" 

-- /Users/aaa/myfile/bitbucket/stackproject/haskell-vim-channel/src/isValidCmd-2023-02-17-13-29-10.x
isValidCmd :: String -> (Bool, String)
isValidCmd s = isOk ? (True, last ls) $ (False, []) 
    where
        ls = splitStr "=" s
        isOk = len ls == 2

-- /Users/aaa/myfile/bitbucket/stackproject/haskell-vim-channel/src/isValidBlock-2023-02-17-13-22-49.x
isValidBlock::[String] -> Bool
isValidBlock  cx = bo 
    where
        ln = len cx 
        bo = if ln > 1 then let h = head cx
                                l = last cx
                                beg = head $ splitStr "=" h
                                end = head $ splitStr "=" l 
                            in beg == "beg" && end == "end"
                        else False

-- /Users/aaa/myfile/bitbucket/stackproject/haskell-vim-channel/src/strToList-2023-02-17-14-16-03.x
strToList :: String -> [String]
strToList s = filter (\x -> (len . trim) x > 0) $ splitStr "=" s


{-|
    === KEY:

    @ 
    protocol:
        pro=vim

        1=raw=beg@
        2=pro=raw@
        3=cmd=insert@
        4=key=k1@
        5=val=v1@
        6=raw=end@
        
        pro=raw@
        cmd=listkey

        pro=raw@
        cmd=deletekey@
        key=k1
        
    @
-}
runConn :: (Socket, SockAddr) -> (String -> String) -> [(String, String)] -> IORef (M.Map String String) -> IO()
runConn (sock, sa) f lt refMap = do
        clientMsg <- recv sock 4242
        fw "runConn clientMsg"
        prex clientMsg
        let bsx = strToStrictByteString "TCP Server:me"

        -- NOTE: If Vim call ch_close(g:tcpChannel)
        --       Then clientMsg is null
        --       The socket is NOT valid any more 
        unless (BS.null clientMsg) $ do
            let clientMsg' = strToStrictByteString $ let s = (f . toStr) clientMsg in s 
            fw "putStrLn clientMsg"
            putStrLn $ toStr clientMsg
            let ls = splitStr "\n" $ toStr clientMsg
            let lr = filter (\x -> (len . trim) x > 0) $ splitStr "@" $ toStr clientMsg
            if len lr > 0 then do
                let pro = head $ splitStr "=" $ head lr 
                if pro == "raw" then do
                    pp "raw protocol"
                else do
                    pp "Not raw protocol"
                pp "ok"
            else do 
                pp "not ok"
            fw "lr"
            pre lr
            fw "ls"
            prex ls
            ff "clientMsg" clientMsg
            ff "clientMsg'" clientMsg'
            prex clientMsg'
            putStrLn $ toStr clientMsg'
            fw "write clientMsg to file"
            writeFile "/tmp/bbb.x" $ toStr clientMsg 
            fw "lines clientMsg'"
            prex $ lines $ toStr clientMsg'
            let lv = filter (\e -> (len . fst) e > 0) $ map (\x -> case x of
                                                              Just v -> v
                                                              Nothing -> ("", "") 
                            ) $ toTupleList $ toStr clientMsg 
                              
            let lw = lt ++ lv
            let count = sum $ map (\x -> x == "beg" || x == "end" ? 1 $ 0) $ map snd lw 
            fw "lv"
            prex lv
            fw "lw"
            prex lw
            

            let tu = (vimDictToTuple . toStr) clientMsg
            fw "tu"
            prex tu
            let vimData = case tu of
                    -- (***) from Control.Arrow
                    -- (***) f g (a, b) => (f a, g b)
                    Just t -> (***) (\x -> read x :: Int) (init . tail) t 
                    Nothing -> (-1, "") 
            fw "vimData"
            prex vimData
            fw "lines"
            prex $ lines $ snd vimData
            fw "s s n"
            prex $ splitStr "\\n" $ snd vimData
            fw "s n"
            prex $ splitStrChar "\n" $ snd vimData
            fw "hex"
            let hex = map (\x -> (integerToHex . fi . charToDecimal $ x, x) ) $ snd vimData
            prex hex

            fw "NUL"
            prex $ splitStrCharNoRegex "\0" $ snd vimData

            -- sendAll sock clientMsg' 
            fw "readFile from /tmp/ccc.x, convert to ByteString, send to client"
            -- bbx <- readFile "/tmp/ccc.x" >>= return . strToStrictByteString
            -- sendAll sock bbx 

            ff "count" $ show count
            let what = receiveVimData lw
            fw "what"
            prex what
            case what of
                Right ls -> do
                    -- tupCmd = (True, "writetotmp")
                    let tupCmd = isValidCmd $ head ls
                    case tupCmd of
                        v | fst v && snd v == "writetotmp" -> do
                            tmpPath <- getEnv "t" 
                            writeToTmp tmpPath lw 
                            let replyMsg = strToStrictByteString $ tupleToListJson lw toUpperStr 
                            -- sendAll sock replyMsg 
                            sendAll sock $ strToStrictByteString $ "[0, \"Ok writetotmp=" ++ tmpPath ++ "\"]"
                            runConn (sock, sa) f [] refMap
                          | fst v && snd v == "insert" -> do
                            fw "insert ls"
                            let lt = tail ls
                            if len lt == 2 then do
                                let key = last $ splitStr "=" $ head lt
                                let val = last $ splitStr "=" $ last lt
                                mref <- readIORef refMap
                                modifyIORef refMap (M.insert key val)
                                map <- readIORef refMap
                                fw "map"
                                pre map 
                            else do
                                pp "List from vim should have size 2"
                            runConn (sock, sa) f [] refMap

                          | fst v && snd v == "deletekey" -> do
                            let lt = tail ls 
                            let isOk = len lt == 1 
                            if isOk then do
                                let key = last $ splitStr "=" $ head lt 
                                modifyIORef refMap (M.delete key)
                                let st = "delete key:" ++ key
                                send sock $ strToStrictByteString $ "[0,\"" ++ st ++ "\"]"
                                pp "isOk"
                            else do
                                pre ls
                                pp "Key is not been deleted"
                            runConn (sock, sa) f [] refMap

                          | fst v && snd v == "listkey" -> do
                            mx <- readIORef refMap
                            let lsm = M.toList mx 
                            let ls = map (\(a, b) -> "[0,\"" ++ a ++ " " ++ b ++ "\"]") lsm
                            let uls = unlines ls
                            fw "lsm"
                            pre lsm 
                            send sock $ strToStrictByteString $ uls
                            runConn (sock, sa) f [] refMap

                          | fst v && snd v == "getkey" -> do
                            let lt = tail ls 
                            let isOk = len lt == 1 
                            if isOk then do
                                let key = last $ splitStr "=" $ head lt 
                                mval <- readIORef refMap >>= return . M.lookup key
                                fw "mval"
                                pre mval
                                case mval of
                                    Just k -> do 
                                        send sock $ strToStrictByteString $ "[0,\"" ++ k ++ "\"]"
                                        pp "Send to Vim client from Haskell TCP Server"
                                    Nothing -> do 
                                        send sock $ strToStrictByteString $ "[0,\"" ++ "NOKEY" ++ "\"]"
                                        print $ "No key is found, key=" ++ key
                                pp "ok"

                            else do
                                pre ls
                                pp "ok"
                            runConn (sock, sa) f [] refMap

                          | otherwise -> do
                            pp "cmd is not been supported"
                            fw "ls"
                            prex ls
                            runConn (sock, sa) f [] refMap
                Left err -> do 
                        print err
                        pp "ERROR: Receive invalid data from client Vim."
                        fw "lw"
                        pre lw
                        runConn (sock, sa) f lw refMap


        let bs = strToStrictByteString "From Haskell TCP Server\n"
        let str = toStr bs
        let s =  strToStrictByteString $ concatStr (splitSPC str) "okok"
        fw "send sock and close sock" 
        -- send sock s 
        close sock


main = do 
        sock <- socket AF_INET Stream 0
        setSocketOption sock ReuseAddr 1
        -- 127.0.0.1 => little-endian architecture 0x100007f in Hex
        bind sock (SockAddrInet 4242 0x100007f)
        listen sock 2
        refMap <- newIORef M.empty 
        mainloop sock refMap

{-|
main = do
        print "ok"
        let s = "abcd\nefgh"
        let ls = map (\x ->  (integerToHex . fi . charToDecimal $ x, x) ) s
        prex ls
-}        
