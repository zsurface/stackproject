isValidCmd :: String -> (Bool, String)
isValidCmd s = isOk ? (True, last ls) $ (False, []) 
    where
        ls = splitStr "=" s
        isOk = len ls == 2