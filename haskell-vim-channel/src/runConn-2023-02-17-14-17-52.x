{-|
    === KEY:

    @ 
    protocol:
        pro=vim

        1=raw=beg@
        2=pro=raw@
        3=cmd=insert@
        4=key=k1@
        5=val=v1@
        6=raw=end@
        
        pro=raw@
        cmd=listkey

        pro=raw@
        cmd=deletekey@
        key=k1
        
    @
-}
runConn :: (Socket, SockAddr) -> (String -> String) -> [(String, String)] -> IORef (M.Map String String) -> IO()
runConn (sock, sa) f lt refMap = do
        clientMsg <- recv sock 4242
        fw "runConn clientMsg"
        prex clientMsg
        let bsx = strToStrictByteString "TCP Server:me"

        -- NOTE: If Vim call ch_close(g:tcpChannel)
        --       Then clientMsg is null
        --       The socket is NOT valid any more 
        unless (BS.null clientMsg) $ do
            let clientMsg' = strToStrictByteString $ let s = (f . toStr) clientMsg in s 
            fw "putStrLn clientMsg"
            putStrLn $ toStr clientMsg
            let ls = splitStr "\n" $ toStr clientMsg
            let lr = filter (\x -> (len . trim) x > 0) $ splitStr "@" $ toStr clientMsg
            if len lr > 0 then do
                let pro = head $ splitStr "=" $ head lr 
                if pro == "raw" then do
                    pp "raw protocol"
                else do
                    pp "Not raw protocol"
                pp "ok"
            else do 
                pp "not ok"
            fw "lr"
            pre lr
            fw "ls"
            prex ls
            ff "clientMsg" clientMsg
            ff "clientMsg'" clientMsg'
            prex clientMsg'
            putStrLn $ toStr clientMsg'
            fw "write clientMsg to file"
            writeFile "/tmp/bbb.x" $ toStr clientMsg 
            fw "lines clientMsg'"
            prex $ lines $ toStr clientMsg'
            let lv = filter (\e -> (len . fst) e > 0) $ map (\x -> case x of
                                                              Just v -> v
                                                              Nothing -> ("", "") 
                            ) $ toTupleList $ toStr clientMsg 
                              
            let lw = lt ++ lv
            let count = sum $ map (\x -> x == "beg" || x == "end" ? 1 $ 0) $ map snd lw 
            fw "lv"
            prex lv
            fw "lw"
            prex lw
            

            let tu = (vimDictToTuple . toStr) clientMsg
            fw "tu"
            prex tu
            let vimData = case tu of
                    -- (***) from Control.Arrow
                    -- (***) f g (a, b) => (f a, g b)
                    Just t -> (***) (\x -> read x :: Int) (init . tail) t 
                    Nothing -> (-1, "") 
            fw "vimData"
            prex vimData
            fw "lines"
            prex $ lines $ snd vimData
            fw "s s n"
            prex $ splitStr "\\n" $ snd vimData
            fw "s n"
            prex $ splitStrChar "\n" $ snd vimData
            fw "hex"
            let hex = map (\x -> (integerToHex . fi . charToDecimal $ x, x) ) $ snd vimData
            prex hex

            fw "NUL"
            prex $ splitStrCharNoRegex "\0" $ snd vimData

            -- sendAll sock clientMsg' 
            fw "readFile from /tmp/ccc.x, convert to ByteString, send to client"
            -- bbx <- readFile "/tmp/ccc.x" >>= return . strToStrictByteString
            -- sendAll sock bbx 

            ff "count" $ show count
            let what = receiveVimData lw
            fw "what"
            prex what
            case what of
                Right ls -> do
                    -- tupCmd = (True, "writetotmp")
                    let tupCmd = isValidCmd $ head ls
                    case tupCmd of
                        v | fst v && snd v == "writetotmp" -> do
                            tmpPath <- getEnv "t" 
                            writeToTmp tmpPath lw 
                            let replyMsg = strToStrictByteString $ tupleToListJson lw toUpperStr 
                            -- sendAll sock replyMsg 
                            sendAll sock $ strToStrictByteString $ "[0, \"Ok writetotmp=" ++ tmpPath ++ "\"]"
                            runConn (sock, sa) f [] refMap
                          | fst v && snd v == "insert" -> do
                            fw "insert ls"
                            let lt = tail ls
                            if len lt == 2 then do
                                let key = last $ splitStr "=" $ head lt
                                let val = last $ splitStr "=" $ last lt
                                mref <- readIORef refMap
                                modifyIORef refMap (M.insert key val)
                                map <- readIORef refMap
                                fw "map"
                                pre map 
                            else do
                                pp "List from vim should have size 2"
                            runConn (sock, sa) f [] refMap

                          | fst v && snd v == "deletekey" -> do
                            let lt = tail ls 
                            let isOk = len lt == 1 
                            if isOk then do
                                let key = last $ splitStr "=" $ head lt 
                                modifyIORef refMap (M.delete key)
                                let st = "delete key:" ++ key
                                send sock $ strToStrictByteString $ "[0,\"" ++ st ++ "\"]"
                                pp "isOk"
                            else do
                                pre ls
                                pp "Key is not been deleted"
                            runConn (sock, sa) f [] refMap

                          | fst v && snd v == "listkey" -> do
                            mx <- readIORef refMap
                            let lsm = M.toList mx 
                            let ls = map (\(a, b) -> "[0,\"" ++ a ++ " " ++ b ++ "\"]") lsm
                            let uls = unlines ls
                            fw "lsm"
                            pre lsm 
                            send sock $ strToStrictByteString $ uls
                            runConn (sock, sa) f [] refMap

                          | fst v && snd v == "getkey" -> do
                            let lt = tail ls 
                            let isOk = len lt == 1 
                            if isOk then do
                                let key = last $ splitStr "=" $ head lt 
                                mval <- readIORef refMap >>= return . M.lookup key
                                fw "mval"
                                pre mval
                                case mval of
                                    Just k -> do 
                                        send sock $ strToStrictByteString $ "[0,\"" ++ k ++ "\"]"
                                        pp "Send to Vim client from Haskell TCP Server"
                                    Nothing -> do 
                                        send sock $ strToStrictByteString $ "[0,\"" ++ "NOKEY" ++ "\"]"
                                        print $ "No key is found, key=" ++ key
                                pp "ok"

                            else do
                                pre ls
                                pp "ok"
                            runConn (sock, sa) f [] refMap

                          | otherwise -> do
                            pp "cmd is not been supported"
                            fw "ls"
                            prex ls
                            runConn (sock, sa) f [] refMap
                Left err -> do 
                        print err
                        pp "ERROR: Receive invalid data from client Vim."
                        fw "lw"
                        pre lw
                        runConn (sock, sa) f lw refMap


        let bs = strToStrictByteString "From Haskell TCP Server\n"
        let str = toStr bs
        let s =  strToStrictByteString $ concatStr (splitSPC str) "okok"
        fw "send sock and close sock" 
        -- send sock s 
        close sock
