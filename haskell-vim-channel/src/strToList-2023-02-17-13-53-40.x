strToList :: String -> [String]
strToList s = filter (\x -> (len . trim) x > 0) $ splitStr "=" s