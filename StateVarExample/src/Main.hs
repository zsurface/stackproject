-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE CPP #-} 

{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE QuasiQuotes       #-} -- support raw string [r|<p>dog</p> |]

-- import Turtle--{{{
-- echo "turtle"
import Text.RawString.QQ       -- Need QuasiQuotes too 
-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent
import qualified System.Console.Pretty as SCP

import qualified Text.Regex.TDFA as TD
import qualified Data.Array.IO as AIO
import Data.Array.IO
import qualified Data.Array.Base as DB

import Foreign.Marshal.Alloc
import Data.StateVar
import Foreign.Ptr
import Foreign.Storable

-- shell command template:
-- 
--        argList <- getArgs
--        if len argList == 2 then do
--            let n = stringToInt $ head argList
--            let s = last argList
--            putStr $ drop (fromIntegral n) s
--        else print "drop 2 'abcd'"
/*--}}}*/

import AronModule 

p1 = "/Users/aaa/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

-- KEY: state var
makeStateVarFromPtr :: Storable a => Ptr a -> StateVar a
makeStateVarFromPtr p = makeStateVar (peek p) (poke p)

#if 1 
main = do 
        pp "State Var"
        when True $ do
          fw "state var"
          p <- malloc :: IO (Ptr Int)
          let v = makeStateVarFromPtr p
          v $= 11
          v $~ (+20)
          n <- Data.StateVar.get v
          print $ "First n=" ++ show n 
          pp "OK"
          v $~ (+100)
          n <- Data.StateVar.get v
          print $ "Second n=" ++ show n 
#endif


