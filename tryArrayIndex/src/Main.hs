-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
{-# LANGUAGE RankNTypes #-}
-- import Turtle
-- echo "turtle"
-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}
import Data.Array.IO
import Data.Array.MArray
import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

-- data Maybe x = Nothing | Just x
-- data Maybe [x] = Nothing | Just [x]
-- data Maybe (a -> b) = Nothing | Just (a -> b)
-- (->) x Functor
-- (->) x (IO x)
-- data RecordList x = RecordList (IO x) ((->) x (IO x))

-- data RecordList x = RecordList (IO x)  (x -> IO x)

data TypeA = TypeA

class MyClass a where
        fun:: a -> String
        rfun:: String -> a

instance MyClass Int where
        fun 1 = "1"
        rfun "1"  = 1

instance MyClass TypeA where
        fun TypeA = "TypeA"
        rfun "hi" = undefined

-- foo:: (Num a, MyClass a) => String -> a 
-- foo _ = 1

data RecordList x = RecordList (IO x) (x -> IO ())
type MyArray x = [Int] -> RecordList x

data Dummy x = Dummy {xarr11::IOArray Int x}

mallocArray::Int -> x -> IO (MyArray x)
mallocArray len start = do
         arr <- newArray (0, len - 1) start
         let _ = Dummy arr
         return (\[index] -> RecordList (readArray arr index)  (writeArray arr index))

readArray :: MyArray x -> [Int] -> RecordList x 
readArray f i = case f i of RecordList r _ -> r

-- allocMyArray :: Int -> x -> IO (MyArray x)
-- allocMyArray len start = do
--         arr <- newArray (0, len - 1) start :: IO(IOArray Int x)
--         return (\[index] -> RecordList (readArray arr index) (writeArray arr index))

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        let may = Just [1, 2, 3]
        arr <- newArray(0, 10) "cat" :: IO(IOArray Int String)
        ele <- readArray arr 1
        myarr <- mallocArray 10 "dog"
        pp ele
        pp "done!"
