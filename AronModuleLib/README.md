# AronModuleLib

Sun 22 Nov 21:53:32 2022 
### Remove all Typeable from AronModule so that GHCi is not complaining missing instance
* I have no idea what does the error mean but Typeable causes the error
```
    Data.Typeable
```


Mon 21 Nov 21:55:42 2022 
### Added back function, GHCi is not complaining today, I have no idea why??
```
    typeChar::Typeable a => a -> Bool
    typeChar s = typeOf s == typeOf 'a'
```
