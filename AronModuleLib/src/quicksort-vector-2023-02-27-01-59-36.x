partitionV :: (Ord a, PrimMonad m) =>
              V.MVector (PrimState m) a ->
              Int -> Int -> Int -> m Int
              -- Int -> Int -> m (V.MVector (PrimState m) a)
partitionV v lo b hi = do
                   if lo <= hi then do
                       -- Use the last element as pivot
                       pivot <- MV.read v hi
                       sn <- MV.read v lo
                       if sn <= pivot then do
                         MV.swap v lo b
                         partitionV v (lo + 1) (b + 1) hi
                       else do
                         partitionV v (lo + 1) b hi
                   else do
                     -- The last operation is always swap
                     -- ∵ the last element compares to pivot is always equal.
                     return $ b - 1
{-|
    === KEY: quick sort in mutable vector
-}
quickSortV :: (Ord a, PrimMonad m) =>
              V.MVector (PrimState m) a ->
              Int ->  --  lower index
              Int ->  --  high index
              m ()
quickSortV v lo hi = do
                     if lo < hi then do
                         px <- partitionV v lo lo hi
                         quickSortV v lo (px - 1)
                         quickSortV v (px + 1) hi
                     else return ()
main = do
        when True $ do
            pre "ok"
            fw "\NUL"
            pre $ splitStrCharNoRegex "\NUL" "123\NULabcd\0efgh\NUL"
            fw "\0"
            pre $ splitStrCharNoRegex "\0" "123\NULabcd\0efgh\NUL"
            let phoneBook = M.fromList [(1234, "Erik"), (5678, "Patrik")]

            let em = M.empty                   -- empty map
            let mm = M.insert 1 2 em
            pre mm
            pre $ M.insert 10 100 mm
            pre $ composeAct 10

            let ls' = [["a", "b"], [" ", "b", ""], [" "]]
            let ss = ["a", "b"]
            let lu = removeFirstList ss $ filter (not . null) $ map trimList ls'
            pre lu

        when True $ do
            print "ok"
            v <- MV.replicate 10 1
            let w = MV.init v
            x <- MV.read w 0
            vv <- V.thaw $ V.fromList [1, 2, 3, 4]
            MV.swap vv 0 1
            let ln = MV.length vv
            pp ln
            pp x
            y <- MV.read vv 0
            pp y
            -- Create a mutable MVector
            mv <- MV.new 10
            MV.write mv 0 39
            w <- MV.read mv 0
            fw "w"
            pp w
            pp "ok"
            mw <- MV.new 10
            MV.write mw 0 "abc"
            pp "kk"
            fromList [1, 2, 3] >>= \v -> MV.read v 0 >>= print
        when True $ do
            v <- fromList [1, 2, 3]
            pp "ok"

        when True $ do
            v <- fromList [1, 3, 2]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "partitionV  v 0 0"
            MV.read v 0 >>= print
            MV.read v 1 >>= print
            MV.read v 2 >>= print
            fw "pivotInx"
            pp $ pivotInx == 1

        when True $ do
            v <- fromList [1]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "pivotInx"
            pp $ pivotInx == 0

        when True $ do
            v <- fromList [1, 2, 3, 4]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "pivotInx"
            pp $ pivotInx == 3

        when True $ do
            v <- fromList [3, 2, 1]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "pivotInx"
            pp $ pivotInx == 0

        when True $ do
            v <- fromList [0, 0, 0]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "pivotInx [0, 0, 0]"
            pp $ pivotInx == 2

        when True $ do
            fw "quickSortV 0"
            v <- fromList [1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pp $ lt == [1]

        when True $ do
            fw "quickSortV 1"
            v <- fromList [2, 1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pp $ lt == [1, 2]

        when True $ do
            fw "quickSortV 2"
            v <- fromList [2, 1, 3]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pp $ lt == [1, 2, 3]

        when True $ do
            fw "quickSortV 3"
            v <- fromList [3, 2, 1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pp $ lt == [1, 2, 3]

        when True $ do
            fw "quickSortV 4"
            v <- fromList [2, 2, 2]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pre lt
            pp $ lt == [2, 2, 2]

        when True $ do
            fw "quickSortV 5"
            v <- fromList [3, 2, 1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pre lt
            pp $ lt == [1, 2, 3]

        when True $ do
            fw "quickSortV 6"
            v <- fromList [2, 1, 1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pre lt
            pp $ lt == [1, 1, 2]

        when True $ do
            fw "quickSortV 7"
            v <- fromList [2, 1, 1, 0, 0, 2, 3]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pre lt
            pp $ lt == [0, 0, 1, 1, 2, 2, 3]
