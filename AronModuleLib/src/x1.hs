import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

import AronModule
import AronAlias
import AronHtml2
import AronGraphic
import AronToken

import           Codec.Picture
import           Codec.Picture.Drawing
import           Codec.Picture.Types
import           Control.Monad.Primitive

changeColor :: Image PixelRGBA8 -> Image PixelRGBA8
changeColor = pixelMap $ \(PixelRGBA8 r g b a) -> PixelRGBA8  r g b ( 30 < r && r < 60 && 34 < g && g < 80 && 40 < b && b < 85 ? 0 $ a )
  
{-|

  * Wednesday, 18 September 2024 23:40 PDT

  @
  :e src/x1.hs
  :load src/x1.hs
  :main
  @
-}
  
main = do
  print "ok"
  rmBackground changeColor "/tmp/bigboss.png" "/tmp/bigboss_k.png" 
