{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE MultiWayIf       #-}

-- Sat 10 Dec 14:18:31 2022 
-- BUG: There is bug between CPP and Text.RawString.QQ 
-- {-# LANGUAGE CPP #-}
--
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-}
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
-- import Control.Monad
-- import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
-- import qualified Data.List as L
-- import Data.List.Split
-- import Data.Time
-- import Data.Time.Clock.POSIX
import Data.Char
import System.Directory
import System.Environment

partitionX :: (Ord a, PrimMonad m) => 
              V.MVector (PrimState m) a ->
              Int -> Int -> m (V.MVector (PrimState m) a) 
partitionX v b s = do 
                   let n = VM.length v 
                   VM.swap v 0 1
                   return v 
                

main = do
        when True $ do
            pre "ok" 
            fw "\NUL"
            pre $ splitStrCharNoRegex "\NUL" "123\NULabcd\0efgh\NUL"
            fw "\0"
            pre $ splitStrCharNoRegex "\0" "123\NULabcd\0efgh\NUL"
            let phoneBook = M.fromList [(1234, "Erik"), (5678, "Patrik")]

            let em = M.empty                   -- empty map
            let mm = M.insert 1 2 em
            pre mm
            pre $ M.insert 10 100 mm
            pre $ composeAct 10

            let ls' = [["a", "b"], [" ", "b", ""], [" "]]
            let ss = ["a", "b"]
            let lu = removeFirstList ss $ filter (not . null) $ map trimList ls'
            pre lu

        when True $ do
            print "ok"
            v <- VM.replicate 10 1 
            let w = VM.init v
            x <- VM.read w 0
            vv <- V.thaw $ V.fromList [1, 2, 3, 4] 
            VM.swap vv 0 1 
            let ln = VM.length vv
            pp ln
            pp x 
            y <- VM.read vv 0
            pp y
            -- Create a mutable MVector
            mv <- VM.new 10
            VM.write mv 0 39
            w <- VM.read mv 0
            fw "w"
            pp w
            pp "ok"
            mw <- VM.new 10
            VM.write mw 0 "abc"
            pp "kk"
            fromList [1, 2, 3] >>= \v -> VM.read v 0 >>= print
            


