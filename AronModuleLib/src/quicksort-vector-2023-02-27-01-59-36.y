{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE MultiWayIf                #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE TypeFamilies              #-}

-- Sat 10 Dec 14:18:31 2022
-- BUG: There is bug between CPP and Text.RawString.QQ
-- {-# LANGUAGE CPP #-}
--
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-}
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List
-- import Control.Monad
-- import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
-- import qualified Data.List as L
-- import Data.List.Split
-- import Data.Time
-- import Data.Time.Clock.POSIX
import           Control.Monad                (join, unless, when)
import           Control.Monad.Primitive
import           Data.Char
import           System.Directory
import           System.Environment
import           System.Exit
import           System.FilePath.Posix
import           System.IO
import           System.Posix.Files
import           System.Posix.Unistd
import           Text.Format
import           Text.Printf

-- import System.Process
-- import Text.Read
-- import Text.Regex
-- import Text.Regex.Base
-- import Text.Regex.Base.RegexLike
-- import Text.Regex.Posix
-- import Data.IORef
-- import Control.Concurrent



import           AronAlias
import           AronHtml2
import           AronModule                   hiding (CodeBlock (..))
-- import AronHtml
import           AronGraphic
-- import GenePDFHtmlLib
-- import WaiLib
-- import WaiConstant

-- import qualified Diagrams.Prelude as DIA
-- import Diagrams.Backend.SVG.CmdLine
-- import Diagrams.TrailLike
import           Control.Applicative
import qualified Data.Map.Strict              as M
import qualified Data.Text                    as TS
import           Diagrams.Backend.SVG.CmdLine
import           Diagrams.Prelude             hiding (blue, getSub, pre, trim)
import qualified System.Console.Pretty        as PRE
import           Text.RawString.QQ
import qualified Text.Regex.Applicative.Text  as TA
import qualified Text.Regex.TDFA              as TD

import qualified Data.Vector                  as V
import qualified Data.Vector.Mutable          as MV

{-|
f = \x -> 1*x^3 - 4*x^2 - x

s = map(\x -> (x, f x)) [0, 0.1..4]
-- pts = map p2 [(0,0), (1,1), (2,1), (3,0), (3.5,0), (4.6, 2)]
pts = map p2 s

spline :: Located (Trail V2 Double)
spline = cubicSpline False pts


param = 0.45 -- parameter on the curve where the tangent and normal are drawn
pt = atParam spline param
tangentVector = tangentAtParam spline param
normalVector = normalAtParam spline param

symmetricLine v = fromOffsets [2 *^ v] # center
mytangentLine = symmetricLine tangentVector
mynormalLine = symmetricLine normalVector

rightAngleSquare = square 0.1 # alignBL # rotate (signedAngleBetween tangentVector unitX)


example :: Diagram B
example = frame 0.5 $
  strokeLocTrail spline
  <> mconcat
     [ mytangentLine
     , baselineText "tangent" # translate tangentVector
     , mynormalLine
     , topLeftText "normal" # translate normalVector
     , rightAngleSquare
     ] # moveTo pt # fontSize large
-}


p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

--main = do
--        print "Main => Run OK"
--        argList <- getArgs
--        pp $ length argList
--        pp "done!"
{-|
    hsep

    stack exec  AronModuleLib -- -h 900 -w 1000  -O /tmp/x.svg

    https://hackage.haskell.org/package/diagrams-lib-1.4.3/docs/src/Diagrams.TwoD.Combinators.html#hsep
-}



fun::Int -> Int
fun x = x + 1

fun2::Int -> Int
fun2 x = x + 1

-- myrect :: Diagram B
-- myrect = square 0.4
{-|
main = do
       p <- (</> "publicfile" </> "pushup.txt") <$> getEnv "b"
       ls <- readFileLatin1ToList p
       let lss = splitListEmptyLine ls
       pre lss
       -- let lss = [["1"], ["2"], ["3"], ["4"], ["5"], ["6"]]
       let m = map (\cx -> let s = head cx; x = read (last cx)::Double in myrect # alignB # scaleX 1 # scaleY (x/10) # fc DIA.green # scale 0.5) lss
       -- mainWith $ hsep 0.5 [myrect # scaleY 2 # fc green # alignB # showOrigin, myrect # scaleY 4 # alignB # showOrigin]
       -- mainWith $ hsep 1 m
       -- mainWith $ pad 1.0 . centerXY $ hcat' (with & sep .~ 0.1) m
       mainWith (example :: Diagram B)
       print "DONE"
-}

{-|
main = do
       print "Hello World"
       -- print_ascii_f
       print "ok"
-}


myCircle :: Diagram B
myCircle = circle 1

{-|
main = mainWith myCircle
-}

{-|
prex = AronModule.pre
main = do
        print "dog"
        let s = [r|
                    for i in $(ls src/*.hs); do
                        echo $i;
                        cp $i /tmp
                    done
                |]
        ls <- run s
        prex ls
        putStrLn "Argintina VS Croatia and France VS Morocco"
        putStrLn ":e src/Main.hs => Edit Main.hs"
-}

{-|
main = do hSetBuffering stdout NoBuffering
          putStr "Type your age: "
          age <- readLn::IO Int
          return ()
-}

{-|
main = do
        let maxCount = 6
        print "OK"
        path <- getEnv "ff" >>= \x -> return $ x </> "password/buffer_clipboard.txt"
        print path
        block <- rfl path >>= \cx -> return $ filter (\x -> len x > 0) $ splitBlock cx "="
        pre block
        print $ "len block=" ++ (show $ len block)
        let s = [r|
                   cat "$ff/password/buffer_clipboard.txt" | grep '======' | wc -l
                |]
        cx <- run s
        if len cx > 0 then do
            let n = (strToInt . trim . head) cx
            if n < maxCount then do
                pp n
                pp " <= 6"
                let ls = map (\x -> unlines x) block
                fw "ls"
                pre ls
                hSetBuffering stdout NoBuffering
                inx <- readLn::IO Int
                let s1 = ls !! inx
                let cmd = "echo \"" ++ s1 ++ "\" | pbcopy"
                sys cmd
                pp "kk"
            else do
                pp " > 6"
        else do
            pp "len cx <= 0"

        print "done"
-}


left="\\draw[draw=green, fill=green] "
right=" circle (5pt) node[below] {};"

color = ["green", "red", "cyan", "blue", "pink", "gray", "black", "magenta"]

{-|
    % Y-Axis
	\draw[-latex, draw=red] (0,0,0) -- (0,10,0) node[pos = 1.05] {$\vec{w}$};
    % X-Axis
	\draw[-latex, draw=blue] (0,0,0) -- (10,0,0) node[pos = 1.12] {$\vec{u} \times \vec{v}$};
-}

axisXYZ::(Int, Int, Int) -> [String]
axisXYZ (x, y, z) = [xAxis, yAxis, origin]
    where
        xAxis = format "\\draw[-latex, draw=red] (0,0,0) -- ({0}, 0, 0) node[pos = 1.05] {$X$};" [show x]
        yAxis = format "\\draw[-latex, draw=blue] (0,0,0) -- ( 0, {0},0) node[pos = 1.05] {$Y$};" [show y]
        zAxis = ""
        origin = format "\\draw[draw=red, fill=black] (0, 0, 0) circle (2pt) node[below] {${O}$};" []

circleL::(Float, Float, Float) -> Float -> [String]
circleL (x, y, z) r = [s]
    where
        s = "\\draw[draw=green, fill=green]" + show (x, y, z) + " " + "circle" + " " + radius + " " ++ "node[] {};"
        (+) = (++)
        radius = "(" + show r + "pt" + ")"

drawLineL::(Float, Float, Float) -> (Float, Float, Float) -> [String]
drawLineL p0 p1 = [s]
    where
        s = format "\\draw [black line width=0.2] {0} -- {1}" [show p0, show p1]

header::[String] -> [String]
header cx = (map trim $ lines s) ++ cx
    where
        s = [r|
            \documentclass[UTF8]{article}     % normal page size
            \usepackage{pagecolor,lipsum}
            \usepackage{amsmath}
            \usepackage{amsfonts}
            \usepackage{amssymb}
            \usepackage{amsthm}
            \usepackage{centernot}
            \usepackage{xcolor,colortbl}
            \usepackage{graphicx}
            \usepackage{svg}
            \usepackage{mathtools}
            \usepackage{hyperref}
            \usepackage{fancyvrb} % Verbatim
            \input{/Users/aaa/myfile/bitbucket/math/aronlib.tex}
            |]

tikz::[String] -> [String]
tikz cx = begin ++ cx ++ end
    where
        begin = ["\\begin{tikzpicture}"]
        end   = ["\\end{tikzpicture}"]

document::[String] -> [String]
document cx = begin ++ cx ++ end
    where
        begin = ["\\begin{document}"]
        end   = ["\\end{document}"]


-- x = 0  => identify matrix => [ [1  0
--                                 0, 1]]
-- x = 90 => [ [0 -1
--              1 0
--
m1 x = [ [ cos x, -sin x ],
         [ sin x,  cos x ]
       ]

--              x      y     z
rot3  x = [ [ cos x, -sin x, 0 ],
            [ sin x,  cos x, 0 ],
            [ 0,          0, 0 ]
          ]

mkm::(Float, Float, Float) -> [[Float]]
mkm (x, y, z) = [ [x, 0, 0],
                  [y, 0, 0],
                  [0, 0, 0]
                ]

{-|
main = do
        let s = circleL (1, 1, 0) 10.0
        let ls = map (\x -> (x, x^2, 0)) [-3, (-3 + 0.1) .. 3]
        let ls' = map (\t -> let m = let r = rotz (pi/2) in multiMat r $ mkm t
                                 l = join $ getColumn m 1
                             in (l !! 0, l !! 1, 0) ) ls

        let cir  = map (\c -> head $ circleL c 1.0) ls
        let cir' = map (\c -> head $ circleL c 1.0) ls'
        let ss = axisXYZ (10, 10, 0)
        let line = drawLineL (0.5, 0.5, 0) (2, 2, 0)
        let ret = ss ++ cir ++ cir' ++ line
        pre ls
        pre s
        fw "ret"
        pre ret
        wfl "/tmp/k.k" ret
        sys "cat /tmp/k.k | pbcopy"
        let ti = tikz s
        fw "ti"
        pre ti
        let lex = header $ document $ tikz ret
        fw "lex"
        pre lex
        mathPath <- getEnv "m" >>= \x -> return $ x </> "abc.tex"
        wfl mathPath lex
        sys $ "latexsync.sh " ++ mathPath
        let m2 = [[1, 0, 0],
                  [0, 0, 0],
                  [0, 0, 0]
                 ]
        let m3 = [[0, 0, 0],
                  [1, 0, 0],
                  [0, 0, 0]
                 ]
        let mx2 = multiMat (rotz (pi/2)) m2
        fw "mx2"
        pre mx2
        pre $ getColumn mx2 1
        let mx3 = multiMat (rotz (pi/2)) m3
        fw "mx3"
        pre mx3
        pre $ getColumn mx3 1
        pre $ drawLineL (1, 1, 0) (2, 2, 0)

-}


{-|
main = do
          fw "str"
          let count = 40
          pre $ trimList ["", " ", "a", "", "b", " ", ""]
          let l = len color
          s1 <- randomFloat count  >>= \cs -> return $ map (10*) cs
          s2 <- randomFloat count  >>= \cs -> return $ map (10*) cs
          let mx = len color - 1
          rs <- randomIntList count (0, mx)
          radius <- randomIntList count (1, 30)
          fw "rs"
          pre rs
          let ls = zipWith3(\a b c -> (a, b, c)) s1 s2 rs
          pre ls
          let s = map (\(a, b, c) -> "\\draw[draw=" ++  (color !! c) ++ " , fill=" ++ (color !! c) ++ "] " ++ show (a, b) ++ right ) ls

          let rr = format "{0} {1}" ["dog", show 100]
          pre rr
          -- let t = map (\(a, b, c) -> printf "%s" ("dog"::String)) ls
          let tt = map(\(a, b, ix) -> format "\\draw[draw={0} , fill={0}] {1} circle ({2}pt) node[below] {};" [color !! ix, show (a, b, 0), show $ radius !! ix] ) ls
          let cirStr = circleL (1, 1, 0) 14
          let ss = axisXYZ (10, 10, 0)
          let ret = ss ++ tt ++ cirStr
          pre ret
          wfl "/tmp/k.k" ret
          sys "cat /tmp/k.k | pbcopy"
-}


isOp :: Char -> Bool
isOp c = elem c ['+', '-', '*', '/']

isSpaceX :: Char -> Bool
isSpaceX c = c == ' '

isAlphaNumX :: Char -> Bool
isAlphaNumX c = isLetter c || isDigit c

{-|
    ===

    @
        ab12- => (ab12, -)

        "" "ab12-"

        "a" "b12-"
        "ab" "12-"
        "ab1" "2-"
        "ab12" "-"

    @
-}
alnums :: String -> (String, String)
alnums s = getAlphaNum [] s

getAlphaNum :: String -> String -> (String, String)
getAlphaNum a []      = (a, [])
getAlphaNum a (c:cx)  | isAlphaNumX c = let (a', cx') = getAlphaNum a cx  -- look ahead ?, not sure
                                        in (c:a', cx')
                      | otherwise     = (a, c:cx)

getAlphaNum2 :: String -> String -> (String, String)
getAlphaNum2 a []      = (a, [])
getAlphaNum2 a (c:cx)  | isLetter c = let (a', cx') = getAlphaNum2 a cx
                                      in (c:a', cx')

--                         isAlphaNumX c = let (a', cx') = getAlphaNum2 a cx  -- look ahead ?, not sure
--                                         in getAlphaNum2 (c:a') cx'
                       | otherwise     = (a, c:cx)

getAlphaNum' :: String -> String -> (String, String)
getAlphaNum' a  []     = (a, [])
getAlphaNum' a (c:cx)  | isAlphaNumX c = getAlphaNum' (a ++ [c]) cx
                       | otherwise     = (a, c:cx)


{-|

    === Extract digits from a string

    @
        getDigit "" "123abc"   => ("123", "abc")
        getDigit "" "+123"     => ("",    "+123")
        getDigit "KK" "123ab"  => ("123KK", "ab")


                [get "KK" "123ab"
                   c = 1
                   [get "KK" "23ab"
                      c = 2
                      [get "KK" "3ab"
                        c = 3
                        [get "KK" "ab"
                          c = a
                        ]
                        ("KK", "ab")
                      ]
                      ("3KK" "ab")
                   ]
                   ("23KK" "ab")
               ]
               ("123KK" "ab")
    @

            accum     inputStr  (accum, restOfStr)
              ↓         ↓              ↓
-}
getDigit :: String -> String -> (String, String)
getDigit a [] = (a, [])
getDigit a (c:cx) | isDigit c = let (c',   cx') = getDigit a cx
                                in  (c:c', cx')
                  | otherwise = (a, c:cx)


-- Generalize the getAlphaNum and getDigit
spanX::(a -> Bool) -> [a] -> [a] -> ([a], [a])
spanX f a []     = (a, [])
spanX f a (c:cx) | f c = let (a', cx') = spanX f a cx
                         in  (c:a', cx')
                 | otherwise = (a, c:cx)

getWSpace :: String -> String -> (String, String)
getWSpace a s = spanX isSpace a s



isStrDQ :: (String, String) -> Bool
isStrDQ (a, s) = len a >= 2 && head a == '"' && last a == '"'


getStrDQ :: String -> String -> (String, String)
getStrDQ a []     = (a, [])
getStrDQ a (c:cx) | isDQ c = let (q0, cx') = oneDQ [] (c:cx)
                                 (a', cy') = nonDQ [] cx'
                                 (q1, cz') = oneDQ [] cy'
                                 s1 = q0 ++ a' ++ q1
                             in  isStrDQ (s1, cz') ?  (s1 ++ a, cz') $ (a, c:cx)
                  | otherwise = (a, c:cx)

yesDQ :: String -> String -> (String, String)
yesDQ a [] = (a, [])
yesDQ a (c:cx) | isDQ c = let (a', cx') = yesDQ a  cx
                          in (c:a', cx')
               | otherwise = (a, c:cx)

oneDQ :: String -> String -> (String, String)
oneDQ a [] = (a, [])
oneDQ a (c:cx) | isDQ c = (c:a, cx)
               | otherwise = (a, c:cx)


{-|
-}
isStrSQ :: (String, String) -> Bool
isStrSQ (a, s) = len a >= 2 && head a == '\'' && last a == '\''

{-|
-}
isSQ :: Char -> Bool
isSQ c = c == '\''

{-|
-}
oneSQ :: String -> String -> (String, String)
oneSQ a [] = (a, [])
oneSQ a (c:cx) | isSQ c = (c:a, cx)
               | otherwise = (a, c:cx)

{-|
-}
nonSQ :: String -> String -> (String, String)
nonSQ a []  = (a, [])
nonSQ a (c:cx) | (not . isSQ) c = let (a', cx') = nonSQ a cx
                                  in (c:a', cx')
               | otherwise = (a, c:cx)


{-|
    String has no Double Quotes
-}
nonDQ :: String -> String -> (String, String)
nonDQ a [] = (a, [])
nonDQ a (c:cx) | (not . isDQ) c = let (a', cx') = nonDQ a cx
                                  in (c:a', cx')
               | otherwise = (a, c:cx)


{-|
   @
     getStrSQ "" 'ab'cdef => ("'ab'", "efg")
     getStrSQ "" "'abc"   => ("",   "'abc")
     getStrSQ "" "''abc"  => ("''",   "'abc")

   @
-}
getStrSQ :: String -> String -> (String, String)
getStrSQ a []     = (a, [])
getStrSQ a (c:cx) | isSQ c = let (q0, cx') = oneSQ [] (c:cx)
                                 (a', cy') = nonSQ [] cx'
                                 (q1, cz') = oneSQ [] cy'
                                 s1 = q0 ++ a' ++ q1
                             in  isStrSQ (s1, cz') ? (s1 ++ a, cz') $ (a, c:cx)
                  | otherwise = (a, c:cx)

{-|
    Single Quotes String
-}
--getStrSQ :: String -> String -> (String, String)
--getStrSQ a s = extractStrSQ a s


isDoubleQuote :: Char -> Bool
isDoubleQuote c = c == '"'

isDQ = isDoubleQuote


getEqualRightArrow :: String -> String -> (String, String)
getEqualRightArrow a []      = (a, [])
getEqualRightArrow a (b:c:cx) | b == '=' && c == '>' = (b:c:a, cx)
                              | otherwise = (a, b:c:cx)
getEqualRightArrow a s  = (a, s)

getTwoColon :: String -> String -> (String, String)
getTwoColon a []      = (a, [])
getTwoColon a (b:c:cx) | b == ':' && c == ':' = (b:c:a, cx)
                       | otherwise = (a, b:c:cx)
getTwoColon a s  = (a, s)

{-|
    Single Quotes String

    @
    ->
    @
-}
getSubRightArrow :: String -> String -> (String, String)
getSubRightArrow a []      = (a, [])
getSubRightArrow a (b:c:cx) | b == '-' && c == '>' = (b:c:a, cx)
                            | otherwise = (a, b:c:cx)
getSubRightArrow a s  = (a, s)

getTwoEqual :: String -> String -> (String, String)
getTwoEqual a []      = (a, [])
getTwoEqual a (b:c:cx) | b == '=' && c == '=' = (b:c:a, cx)
                       | otherwise = (a, b:c:cx)
getTwoEqual a s  = (a, s)

getEqual :: String -> String -> (String, String)
getEqual a [] = (a, [])
getEqual a (c:cx) | c == '='   = (c:a, cx)
                  | otherwise  = (a, c:cx)

getSingleQuote :: String -> String -> (String, String)
getSingleQuote a [] = (a, [])
getSingleQuote a (c:cx) | c == '\'' = (c:a, cx)
                        | otherwise = (a, c:cx)


getDoubleQuote :: String -> String -> (String, String)
getDoubleQuote a [] = (a, [])
getDoubleQuote a (c:cx) | c == '"' = (c:a, cx)
                        | otherwise = (a, c:cx)

getForwardslash :: String -> String -> (String, String)
getForwardslash a [] = (a, [])
getForwardslash a (c:cx) | c == '/' = (c:a, cx)
                         | otherwise = (a, c:cx)

getBackslash :: String -> String -> (String, String)
getBackslash a [] = (a, [])
getBackslash a (c:cx) | c == '\\' = (c:a, cx)
                      | otherwise = (a, c:cx)

getGT :: String -> String -> (String, String)
getGT a [] = (a, [])
getGT a (c:cx) | c == '>' = (c:a, cx)
               | otherwise = (a, c:cx)

getLT :: String -> String -> (String, String)
getLT a [] = (a, [])
getLT a (c:cx) | c == '<' = (c:a, cx)
               | otherwise = (a, c:cx)

getSub :: String -> String -> (String, String)
getSub a [] = (a, [])
getSUb a (c:cx) | c == '-'  = (c:a, cx)
                | otherwise = (a, c:cx)

{-|
    === Get Right Curly Bracket
-}
getRCB :: String -> String -> (String, String)
getRCB a [] = (a, [])
getRCB a (c:cx) | c == '}' = (c:a, cx)
                | otherwise = (a, c:cx)
{-|
    === Get Left Curly Bracket
-}
getLCB :: String -> String -> (String, String)
getLCB a [] = (a, [])
getLCB a (c:cx) | c == '{' = (c:a, cx)
                | otherwise = (a, c:cx)


{-|
    === Get Right Parenthese or Right Round Bracket
-}
getRP :: String -> String -> (String, String)
getRP a [] = (a, [])
getRP a (c:cx) | c == ')' = (c:a, cx)
               | otherwise = (a, c:cx)
{-|
    === Get Left Parenthese or Left Round Bracket
-}
getLP :: String -> String -> (String, String)
getLP a [] = (a, [])
getLP a (c:cx) | c == '(' = (c:a, cx)
               | otherwise = (a, c:cx)

{-|
    === Get Right Square Bracket
-}
getRSB :: String -> String -> (String, String)
getRSB a [] = (a, [])
getRSB a (c:cx) | c == ']' = (c:a, cx)
                | otherwise = (a, c:cx)
{-|
    === Get Left Square Bracket
-}
getLSB :: String -> String -> (String, String)
getLSB a [] = (a, [])
getLSB a (c:cx) | c == '[' = (c:a, cx)
                | otherwise = (a, c:cx)


data Token2 = AlphaNum String
            | TokLP String
            | TokRP String
            | TokLCB String
            | TokRCB String
            | TokLSB String   --  [
            | TokRSB String   --  ]
            | TokGT String
            | TokLT String
            | TokEqual String
            | TokSQ String
            | TokDQ String
            | TokColon String
            | TokForwardslash String
            | TokBackslash String
            | OpAdd String
            | OpSub String
            | OpDiv String
            | OpMul String
            | DQString String
            | SQString String
            | WSpace String
            | SymEqualRightArrow String             --  =>
            | SymTwoColon String                    --  ::
            | SymSubRightArrow String               --  ->
            | SymTwoEqual String                    --  ==
            | Unknown String                        --
            | NumberX String deriving(Show, Eq)

-- xx1
tokenize :: String -> [Token2]
tokenize [] = []
tokenize (c:cx) | isLetter c = let (a, cx') = getAlphaNum2 [] (c:cx)
                                in AlphaNum a : tokenize cx'
                 | isDigit c  = let (a, cx') = getDigit [] (c:cx)
                                in NumberX a : tokenize cx'
                 | isSpace c  = let (a, cx') = getWSpace [] (c:cx)
                                in WSpace a : tokenize cx'
                 | c == '='   = let (a, cx') = getEqualRightArrow [] (c:cx)
                                in case a of
                                       var | a /= [] -> SymEqualRightArrow a : tokenize cx'
                                           | otherwise -> let (a, cx') = getTwoEqual [] (c:cx)
                                                          in case a of
                                                               var | a /= [] -> SymTwoEqual a : tokenize cx'
                                                                   | otherwise -> let (a, cx') = getEqual [] (c:cx)
                                                                                  in TokEqual a : tokenize cx'


                 | c == '['   = let (a, cx') = getLSB [] (c:cx)
                                in case a of
                                     var | a /= [] -> TokLSB [c] : tokenize cx
                                         | otherwise -> error "ERROR: Unknow error 3"

                 | c == ']'   = let (a, cx') = getRSB [] (c:cx)
                                in case a of
                                     var | a /= [] -> TokRSB [c] : tokenize cx
                                         | otherwise -> error "ERROR: Unknow error 4"

                 | c == '-'   = let (a, cx') = getSubRightArrow [] (c:cx)
                                in case a of
                                      var | a /= [] -> SymSubRightArrow a : tokenize cx'
                                          | otherwise -> OpSub [c] : tokenize cx

                 | c == ':'   = let (a, cx') = getTwoColon [] (c:cx)
                                in case a of
                                      var | a /= [] -> SymTwoEqual a : tokenize cx'
                                          | otherwise -> TokColon [c] : tokenize cx

                 | c == '/'   = let (a, cx') = getForwardslash [] (c:cx)
                                in case a of
                                      var | a /= [] -> TokForwardslash a : tokenize cx'
                                          | otherwise -> error "ERROR: Unknown Error 1"

                 | c == '\\'   = let (a, cx') = getBackslash [] (c:cx)
                                in case a of
                                      var | a /= [] -> TokBackslash a : tokenize cx'
                                          | otherwise -> error "ERROR: Unknown Error 1"


                 | c == '<'   = let (a, cx') = getLT [] (c:cx)
                                in TokLT a : tokenize cx'
                 | c == '>'   = let (a, cx') = getGT [] (c:cx)
                                in TokGT a : tokenize cx'
                 | c == '('   = let (a, cx') = getLP [] (c:cx)
                                in TokLP a : tokenize cx'
                 | c == ')'   = let (a, cx') = getRP [] (c:cx)
                                in TokRP a : tokenize cx'
                 | c == '{'   = let (a, cx') = getLCB [] (c:cx)
                                in TokLCB a : tokenize cx'
                 | c == '}'   = let (a, cx') = getRCB [] (c:cx)
                                in TokRCB a : tokenize cx'
                 | c == '"'   = let (a, cx') = getStrDQ [] (c:cx)
                                in  case a of
                                        var | a /= [] -> DQString a : tokenize cx'
                                            | otherwise -> let (a, cx') = getDoubleQuote [] (c:cx)
                                                           in TokDQ a : tokenize cx'

                 | c == '\''  = let (a, cx') = getStrSQ [] (c:cx)
                                in  case a of
                                        var | a /= [] -> SQString a : tokenize cx'
                                            | otherwise -> let (a, cx') = getSingleQuote [] (c:cx)
                                                           in TokSQ a : tokenize cx'

                 | otherwise  = Unknown [c] : tokenize cx

colorToken :: [Token2] -> [String]
colorToken [] = []
colorToken ls = map(\t -> case t of
                                AlphaNum s           -> s
                                NumberX  s           -> colorfgStr 150 s
                                TokLP s              -> col PRE.Green s
                                TokRP s              -> col PRE.Green s
                                TokLCB s             -> col PRE.Red s
                                TokRCB s             -> col PRE.Red s
                                TokLSB s             -> col PRE.Green s
                                TokRSB s             -> col PRE.Green s
                                TokLT s              -> col PRE.Yellow s
                                TokGT s              -> col PRE.Yellow s
                                TokColon s           -> col PRE.Yellow s
                                TokEqual s           -> colorfgStr 100 s
                                TokForwardslash s    -> colorfgStr 30 s
                                TokBackslash s       -> colorfgStr 170 s
                                TokSQ    s           -> colorfgStr 120 s
                                TokDQ    s           -> colorfgStr 20 s
                                OpSub    s           -> colorfgStr 50 s
                                DQString s           -> col PRE.Cyan s
                                SQString s           -> col PRE.Blue s
                                WSpace s             -> s
                                SymEqualRightArrow s -> col PRE.Red s
                                SymSubRightArrow   s -> colorfgStr 160 s
                                SymTwoEqual s        -> col PRE.Magenta s
                                SymTwoColon s        -> col PRE.Red s
                                Unknown s            -> s
                                _                    -> []
                    ) ls
            where
              col = PRE.color
{-|
main = do
        argList <- getArgs
        let ln = len argList
        if ln == 1 then do
            let s = head argList
            let ls = tokenize s
            pre ls
            let lt = colorToken ls
            pre lt
            putStrLn $ concat lt
        else do
            print "Need one argument"

        let s = "abc123Casper 1 + 3 "

        when False $ do
            let s = "ab12"
            fw "getAlphaNum"
            pre $ getAlphaNum "" s
            fw "getAlphaNum'"
            pre $ getAlphaNum' "" s
            fw "getAlphaNum2"
            pre $ getAlphaNum2 "" s

        when False $ do
            let s = "ab12--"
            fw "getAlphaNum"
            pre $ getAlphaNum "" s
            fw "getAlphaNum'"
            pre $ getAlphaNum' "" s
            fw "getAlphaNum2"
            pre $ getAlphaNum2 "" s

        when False $ do
            let s = "ab12+Casper"
            fw "getAlphaNum"
            pre $ getAlphaNum "" s
            fw "getAlphaNum'"
            pre $ getAlphaNum' "" s
            fw "getAlphaNum2"
            pre $ getAlphaNum2 "" s

        when False $ do
            let s = "ab12+Casper"
            fw "alnums"
            pre $ ("ab12", "+Casper") == alnums s


        when False $ do
            let s = "12+Casper"
            fw "spanX"
            pre s
            pre $ ("12", "+Casper") == spanX isDigit "" s

        when False $ do
            let s = "abc123+Casper"
            fw "spanX"
            pre s
            pre $ ("abc123", "+Casper") == spanX isAlphaNumX "" s

        when False $ do
            let s = "abc123+Casper"
            fw "nonDQ"
            pre s
            pre $ ("abc123+Casper", "") == nonDQ "" s

        when False $ do
            let s = "abc123+\"Casper"
            fw "nonDQ"
            pre s
            pre $ ("abc123+", "\"Casper") == nonDQ "" s

        when False $ do
            let s = "ab\"kk"
            fw "yesDQ"
            pre s
            pre $ ("", "ab\"kk") == yesDQ "" s

        when False $ do
            let s = "\"abc"
            fw "yesDQ"
            pre s
            pre $ ("\"", "abc") == yesDQ "" s

        when False $ do
            let s = "\"\"abc"
            fw "yesDQ"
            pre s
            pre $ ("\"\"", "abc") == yesDQ "" s

        when False $ do
            let s = "\"\"\"abc"
            fw "yesDQ"
            pre s
            pre $ ("\"\"\"", "abc") == yesDQ "" s

        when False $ do
            let s = "abc"
            fw "oneDQ"
            pre s
            pre $ ("", "abc") == oneDQ "" s

        when False $ do
            let s = "\"abc"
            fw "oneDQ"
            pre s
            pre $ ("\"", "abc") == oneDQ "" s

        when False $ do
            let s = "\"\"\"abc"
            fw "oneDQ"
            pre s
            pre $ ("\"", "\"\"abc") == oneDQ "" s

        when False $ do

            let s = "\"\"\"abc"
            fw "oneDQ"
            pre s
            pre $ ("\"KK", "\"\"abc") == oneDQ "KK" s

        when False $ do
            let s = "\"\"\"abc"
            fw "oneDQ"
            pre s
            pre $ ("\"\"", "\"\"abc") == oneDQ "\"" s

        when False $ do
            let s = "\"Casper"
            fw "oneDQ"
            pre s
            pre $ ("\"ab", "Casper") == oneDQ "ab" s

        when False $ do
            let s = "\"ab\"Casper"
            fw "getStrDQ"
            pre s
            pre $ ("\"ab\"", "Casper") == getStrDQ "" s

        when False $ do
            let s = "\"abCasper"
            fw "getStrDQ"
            pre s
            pre $ ("", "\"abCasper") == getStrDQ "" s

        when False $ do
            let s = "\"\"abCasper"
            fw "getStrDQ"
            pre s
            pre $ ("\"\"", "abCasper") == getStrDQ "" s

        when False $ do
            let s = "\"\\\"abCasper"
            fw "getStrDQ"
            pre s
            pre $ ("\"\\\"", "abCasper") == getStrDQ "" s

        when False $ do
            let s = "\"+\"abCasper"
            fw "getStrDQ"
            pre s
            pre $ ("\"+\"", "abCasper") == getStrDQ "" s


        when False $ do
            let s = " "
            fw "getWSpace"
            pre s
            pre $ (" ", "") == getWSpace "" s

        when False $ do
            let s = "  "
            fw "getWSpace"
            pre s
            pre $ ("  ", "") == getWSpace "" s


        when False $ do
            let s = "="
            fw "getEqualRightArrow"
            pre s
            pre $ getEqualRightArrow "" s

        when False $ do
            let s = "=ab"
            fw "getEqualRightArrow"
            pre s
            pre $ getEqualRightArrow "" s

        when False $ do
            let s = "=>ab"
            fw "getEqualRightArrow"
            pre s
            pre $ getEqualRightArrow "" s

        when False $ do
            let s = ">=ab"
            fw "getEqualRightArrow"
            pre s
            pre $ getEqualRightArrow "" s

        when False $ do
            let s = ">=>=ab"
            fw "getEqualRightArrow"
            pre s
            pre $ getEqualRightArrow "" s

        when False $ do
            let s = "=>>ab"
            fw "getEqualRightArrow"
            pre s
            pre $ getEqualRightArrow "" s

        when False $ do
            let s = "123 abc123 => >ab"
            fw "getEqualRightArrow"
            pre s
            pre $ getEqualRightArrow "" s

        when False $ do
            let s = "12+Casper"
            fw "getDigit"
            let t = getDigit "" s
            pre t
            pre $ ("12", "+Casper") == t

        when False $ do
            let s = "123abc"
            fw "getDigit"
            let t = getDigit "KK" s
            pre t
            pre $ ("123KK", "abc") == t


        when False $ do
            let s = ">abc"
            fw "getGT"
            pre s
            pre $ (">", "abc") == getGT "" s

        when False $ do
            let s = ">abc"
            fw "getGT"
            pre s
            let t = getGT "12" s
            pre t
            pre $ (">12", "abc") == t

        when False $ do
            let s = ">>abc"
            fw "getGT"
            pre s
            let t = getGT "12" s
            pre t
            pre $ (">12", ">abc") == t

        when False $ do
            let s = "<abc"
            fw "getLT"
            pre s
            let t = getLT "12" s
            pre t
            pre $ ("<12", "abc") == t

        when False $ do
            let s = "<"
            fw "getLT"
            pre s
            let t = getLT "" s
            pre t
            pre $ ("<", "") == t

        when False $ do
            let s = "<ab"
            fw "getLT"
            pre s
            let t = getLT "" s
            pre t
            pre $ ("<", "ab") == t

        when False $ do
            let s = "\'"
            fw "isSQ"
            pre s
            let t = isSQ '\''
            pre t
            pre $ True == t

        when False $ do
            let s = "\'"
            fw "oneSQ"
            pre s
            let t = oneSQ "" s
            pre t
            pre $ ("\'", "") == t

        when False $ do
            let s = "\'abc"
            fw "oneSQ"
            pre s
            let t = oneSQ "" s
            pre t
            pre $ ("\'", "abc") == t

        when False $ do
            let s = "\'abc"
            fw "oneSQ"
            pre s
            let t = oneSQ "KK" s
            pre t
            pre $ ("\'KK", "abc") == t

        when False $ do
            let s = "abc"
            fw "nonSQ"
            pre s
            let t = nonSQ "" s
            pre t
            pre $ ("abc", "") == t

        when False $ do
            let s = "\'abc"
            fw "nonSQ"
            pre s
            let t = nonSQ "" s
            pre t
            pre $ ("", "\'abc") == t

        when False $ do
            let s = "abc"
            fw "nonSQ"
            pre s
            let t = nonSQ "KK" s
            pre t
            pre $ ("abcKK", "") == t

        when False $ do
            let s = "'ab'Casper"
            fw "getStrSQ"
            pre s
            pre $ ("'ab'", "Casper") == getStrSQ "" s

        when False $ do
            let s = "'abCasper"
            fw "getStrSQ"
            pre s
            pre $ ("", "'abCasper") == getStrSQ "" s

        when False $ do
            let s = "''abCasper"
            fw "getStrSQ"
            pre s
            pre $ ("''", "abCasper") == getStrSQ "" s

        when False $ do
            let s = "'\\'abCasper"
            fw "getStrSQ"
            pre s
            pre $ ("'\\'", "abCasper") == getStrSQ "" s

        when False $ do
            let s = "'+'abCasper"
            fw "getStrSQ"
            pre s
            pre $ ("'+'", "abCasper") == getStrSQ "" s

        when False $ do
            let s = "'+'abCasper"
            fw "getStrSQ"
            pre s
            let t = getStrSQ "KK" s
            pre t
            pre $ ("'+'KK", "abCasper") == t

        when False $ do
            let s = "'+''abCasper"
            fw "getStrSQ"
            pre s
            let t = getStrSQ "KK" s
            pre t
            pre $ ("'+'KK", "'abCasper") == t

        when False $ do
            let s = "abc123"
            fw "tokenize"
            pre s
            pre $ tokenize s

        when False $ do
            let s = "123"
            fw "tokenize"
            pre s
            pre $ tokenize s

        when False $ do
            let s = "abc123 456"
            fw "tokenize"
            pre s
            pre $ tokenize s

        when False $ do
            let col = PRE.color
            let s = "=>abc123 456=>>(dog){cat}"
            fw "tokenize"
            pre s
            let ls = tokenize s

            let lt = colorToken ls
            pre lt
            putStrLn $ concat lt

        when False $ do
            let col = PRE.color
            -- let s = "\"abc\"abc (do){cat}<cow> 'SingleQuote' abc=>> '123\"KK"
            let s = "\"abc\"abc (do){cat}<cow> 'SingleQuote' abc=>> 123KK ==> ab=123"
            fw "tokenize"
            let ls = tokenize s
            pre ls
            let lt = colorToken ls
            pre lt
            pre s
            putStrLn $ concat lt

        when False $ do
            let col = PRE.color
            let s = "=123\"abc\"\"='456'' 3-4-> x--|: fun::Int -> Int 3.14=>2.71[cow]->{dog} s/Croatia/Portugal/  \\abc\\=>"
            pre s
            fw "tokenize"
            let ls = tokenize s
            pre ls
            let lt = colorToken ls
            pre lt
            putStrLn $ concat lt
-}

{-|
data Token2 = AlphaNum String
            | TokLP String
            | TokRP String
            | TokLCB String
            | TokRCB String
            | TokLSB String
            | TokRSB String
            | TokGT String
            | TokLT String
            | OpAdd String
            | OpSub String
            | OpDiv String
            | OpMul String
            | DQString String
            | WSpace String
            | SymEqualRightArrow String               -- =>
            | NumberX String deriving(Show, Eq)
-}

splitSText :: TS.Text -> TS.Text -> Maybe (String, TS.Text)
splitSText pat s = TA.findFirstPrefix (TA.few TA.anySym TA.<* (TA.string pat)) s
{-|
main = do
        pp "ok"
        let s = " a\24515b :abc: @=\24515\24515 abc " :: TS.Text
        let pat = "\24515"
        fw "s"
        pre s
        let xx = TA.findFirstPrefix (TA.few TA.anySym TA.<* pat) s
        let xe = case xx of
                    Just x -> x
                    Nothing -> error "error"
        fw "xe"
        pre xe
        let tu = (fst xe, snd xe)
        fw "tu"
        pre tu
        let fs = fst tu
        let sn = snd tu
        ff "fs" fs
        pre $ trim fs
        fw "sn"
        pre sn
        pre $ trimT sn
--        let xs = case xx of
--                    Just x -> (trimT $ fst x, trimT $ snd x)
--                    Nothing -> (strToStrictText [], strToStrictText [])
--        pre s
--        fw "xx"
        pre xx

        let cy = "a\24515bc"
        putStrLn cy
        wfl "/tmp/b.x" [cy]
        fw "splitSText"
        pre $ splitSText "<" s

        let str = strictTextToStr s
        fw "str"
        pre str
        ff "len=" $ (show . len) str
        let lt = map (\x -> [x]) str
        fw "lt"
        pre lt
        let lr = dropWhile (\x -> x /= "\24515") lt
        let lx = map (\x -> trim x == "\24515" ? 1 $ 0) lt
        let colons = map (\x -> trim x == ":" ? 1 $ 0) lt
        fw "colons"
        pre colons
        fw "lx"
        pre lx
        fw "lr"
        pre lr
-}

{-|
    === KEY: Split String to a list

    @
        pre $ splitStrCharNoRegex "\NUL" "123\NULabcd\0efgh\NUL"

        [ "123"
        , "abcd"
        , "efgh"
        ]

        pre $ splitStrCharNoRegex "\0" "123\NULabcd\0efgh\NUL"

        [ "123"
        , "abcd"
        , "efgh"
        ]
    @
-}

{-|
main = do
        fw "withFile"
        fw "withFile :: FilePath -> ReadMode -> (Handler -> IO r) -> IO r"
        print "ok"
        printFile "/tmp/ab.x"
-}

maybeFail :: a -> Maybe Int
maybeFail a = Nothing

defAction :: a -> Maybe Int
defAction a = Just 3

composeAct :: a -> Maybe Int
composeAct x = maybeFail x <|> defAction x

{-|
main = do
        pre "ok"
        fw "\NUL"
        pre $ splitStrCharNoRegex "\NUL" "123\NULabcd\0efgh\NUL"
        fw "\0"
        pre $ splitStrCharNoRegex "\0" "123\NULabcd\0efgh\NUL"
        let phoneBook = M.fromList [(1234, "Erik"), (5678, "Patrik")]

        let em = M.empty                   -- empty map
        let mm = M.insert 1 2 em
        pre mm
        pre $ M.insert 10 100 mm
        pre $ composeAct 10

        let ls' = [["a", "b"], [" ", "b", ""], [" "]]
        let ss = ["a", "b"]
        let lu = removeFirstList ss $ filter (not . null) $ map trimList ls'
        pre lu
-}

-- /Users/aaa/myfile/bitbucket/stackproject/AronModuleLib/src/mainvector-2023-02-26-16-22-22.x


fromList :: (PrimMonad m) => [a] -> m (V.MVector (PrimState m) a)
fromList s = V.thaw $ V.fromList s

toList :: (PrimMonad m) => V.MVector (PrimState m) a -> m [a]
toList v = do
           let n = MV.length v
           mapM (\i -> MV.read v i ) [0.. (n - 1)]


partitionV :: (Ord a, PrimMonad m) =>
              V.MVector (PrimState m) a ->
              Int -> Int -> Int -> m Int
              -- Int -> Int -> m (V.MVector (PrimState m) a)
partitionV v lo b hi = do
                   if lo <= hi then do
                       -- Use the last element as pivot
                       pivot <- MV.read v hi
                       sn <- MV.read v lo
                       if sn <= pivot then do
                         MV.swap v lo b
                         partitionV v (lo + 1) (b + 1) hi
                       else do
                         partitionV v (lo + 1) b hi
                   else do
                     -- The last operation is always swap
                     -- ∵ the last element compares to pivot is always equal.
                     return $ b - 1
{-|
    === KEY: quick sort in mutable vector
-}
quickSortV :: (Ord a, PrimMonad m) =>
              V.MVector (PrimState m) a ->
              Int ->  --  lower index
              Int ->  --  high index
              m ()
quickSortV v lo hi = do
                     if lo < hi then do
                         px <- partitionV v lo lo hi
                         quickSortV v lo (px - 1)
                         quickSortV v (px + 1) hi
                     else return ()
main = do
        when True $ do
            pre "ok"
            fw "\NUL"
            pre $ splitStrCharNoRegex "\NUL" "123\NULabcd\0efgh\NUL"
            fw "\0"
            pre $ splitStrCharNoRegex "\0" "123\NULabcd\0efgh\NUL"
            let phoneBook = M.fromList [(1234, "Erik"), (5678, "Patrik")]

            let em = M.empty                   -- empty map
            let mm = M.insert 1 2 em
            pre mm
            pre $ M.insert 10 100 mm
            pre $ composeAct 10

            let ls' = [["a", "b"], [" ", "b", ""], [" "]]
            let ss = ["a", "b"]
            let lu = removeFirstList ss $ filter (not . null) $ map trimList ls'
            pre lu

        when True $ do
            print "ok"
            v <- MV.replicate 10 1
            let w = MV.init v
            x <- MV.read w 0
            vv <- V.thaw $ V.fromList [1, 2, 3, 4]
            MV.swap vv 0 1
            let ln = MV.length vv
            pp ln
            pp x
            y <- MV.read vv 0
            pp y
            -- Create a mutable MVector
            mv <- MV.new 10
            MV.write mv 0 39
            w <- MV.read mv 0
            fw "w"
            pp w
            pp "ok"
            mw <- MV.new 10
            MV.write mw 0 "abc"
            pp "kk"
            fromList [1, 2, 3] >>= \v -> MV.read v 0 >>= print
        when True $ do
            v <- fromList [1, 2, 3]
            pp "ok"

        when True $ do
            v <- fromList [1, 3, 2]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "partitionV  v 0 0"
            MV.read v 0 >>= print
            MV.read v 1 >>= print
            MV.read v 2 >>= print
            fw "pivotInx"
            pp $ pivotInx == 1

        when True $ do
            v <- fromList [1]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "pivotInx"
            pp $ pivotInx == 0

        when True $ do
            v <- fromList [1, 2, 3, 4]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "pivotInx"
            pp $ pivotInx == 3

        when True $ do
            v <- fromList [3, 2, 1]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "pivotInx"
            pp $ pivotInx == 0

        when True $ do
            v <- fromList [0, 0, 0]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "pivotInx [0, 0, 0]"
            pp $ pivotInx == 2

        when True $ do
            fw "quickSortV 0"
            v <- fromList [1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pp $ lt == [1]

        when True $ do
            fw "quickSortV 1"
            v <- fromList [2, 1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pp $ lt == [1, 2]

        when True $ do
            fw "quickSortV 2"
            v <- fromList [2, 1, 3]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pp $ lt == [1, 2, 3]

        when True $ do
            fw "quickSortV 3"
            v <- fromList [3, 2, 1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pp $ lt == [1, 2, 3]

        when True $ do
            fw "quickSortV 4"
            v <- fromList [2, 2, 2]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pre lt
            pp $ lt == [2, 2, 2]

        when True $ do
            fw "quickSortV 5"
            v <- fromList [3, 2, 1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pre lt
            pp $ lt == [1, 2, 3]

        when True $ do
            fw "quickSortV 6"
            v <- fromList [2, 1, 1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pre lt
            pp $ lt == [1, 1, 2]

        when True $ do
            fw "quickSortV 7"
            v <- fromList [2, 1, 1, 0, 0, 2, 3]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pre lt
            pp $ lt == [0, 0, 1, 1, 2, 2, 3]

