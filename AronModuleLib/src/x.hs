
module Main where
import AronModule
import Data.IORef
import Control.Lens hiding (pre, re)

data MyRec = MyRec {a_ :: Int, b_ :: Int} deriving (Eq, Show)

main = do
       let r = MyRec {a_ = 3, b_ =  4}
       refR <- newIORef r
       modifyIORef refR (\s  -> let s' = s{a_ = 100} in s'{a_ = a_ s' + 100})
       rt <- readIORef refR
       print $ show rt

       s <- randomIntList 20 (1, 1000)
       let ls = partList 2 s
       let lv = map (\(x:y:_) -> (x, y)) ls
       prex lv
       let lt = init $ partList 3 lv
       prex lt
       
