{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE MultiWayIf                #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE ScopedTypeVariables #-}

{-# LANGUAGE RankNTypes #-}   -- forall

module Main where

-- Sat 10 Dec 14:18:31 2022
-- BUG: There is bug between CPP and Text.RawString.QQ
-- {-# LANGUAGE CPP #-}
--
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-}
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List
-- import Control.Monad
-- import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
-- import qualified Data.List as L
-- import Data.List.Split
-- import Data.Time
-- import Data.Time.Clock.POSIX
-- /Users/aaa/myfile/bitbucket/stackproject/AronModuleLib/src/xx2-2023-02-27-02-10-34.x
import           Control.Monad                (join, unless, when)
import           Control.Monad.Primitive
import           Data.Char
import           System.Directory
import           System.Environment
import           System.Exit
import           System.FilePath.Posix
import           System.IO
import           System.IO.Extra
import           System.Posix.Files
import           System.Posix.Unistd
import           Text.Format
import           Text.Printf
import           Data.List (tails)
import qualified Data.List.Split as DLS
import Graphics.Rendering.OpenGL

-- import System.Process
-- import Text.Read
-- import Text.Regex
-- import Text.Regex.Base
-- import Text.Regex.Base.RegexLike
-- import Text.Regex.Posix
-- import Data.IORef
-- import Control.Concurrent



import           AronAlias
import           AronHtml2
-- import           AronDevLib                   
import           AronModule                   hiding (CodeBlock (..), watchDir)
-- import AronHtml
import           AronGraphic
import           AronOpenGL
import           AronSymbolicAlgebra
import           AronToken
import           AronFFI
import           WaiLib
  
-- import GenePDFHtmlLib
-- import WaiLib
-- import WaiConstant

-- import qualified Diagrams.Prelude as DIA
-- import Diagrams.Backend.SVG.CmdLine
-- import Diagrams.TrailLike
import           Control.Applicative
import qualified Data.Map.Strict              as M
import qualified Data.Text                    as TS
import           Diagrams.Backend.SVG.CmdLine
import           Diagrams.Prelude             hiding (blue, getSub, pre, trim, diff, rewrite)
import qualified System.Console.Pretty        as PRE
import           Text.RawString.QQ
import qualified Text.Regex.Applicative.Text  as TA
import qualified Text.Regex.TDFA              as TD

import Data.Functor.Identity

-- /Users/aaa/myfile/bitbucket/stackproject/AronModuleLib/src/xx-2023-02-27-02-06-14.x
import qualified Data.Vector                  as V
import qualified Data.Vector.Mutable          as MV

import Control.Monad.ST
import Data.Array.MArray
import Data.Array.IO


import System.FSNotify
import Control.Concurrent (threadDelay)
import Control.Monad (forever)

import           Data.Int (Int64)
import           Database.SQLite.Simple
import           Database.SQLite.Simple.FromRow
import           Database.SQLite.Simple.FromField
import           Database.SQLite.Simple.ToField
import           Database.SQLite.Simple.Internal
import           Database.SQLite.Simple.Ok


{-|
f = \x -> 1*x^3 - 4*x^2 - x

s = map(\x -> (x, f x)) [0, 0.1..4]
-- pts = map p2 [(0,0), (1,1), (2,1), (3,0), (3.5,0), (4.6, 2)]
pts = map p2 s

spline :: Located (Trail V2 Double)
spline = cubicSpline False pts


param = 0.45 -- parameter on the curve where the tangent and normal are drawn
pt = atParam spline param
tangentVector = tangentAtParam spline param
normalVector = normalAtParam spline param

symmetricLine v = fromOffsets [2 *^ v] # center
mytangentLine = symmetricLine tangentVector
mynormalLine = symmetricLine normalVector

rightAngleSquare = square 0.1 # alignBL # rotate (signedAngleBetween tangentVector unitX)


example :: Diagram B
example = frame 0.5 $
  strokeLocTrail spline
  <> mconcat
     [ mytangentLine
     , baselineText "tangent" # translate tangentVector
     , mynormalLine
     , topLeftText "normal" # translate normalVector
     , rightAngleSquare
     ] # moveTo pt # fontSize large
-}


p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

--main = do
--        print "Main => Run OK"
--        argList <- getArgs
--        pp $ length argList
--        pp "done!"
{-|
    hsep

    stack exec  AronModuleLib -- -h 900 -w 1000  -O /tmp/x.svg

    https://hackage.haskell.org/package/diagrams-lib-1.4.3/docs/src/Diagrams.TwoD.Combinators.html#hsep
-}



fun::Int -> Int
fun x = x + 1

fun2::Int -> Int
fun2 x = x + 1

-- myrect :: Diagram B
-- myrect = square 0.4
{-|
main = do
       p <- (</> "publicfile" </> "pushup.txt") <$> getEnv "b"
       ls <- readFileLatin1ToList p
       let lss = splitListEmptyLine ls
       pre lss
       -- let lss = [["1"], ["2"], ["3"], ["4"], ["5"], ["6"]]
       let m = map (\cx -> let s = head cx; x = read (last cx)::Double in myrect # alignB # scaleX 1 # scaleY (x/10) # fc DIA.green # scale 0.5) lss
       -- mainWith $ hsep 0.5 [myrect # scaleY 2 # fc green # alignB # showOrigin, myrect # scaleY 4 # alignB # showOrigin]
       -- mainWith $ hsep 1 m
       -- mainWith $ pad 1.0 . centerXY $ hcat' (with & sep .~ 0.1) m
       mainWith (example :: Diagram B)
       print "DONE"
-}

{-|
main = do
       print "Hello World"
       -- print_ascii_f
       print "ok"
-}


myCircle :: Diagram B
myCircle = circle 1

{-|
main = mainWith myCircle
-}

{-|
prex = AronModule.pre
main = do
        print "dog"
        let s = [r|
                    for i in $(ls src/*.hs); do
                        echo $i;
                        cp $i /tmp
                    done
                |]
        ls <- run s
        prex ls
        putStrLn "Argintina VS Croatia and France VS Morocco"
        putStrLn ":e src/Main.hs => Edit Main.hs"
-}

{-|
main = do hSetBuffering stdout NoBuffering
          putStr "Type your age: "
          age <- readLn::IO Int
          return ()
-}

{-|
main = do
        let maxCount = 6
        print "OK"
        path <- getEnv "ff" >>= \x -> return $ x </> "password/buffer_clipboard.txt"
        print path
        block <- rfl path >>= \cx -> return $ filter (\x -> len x > 0) $ splitBlock cx "="
        pre block
        print $ "len block=" ++ (show $ len block)
        let s = [r|
                   cat "$ff/password/buffer_clipboard.txt" | grep '======' | wc -l
                |]
        cx <- run s
        if len cx > 0 then do
            let n = (strToInt . trim . head) cx
            if n < maxCount then do
                pp n
                pp " <= 6"
                let ls = map (\x -> unlines x) block
                fw "ls"
                pre ls
                hSetBuffering stdout NoBuffering
                inx <- readLn::IO Int
                let s1 = ls !! inx
                let cmd = "echo \"" ++ s1 ++ "\" | pbcopy"
                sys cmd
                pp "kk"
            else do
                pp " > 6"
        else do
            pp "len cx <= 0"

        print "done"
-}


left="\\draw[draw=green, fill=green] "
right=" circle (5pt) node[below] {};"

color = ["green", "red", "cyan", "blue", "pink", "gray", "black", "magenta"]

{-|
    % Y-Axis
	\draw[-latex, draw=red] (0,0,0) -- (0,10,0) node[pos = 1.05] {$\vec{w}$};
    % X-Axis
	\draw[-latex, draw=blue] (0,0,0) -- (10,0,0) node[pos = 1.12] {$\vec{u} \times \vec{v}$};
-}

axisXYZ::(Int, Int, Int) -> [String]
axisXYZ (x, y, z) = [xAxis, yAxis, origin]
    where
        xAxis = format "\\draw[-latex, draw=red] (0,0,0) -- ({0}, 0, 0) node[pos = 1.05] {$X$};" [show x]
        yAxis = format "\\draw[-latex, draw=blue] (0,0,0) -- ( 0, {0},0) node[pos = 1.05] {$Y$};" [show y]
        zAxis = ""
        origin = format "\\draw[draw=red, fill=black] (0, 0, 0) circle (2pt) node[below] {${O}$};" []

circleL::(Float, Float, Float) -> Float -> [String]
circleL (x, y, z) r = [s]
    where
        s = "\\draw[draw=green, fill=green]" + show (x, y, z) + " " + "circle" + " " + radius + " " ++ "node[] {};"
        (+) = (++)
        radius = "(" + show r + "pt" + ")"

drawLineL::(Float, Float, Float) -> (Float, Float, Float) -> [String]
drawLineL p0 p1 = [s]
    where
        s = format "\\draw [black line width=0.2] {0} -- {1}" [show p0, show p1]

header::[String] -> [String]
header cx = (map trim $ lines s) ++ cx
    where
        s = [r|
            \documentclass[UTF8]{article}     % normal page size
            \usepackage{pagecolor,lipsum}
            \usepackage{amsmath}
            \usepackage{amsfonts}
            \usepackage{amssymb}
            \usepackage{amsthm}
            \usepackage{centernot}
            \usepackage{xcolor,colortbl}
            \usepackage{graphicx}
            \usepackage{svg}
            \usepackage{mathtools}
            \usepackage{hyperref}
            \usepackage{fancyvrb} % Verbatim
            \input{/Users/aaa/myfile/bitbucket/math/aronlib.tex}
            |]

tikz::[String] -> [String]
tikz cx = begin ++ cx ++ end
    where
        begin = ["\\begin{tikzpicture}"]
        end   = ["\\end{tikzpicture}"]

document::[String] -> [String]
document cx = begin ++ cx ++ end
    where
        begin = ["\\begin{document}"]
        end   = ["\\end{document}"]


-- x = 0  => identify matrix => [ [1  0
--                                 0, 1]]
-- x = 90 => [ [0 -1
--              1 0
--
m1 x = [ [ cos x, -sin x ],
         [ sin x,  cos x ]
       ]

--              x      y     z
rot3  x = [ [ cos x, -sin x, 0 ],
            [ sin x,  cos x, 0 ],
            [ 0,          0, 0 ]
          ]

mkm::(Float, Float, Float) -> [[Float]]
mkm (x, y, z) = [ [x, 0, 0],
                  [y, 0, 0],
                  [0, 0, 0]
                ]

{-|
main = do
        let s = circleL (1, 1, 0) 10.0
        let ls = map (\x -> (x, x^2, 0)) [-3, (-3 + 0.1) .. 3]
        let ls' = map (\t -> let m = let r = rotz (pi/2) in multiMat r $ mkm t
                                 l = join $ getColumn m 1
                             in (l !! 0, l !! 1, 0) ) ls

        let cir  = map (\c -> head $ circleL c 1.0) ls
        let cir' = map (\c -> head $ circleL c 1.0) ls'
        let ss = axisXYZ (10, 10, 0)
        let line = drawLineL (0.5, 0.5, 0) (2, 2, 0)
        let ret = ss ++ cir ++ cir' ++ line
        pre ls
        pre s
        fw "ret"
        pre ret
        wfl "/tmp/k.k" ret
        sys "cat /tmp/k.k | pbcopy"
        let ti = tikz s
        fw "ti"
        pre ti
        let lex = header $ document $ tikz ret
        fw "lex"
        pre lex
        mathPath <- getEnv "m" >>= \x -> return $ x </> "abc.tex"
        wfl mathPath lex
        sys $ "latexsync.sh " ++ mathPath
        let m2 = [[1, 0, 0],
                  [0, 0, 0],
                  [0, 0, 0]
                 ]
        let m3 = [[0, 0, 0],
                  [1, 0, 0],
                  [0, 0, 0]
                 ]
        let mx2 = multiMat (rotz (pi/2)) m2
        fw "mx2"
        pre mx2
        pre $ getColumn mx2 1
        let mx3 = multiMat (rotz (pi/2)) m3
        fw "mx3"
        pre mx3
        pre $ getColumn mx3 1
        pre $ drawLineL (1, 1, 0) (2, 2, 0)

-}


{-|
main = do
          fw "str"
          let count = 40
          pre $ trimList ["", " ", "a", "", "b", " ", ""]
          let l = len color
          s1 <- randomFloat count  >>= \cs -> return $ map (10*) cs
          s2 <- randomFloat count  >>= \cs -> return $ map (10*) cs
          let mx = len color - 1
          rs <- randomIntList count (0, mx)
          radius <- randomIntList count (1, 30)
          fw "rs"
          pre rs
          let ls = zipWith3(\a b c -> (a, b, c)) s1 s2 rs
          pre ls
          let s = map (\(a, b, c) -> "\\draw[draw=" ++  (color !! c) ++ " , fill=" ++ (color !! c) ++ "] " ++ show (a, b) ++ right ) ls

          let rr = format "{0} {1}" ["dog", show 100]
          pre rr
          -- let t = map (\(a, b, c) -> printf "%s" ("dog"::String)) ls
          let tt = map(\(a, b, ix) -> format "\\draw[draw={0} , fill={0}] {1} circle ({2}pt) node[below] {};" [color !! ix, show (a, b, 0), show $ radius !! ix] ) ls
          let cirStr = circleL (1, 1, 0) 14
          let ss = axisXYZ (10, 10, 0)
          let ret = ss ++ tt ++ cirStr
          pre ret
          wfl "/tmp/k.k" ret
          sys "cat /tmp/k.k | pbcopy"
-}


isOp :: Char -> Bool
isOp c = elem c ['+', '-', '*', '/']

isSpaceX :: Char -> Bool
isSpaceX c = c == ' '

isAlphaNumX :: Char -> Bool
isAlphaNumX c = isLetter c || isDigit c

{-|
main = do
        argList <- getArgs
        let ln = len argList
        if ln == 1 then do
            let s = head argList
            let ls = tokenize s
            pre ls
            let lt = colorToken ls
            pre lt
            putStrLn $ concat lt
        else do
            print "Need one argument"

        let s = "abc123Casper 1 + 3 "

        when False $ do
            let s = "ab12"
            fw "getAlphaNum"
            pre $ getAlphaNum "" s
            fw "getAlphaNum'"
            pre $ getAlphaNum' "" s
            fw "getAlphaNum2"
            pre $ getAlphaNum2 "" s

        when False $ do
            let s = "ab12--"
            fw "getAlphaNum"
            pre $ getAlphaNum "" s
            fw "getAlphaNum'"
            pre $ getAlphaNum' "" s
            fw "getAlphaNum2"
            pre $ getAlphaNum2 "" s

        when False $ do
            let s = "ab12+Casper"
            fw "getAlphaNum"
            pre $ getAlphaNum "" s
            fw "getAlphaNum'"
            pre $ getAlphaNum' "" s
            fw "getAlphaNum2"
            pre $ getAlphaNum2 "" s

        when False $ do
            let s = "ab12+Casper"
            fw "alnums"
            pre $ ("ab12", "+Casper") == alnums s


        when False $ do
            let s = "12+Casper"
            fw "spanX"
            pre s
            pre $ ("12", "+Casper") == spanX isDigit "" s

        when False $ do
            let s = "abc123+Casper"
            fw "spanX"
            pre s
            pre $ ("abc123", "+Casper") == spanX isAlphaNumX "" s

        when False $ do
            let s = "abc123+Casper"
            fw "nonDQ"
            pre s
            pre $ ("abc123+Casper", "") == nonDQ "" s

        when False $ do
            let s = "abc123+\"Casper"
            fw "nonDQ"
            pre s
            pre $ ("abc123+", "\"Casper") == nonDQ "" s

        when False $ do
            let s = "ab\"kk"
            fw "yesDQ"
            pre s
            pre $ ("", "ab\"kk") == yesDQ "" s

        when False $ do
            let s = "\"abc"
            fw "yesDQ"
            pre s
            pre $ ("\"", "abc") == yesDQ "" s

        when False $ do
            let s = "\"\"abc"
            fw "yesDQ"
            pre s
            pre $ ("\"\"", "abc") == yesDQ "" s

        when False $ do
            let s = "\"\"\"abc"
            fw "yesDQ"
            pre s
            pre $ ("\"\"\"", "abc") == yesDQ "" s

        when False $ do
            let s = "abc"
            fw "oneDQ"
            pre s
            pre $ ("", "abc") == oneDQ "" s

        when False $ do
            let s = "\"abc"
            fw "oneDQ"
            pre s
            pre $ ("\"", "abc") == oneDQ "" s

        when False $ do
            let s = "\"\"\"abc"
            fw "oneDQ"
            pre s
            pre $ ("\"", "\"\"abc") == oneDQ "" s

        when False $ do

            let s = "\"\"\"abc"
            fw "oneDQ"
            pre s
            pre $ ("\"KK", "\"\"abc") == oneDQ "KK" s

        when False $ do
            let s = "\"\"\"abc"
            fw "oneDQ"
            pre s
            pre $ ("\"\"", "\"\"abc") == oneDQ "\"" s

        when False $ do
            let s = "\"Casper"
            fw "oneDQ"
            pre s
            pre $ ("\"ab", "Casper") == oneDQ "ab" s

        when False $ do
            let s = "\"ab\"Casper"
            fw "getStrDQ"
            pre s
            pre $ ("\"ab\"", "Casper") == getStrDQ "" s

        when False $ do
            let s = "\"abCasper"
            fw "getStrDQ"
            pre s
            pre $ ("", "\"abCasper") == getStrDQ "" s

        when False $ do
            let s = "\"\"abCasper"
            fw "getStrDQ"
            pre s
            pre $ ("\"\"", "abCasper") == getStrDQ "" s

        when False $ do
            let s = "\"\\\"abCasper"
            fw "getStrDQ"
            pre s
            pre $ ("\"\\\"", "abCasper") == getStrDQ "" s

        when False $ do
            let s = "\"+\"abCasper"
            fw "getStrDQ"
            pre s
            pre $ ("\"+\"", "abCasper") == getStrDQ "" s


        when False $ do
            let s = " "
            fw "getWSpace"
            pre s
            pre $ (" ", "") == getWSpace "" s

        when False $ do
            let s = "  "
            fw "getWSpace"
            pre s
            pre $ ("  ", "") == getWSpace "" s


        when False $ do
            let s = "="
            fw "getEqualRightArrow"
            pre s
            pre $ getEqualRightArrow "" s

        when False $ do
            let s = "=ab"
            fw "getEqualRightArrow"
            pre s
            pre $ getEqualRightArrow "" s

        when False $ do
            let s = "=>ab"
            fw "getEqualRightArrow"
            pre s
            pre $ getEqualRightArrow "" s

        when False $ do
            let s = ">=ab"
            fw "getEqualRightArrow"
            pre s
            pre $ getEqualRightArrow "" s

        when False $ do
            let s = ">=>=ab"
            fw "getEqualRightArrow"
            pre s
            pre $ getEqualRightArrow "" s

        when False $ do
            let s = "=>>ab"
            fw "getEqualRightArrow"
            pre s
            pre $ getEqualRightArrow "" s

        when False $ do
            let s = "123 abc123 => >ab"
            fw "getEqualRightArrow"
            pre s
            pre $ getEqualRightArrow "" s

        when False $ do
            let s = "12+Casper"
            fw "getDigit"
            let t = getDigit "" s
            pre t
            pre $ ("12", "+Casper") == t

        when False $ do
            let s = "123abc"
            fw "getDigit"
            let t = getDigit "KK" s
            pre t
            pre $ ("123KK", "abc") == t


        when False $ do
            let s = ">abc"
            fw "getGT"
            pre s
            pre $ (">", "abc") == getGT "" s

        when False $ do
            let s = ">abc"
            fw "getGT"
            pre s
            let t = getGT "12" s
            pre t
            pre $ (">12", "abc") == t

        when False $ do
            let s = ">>abc"
            fw "getGT"
            pre s
            let t = getGT "12" s
            pre t
            pre $ (">12", ">abc") == t

        when False $ do
            let s = "<abc"
            fw "getLT"
            pre s
            let t = getLT "12" s
            pre t
            pre $ ("<12", "abc") == t

        when False $ do
            let s = "<"
            fw "getLT"
            pre s
            let t = getLT "" s
            pre t
            pre $ ("<", "") == t

        when False $ do
            let s = "<ab"
            fw "getLT"
            pre s
            let t = getLT "" s
            pre t
            pre $ ("<", "ab") == t

        when False $ do
            let s = "\'"
            fw "isSQ"
            pre s
            let t = isSQ '\''
            pre t
            pre $ True == t

        when False $ do
            let s = "\'"
            fw "oneSQ"
            pre s
            let t = oneSQ "" s
            pre t
            pre $ ("\'", "") == t

        when False $ do
            let s = "\'abc"
            fw "oneSQ"
            pre s
            let t = oneSQ "" s
            pre t
            pre $ ("\'", "abc") == t

        when False $ do
            let s = "\'abc"
            fw "oneSQ"
            pre s
            let t = oneSQ "KK" s
            pre t
            pre $ ("\'KK", "abc") == t

        when False $ do
            let s = "abc"
            fw "nonSQ"
            pre s
            let t = nonSQ "" s
            pre t
            pre $ ("abc", "") == t

        when False $ do
            let s = "\'abc"
            fw "nonSQ"
            pre s
            let t = nonSQ "" s
            pre t
            pre $ ("", "\'abc") == t

        when False $ do
            let s = "abc"
            fw "nonSQ"
            pre s
            let t = nonSQ "KK" s
            pre t
            pre $ ("abcKK", "") == t

        when False $ do
            let s = "'ab'Casper"
            fw "getStrSQ"
            pre s
            pre $ ("'ab'", "Casper") == getStrSQ "" s

        when False $ do
            let s = "'abCasper"
            fw "getStrSQ"
            pre s
            pre $ ("", "'abCasper") == getStrSQ "" s

        when False $ do
            let s = "''abCasper"
            fw "getStrSQ"
            pre s
            pre $ ("''", "abCasper") == getStrSQ "" s

        when False $ do
            let s = "'\\'abCasper"
            fw "getStrSQ"
            pre s
            pre $ ("'\\'", "abCasper") == getStrSQ "" s

        when False $ do
            let s = "'+'abCasper"
            fw "getStrSQ"
            pre s
            pre $ ("'+'", "abCasper") == getStrSQ "" s

        when False $ do
            let s = "'+'abCasper"
            fw "getStrSQ"
            pre s
            let t = getStrSQ "KK" s
            pre t
            pre $ ("'+'KK", "abCasper") == t

        when False $ do
            let s = "'+''abCasper"
            fw "getStrSQ"
            pre s
            let t = getStrSQ "KK" s
            pre t
            pre $ ("'+'KK", "'abCasper") == t

        when False $ do
            let s = "abc123"
            fw "tokenize"
            pre s
            pre $ tokenize s

        when False $ do
            let s = "123"
            fw "tokenize"
            pre s
            pre $ tokenize s

        when False $ do
            let s = "abc123 456"
            fw "tokenize"
            pre s
            pre $ tokenize s

        when False $ do
            let col = PRE.color
            let s = "=>abc123 456=>>(dog){cat}"
            fw "tokenize"
            pre s
            let ls = tokenize s

            let lt = colorToken ls
            pre lt
            putStrLn $ concat lt

        when False $ do
            let col = PRE.color
            -- let s = "\"abc\"abc (do){cat}<cow> 'SingleQuote' abc=>> '123\"KK"
            let s = "\"abc\"abc (do){cat}<cow> 'SingleQuote' abc=>> 123KK ==> ab=123"
            fw "tokenize"
            let ls = tokenize s
            pre ls
            let lt = colorToken ls
            pre lt
            pre s
            putStrLn $ concat lt

        when False $ do
            let col = PRE.color
            let s = "=123\"abc\"\"='456'' 3-4-> x--|: fun::Int -> Int 3.14=>2.71[cow]->{dog} s/Croatia/Portugal/  \\abc\\=>"
            pre s
            fw "tokenize"
            let ls = tokenize s
            pre ls
            let lt = colorToken ls
            pre lt
            putStrLn $ concat lt
-}

{-|
data Token2 = AlphaNum String
            | TokLP String
            | TokRP String
            | TokLCB String
            | TokRCB String
            | TokLSB String
            | TokRSB String
            | TokGT String
            | TokLT String
            | OpAdd String
            | OpSub String
            | OpDiv String
            | OpMul String
            | DQString String
            | WSpace String
            | SymEqualRightArrow String               -- =>
            | NumberX String deriving(Show, Eq)
-}

splitSText :: TS.Text -> TS.Text -> Maybe (String, TS.Text)
splitSText pat s = TA.findFirstPrefix (TA.few TA.anySym TA.<* (TA.string pat)) s
{-|
main = do
        pp "ok"
        let s = " a\24515b :abc: @=\24515\24515 abc " :: TS.Text
        let pat = "\24515"
        fw "s"
        pre s
        let xx = TA.findFirstPrefix (TA.few TA.anySym TA.<* pat) s
        let xe = case xx of
                    Just x -> x
                    Nothing -> error "error"
        fw "xe"
        pre xe
        let tu = (fst xe, snd xe)
        fw "tu"
        pre tu
        let fs = fst tu
        let sn = snd tu
        ff "fs" fs
        pre $ trim fs
        fw "sn"
        pre sn
        pre $ trimT sn
--        let xs = case xx of
--                    Just x -> (trimT $ fst x, trimT $ snd x)
--                    Nothing -> (strToStrictText [], strToStrictText [])
--        pre s
--        fw "xx"
        pre xx

        let cy = "a\24515bc"
        putStrLn cy
        wfl "/tmp/b.x" [cy]
        fw "splitSText"
        pre $ splitSText "<" s

        let str = strictTextToStr s
        fw "str"
        pre str
        ff "len=" $ (show . len) str
        let lt = map (\x -> [x]) str
        fw "lt"
        pre lt
        let lr = dropWhile (\x -> x /= "\24515") lt
        let lx = map (\x -> trim x == "\24515" ? 1 $ 0) lt
        let colons = map (\x -> trim x == ":" ? 1 $ 0) lt
        fw "colons"
        pre colons
        fw "lx"
        pre lx
        fw "lr"
        pre lr
-}

{-|
    === KEY: Split String to a list

    @
        pre $ splitStrCharNoRegex "\NUL" "123\NULabcd\0efgh\NUL"

        [ "123"
        , "abcd"
        , "efgh"
        ]

        pre $ splitStrCharNoRegex "\0" "123\NULabcd\0efgh\NUL"

        [ "123"
        , "abcd"
        , "efgh"
        ]
    @
-}

{-|
main = do
        fw "withFile"
        fw "withFile :: FilePath -> ReadMode -> (Handler -> IO r) -> IO r"
        print "ok"
        printFile "/tmp/ab.x"
-}

maybeFail :: a -> Maybe Int
maybeFail a = Nothing

defAction :: a -> Maybe Int
defAction a = Just 3

composeAct :: a -> Maybe Int
composeAct x = maybeFail x <|> defAction x

{-|
main = do
        pre "ok"
        fw "\NUL"
        pre $ splitStrCharNoRegex "\NUL" "123\NULabcd\0efgh\NUL"
        fw "\0"
        pre $ splitStrCharNoRegex "\0" "123\NULabcd\0efgh\NUL"
        let phoneBook = M.fromList [(1234, "Erik"), (5678, "Patrik")]

        let em = M.empty                   -- empty map
        let mm = M.insert 1 2 em
        pre mm
        pre $ M.insert 10 100 mm
        pre $ composeAct 10

        let ls' = [["a", "b"], [" ", "b", ""], [" "]]
        let ss = ["a", "b"]
        let lu = removeFirstList ss $ filter (not . null) $ map trimList ls'
        pre lu
-}

-- /Users/aaa/myfile/bitbucket/stackproject/AronModuleLib/src/mainvector-2023-02-26-16-22-22.x


fromList :: (PrimMonad m) => [a] -> m (V.MVector (PrimState m) a)
fromList s = V.thaw $ V.fromList s

toList :: (PrimMonad m) => V.MVector (PrimState m) a -> m [a]
toList v = do
           let n = MV.length v
           mapM (\i -> MV.read v i ) [0.. (n - 1)]

-- /Users/aaa/myfile/bitbucket/stackproject/AronModuleLib/src/quicksort-vector-2023-02-27-01-59-36.x
partitionV :: (Ord a, PrimMonad m) =>
              V.MVector (PrimState m) a ->
              Int -> Int -> Int -> m Int
              -- Int -> Int -> m (V.MVector (PrimState m) a)
partitionV v lo b hi = do
                   if lo <= hi then do
                       -- Use the last element as pivot
                       pivot <- MV.read v hi
                       sn <- MV.read v lo
                       if sn <= pivot then do
                         MV.swap v lo b
                         partitionV v (lo + 1) (b + 1) hi
                       else do
                         partitionV v (lo + 1) b hi
                   else do
                     -- The last operation is always swap
                     -- ∵ the last element compares to pivot is always equal.
                     return $ b - 1
{-|
    === KEY: quick sort in mutable vector
-}
quickSortV :: (Ord a, PrimMonad m) =>
              V.MVector (PrimState m) a ->
              Int ->  --  lower index
              Int ->  --  higher index
              m ()
quickSortV v lo hi = do
                     if lo < hi then do
                         px <- partitionV v lo lo hi
                         quickSortV v lo (px - 1)
                         quickSortV v (px + 1) hi
                     else return ()

mainX = do
-- main = do
        when True $ do
            pre "ok"
            fw "\NUL"
            pre $ splitStrCharNoRegex "\NUL" "123\NULabcd\0efgh\NUL"
            fw "\0"
            pre $ splitStrCharNoRegex "\0" "123\NULabcd\0efgh\NUL"
            let phoneBook = M.fromList [(1234, "Erik"), (5678, "Patrik")]

            let em = M.empty                   -- empty map
            let mm = M.insert 1 2 em
            pre mm
            pre $ M.insert 10 100 mm
            pre $ composeAct 10

            let ls' = [["a", "b"], [" ", "b", ""], [" "]]
            let ss = ["a", "b"]
            let lu = removeFirstList ss $ filter (not . null) $ map trimList ls'
            pre lu

        when True $ do
            print "ok"
            v <- MV.replicate 10 1
            let w = MV.init v
            x <- MV.read w 0
            vv <- V.thaw $ V.fromList [1, 2, 3, 4]
            MV.swap vv 0 1
            let ln = MV.length vv
            pp ln
            pp x
            y <- MV.read vv 0
            pp y
            -- Create a mutable MVector
            mv <- MV.new 10
            MV.write mv 0 39
            w <- MV.read mv 0
            fw "w"
            pp w
            pp "ok"
            mw <- MV.new 10
            MV.write mw 0 "abc"
            pp "kk"
            fromList [1, 2, 3] >>= \v -> MV.read v 0 >>= print
        when True $ do
            v <- fromList [1, 2, 3]
            pp "ok"

        when True $ do
            v <- fromList [1, 3, 2]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "partitionV  v 0 0"
            MV.read v 0 >>= print
            MV.read v 1 >>= print
            MV.read v 2 >>= print
            fw "pivotInx"
            pp $ pivotInx == 1

        when True $ do
            v <- fromList [1]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "pivotInx"
            pp $ pivotInx == 0

        when True $ do
            v <- fromList [1, 2, 3, 4]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "pivotInx"
            pp $ pivotInx == 3

        when True $ do
            v <- fromList [3, 2, 1]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "pivotInx"
            pp $ pivotInx == 0

        when True $ do
            v <- fromList [0, 0, 0]
            pivotInx <- partitionV v 0 0 $ MV.length v - 1
            fw "pivotInx [0, 0, 0]"
            pp $ pivotInx == 2

        when True $ do
            fw "quickSortV 0"
            v <- fromList [1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pp $ lt == [1]

        when True $ do
            fw "quickSortV 1"
            v <- fromList [2, 1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pp $ lt == [1, 2]

        when True $ do
            fw "quickSortV 2"
            v <- fromList [2, 1, 3]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pp $ lt == [1, 2, 3]

        when True $ do
            fw "quickSortV 3"
            v <- fromList [3, 2, 1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pp $ lt == [1, 2, 3]

        when True $ do
            fw "quickSortV 4"
            v <- fromList [2, 2, 2]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pre lt
            pp $ lt == [2, 2, 2]

        when True $ do
            fw "quickSortV 5"
            v <- fromList [3, 2, 1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pre lt
            pp $ lt == [1, 2, 3]

        when True $ do
            fw "quickSortV 6"
            v <- fromList [2, 1, 1]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pre lt
            pp $ lt == [1, 1, 2]

        when True $ do
            fw "quickSortV 7"
            v <- fromList [2, 1, 1, 0, 0, 2, 3]
            let lo = 0
            quickSortV v lo $ MV.length v - 1
            lt <- toList v
            pre lt
            pp $ lt == [0, 0, 1, 1, 2, 2, 3]

        when False $ do
            fw "frequenceCount"
            -- let f x = map (\x -> (head x, len x)) $ groupBy (\a b -> a == b)  $ sort x
            s <- readFileList "/tmp/xx1.x"
            let ls = map (\x -> read x :: Int) s 
            pre $ frequenceCount ls 

        when True $ do
            -- Create a new mutable array of 5 integers
            arr <- newArray (1, 5) 0 :: IO (IOArray Int Int)

            -- Write values to the array
            writeArray arr 1 10
            writeArray arr 2 20
            writeArray arr 3 30
            writeArray arr 4 40
            writeArray arr 5 50

            -- Read values from the array
            val1 <- readArray arr 1
            val3 <- readArray arr 3

            -- Print the values
            putStrLn $ "Value at index 1: " ++ show val1 -- Value at index 1: 10
            putStrLn $ "Value at index 3: " ++ show val3 -- Value at index 3: 30

            -- Update a value in the array
            writeArray arr 2 25

            -- Print the updated value
            val2 <- readArray arr 2
            putStrLn $ "Value at index 2: " ++ show val2 -- Value at index 2: 25

            -- Freeze the array to make it immutable
            -- frozenArr <- freeze arr :: IO (IOArray Int Int) 
            ls <- getElems arr
            pre ls

        when True $ do
            pp "ok"
            let ls = [[0.3, 0.4], [0.333, 0.444444]]
            let ls' = (map . map) show ls
            let mx = foldl (\a b -> max a b) 0 $ map len $ join ls'
            let lt = (map . map) (\x -> let d = mx - (len x)
                                            p = replicate d ' '
                                            s = x ++ p
                                        in s) ls'
            
            let lv = map concat lt
           
            (mapM_) putStrLn lv
  
            pre ls'
            pp mx
            printMat ls
            let lxx = [[1..5] | x <- [1..5]]
            printMat lxx
            cx <- randomFloat 16
            let cxx = partList 4 cx
            printMat cxx

        when True $ do
            pp "ok"
            let fov = 90.0
            let fov' = fov/2.0
            let rad = fov' * (pi/180)
            let zn = 1.0 
            let zf = 5.0 
            let fv = 1/(tan rad)
            let a33 = (zf + zn)/(zn - zf)
            let a34 = (2*zf * zn)/(zn - zf)
            let mat = [
                       [fv, 0, 0,   0]
                      ,[0, fv, 0,   0]
                      ,[0, 0,  a33, a34]
                      ,[0, 0,  (-1),   0]
                      ]
            fw "glPerspective"
            printMat mat

        when True $ do
            printMat mat
            let w = [1, 2, 3]            

            let mu m v = getColumn (multiMat m mm) 1
                        where
                          gm mx n = replicate mx $ replicate n 0
                          vc = tran [v]
                          nRow = fst $ dim m 
                          nCol = snd $ dim m 
                          b1 = nRow == nCol && len v == nRow 
                          m1 = gm nRow (nCol - 1) 
                          mm = zipWith (++) vc m1 

            fw "mu mat v"
            printMat $ getColumn (mu mat w) 1
            fw "multiVec mat w"
            printMat $ multiVec mat w 
            print "ok" 
            -- Print the immutable array
            -- putStrLn $ "Immutable array: " ++ show frozenArr -- Immutable array: array (1,5) [(1,10),(2,25),(3,30),(4,40),(5,50)]

        when True $ do
            pp "ok"
            let v1 = ["a", "b"]
            let v2 = ["x", "y"]
            let dotS u v = zipWith (\a b -> a ++ b) u v
            pp "kk"

        when True $ do
            pp "Fraction a"
            pp "ok"
            let fra = X :+: Constf 3
            ff "fra" $ eval fra 1
            let f1 = X :+: X :*: Constf 2
            ff "f1" $ eval f1 1
            pp "kk"

-- data MyRec = MyRec { name_ :: String, counter_ :: Int} deriving(Show , Eq)
data MyRec = MyRec { 
                name_ :: String, 
                counter_ :: Int,
                age_ :: Int
                } deriving(Show , Eq)

-- name_ :: MyRec -> String
-- counter_ :: MyRec -> Int

modifyMyRec0 :: (Int -> Int) -> MyRec -> MyRec
modifyMyRec0 f r@(MyRec _ c _) = r{counter_ = f c}

modifyMyRec :: (Int -> Int) -> MyRec -> MyRec
modifyMyRec f r = (\c -> r {counter_ = c}) $ f (counter_ r)

counterLens :: forall f. Functor f => (Int -> f Int) -> (MyRec -> f MyRec)
counterLens f p = (\c -> p {counter_ = c}) <$> f (counter_ p)

nameLens :: forall f. Functor f => (String -> f String) -> (MyRec -> f MyRec)
nameLens f p = (\s -> p {name_ = s}) <$> f (name_ p)

-- readFileList >>= return . unlines
-- (>>=) :: m a -> (a -> m b) -> m b
type Lensx' a b = forall f. Functor f => (b -> f b) -> (a -> f a)

overx :: Lensx' a b -> (b -> b) -> a -> a
overx l f p = runIdentity $ l (Identity . f) p

{--
    let g = Identity . f = a -> Identity a

        (b -> f b)
    g :: a -> Identity a
     
        (a -> f a)
         v
        MyRec

    runIdentity $ (b -> f b) -> (a -> f a) (b -> Identity b) 
                  |          l           |
                  l (b -> Identity b) = (a -> f a)
                  (a -> f a) p
                  f p
--}

{--
v0 :: (Int, Char -> (String, Bool))
v0 = (3, c -> ([c, 'q', c, 'r'], isDigit c))
--}

{--
v0 :: (Int, Char -> (String,Bool))
v0 = (3,  c -> ([c,'q',c,'r'], isDigit c))
--}

funx :: [Int] -> Int -> [Int] -> [[Int]]
funx [] n acc = [acc] 
funx (c:cx) n acc = if c + sum acc <= n then funx cx  n (c:acc) else acc:funx (c:cx) n []
  
modList :: Int -> Int -> [Int]
modList n max = undefined 





{-|
     @
     [] => [[]]
     [1] => (x:[]) [[1]] ++ [[]] => [[], [1]]
     [1,2] => (1:[2]) => map (1:) $ [[],[2]) ++ [[], [2]]
                  => [[1],[1,2]] ++ [[], [2]]
                  => [[], [1], [2], [1,2]]
     @
-}
{--
powerSet :: [a] -> [[a]]
powerSet [] = [[]]
powerSet (x:cx) = map (x:) (powerSet cx) ++ powerSet cx
--}
  
{--  
main = do
       pp "ok"
       let rec = MyRec { name_ = "Rebecca", counter_ = 10, age_ = 20}
       print rec
       let f = const 3
       let r2 = modifyMyRec (+1) rec
       print r2
       let r3 = modifyMyRec (const 3) rec
       print r3
       fw "r4"
       pre rec
       let r4 = overx counterLens (+1) rec
       pre r4
       let r5 = overx nameLens (const "Sarina") rec
       fw "r5"
       pre rec
       pre r5
       let r6 = overx nameLens (\x -> x ++ "Mark") rec
       fw "r6"
       pre r6
       pre $ modifyMyRec (+10) rec
       pre $ modifyMyRec (const 100) rec
       when True $ do
         pp "ok"
         let m = M.fromList [(1, "a"), (2, "b")]
         pre m
         fw "m1"
         let m1 = M.delete 1 m
         pre m1
       when True $ do
         let ls = [1, 4, 3, 2, 1, 4, 1, 3]
         let ss = funx ls 5 []
         pre ss
       when False $ do
         ss <- rfl "/tmp/a.x"
         pre $ hasStrBlock "KOOD" ss
       when True $ do
         let s = [] :: [Integer]
         fw "p1"
         let ps = powerSet s 
         pre ps
       when True $ do
         fw "p2"
         let ps = powerSet [1]
         pre ps
       when True $ do
         fw "p3"
         let ps = powerSet [1, 2]
         pre ps
       when True $ do
         fw "swap" 
         pre $ swap (1, 2) [1, 2, 3, 4]
         pre $ swap (1, 1) [1, 2, 3, 4]
  
--}


fileNameAndTitle :: FilePath -> IO [(String, String)]
fileNameAndTitle fp = do
                   ls <- lsFileFull fp
                   mapM (\x -> do
                              fstLine <- readFileList x >>= return . head
                              let title = (trim . last) $ splitStr "TITLE:" fstLine
                              let fullName = x
                              print $ "title " ++ fullName
                              pre $ fullName ++ " " ++ title
                              return (fullName, title)
                              
                       ) ls


{--  
main = do
       pp "ok"
       let fn = "/tmp/aa.x"
       tu@(v, rx) <- fileExistA fn >>= (\b -> b ? readFileStrict fn  $ error "ERROR: No file") >>= \x -> return (read x :: (Vertex3 GLfloat, GLfloat))
       mapM_ (\x -> do
                 let t = (v, rx * x)
                 writeFile fn (show t)
                 sleepSec 1
                 print $ show t
                 ) [1..30]

--}


{--
main =
  withManager $ \mgr -> do
    -- start a watching job (in the background)
    watchDir
      mgr          -- manager
      "."          -- directory to watch
      (const True) -- predicate
      print        -- action

    -- sleep forever (until interrupted)
    forever $ threadDelay 1000000
--}


{--
    1  2  3

    1 (2 3)
         2 (3)
             3 ()
--}

mytails :: [a] -> [[a]]
mytails [] = [[]]
mytails (x:cx) = (x:cx):(mytails cx)
  
comx :: Int -> [a] -> [[a]]
comx 0 _  = [ [] ]
comx n xs = [ y:ys | y:xs' <- mytails xs
                      , ys <- comx (n-1) xs']
  
mainX9 = do
       ls <- rfl "/tmp/symboliclink.txt"
       let lv = filter (\x -> (len . trim) x > 0) ls
       let lt = map splitSPC lv
       prex lt
       mapM_ (\xx -> do
                      let t = head xx
                      let s = last xx
                      b <- fileExist s
                      case b of
                           True -> createSymbolicLink s $ "/Users/cat/myfile/symbin/" ++ t
                           _    -> print $ "file does not exist => " ++ s
             ) lt
       -- createSymbolicLink "/Users/cat/myfile/try/bb.x" "/tmp/linkx"
       print "OK"

main = do
      -- dbFileName = "myfile/bitbucket/database/haskellwebapp2_sqlite3.db"
      let dbTest = "myfile/bitbucket/database/haskellwebapp2_sqlite3_test.db"
      argList <- getArgs
      home <- getEnv "HOME"
      conn <- open $ home </> dbTest 
      let str = "abc:*:mytest11\nadd my test"
      insertStrToCodeBlock conn dbTest str
