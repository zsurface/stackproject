
:{

let prependColumn :: [a] -> [[a]] -> [[a]] 
    prependColumn c cx =  tran $ [c] ++ (tran cx) 
      where
        t = tran cx
:}
:{
let appendColumn :: [a] -> [[a]] -> [[a]] 
    appendColumn c cx =  tran $ (tran cx) ++ [c]
      where
        t = tran cx
:}


p = printMat
m1 = prependColumn [10, 20, 30] mat
fw "m1"
p m1

p mat

m2 = appendColumn [10, 20, 30] mat
fw "append"
p m2
