{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE CPP #-}

-- {-# LANGUAGE CPP #-}

module Main where

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import qualified Graphics.UI.GLFW as G
import Graphics.Rendering.OpenGL.GLU.Matrix as GM
import Graphics.Rendering.OpenGL 
import Graphics.Rendering.OpenGL.GL.CoordTrans
-- import qualified Graphics.Rendering.FTGL as FTGL
  
import qualified Graphics.UI.GLUT as GLUT
{--
   Only GLUT is been used: GLUT.renderString, GLUT.Roman
   preservingMatrix $ GLUT.renderString GLUT.Roman str
--}
  
import Graphics.GL.Types

import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.IORef 
import Data.Int
import Data.Maybe
import qualified Data.Set as S
import qualified Data.List as L
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import Data.Typeable
import qualified Text.Printf as PR
import Language.Haskell.Interpreter

import GHC.Float.RealFracMethods

import System.Directory
import System.Environment
import System.Exit
import System.IO
import Control.Concurrent
import Control.Arrow
import Data.Maybe

import Data.StateVar
import Foreign.ForeignPtr
import Foreign.Marshal.Alloc
import Foreign.Marshal.Array
import Foreign.Marshal.Utils
import Foreign.Ptr
import Foreign.Storable
import System.FilePath.Posix

-- Hidden modules
-- import Graphics.Rendering.OpenGL.GL.Capability
-- import Graphics.Rendering.OpenGL.GL.Exception
-- import Graphics.Rendering.OpenGL.GL.MatrixComponent
-- import Graphics.Rendering.OpenGL.GL.PeekPoke
-- import Graphics.Rendering.OpenGL.GL.QueryUtils
-- import Graphics.Rendering.OpenGL.GL.Texturing.TextureUnit
-- import Graphics.Rendering.OpenGL.GLU.ErrorsInternal
import Graphics.GL



-- BEG
-- KEY: ansi color, console color
-- import Rainbow
-- import System.Console.Pretty (Color (..), Style (..), bgColor, color, style, supportsPretty)
-- https://hackage.haskell.org/package/ansi-terminal-0.10.3/docs/System-Console-ANSI.html
-- import System.IO (hFlush, stdout)
-- import qualified System.Console.ANSI as AN
-- END

-- import Graphics.UI.GLUT
-- import Graphics.UI.GLUT.Callbacks.Global
import AronModule hiding (rw)
import AronGraphic hiding (dist)
import AronOpenGL
-- import AronDevLib

import qualified Data.Vector as VU

{-|

   | -------------------------------------------------------------------------------- 
   | compile: run.sh  
   | ghc -i/Users/cat/myfile/bitbucket/haskelllib -o file file.hs
   | 
   | KEY: keyboard example, keypress example, modifyIORef example,
   |
   | Tuesday, 09 November 2021 11:56 PST
   |
   | TODO: Combine Cam{..} and Step{..} in one function with Keyboard input
   |     Current issue: try to implement orthogonal projective with key one press
   |                    but Step{..} and Cam{..} are different type class.
   |
   | mainLoop w refCam refStep refCount lssVex
   | keyboardRot refCam refStep (fromIntegral width) (fromIntegral height)

   @
   data Cam = Cam{alpha::Double, beta::Double, gramma::Double, dist::Double} deriving(Show)

   data Step = Step{xx::Double, yy::Double, zz::Double, ww::Double} deriving(Show)
   initCam = Cam{alpha=0.0, beta=0.0, gramma=0.0, dist = 0.0}
   initStep = Step{xx=0.0, yy=0.0, zz=0.0, ww = 0.01}
   @
-}


  
tmpfile = "/tmp/tmpfile.txt"

mc::(GLfloat, GLfloat) -> (GLfloat, GLfloat) -> (GLfloat, GLfloat)
mc (a, b) (a', b') = (a*a' - b*b', a*b' + a'*b)
  
-- xx2
{-|
convexPts::IO [Vertex3 GLfloat]
convexPts = return cx
    where
      cx = [
              Vertex3 0.1   0.1  0
             ,Vertex3 0.2   0.6  0
             ,Vertex3 0.88  0.9  0
             ,Vertex3 0.25  0.34 0
             ,Vertex3 0.12  0.8  0
             ,Vertex3 1.3   0.12 0
             ,Vertex3 0.32   0.44 0
             ,Vertex3 0.13   0.56 0
            ]
-}

convexPts::IO [Vertex3 GLfloat]
convexPts = return cx
    where
      cx = [
              Vertex3 (-0.3)   0.1  0
             ,Vertex3 (-0.2)   0.6  0
             ,Vertex3 (0.88)  0.9  0
             ,Vertex3 (0.25)  0.34 0
             ,Vertex3 0.32   0.44 0
            ]
  
drawCylinderX::[[Vertex3 GLfloat]] -> IO()
drawCylinderX cx = drawSurfaceFromList cx

  
helpme::IO()
helpme = do
  let (+) = (++)
  b <- en "b"
  -- AronModule.clear
  let ls = ["file => " + b + "/tmp/draw.x",
            "PlotGeometry -h => help     ",
            "                            ",
            "point                       ",
            "0.1 0.1 0.1                 ",
            "endpoint                    ",
            "                            ",
            "Support primitives:         ",
            "point, segment and triangle "
           ]
  printBox 4 ls


{-|   
renderText :: GLUT.Font -> String -> IO ()
renderText font str = do
    GL.scale (1/64 :: GL.GLdouble) (1/64) 1
    GLUT.renderString GLUT.Roman str
-}    
mymain :: IO ()
mymain = do
    successfulInit <- G.init
    G.windowHint (G.WindowHint'DoubleBuffer True)
    -- if init failed, we exit the program
    bool successfulInit exitFailure $ do
        let winSizeX = 1000 :: Int
        let winSizeY = 1000 :: Int
        mw <- G.createWindow winSizeX winSizeY "PlotGeometry" Nothing Nothing
        maybe' mw (G.terminate >> exitFailure) $ \window -> do
            G.makeContextCurrent mw
            ref <- newIORef initCam 
            refStep <- newIORef initStep
            refGlobal <- newIORef $ setWindowSize (winSizeX, winSizeY) initGlobal
            globalRef <- readIORef refGlobal
            writeIORef refGlobal globalRef
            -- pre cylinderPt
            -- cylinder
            -- writeIORef refGlobal $ setDrawPts globalRef cylinderPt
            -- sphere
            -- randomPts <- randomVertex (10*3)
            randomPts <- convexPts
            writeIORef refGlobal $ setDrawPts   globalRef spherePtsX
            globalRef2 <- readIORef refGlobal
            writeIORef refGlobal $ setRandomPts globalRef2 randomPts

            refFrame <- (timeNowMilli >>= \x -> newIORef FrameCount{frameTime = x, frameCount = 1, frameIndex = 0})
    
            let cx = circleNArc' (Vertex3 0.4 0 0) 0.4 40 (0, pi)
            let cy = curvePtK (\x -> 0) (0, 0.8) 40
            let cx' = [cx, cy]
            mainLoop window ref refStep refGlobal refFrame cx' draw
            G.destroyWindow window
            G.terminate
            exitSuccess
    
{-|
    KEY: convert 'String' to 'Vertex3' 'GLfloat'

    @
     Input:
     segment
     0.3 0.4 0.0
     0.2 0.3 0.0
     0.1 0.2 0.0
     endsegment

     => [Vertex3 0.3 0.4 0.0, Vertex3 0.2 0.3 0.0, Vertex3 0.1 0.2 0.0]
    @
-}
takeSegment::[String] -> [Vertex3 GLfloat]
takeSegment [] = []
takeSegment cx = cs
  where
    beg = "segment"
    end = "endsegment"
    ss = filter(\x -> (len . trim) x > 0) $ takeBetweenExc beg end cx
    cs = map(\x -> strToVertex3 x ) ss

{-|
    KEY: convert 'String' to 'Vertex3' 'GLfloat'

    @
     Input:
     point
     0.3 0.4 0.0
     0.2 0.3 0.0
     endpoint

     => [Vertex3 0.3 0.4 0.0, Vertex3 0.2 0.3 0.0]
    @
-}
takePoint::[String] -> [Vertex3 GLfloat]
takePoint [] = []
takePoint cx = cs
  where
    beg = "point"
    end = "endpoint"
    ss = filter(\x -> (len . trim) x > 0) $ takeBetweenExc beg end cx
    cs = map(\x -> strToVertex3 x ) ss
                                                      
circleNArc'::Vertex3 GLfloat -> Double -> Integer -> (GLfloat, GLfloat) -> [Vertex3 GLfloat]
circleNArc' (Vertex3 x₀ y₀ z₀) r n (r₀, r₁) = [let δ = (r₁ - r₀)/(rf n); r' = rf r
                                             in Vertex3 (r' * cos (r₀  + (rf x)* δ) + x₀)  (r' * sin (r₀ + (rf x)* δ) + y₀)  z₀ | x <- [0..n]]
                                               
{-|
   [0, 1, 2]
   x0 -- x1 -- x2

   curvePtK::(GLfloat -> GLfloat)->(GLfloat,    GLfloat) -> Integer ->[Vertex3 GLfloat]
                                     ↑             ↑          ↑ 
                                     +-  interval  +          |
                                                              + - n steps 
                                     |             |
                                     x_0           x_1
                                        

                                      (x_1 - x_0) / n
                    f(x) = x^2 


    curvePtK f (0, 1.0) 10
     f(x) = x^2
    |-----+------|
    |   x |  x^2 |
    | 0.1 | 0.01 |
    | 0.2 | 0.04 |
    | 0.3 | 0.09 |
    | 0.4 | 0.16 |
    | 0.5 | 0.25 |
    | 0.6 | 0.36 |
    | 0.7 | 0.49 |
    | 0.8 | 0.64 |
    | 0.9 | 0.81 |
    | 1.0 |  1.0 |
    |-----+------|  
-}
curvePtK::(GLfloat -> GLfloat)->(GLfloat, GLfloat) -> Integer ->[Vertex3 GLfloat]
curvePtK f (x₀, x₁) n = [Vertex3 x (f x) 0 | x <- let δ = (x₁ - x₀)/(rf n) in map(\x -> x₀ + (rf x) * δ) [0..n]]
          


{-|
   === KEY: use multMatrix, multiply matrix, multiply matrix with vector

   https://hackage.haskell.org/package/OpenGL-3.0.3.0/docs/Graphics-Rendering-OpenGL-GL-CoordTrans.html

  @
      (cos α  + i sin α) ( cos β + i sin β)
    = cos α  * cos β + sin α * cos β + i cos α * cos β + i cos α * sin β
    = (cos α  * cos β + sin α * cos β) + i (cos α * cos β + cos α * sin β) 
    = cos (α + β) + i sin (α + β)

     m 1    => 0 
       0       1

     m 0    => -1
       1       0

    1  0      0 -1 
    0  1      1  0

                     →  X -  \         
                              ↓       
                     |       Y                 
                     \      /              
                       Z  ←                      

                Right-hand Rule

                      y
                      ^
                      |                       
                      + - ->   x
                     /
                    /
                   z

                 x (x) y => Z     
             rotate around Z-axis
             cos x   -sin x  0 
             sin x    cos x  0    
             0        0      1
                   ↓ 
             Swap row(2, 3) => M = -M
                   ↓ 

             rotate around X-axis
             cos x   sin x  0 
             0        0      1
             -sin x    cos x  0     

                    ↓  
             Swap row (1, 2) => M = -M
                    ↓         

             rotate around Y-axis
             0        0      1
             cos x   -sin x  0 
             sin x    cos x  0     
  @
-}
testmultMatrix::IO()
testmultMatrix = do
                  let x = 1.0::GLfloat
                  let y = 0.0::GLfloat
                  let z = 0.0::GLfloat
                  mat <- newMatrix RowMajor [ 1, 0, 0, x
                                             ,0, 1, 0, y
                                             ,0, 0, 1, z
                                             ,0, 0, 0, 1]::IO (GLmatrix GLfloat)

                  -- https://hackage.haskell.org/package/OpenGL-3.0.3.0/docs/Graphics-Rendering-OpenGL-GL-CoordTrans.html
                  -- multMatrix :: (Matrix m, MatrixComponent c) => m c -> IO ()
                  GL.multMatrix mat

                  -- https://hackage.haskell.org/package/OpenGL-3.0.3.0/docs/src/Graphics.Rendering.OpenGL.GL.CoordTrans.html#getMatrixComponents
                    -- getMatrixComponents :: MatrixComponent c => MatrixOrder -> m c -> IO [c]
                  ls <- getMatrixComponents RowMajor mat  -- [GLfloat]
                  -- pre ls
                  writeFileList "/tmp/m.x" $ map show ls

multModelviewVec::Vertex3 GLfloat -> IO [GLfloat]
multModelviewVec (Vertex3 x y z) = do
--                      let x = 1.0::GLfloat
--                      let y = 0.0::GLfloat
--                      let z = 0.0::GLfloat
                      mat <- newMatrix RowMajor [ 1, 0, 0, x
                                                 ,0, 1, 0, y
                                                 ,0, 0, 1, z
                                                 ,0, 0, 0, 1]::IO (GLmatrix GLfloat)

                      -- https://hackage.haskell.org/package/OpenGL-3.0.3.0/docs/Graphics-Rendering-OpenGL-GL-CoordTrans.html
                      -- multMatrix :: (Matrix m, MatrixComponent c) => m c -> IO ()
                      GL.multMatrix mat

                      -- https://hackage.haskell.org/package/OpenGL-3.0.3.0/docs/src/Graphics.Rendering.OpenGL.GL.CoordTrans.html#getMatrixComponents
                        -- getMatrixComponents :: MatrixComponent c => MatrixOrder -> m c -> IO [c]
                      ls <- getMatrixComponents RowMajor mat  -- [GLfloat]
                      -- pre ls
                      writeFileList "/tmp/m.x" $ map show ls
                      return ls 


getMatrixTest :: IO()
getMatrixTest = do
                 -- let pnameMatrix = getMatrix $ matrixModeToGetMatrix Projection 
                 -- matrixModeToGetMatrix :: MatrixMode -> GetPName
                 -- let m = matrixModeToGetMatrix Projection 

                 -- matrixModeToGetMatrix is from hidden module
                 -- Use src/Graphics
                 -- let m = matrixModeToGetMatrix $ Modelview 16 
                 -- getMatrixf :: p -> Ptr GLfloat -> IO ()
--                 let ls = [ 0, 0, 0, 0,
--                            0, 0, 0, 0,
--                            0, 0, 0, 0,
--                            0, 0, 0, 0
--                          ]::[GLfloat]
--
--                 getMatrixf m (Ptr ls)
                 fl
                 fl
                 -- let m2 = getMatrix m
                 return ()

getModelviewMatrix :: IO [GLfloat]
getModelviewMatrix = do
                      let stateVar = GL.matrix (Just $ Modelview 16) :: StateVar (GLmatrix GLfloat)
                      m1 <- Data.StateVar.get stateVar
                      -- ls <- getMatrixComponents ColumnMajor m1  -- [GLfloat]
                      ls <- getMatrixComponents RowMajor m1  -- [GLfloat]
                      let lt = partList 4 ls
                      fw "GL_MODELVIEW_MATRIX"
                      printMat lt
                      logFileG $ matrixToStr lt
                      -- writeFileList "/tmp/m1.x" $ map show ls 
                      return ls
  
getProjectionMatrix :: IO [GLfloat]
getProjectionMatrix = do
                      let stateVar = GL.matrix (Just $ Projection) :: StateVar (GLmatrix GLfloat)
                      m1 <- Data.StateVar.get stateVar
                      ls <- getMatrixComponents RowMajor m1  -- [GLfloat]
                      -- ls <- getMatrixComponents ColumnMajor m1  -- [GLfloat]
                      let lt = partList 4 ls
                      fw "GL_PROJECTION_MATRIX"
                      printMat lt
                      logFileG $ matrixToStr lt
                      return ls 
  
matrixTest::IO()
matrixTest = do
              let stateVar = GL.matrix (Just $ Modelview 16) :: StateVar (GLmatrix GLfloat)
              m1 <- Data.StateVar.get stateVar
              pre m1
              ls <- getMatrixComponents RowMajor m1  -- [GLfloat]
              pre ls
              writeFileList "/tmp/m1.x" $ map show ls 
              return ()


xor::Bool -> Bool -> Bool
xor True True = False
xor True False = True
xor False True = True
xor False False = False

{-|
   x  y  z
   0  1  0       
   1  0  0   XOR
  ---------
   1  1  0

   x  y  z
   0  1  0
   0  1  0   XOR
  ---------
   0  0  0

   x  y  z
   0  1  0
   0  0  1   XOR
  ---------
   0  1  1

-}
flipAxis::XYZAxis -> XYZAxis -> XYZAxis
flipAxis axisOld axisNew | x' = XYZAxis{xa = xor x x', ya = False,    za = False}
                         | y' = XYZAxis{xa = False,    ya = xor y y', za = False}
                         | z' = XYZAxis{xa = False,    ya = False,    za = xor z z'}
                         | otherwise = XYZAxis{xa = False, ya = False, za = False}
  where
    x = xa axisOld
    y = ya axisOld
    z = za axisOld
    x' = xa axisNew
    y' = ya axisNew
    z' = za axisNew
  
xAxis::XYZAxis
xAxis = XYZAxis{xa = True, ya = False, za = False}
  
yAxis::XYZAxis
yAxis = XYZAxis{xa = False, ya = True, za = False}
  
zAxis::XYZAxis
zAxis = XYZAxis{xa = False, ya = False, za = True}

initXYZAxis::XYZAxis
initXYZAxis = XYZAxis{xa = False, ya = False, za = False}


initGlobal::GlobalRef
initGlobal = GlobalRef{
             str_ = "" 
             ,cursor_ = (0.0, 0.0) 
             ,xyzAxis_ = initXYZAxis 
             ,mousePressed_ = (False, (0.0, 0.0))
             ,drawRectX_ = (Vertex3 (-0.2) (-0.2) (0.0::GLfloat), Vertex3 0.2 0.2 (0.0::GLfloat)) 
             ,tranDrawRectX_ = Vector3 0.0 0.0 (0.0::GLdouble)
             ,fovDegree_ = 90.0
             ,drawPts_ = [[Vertex3 0.0 0.0 0.0]]
             }

screenInfo :: G.Window -> IORef GlobalRef -> IO String
screenInfo window refGlobalRef = do
    ws <- G.getWindowSize window
    (fbw, fbh) <- G.getFramebufferSize window 
    pos <- getCursorPosf window
    fov <- getFOVDegree refGlobalRef
    let str = PR.printf "cx= %.2f cy= %.2f wx= %d wy= %d bx= %d by= %d fov= %.2f" (fst pos) (snd pos) (fst ws) (snd ws) fbw fbh fov :: String
    ref <- readIORef refGlobalRef
    writeIORef refGlobalRef $ setStr ref str
    return str
-- mouseCallback:: IORef GlobalRef -> G.MouseButtonCallback
  
-- https://hackage.haskell.org/package/GLFW-b-3.3.0.0/docs/Graphics-UI-GLFW.html#t:WindowSizeCallback
-- type WindowSizeCallback = Window -> Int -> Int -> IO ()
windowSizeCallBack :: IORef GlobalRef ->  G.WindowSizeCallback
windowSizeCallBack globalRef window width height = do
    str <- screenInfo window globalRef
    ps $ "str=" ++ str
    ps $ "height=" ++ show height
  where
    a = 3

{-|
    KEY: 
    NOTE: USED
-}
keyBoardCallBack2::IORef Step -> IORef GlobalRef -> G.KeyCallback
keyBoardCallBack2 refStep refGlobalRef window key scanCode keyState modKeys = do
    ps "keyBoardCallBack in $b/haskelllib/AronOpenGL.hs"
    ps $ "inside =>" ++ show keyState ++ " " ++ show key
    globalRef <- readIORef refGlobalRef
    (fbw, fbh) <- G.getFramebufferSize window 

    pos <- getCursorPosf window
    ws <- G.getWindowSize window
    fov <- getFOVDegree refGlobalRef
    screenInfo window refGlobalRef
  
    let axisOld = xyzAxis_ globalRef
    let fovOld = fovDegree_ globalRef
    logFileG ["fovOld=" ++ show fovOld]
    case keyState of
        G.KeyState'Pressed -> do 
            -- write Step{...} to ref
            case key of
              k | k == G.Key'Right -> writeIORef refStep Step{xx=_STEP,    yy =0.0,      zz = 0.0,    ww = 0.0}
                | k == G.Key'Left  -> writeIORef refStep Step{xx=(-_STEP), yy =0.0,      zz = 0.0,    ww = 0.0}
                | k == G.Key'Up    -> writeIORef refStep Step{xx=0.0,      yy =_STEP,    zz = 0.0,    ww = 0.0}
                | k == G.Key'Down  -> writeIORef refStep Step{xx=0.0,      yy =(-_STEP), zz = 0.0,    ww = 0.0}
                | k == G.Key'9     -> writeIORef refStep Step{xx=0.0,      yy =0.0,      zz = _STEP,  ww = 0.0}
                | k == G.Key'0     -> writeIORef refStep Step{xx=0.0,      yy =0.0,      zz = -_STEP, ww = 0.0}
                | k == G.Key'8     -> writeIORef refStep Step{xx=0.0,      yy =0.0,      zz = 0.0,    ww = _STEP}
                | k == G.Key'7     -> writeIORef refStep Step{xx=0.0,      yy =0.0,      zz = 0.0,    ww = -_STEP}
                
                | k == G.Key'X     -> writeIORef refGlobalRef $ setXYZAxis globalRef $ flipAxis axisOld xAxis
                --                                  ↑  
                --                                  + -> Update Coord to YZ-plane

                | k == G.Key'Y     -> writeIORef refGlobalRef $ setXYZAxis globalRef $ flipAxis axisOld yAxis
                --                                  ↑ 
                --                                  + -> Update Coord to XZ-plane

                | k == G.Key'Z     -> writeIORef refGlobalRef $ setXYZAxis globalRef $ flipAxis axisOld zAxis
                --                                  ↑ 
                --                                  + -> Update Coord to XY-plane

                -- zoom out
                | k == G.Key'O     -> do
                    writeIORef refGlobalRef $ setFOVDegree globalRef $ fovOld + 5.0
                --                                  ↑ 
                --                                  + -> Update Coord to XY-plane
                -- zoom in 
                | k == G.Key'I     -> writeIORef refGlobalRef $ setFOVDegree globalRef $ fovOld - 5.0
                --                                  ↑ 
                --                                  + -> Update Coord to XY-plane
  
                -- TODO: In orthogonal projective status, 
                | k == G.Key'Space -> writeIORef refStep initStep
                | otherwise -> ps "Unknown Key Press"
        G.KeyState'Released -> do
            if key == G.Key'Right then ps "Release Key => Right" else ps "Press No Right"
            if key == G.Key'Left  then ps "Release Key => left"  else ps "Press No Right"
            if key == G.Key'Up    then ps "Release Key => up"    else ps "Release No Up"
            if key == G.Key'Down  then ps "Release Key => Down"  else ps "Release No Down"
        _   -> ps "Unknow keyState"
    when (key == G.Key'Escape && keyState == G.KeyState'Pressed)
        (G.setWindowShouldClose window True)

  
vecdTovecf::Vector3 GLdouble -> Vector3 GLfloat
vecdTovecf (Vector3 x y z) = Vector3 x' y' z'
    where
        x' = rf x
        y' = rf y 
        z' = rf z 


{-|
   === KEY: points set for sphere
   The function from AronGraphic.hs spherePts
-}
spherePtsX::[[Vertex3 GLfloat]]
spherePtsX = geneParamSurface fx fy fz n
    where
        n = 20::Int
        δ = (2*pi)/(rf(n-1)) :: Float
        r = 0.1
        br = 0.2
        σ = 1/rf(n-1)

        fx::Int -> Int -> GLfloat
        fx i j = let i' = rf i
                     j' = rf j
                     α  = δ*i'
                     β  = δ*j'
                 in r*cos(α)*cos(β)
        fy::Int -> Int -> GLfloat
        fy i j = let i' = rf i
                     j' = rf j
                     α  = δ*i'
                     β  = δ*j'
                 in r*cos(α)*sin(β)
        
        fz::Int -> Int -> GLfloat
        fz i j = let i' = rf i
                     j' = rf j
                     α  = δ*i'
                     β  = δ*j'
                 in r*sin(α)



d2f::Vector3 GLdouble -> Vector3 GLfloat
d2f(Vector3 x y z) = Vector3 x' y' z'
    where
        x' = rf x
        y' = rf y 
        z' = rf z 

{-|
    === KEY: check whether point (x, y) is inside the rectangle

    @ 
                    | upLeftY
         upLeftX -> v +----------+ 
                                 |
                                 |
                      +          + 
                                 ^ <-  downRightX 
                                 |
                              downRightY                                    
                                                                             +- - -> translate Vector3
                                 + -> upLeft                                 |
                                 |                                           |
                                 |                     + -> downRight        |
                                 |                     |                     |                    +-> cursor pos
                                 ↓                     ↓                     ↓                    ↓       
    @

-}
isPtInsideRectTran::G.Window ->(Vertex3 GLfloat, Vertex3 GLfloat) -> Vector3 GLdouble -> IO Bool
isPtInsideRectTran w (p0, p1) (Vector3 a b c) = do
    cursorPos <- getCursorPosf w  -- IO (GLfloat, GLfloat)
    let tvec = Vector3 a (-b) c
    isPtInsideRect w (p0 +: (d2f tvec), p1 +: (d2f tvec)) cursorPos 

isPtInsideRect::G.Window -> (Vertex3 GLfloat, Vertex3 GLfloat) -> (GLfloat, GLfloat) -> IO Bool
isPtInsideRect w (Vertex3 x0 y0 z0, Vertex3 x1 y1 z1) (x, y) = do
        let ndcWidth = 4.0
        (winWidth, winHeight) <- G.getWindowSize w
        let (winW, winH) = (rf winWidth, rf winHeight)
        -- let (w, h) = (rf width, rf height)
        --
        --                 + -> Shift to the Right in half window 
        --                 ↓ 
        let upLeftX = winW / 2 + x0 * (winW / ndcWidth)
        let upLeftY = winH / 2 + y0 * (winH / ndcWidth)
        let downRightX = winW / 2 + x1 * (winW / ndcWidth)
        let downRightY = winH / 2 + y1 * (winH / ndcWidth)
        if upLeftX <= x && x <= downRightX && upLeftY <= y && y <= downRightY
            then return True
            else return False


setStr::GlobalRef -> String -> GlobalRef
setStr gf s = GlobalRef{ 
                str_ = s,
                windowSize_ = ws,
                cursor_ = cursor, 
                xyzAxis_ = xyzaxis, 
                mousePressed_ = mp, 
                drawRectX_ = vx,
                tranDrawRectX_ = td,
                fovDegree_ = fov,
                drawPts_ = drawPts,
                randomPts_ = randomPts
                }
  where
    xyzaxis = xyzAxis_ gf
    ws = windowSize_ gf
    cursor = cursor_ gf
    mp = mousePressed_ gf
    vx = drawRectX_ gf
    td = tranDrawRectX_ gf
    fov= fovDegree_ gf
    drawPts = drawPts_ gf
    randomPts = randomPts_ gf


setDrawPts::GlobalRef -> [[Vertex3 GLfloat]] -> GlobalRef
setDrawPts gf cx = GlobalRef{ 
                str_ = str,
                windowSize_ = ws,
                cursor_ = cursor, 
                xyzAxis_ = xyzaxis, 
                mousePressed_ = mp, 
                drawRectX_ = vx,
                tranDrawRectX_ = td,
                fovDegree_ = fov,
                drawPts_ = cx,
                randomPts_ = rts
                }
  where
    str = str_ gf
    ws = windowSize_ gf
    xyzaxis = xyzAxis_ gf
    cursor = cursor_ gf
    mp = mousePressed_ gf
    vx = drawRectX_ gf
    td = tranDrawRectX_ gf
    fov= fovDegree_ gf
    rts= randomPts_ gf

setRandomPts::GlobalRef -> [Vertex3 GLfloat] -> GlobalRef
setRandomPts gf rpt = GlobalRef{ 
                str_ = str,
                windowSize_ = ws,
                cursor_ = cursor, 
                xyzAxis_ = xyzaxis, 
                mousePressed_ = mp, 
                drawRectX_ = vx,
                tranDrawRectX_ = td,
                fovDegree_ = fov,
                drawPts_ = dp,
                randomPts_ = rpt
                }
  where
    str = str_ gf
    ws = windowSize_ gf
    xyzaxis = xyzAxis_ gf
    cursor = cursor_ gf
    mp = mousePressed_ gf
    vx = drawRectX_ gf
    td = tranDrawRectX_ gf
    fov= fovDegree_ gf
    dp = drawPts_ gf
  

{-|
    KEY: getter for fovDegree_ 
    NOTE: zoom in, zoom out
-}
getFOVDegree::IORef GlobalRef -> IO GLdouble 
getFOVDegree ioGlobalRef = readIORef ioGlobalRef >>= return . fovDegree_ 

{-|
    KEY: getter for setFOVDegree 

    NOTE: zoom in, zoom out
-}
setFOVDegree::GlobalRef -> GLdouble -> GlobalRef
setFOVDegree gf fov = GlobalRef{
                  str_ = s,
                  windowSize_ = ws,
                  cursor_ = g, 
                  xyzAxis_ = xyzaxis, 
                  mousePressed_ = mp, 
                  drawRectX_ = vx,
                  tranDrawRectX_ = td,
                  fovDegree_ = fov,
                  drawPts_ = drawPts,
                  randomPts_ = randomPts
                  }
  where
    s = str_ gf
    ws = windowSize_ gf
    g = cursor_ gf
    xyzaxis = xyzAxis_ gf
    mp = mousePressed_ gf
    vx = drawRectX_ gf
    td = tranDrawRectX_ gf
    drawPts = drawPts_ gf
    randomPts = randomPts_ gf

{-|
    KEY: getter for str_ 
-}
getStr::IORef GlobalRef -> IO String
getStr ioGlobalRef = readIORef ioGlobalRef >>= return . str_

    
{-|
    KEY: setter for xyzAxis_ 
-}
setXYZAxis::GlobalRef -> XYZAxis -> GlobalRef
setXYZAxis gf xyzAxis = GlobalRef{
                  str_ = s,
                  windowSize_ = ws,
                  cursor_ = g, 
                  xyzAxis_ = xyzAxis, 
                  mousePressed_ = mp, 
                  drawRectX_ = vx,
                  tranDrawRectX_ = td,
                  fovDegree_ = fov,
                  drawPts_ = drawPts,
                  randomPts_ = randomPts
                  }
  where
    s = str_ gf
    ws = windowSize_ gf
    g = cursor_ gf
    mp = mousePressed_ gf
    vx = drawRectX_ gf
    td = tranDrawRectX_ gf
    fov = fovDegree_ gf
    drawPts = drawPts_ gf
    randomPts = randomPts_ gf
  
{-|
    KEY: getter for xyzAxis_
-}
getXYZAxis::IORef GlobalRef -> IO XYZAxis
getXYZAxis ioGlobalRef = readIORef ioGlobalRef >>= return . xyzAxis_

{-|
    KEY: getter for drawPts_
-}
getDrawPts::IORef GlobalRef -> IO [[Vertex3 GLfloat]]
getDrawPts ioGlobalRef = readIORef ioGlobalRef >>= return . drawPts_
  
getRandomPts::IORef GlobalRef -> IO [Vertex3 GLfloat]
getRandomPts ioGlobalRef = readIORef ioGlobalRef >>= return . randomPts_
  
--setCursor::GlobalRef -> (GLfloat, GLfloat) -> GlobalRef
--setCursor gf pos = GlobalRef{str_ = s, cursor_ = pos, xyzAxis_ = xyz}
--    where
--      s = str_ gf
--      xyz = xyzAxis_ gf

{-|
    KEY: setter for cursor_ 
-}
setCursor::(GLfloat, GLfloat) -> GlobalRef -> GlobalRef
setCursor pos gf = GlobalRef{
                    str_ = s,
                    windowSize_ = ws,
                    cursor_ = pos, 
                    xyzAxis_ = xyz, 
                    mousePressed_ = mp, 
                    drawRectX_ = vx,
                    tranDrawRectX_ = td,
                    fovDegree_ = fov,
                    drawPts_ = drawPts,
                    randomPts_ = randomPts
                    }
    where
      s = str_ gf
      ws = windowSize_ gf
      xyz = xyzAxis_ gf
      mp = mousePressed_ gf
      vx = drawRectX_ gf
      td = tranDrawRectX_ gf
      fov = fovDegree_ gf
      drawPts = drawPts_ gf
      randomPts = randomPts_ gf
--setMousePressed::Bool -> GlobalRef -> GlobalRef
--setMousePressed b gf = GlobalRef{str_ = s, cursor_ = cursor, xyzAxis_ = xyz, mousePressed_ = b} 
--    where
--      s = str_ gf
--      xyz = xyzAxis_ gf
--      cursor = cursor_ gf

{-|
    KEY: setter for mousePressed_ 
-}
setMousePressed::(Bool, (GLfloat, GLfloat)) -> GlobalRef -> GlobalRef
setMousePressed (b, mpos) gf = GlobalRef{
                    str_ = s,
                    windowSize_ = ws,
                    cursor_ = cursor, 
                    xyzAxis_ = xyz, 
                    mousePressed_ = (b, mpos), 
                    drawRectX_ = vx,
                    tranDrawRectX_ = td,
                    fovDegree_ = fov,
                    drawPts_ = drawPts,
                    randomPts_ = randomPts
                    } 
    where
      s = str_ gf
      ws = windowSize_ gf
      xyz = xyzAxis_ gf
      cursor = cursor_ gf
      vx = drawRectX_ gf
      td = tranDrawRectX_ gf
      fov= fovDegree_ gf
      drawPts = drawPts_ gf
      randomPts = randomPts_ gf

setWindowSize :: (Int, Int) -> GlobalRef -> GlobalRef
setWindowSize ws gf = GlobalRef{
                    str_ = s,
                    windowSize_ = ws,
                    cursor_ = cursor, 
                    xyzAxis_ = xyz, 
                    mousePressed_ = mp,
                    drawRectX_ = vx,
                    tranDrawRectX_ = td,
                    fovDegree_ = fov,
                    drawPts_ = drawPts,
                    randomPts_ = randomPts
                    } 
    where
      s = str_ gf
      xyz = xyzAxis_ gf
      mp = mousePressed_ gf
      cursor = cursor_ gf
      vx = drawRectX_ gf
      td = tranDrawRectX_ gf
      fov= fovDegree_ gf
      drawPts = drawPts_ gf
      randomPts = randomPts_ gf  

{-|
    KEY: 
-}
getCursorPosf::G.Window -> IO (GLfloat, GLfloat)
getCursorPosf w = G.getCursorPos w >>= \(x, y) -> return $ (rf x, rf y) :: IO (GLfloat, GLfloat)

{-|
    KEY: 
-}
getMousePressed::IORef GlobalRef -> IO (Bool, (GLfloat, GLfloat)) 
getMousePressed ioGlobalRef = readIORef ioGlobalRef >>= return . mousePressed_


getTranVecDrawRectX::IORef GlobalRef -> IO (Vector3 GLdouble)
getTranVecDrawRectX ioGlobalRef = readIORef ioGlobalRef >>= return . tranDrawRectX_

getWindowSize :: IORef GlobalRef -> IO (Int, Int)
getWindowSize ioGlobalRef = readIORef ioGlobalRef >>= return . windowSize_

screenInfoStr :: IORef GlobalRef -> IO String
screenInfoStr ioGlobalRef = undefined
    

{-|
    KEY:

    t0 pressed
       (x0, y0) 

    t1 released
       (x1, y1)

    USE NOW
-}
mouseCallback:: IORef GlobalRef -> G.MouseButtonCallback
mouseCallback globalRef window but butState mk = do
  case butState of
    G.MouseButtonState'Pressed -> do
      case but of
        v | v == G.MouseButton'1 -> do
              (fbw, fbh) <- G.getFramebufferSize window 
              pos <- G.getCursorPos window >>= \(x, y) -> return $ (rf x, rf y) :: IO (GLfloat, GLfloat)
              ws <- G.getWindowSize window
              -- let gr = setWindowSize ws globalRef
              refG <- readIORef globalRef >>= \gf -> (return $ setWindowSize ws gf) >>= \g -> return $ setCursor pos g
              writeIORef globalRef refG
              fov <- getFOVDegree globalRef
              
              str <- screenInfo window globalRef

              gRef <- readIORef globalRef
              -- ↑ 
              -- +---------------------------+
              --                             ↓ 
              -- writeIORef globalRef $ setStr gRef str
              --                       ↑
              --                       + -> getStr
              -- newGlobalRef <- readIORef globalRef >>= return . setCursor pos 
              readIORef globalRef >>= return . setCursor pos >>= \x -> writeIORef globalRef $ setMousePressed (True, pos) x 
              --  ↑ 
              --  +--------------------------------------------------+
              --                                                     ↓  
              -- writeIORef globalRef $ setMousePressed (True, pos) newGlobalRef
              fovStr <- getFOVDegree globalRef >>= return . show
              putStrLn $ "getFOVDegree= " ++ fovStr
              putStrLn str
              putStrLn "G.MouseButton'1 is pressed."
              cursorVex <- screenCSToGraphicCS window globalRef (0, 0)
              putStrLn $ "cursorVex= " ++ show cursorVex
  
          | otherwise            -> putStrLn "No button is pressed"
    G.MouseButtonState'Released -> do
      -- pos <- G.getCursorPos window >>= \(x, y) -> return $ (rf x, rf y) :: IO (GLfloat, GLfloat)
      pos <- G.getCursorPos window >>= \(x, y) -> return $ (rf x, rf y) :: IO (GLfloat, GLfloat)
      -- readIORef globalRef >>= \x -> writeIORef globalRef $ setMousePressed (False, pos) x
      putStrLn "G.MouseButton'1 is released"
  
{-| 
    === KEY: screen coordinates-system to graphic coordinates-system

    @
        (x, y) <- getCursorPosf w = G.getCursorPos w >>= \(x, y) -> return $ (rf x, rf y)
        screen Coordinates-System

        [winW = 1000, winH = 1000]
        topLeft
         ↓ (0,0) 
         + - - - ->  winW
         |
         |
         |
         v winH

        Scale XY-axes to [0, 1.0]
        (winW, winH) <- G.getWindowSize w >>= \(u, v) -> return (rf u, rf v)

        (x/winW, y/winH)

         topLeft
         ↓ (0,0)
         + - - - ->  1.0 
         |
         |
         |
         v 1.0 


         Move (0,0) => (0, 0.5) 
         (x/winW, y/winH - 0.5)
         topLeft
         ↓
         + -     ->  1.0 
         |                 
 (0,0)   + - - - -> 
         |
         v 1.0 

         Flip Y-Axis
         (x/winW, (0.5 - y/winH))

         ^
         | 
         |
         + - - - ->
         |
         |

         ↑ 
         bottomLeft
         
         Move (0,0) to (0, 0.5), 0.5 on X-axis
         (0.5, 0.5) => (0.0, 0.0)

         (x/winW - 0.5,  0.5 - y/winH)

                 ^
                 |
                 | 
                 | (0,0)
          -------+------->
                 |
                 |
                 |
                 

         Test 1, x = winW/2, y=winH/2 => (0, 0)       ✓ 
         Test 2, x = winW,   y=winH   => (0.5, -0.5)  ✓ 
         Test 3, x = (1/4)winW, y= (1/4) winH => (0.25 - 0.5, 0.5 - 0.25) = (-0.25, 0.25)  ✓
    @

    SEE: http://localhost/pdf/screenSpaceToNDC.pdf
-} 
screenCSToGraphicCS::G.Window -> IORef GlobalRef -> (GLfloat, GLfloat) -> IO (Vertex3 GLfloat)
screenCSToGraphicCS win globalRef (curX, curY) = do
    (winW, winH) <- G.getWindowSize win >>= \(u, v) -> return (rf u, rf v)
    -- (curX, curY) <- getCursorPosf win
    fovDegree <- getFOVDegree globalRef
    let rad = degreeToRadian $ rf fovDegree/2.0
    let hwinW = winW/2
    let hwinH = winH/2
    -- let gX = (tan rad) * ((curX/hwinW) - 1)
    let gX = ((curX/hwinW) - 1) * (tan rad)
    -- let gY = negate $ (tan rad) * ((curY/hwinH) - 1)
    let gY = negate $ ((curY/hwinH) - 1) * (tan rad)
    let gX' = (curX/hwinW) - 1
    let gY' = (curY/hwinH) - 1
    ps $ "gX'=" ++ show gX'
    ps $ "gY'=" ++ show gY'
    ps $ "fovDegree=" ++ show fovDegree
    ps $ "rad=" ++ show rad
    ps $ "gX=" ++ show gX
    ps $ "gY=" ++ show gY
    return $ Vertex3 gX gY 0.0

normDist::(GLfloat, GLfloat) -> (GLfloat, GLfloat) -> GLfloat
normDist (x0, y0) (x1, y1) = (x1 - x0)^2 + (y1 - y0)^2
 
-- sort vertex

seg::Vertex3 GLfloat -> Vertex3 GLfloat -> [Vertex3 GLfloat]
seg v0 v1 = cmpVex v0 v1 ? [v0, v1] $ [v1, v0]

findAllChildren::Vertex3 GLfloat -> [(Vertex3 GLfloat, Vertex3 GLfloat)] -> [Vertex3 GLfloat]
findAllChildren v cx = unique $ map fromJust $ filter (\x -> x /= Nothing ) $ map (\(v0, v1) -> v == v0 ? Just v1 $ (v == v1 ? Just v0 $ Nothing)) cx

  
mainLoop :: G.Window ->
            IORef Cam ->
            IORef Step ->
            IORef GlobalRef ->
            IORef FrameCount ->
            [[Vertex3 GLfloat]]-> IO() -> IO ()
mainLoop w refCam refStep refGlobal refCount lssVex draw = unless' (G.windowShouldClose w) $ do
    (width, height) <- G.getFramebufferSize w
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    GL.clear [ColorBuffer, DepthBuffer]

    G.setKeyCallback w (Just $ keyBoardCallBack2 refStep refGlobal)    -- AronOpenGL
    G.setMouseButtonCallback w (Just $ mouseCallback refGlobal)  -- mouse event
  
    -- https://hackage.haskell.org/package/GLFW-b-3.3.0.0/docs/Graphics-UI-GLFW.html#t:WindowSizeCallback
    G.setWindowSizeCallback w (Just $ windowSizeCallBack refGlobal)
    -- lightingInfo
    loadIdentity  -- glLoadIdentity

    step <- readIORef refStep
    xyzAxis <- getXYZAxis refGlobal
    fovNew <- getFOVDegree refGlobal
    case xyzAxis of
      --                                 +---> YZ-plane
      --                                 ↓
      var | xa var -> GL.lookAt (Vertex3 1.0 0 0::Vertex3 GLdouble) (Vertex3 0 0 0::Vertex3 GLdouble) (Vector3 0 1 0::Vector3 GLdouble)
  
          --                               +---> XZ-plane
          --                               ↓
          | ya var -> GL.lookAt (Vertex3 0 1.0 0::Vertex3 GLdouble) (Vertex3 0 0 0::Vertex3 GLdouble) (Vector3 1 0 0::Vector3 GLdouble)

          --                                 +---> XY-plane
          --                                 ↓
          | za var -> do 
            -- Graphics.Rendering.OpenGL.GLU.Matrix perspective :: GLdouble -> GLdouble -> GLdouble -> GLdouble -> IO ()

            -- glFrustum describes a perspective matrix that produces a perspective projection. (left,bottom,-near) and (right,top,-near) specify the points on the near clipping plane that
            -- are mapped to the lower left and upper right corners of the window, respectively, assuming that the eye is located at (0, 0, 0). -far specifies the location of the far clipping
            -- plane. Both near and far must be positive. The corresponding matrix is

            let zn = 1.0
            let zf = 5.0
            -- perspective 90 1.0 zf (zf + 4.0) 
            -- perspective 126.934 1.0 zf (zf + 4.0) 
            -- zoom in, zoom out
            -- perspective (field of view) width/height zNear zFar
            fw "before perspective"
            lt <- getModelviewMatrix::IO[GLfloat]
            -- perspective fovNew 1.0 zn zf
            perspective 90 1.0 zn zf
            fw "after perspective"
            lr <- getModelviewMatrix::IO[GLfloat]
            let m1 = partList 4 lr
            _ <- getProjectionMatrix::IO[GLfloat]
            -- GL.lookAt (Vertex3 0 4 1.0::Vertex3 GLdouble) (Vertex3 0 4 0::Vertex3 GLdouble) (Vector3 0 1 0::Vector3 GLdouble)
            -- GL.lookAt (Vertex3 0 0 (1.0 + fovNew/90)::Vertex3 GLdouble) (Vertex3 0 0 0::Vertex3 GLdouble) (Vector3 0 1 0::Vector3 GLdouble)
            -- loadIdentity
            GL.lookAt (Vertex3 0 2 3::Vertex3 GLdouble) (Vertex3 0 0 0::Vertex3 GLdouble) (Vector3 0 1 0::Vector3 GLdouble)
            fw "after GL.lookAt"
            -- GL.lookAt => generate a view matrix = change basic matrix × translation from center to eye matrix
            lw <- getModelviewMatrix::IO[GLfloat]
            let m2 = partList 4 lw
            let m3 = multiMat m1 m2
            fw "m1 => perspective 90 1.0 zn zf, RowMajor"
            pmat m1
            fw "m2 GL.lookAt (Vertex3 0 2 3::Vertex3 GLdouble) (Vertex3 0 0 0::Vertex3 GLdouble) (Vector3 0 1 0::Vector3 GLdouble)"
            pmat m2
            fw "m1 x m2 = m3 = multiMat m1 m2"
            pmat m3
            
  
            _  <- getProjectionMatrix::IO[GLfloat]
            pp "ok"
            -- https://registry.khronos.org/OpenGL-Refpages/gl2.1/xhtml/gluLookAt.xml
            --                  eye                                  At                                 Up

          | otherwise -> do
              GM.lookAt (Vertex3 0.2 0.2 0.2::Vertex3 GLdouble) (Vertex3 0 0 0::Vertex3 GLdouble) (Vector3 0 1 0::Vector3 GLdouble)
              keyboardRot refCam refStep (fromIntegral width) (fromIntegral height)

    -- AronOpenGL.hs
    -- delta refStep to modify Cam{xx, yy, zz} in degree
    -- keyboardRot => rotate around {x-axis, y-axis, y-axis} in some degrees
    -- keyboardRot refCam refStep (fromIntegral width) (fromIntegral height)
    renderCoordinates
    -- show3dStr "1234" red 0.8

    curStr <- getStr refGlobal 
    show3dStr curStr red 0.8

    genePts <- getRandomPts refGlobal
  
    -- BEG_triangulation
    preservingMatrix $ do
        pressXY <- getMousePressed refGlobal
        print $ "pressXY=" ++ show pressXY
        (Vertex3 x y z) <- screenCSToGraphicCS w refGlobal $ snd pressXY
        let x' = rf x
        let y' = rf y
        pp $ "x=" ++ show x'
        pp $ "y=" ++ show y'

        fovDegree <- getFOVDegree refGlobal
        let rad = degreeToRadian $ rf fovDegree/2.0
        let scale = rf $ tan rad
        ps $ "tan rad=" ++ show scale
        -- translate (Vector3 (-x') (-y') 0 :: Vector3 GLdouble)
        fw "before translate"
        ls <- getModelviewMatrix::IO[GLfloat]
        threadDelay $ 10^2
        
        -- translate (Vector3 (-x') (-y') 0 :: Vector3 GLdouble)
        -- GL.scale (scale) (scale) (scale::GLdouble)
        
        fw "after translate"
        lt <- getModelviewMatrix::IO[GLfloat]
        threadDelay $ 10^6
        draw
    -- END_triangulation
    
    G.swapBuffers w
    G.pollEvents
    mainLoop w refCam refStep refGlobal refCount lssVex draw

removeIntersectSeg :: [Vertex3 GLfloat] -> Vertex3 GLfloat -> ([Vertex3 GLfloat], [Vertex3 GLfloat], [[Vertex3 GLfloat]])
removeIntersectSeg cx v = if | len cx /= 3 -> error "ERROR: removeIntersectSeg => List has to contain three (Vertex3 GLfloat)."
                             | interSeg (x, v) (y, z) /= Nothing -> (sortSeg [y, z], sortSeg [x, v], map sortSeg [[v, x, z],[v, x, y]])
                             | interSeg (y, v) (x, z) /= Nothing -> (sortSeg [x, z], sortSeg [y, v], map sortSeg [[v, y, z],[v, x, y]])
                             | interSeg (z, v) (x, y) /= Nothing -> (sortSeg [x, y], sortSeg [z, v], map sortSeg [[v, y, z],[v, x, z]])
                             | otherwise                         -> error $ "ERROR removeIntersectSeg => to remove segment" ++ (show [x, y, z]) ++ " v=" ++ show v
          where
            x = cx !! 0
            y = cx !! 1
            z = cx !! 2
            interSeg = intersectSegNoEndPt2
            sortSeg = qqsort cmpVex
 

type Triangles = [(Vertex3 GLfloat, Vertex3 GLfloat, Vertex3 GLfloat)]
type Segments = [(Vertex3 GLfloat, Vertex3 GLfloat)]
type Vertexes = [Vertex3 GLfloat]
  
{--
  removeIntersectSeg2 :: [Vertex3 GLfloat] -> Vertex3 GLfloat -> [[Vertex3 GLfloat]] -> Maybe ([Vertex3 GLfloat], [Vertex3 GLfloat], [[Vertex3 GLfloat]])
                              ↑                    ↑                      ↑                        ↑                    ↑                  ↑      
                            triangle       vertex              list of segment                 remove seg            add seg          two triangles
  meshAllTriangle :: [(Vertex3 GLfloat, Vertex3 GLfloat)] -> [Vertex3 GLfloat] -> [[Vertex3 GLfloat]]  
--}
triangulateX :: Segments -> Vertexes -> Vertexes -> Triangles -> Triangles -> (Segments, Triangles)
-- triangulateX seg wx [] [] tx = (seg, tx)
-- triangulateX seg wx [] (t:tri) tx = triangulateX seg wx wx tri tx
triangulateX seg wx [] (t:tri) tx = (seg, t:tri)
triangulateX seg wx (v:vx) (t:tri) tx = (ns, nt)
  where
   seg' = map tupTols2 seg
   ret = removeIntersectSeg2 t v seg'
   (ns, nt) = case ret of
                Just tu -> let rseg = lsToTup2 $ t1 tu
                               aseg = lsToTup2 $ t2 tu
                               tri2  = map lsToTup3 $ t3 tu
                               trix  = removeTriWithSeg rseg (t:tri)
                               nseg = aseg:(removeSeg rseg seg)
                           in triangulateX nseg wx (v:vx) (tri2 ++ trix) (tri2 ++ trix)
                Nothing ->    triangulateX seg  wx vx (t:tri) tx

triangulateXIO :: [(Vertex3 GLfloat, Vertex3 GLfloat)] -> [Vertex3 GLfloat] -> [Vertex3 GLfloat] -> [(Vertex3 GLfloat, Vertex3 GLfloat, Vertex3 GLfloat)] ->[(Vertex3 GLfloat, Vertex3 GLfloat, Vertex3 GLfloat)] -> IO ([(Vertex3 GLfloat, Vertex3 GLfloat)], [(Vertex3 GLfloat, Vertex3 GLfloat, Vertex3 GLfloat)])
triangulateXIO seg wx [] (t:tri) tx = return (seg, tx)
-- triangulateXIO seg wx [] (t:tri) tx = triangulateXIO seg wx wx tri tx
triangulateXIO seg wx [] [] tx = return (seg, tx)
triangulateXIO seg wx (v:vx) (t:tri) tx = case ret of
                                            Just tu -> let rseg = lsToTup2 $ t1 tu
                                                           aseg = lsToTup2 $ t2 tu
                                                           tri2 = map lsToTup3 $ t3 tu
                                                           trix = removeTriWithSeg rseg (tri2 ++ tri)
                                                           nseg = aseg:(removeSeg rseg seg)
                                                           
                                                       in do
                                                          pre nseg
                                                          mapM_ (\se -> drawSegmentList green $ map (\v -> shiftY v 0.5) se) $ map tupTols2 nseg
                                                          writeFileList "/tmp/aa.x" $ map show nseg
                                                          logFileG $ ["nseg"]
                                                          logFileG $ map show nseg
                                                          triangulateXIO nseg wx (v:vx) trix trix
                                            Nothing ->    triangulateXIO seg  wx vx (t:tri) tx

          where
           seg' = map tupTols2 seg
           ret = removeIntersectSeg2 t v seg'

  
   -- rseg = removeSeg (lsToTup2 $ t1 ret) seg
                                   
  {--
  where
    -- center of a triangle
    isTri = map(\x -> map (\v -> case removeIntersectSeg2 x v (map tupTols2 seg) of
                                       Just ret -> triangulateX (let seg' = removeSeg (lsToTup2 $ t1 ret) seg; as = (lsToTup2 $ t2 ret)
                                                                in (as:seg'))
                                                                vx $
                                                                (map lsToTup3 $ t3 ret) ++ tri
                                       Nothing -> (seg, tri)
                         ) vx
               ) tri

  --}
  
{-|

   NOTE: If pt v is on the boundary of circle, => Nothing

            x
           /|\
          / | \
         ---|---
            x

removeIntersectSeg2 :: [Vertex3 GLfloat] -> Vertex3 GLfloat -> [[Vertex3 GLfloat]] -> Maybe ([Vertex3 GLfloat], [Vertex3 GLfloat], [[Vertex3 GLfloat]])
                            ↑                    ↑                      ↑                    ↑                    ↑                  ↑      
                          triangle             vertex              list of segment       remove seg            add seg          two triangles
-}   
removeIntersectSeg2 :: (Vertex3 GLfloat, Vertex3 GLfloat, Vertex3 GLfloat) -> Vertex3 GLfloat -> [[Vertex3 GLfloat]] -> Maybe ([Vertex3 GLfloat], [Vertex3 GLfloat], [[Vertex3 GLfloat]])
removeIntersectSeg2 tri@(x, y, z) v ccx = if
                                           | x == v || y == v || z == v -> Nothing
                                           | not $ isInside -> Nothing
                                           | interSeg (x, v) (y, z) /= Nothing && lnx == 1 -> Just ([y, z], [x, v], [[v, x, z],[v, x, y]])
                                           | interSeg (y, v) (x, z) /= Nothing && lny == 1 -> Just ([x, z], [y, v], [[v, y, z],[v, x, y]])
                                           | interSeg (z, v) (x, y) /= Nothing && lnz == 1 -> Just ([x, y], [z, v], [[v, y, z],[v, x, z]])
                                           | otherwise   -> Nothing
          where
            lnx = len $ filter (/= Nothing) $ intersectSegNoEndPtList (x, v) $ map lsToTup2 ccx
            lny = len $ filter (/= Nothing) $ intersectSegNoEndPtList (y, v) $ map lsToTup2 ccx
            lnz = len $ filter (/= Nothing) $ intersectSegNoEndPtList (z, v) $ map lsToTup2 ccx
            interSeg = intersectSegNoEndPt2
            sortSeg = qqsort cmpVex
            isInside = fst $ isPtInsideInscribeCircle v tri
  
{-|
  KEY:
  @
    removeSeg [v1, v2] [[v1, v2], [v3, v4]] => [[v3, v4]]
  @
-}
removeSeg :: (Vertex3 GLfloat, Vertex3 GLfloat) -> [(Vertex3 GLfloat, Vertex3 GLfloat)] -> [(Vertex3 GLfloat, Vertex3 GLfloat)]
removeSeg (a, b) cx = filter (\(a', b') -> not ((a == a' && b == b') || (a == b' && b == a'))) cx

removeTri :: (Vertex3 GLfloat, Vertex3 GLfloat, Vertex3 GLfloat) -> Triangles -> Triangles
removeTri tr cx = filter (\t -> not $ sortv tr == sortv t)  cx
  where
    sortv ls = qqsort cmpVex $ tupTols3 ls

removeVex :: [Vertex3 GLfloat] -> [Vertex3 GLfloat] -> [Vertex3 GLfloat]
removeVex rv cv = filter (\x -> not $ S.member x set) cv
  where
    set = S.fromList rv
    
isSegInTriangle :: (Vertex3 GLfloat, Vertex3 GLfloat) -> (Vertex3 GLfloat, Vertex3 GLfloat, Vertex3 GLfloat) -> Bool
isSegInTriangle (a, b) (x, y, z) = b1 || b2 || b3
  where
    b1 = (a == x && b == y) || (a == y && b == x)
    b2 = (a == y && b == z) || (a == z && b == y)
    b3 = (a == x && b == z) || (a == z && b == x)

{-|
   === KEY: Check whether a segment is in a triangle
   @
   Check whether a segment (Vertex3 GLfloat, Vertex3 GLfloat) is in [(Vertex3 GLfloat, Vertex3 GLfloat, Vertex3 GLfloat)]
   @
-}
removeTriWithSeg :: (Vertex3 GLfloat, Vertex3 GLfloat) -> Triangles -> Triangles
removeTriWithSeg seg cx = filter (\tri -> not $ isSegInTriangle seg tri) cx

  
{-|

  Remove segment from a list of triangles
  1. Sort all the triangls in the list
-}
-- removeTriWithSeg :: (Vertex3 GLfloat, Vertex3 GLfloat) -> Triangles -> Triangles
-- removeTriWithSeg seg cx = map lsToTup3 $ filter (/= tupTols2 seg) $ sortVex $ map tupTols3 cx


  
-- /Users/aaa/myfile/bitbucket/stackproject/Triangulation/src/removeIntersectSeg2-2023-03-15-23-10-51.x

{-|

   @
   mapM_(\pair -> do 
          drawSegmentList red pair
          -- threadDelay 500
     ) ls
   @
-}
drawAllSeg :: Color3 GLdouble -> [[Vertex3 GLfloat]] -> IO()
drawAllSeg c cx = mapM_ (\vx -> drawSegmentList c vx) cx

uniqueSeg :: [(Vertex3 GLfloat, Vertex3 GLfloat)] -> [(Vertex3 GLfloat, Vertex3 GLfloat)]
uniqueSeg cx = allSeg
  where
    f = \(a, b) (a', b') -> (a == a' && b == b') || (a == b' && b == a')
    lsz = qqsort f cx
    allSeg = map head $ L.groupBy f lsz

uniqueSegX :: [(Vertex3 GLfloat, Vertex3 GLfloat)] -> [(Vertex3 GLfloat, Vertex3 GLfloat)]
uniqueSegX cx = map lsToTup2 ls
  where
    ls = unique $ sortVex $ map tupTols2 cx

noColinearPt :: [Vertex3 GLfloat] -> [Vertex3 GLfloat]
noColinearPt cx = lw
  where
    lv = unique cx
    ls = combin 3 lv
    lt = unique $ join $ filter (not . null) $ map (\[a, b, c] -> isColinear a b c ? [a, b, c] $ []) ls
    lw = (L.\\) lv lt

draw :: IO()
draw = do
      -- BEG_triangulation
    -- Wednesday, 15 March 2023 01:50 PDT
    -- NOTE: We only take one vertex and all its children to run it.
    -- 
    -- convexHullAllSeg::[Vertex3 GLfloat] -> [(Vertex3 GLfloat, Vertex3 GLfloat)]
    -- let randomPts = map (\(Vertex3 x y z) -> Vertex3 (x - 0.7231) y z) genePts
    {--
    let randomPts = [
                    Vertex3 (-0.3)   0.1  0
                   ,Vertex3 (-0.2)   0.6  0
                   ,Vertex3 (0.88)  0.9  0
                   ,Vertex3 (0.25)  0.34 0
                   ,Vertex3 0.32   0.44 0
                   ,Vertex3 0.72   0.94 0
                   ,Vertex3 (-0.34)   0.64 0
                  ]
    --}
    let randomPts = [
                    Vertex3 1 1 0
                   ,Vertex3 0 0 0
                   ,Vertex3 1 0 0
                   ,Vertex3 0.5 0.1 0
                   ,Vertex3 0.5 0.2 0
                   ,Vertex3 0 1 0
                   ,Vertex3 0.3 0.3 0
                   ,Vertex3 0.4 0.6 0
                   ,Vertex3 0.6 (-0.2) 0
                   ,Vertex3 1.2 1 0
                   ,Vertex3 1.4 1 0
                   ,Vertex3 1.6 1 0
                   ,Vertex3 1.7 1 0
                   ,Vertex3 1.8 1 0
                   ,Vertex3 0 0.2 0
                   ,Vertex3 0 0.4 0
                   ,Vertex3 0 0.6 0
                   ,Vertex3 1.8 0.3 0
                   ,Vertex3 1.8 0.5 0
                   ,Vertex3 1.8 0.7 0
                   ]
    
    let lsx = convexHull3 randomPts 
    let allSegHull = uniqueSeg lsx
    logFileG ["lsx", "allSegHull"]
    logFileG $ map show lsx
    logFileG $ map show allSegHull
    when False $ do
      drawAllSeg yellow $ map (\t -> shiftXList (tupTols2 t) (0)) allSegHull
      mapM_ (\v -> drawDot v) randomPts
  
    -- [(Vertex3 GLfloat, Vertex3 GLfloat)] => [[Vertex3 GLfloat]]
    let lt = sortVex $ map tupTols2 allSegHull

    -- Connect all pts inside convexhull to all the pts on the boundary of convexhull, no intersection
    -- URL: http://localhost/image/allsegment.svg
    let segList::[(Vertex3 GLfloat, Vertex3 GLfloat)] -> [Vertex3 GLfloat] -> [(Vertex3 GLfloat, Vertex3 GLfloat)]
        segList sl []     = sl
        segList sl (p:cx) = segList ((nonCrossSegmentNoEndPt sl p) ++ sl) cx
    

    -- http://xfido.com/publicfile/codedoc/h2.txt
    -- rsync: rsync_publicfile.sh 
    -- http://xfido.com/publicfile/codedoc/h1.txt
    -- vertex v to all children
    let toAllChildren = map (\v -> (v, unique $ qqsort cmpVex $ map fromJust $ filter (Nothing /=) $ map(\seg -> vexOnSeg v seg ? Just (otherEnd v seg) $ Nothing ) lt)) randomPts
              where vexOnSeg v [x, y] = v == x || v == y
                    otherEnd v [x, y] = v == x ? y $ x


    logFileNoName ["toAllChildren"]
    logFileNoName $ map show toAllChildren
   
    -- KEY: all children, all vertexes, last vertex
    -- One vertex to all chidlren
    -- BEG_last_pt
    -- Take the last vertex in toAllChildren, why it is the last vertex?

    -- NOTE: We only take one vertex and all its children to run it.
    -- let fb = toAllChildren !! (len randomPts - 1)
    let fb = toAllChildren !! 0
    let vertexPt = fst fb
    let allChildren = snd fb

    {--
         + - ->v1
         |
         x - ->v2
         |
         + - ->v3
    --}
    when False $ do
      let x = -0.3
      mapM_ (\c -> drawSegments cyan [shiftX (fst fb) x, c]) $ map (\v -> shiftX v x) allChildren

    -- find segment that use vertex $ fst fb
    let set = S.fromList allChildren

    -- Cartesian product of two sets allChildren
    -- let cartesian_pair = filter(\x -> x /= []) $ join $ map (\x -> map (\y -> x /= y ? [x, y] $ []) allChildren) allChildren
    let cartesian_pair = combin 2 allChildren
    logFile2 "/tmp/x" [show cartesian_pair]
    let cartesian_pairX = combin 2 allChildren 
    logFile2 "/tmp/y" [show cartesian_pairX]

    -- let tsegs = map (\x -> (head x, last x) ) cartesian_pair
    let qqseg = qqsort (\[a, b] [c, d] -> a == d && b == c) cartesian_pair
    let gsegs = L.groupBy (\[a, b] [c, d] -> a == d && b == c) qqseg
    let pairSeg = map head gsegs
    -- END_last_pt
    
    let sortPairSeg = sortVex pairSeg


    --  a b c d e  f g h i j  k l m n o  p q   r s t  u v w  x y z
    let segSet1 = S.fromList lt


    let pair1 = head pairSeg
    let bo = S.member pair1 segSet1

    let b1 = S.member [Vertex3 0 0 0, Vertex3 0 0 0] segSet1

    let segValid = filter (\x -> x /= [] ) $ map (\x -> S.member x segSet1 ? x $ []) sortPairSeg
    
    -- Wednesday, 15 March 2023 13:17 PDT
    -- NOTE:BUG, need to check whether all allChildren pts are in each triangle
    -- NOTE:Fixed BUG, there is bug in isPtInsideTri, fixed it so far,
    -- TODO: Add tests cases for isPtInsideTri
    let segInTriangle = filter (\seg -> let or ls = foldr (\a b -> a || b) False ls
                                        in not $ or $ map (\pt -> fst $ isPtInsideTriList pt (vertexPt:seg) ) allChildren) segValid
    -- (Vertex3 0.88 0.9 0.0,[Vertex3 (-0.3) 0.1 0.0,Vertex3 (-0.2) 0.6 0.0,Vertex3 0.25 0.34 0.0,Vertex3 0.32 0.44 0.0])
  
    when False $ do
      let v  = Vertex3 0.88 0.9 0.0
      let s1 = Vertex3 (0.0) 0.3 0.0
      let s2 = Vertex3 0.25 0.34 0.0
      let p  = Vertex3 0.32 0.44 0.0
      let ls = [v, s1, s2]
      drawTriangleList red ls
      drawCircle' p 0.02
      let inside = isPtInsideTriList p [v, s1, s2]
      logFile2 "/tmp/xx2.x" $ map show [inside]
    
    when False $ do
      -- shift all segments alone on X-axis
      let x = -0.5
      let ls = map (\w -> map (\v -> shiftX v x) w) segInTriangle
      drawAllSeg yellow ls


    let vexAllTriangle = map (\seg -> vertexPt : seg) segInTriangle

    
    when False $ do
      mapM_ (\cx -> drawTriangleList blue cx) $ map (\(ls, k) -> map (\x -> shiftY x k) ls) $ zip vexAllTriangle [0.1, 0.2..]
  
    when False $ do
      mapM_ (\cx -> drawTriangleList red cx) $ map (\(k, ls) -> map (\x -> shiftX x k) ls) $ zip [-0.5, (-0.1)..] $ vertexWithAllTri allSegHull vertexPt
  
    when False $ do
      mapM_ (\cx -> drawTriangleList white cx) $ map (\(k, ls) -> map (\x -> shiftX x k) ls) $ zip [-0.5, (-0.3)..] $ meshAllTriangle allSegHull randomPts

    let cirList = map (\[x, y, z] ->
                       ([x, y, z],
                         threePtCircle x y z,
                         let may_cen = threePtCircle x y z
                         in case may_cen of
                             Just c  -> distX c y
                             Nothing -> (-1)
                        )
                     ) vexAllTriangle

    -- Check whether other vertexes are inside the circle which is fixed by a three pts(a triangle)
    let isInCirList =  map(\(tri_vex, cen_vex, d) ->
                            map(\vex ->
                                let cen = case cen_vex of
                                            Just c  -> c
                                            Nothing -> error "Three pts can not form a circle."
                                in distX vex cen <= d ? (True, tri_vex, vex) $ (False, tri_vex, vex)
                              ) $ (L.\\) randomPts tri_vex
                           ) cirList

    -- NOTE: newTri has duplicated set of segment and triangle


    {--
    -- f(x) = 2x + h(y)
    -- h(y) = 3y
    
    -- f(x, y) = 2x + 3y
    -- Gy(x) = 2x + 3Y
    -- Hx(y) = 2X + 2y
       x -> Hx(y)
    -- x -> 2x + 3Y
             y -> 3X + 2y
    
    -- g(x) = 2x
    --}
    -- url http://xfido.com/publicfile/codedoc/triangle_split.pdf
    -- Remove the intersection segment from a Triangle, see doc above.
    -- Return two new Triangle with newly added Segment
    -- xx1

    when True $ do
      fn <- getEnv "b" >>= \b -> return $ b </> "tmp/k.x"
      fExist fn >>= \b -> unless b $ touch fn
      ls <- readFileList fn >>= \cx -> return $ map (\x -> read x :: Float) cx
      pre ls
      pre $ partList 2 ls
  
      let lt = map (\[x, y] -> Vertex3 x y 0) $ let ln = partList 2 ls in if even (len ls) then ln else init ln
      
      let upts = noColinearPt lt
      when (len upts >= 6) $ do
        oldTime <- timeNowMilli
        let allSeg = convexHullAllSeg upts
        mapM_ (\s -> drawSegmentList red s) $ map tupTols2 allSeg

        logFileG $ map show allSeg
        drawTriangulation upts allSeg (-1.0, 0)
        newTime <- timeNowMilli
        let diff = newTime - oldTime
        -- print $ "time=" ++ show (fi diff/1000)
        threadDelay $ 10^0
  
    when False $ do
      let a = Vertex3 0 0 0
      let b = Vertex3 1 1 0
      let c = Vertex3 1 1 0
      let d = Vertex3 0.7 1.4 0
      let e = Vertex3 0.2 0.8 0
      let vex = [a, b, c, d, e]
      let lv = unique vex
      let ls = combin 3 lv
      let lt = unique $ join $ filter (not . null) $ map (\[a, b, c] -> isColinear a b c ? [a, b, c] $ []) ls
      let lw = (L.\\) lv lt
      pre lw
      threadDelay $ 10^1
  
    when False $ do
      let a = Vertex3 0 0 0
      let b = Vertex3 1 1 0
      let c = Vertex3 1 0 0
      let d = Vertex3 0.7 1.4 0
      let e = Vertex3 0.2 0.8 0
      let vex = [d, c, e, a, b]
      let seg = [(a, c), (a, b), (a, d), (a, e), (e, d), (d, b), (b, c)]
      let segM = [(True, (a, c)), (True, (a, b)), (False, (b, a)), (False, (c, a)), (True, (d, c))]
      let col = collectSegment segM
      pre col
      threadDelay $ 10^0
  
    when False $ do
      lr <- readFileList "/tmp/k.x" >>= \cx -> return $ map (\x -> read x :: Float) cx
      let ls = (drop 4 lr) ++ (take 4 lr)
      let ltt = map (\[x, y] -> Vertex3 x y 0) $ partList 2 ls
      let lw = unique ltt
      let lc = combin 3 lw
      let lv = filter (\[v1, v2, v3] -> not $ isColinear v1 v2 v3) lc
      let upts = unique $ join lv
      let lt = convexHull (len upts) upts

      -- let allSeg = convexHullAllSeg upts
      -- let allTri = meshAllTriangle allSeg lw
      let lx = convexHullLoop lw
      writeFileList "/tmp/f000.x" $ map show lx
      writeFileList "/tmp/f00.x" $ map show upts
      writeFileList "/tmp/f2.x" $ map show lw

      drawDotRColor (Vertex3 0.81 0.53 0.0) 0 cyan
      when True $ do
        drawAllSeg yellow $ map (\t -> shiftXList (tupTols2 t) (0)) lx
        mapM_ (\v -> drawDot v) lw
  
      -- drawTriangulation upts allSeg (-1.0, -1.0)
      -- threadDelay $ 10^10
  
    when False $ do
      let a = Vertex3 0 0 0
      let b = Vertex3 1 1 0
      let c = Vertex3 1 0 0
      let d = Vertex3 0.7 1.4 0
      let e = Vertex3 0.2 0.8 0
      let vex = [d, c, e, a, b]
      -- let seg = [(a, c), (a, b), (a, d), (a, e), (e, d), (d, b), (b, c)]
      let seg = [(c, a), (a, c), (e, c), (c, e)]
      let ls = uniqueSegX seg
      pre ls
      threadDelay $ 10^0
  
    when False $ do
      let a = Vertex3 0 0 0
      let b = Vertex3 2 2 0
      let c = Vertex3 1 0 0
      -- let d = Vertex3 0.7 1.4 0
      let d = Vertex3 0.7 2.4 0
      let e = Vertex3 0.2 0.8 0
      let f = Vertex3 2.1 0.5 0
      let g = Vertex3 0.915 1.58 0
      let vex = [d, c, e, a, b, f, g]
      -- let vex = [a, b, c, d]
      let seg = [(a, c), (a, b), (a, d), (a, e), (e, d), (d, b), (b, c), (f, c), (f, b), (g, a), (g, d), (g, b)]
      -- let seg = [(a, c), (a, b), (b, c), (a, d), (d, b)]
      let segM = map (\x -> (True, x)) seg
      let goodSeg = triangulateSimple vex vex segM

      {--
      let xseg = collectSegment segM
      let xtrix = meshAllTriangle seg vex
        -- let tup = flipEdge c (map lsToTup3 trix) seg
      let xmayTup = flipEdge c (map lsToTup3 xtrix) xseg
      pre xmayTup
      --}
      print "goodSeg"
      pre goodSeg
      let collectionSeg = collectSegment goodSeg
      let validTri = meshAllTriangle collectionSeg vex
      print "collectionSeg"
      pre collectionSeg
      threadDelay $ 10^0
      mapM_ (\se -> drawSegmentList        red $ map (\v ->shiftX  (shiftY v (-1.7)) (-1.0)) se) $ map tupTols2 collectionSeg
      mapM_ (\vx -> drawCircleThreePtListX blue vx) $ map (\seg -> map (\v -> shiftX (shiftY v (-1.7)) (-1.0)) seg) validTri
  
      mapM_ (\se -> drawSegmentList green se) $ map tupTols2 seg
  
    when False $ do
      let a = Vertex3 0 0 0
      let b = Vertex3 1 1 0
      let c = Vertex3 1 0 0
      let d = Vertex3 0.7 1.4 0
      let e = Vertex3 0.2 0.8 0
      -- let vex = [a, b, c, d, e]
      -- let vex = [b, d, c, e, a]
      let vex = [d, c, e, a, b]
      let seg = [(a, c), (a, b), (a, d), (a, e), (e, d), (d, b), (b, c)]
      let trix = meshAllTriangle seg vex
      -- let tup = flipEdge c (map lsToTup3 trix) seg
      let tup = flipEdge e (map lsToTup3 trix) seg
      -- let tup = flipEdge c [(a, b, d)] seg
      pre trix
      pre tup
      when True $ do
        mapM_ (\cx -> drawCircleThreePtList cx cyan) trix
        mapM_ (\cx -> drawTriangleList yellow cx) trix
        mapM_ (\se -> drawSegmentList white $ map (\v -> shiftX v 0.7) se) $ map tupTols2 seg
      threadDelay $ 10^0
  
    when False $ do
      let a = Vertex3 0 0 0
      let b = Vertex3 1 1 0
      let c = Vertex3 1 0 0
      let d = Vertex3 0.7 1.4 0
      let e = Vertex3 0.2 0.8 0
      let cv = removeVex [a, b] [a, b, c, d, e]
      pre cv
  
    when False $ do
      let a = Vertex3 0 0 0
      let b = Vertex3 1 1 0
      let c =  Vertex3 1 0 0
      let ck = Vertex3 0.999 0 0
      let d = Vertex3 0.7 1.4 0
      let e = Vertex3 0.2 0.8 0
      let t = (a, b, c)
      let isOn = isPtOnInscribeCircle ck (a, b, c)
      pre isOn
      let isInside = isPtInsideInscribeCircle ck (a, b, c)
      pre isInside
      threadDelay $ 10^0
  
    when False $ do
      let a = Vertex3 0 0 0
      let b = Vertex3 1 1 0
      let c = Vertex3 1 0 0
      let d = Vertex3 0.7 1.4 0
      let e = Vertex3 0.2 0.8 0
      -- let vex = [a, b, c, d, e]
      -- let vex = [b, d, c, e, a]
      let vex = [d, c, e, a, b]
      let seg = [(a, c), (a, b), (a, d), (a, e), (e, d), (d, b), (b, c)]
      let trix = meshAllTriangle seg vex
      mapM_ (\cx -> drawTriangleList blue cx) $ map (\(ls, k) -> map (\x -> shiftY x k) ls) $ zip trix [0.1, 0.2..]
      mapM_ (\se -> drawSegmentList white $ map (\v -> shiftX v 0.7) se) $ map tupTols2 seg
    
    when False $ do
      let a = Vertex3 0 0 0
      let b = Vertex3 1 1 0
      let c = Vertex3 1 0 0
      let d = Vertex3 0.7 1.4 0
      let e = Vertex3 0.2 0.8 0
      -- let vex = [a, b, c, d, e]
      -- let vex = [b, d, c, e, a]
      let vex = [d, c, e, a, b]
      let seg = [[a, c], [a, b], [a, d], [a, e], [e, d], [d, b], [b, c]]
      let noIntersectSeg = removeIntersectSeg [Vertex3 0 0 0, Vertex3 1 1 0, Vertex3 1 0 0] (Vertex3 0 1 0)
      mapM_ (\s -> drawSegmentList red s) seg
      drawCircleThreePt a b c green
      -- let mayTri = removeIntersectSeg2 [a, b, c] e seg
      -- let ls = [a, b, d]
      let ls = [a, d, e]
      drawTriangleList blue ls
      drawCircleThreePtList ls green
      let mayTri = removeIntersectSeg2 (lsToTup3 ls) b seg
          
      case mayTri of
           Just tri -> do
             mapM_ (\cx -> drawTriangleList blue cx) $ map (\(ls, k) -> map (\x -> shiftY x k) ls) $ zip (t3 tri) [0.1, 0.2..]
             pre tri
             threadDelay 0
           Nothing -> print "nothingkkk"
      
      let it = intersectSegNoEndPt2 (e, b) (a, d)
      pre it
      -- let lstri = [(a, b, c), (a, b, d), (a, d, e)]
      let lstri = [(a, d, e), (a, b, c), (a, b, d)]
      let tseg = [(a, c), (a, b), (a, d), (a, e), (e, d), (d, b), (b, c)]
      -- let xxtri = triangulateX tseg vex vex lstri lstri
      -- xxtri
      when False $ do
        xxtri <- triangulateXIO tseg vex vex lstri lstri
        pre xxtri
        mapM_ (\cx -> drawCircleThreePtList (tupTols3 cx) cyan) $ snd xxtri
        mapM_ (\cx -> drawTriangleList yellow cx) $ map tupTols3 $ snd xxtri
        mapM_ (\se -> drawSegmentList white $ map (\v -> shiftX v 0.7) se) (map tupTols2 $ fst xxtri)
    {--
       If vertex is in the circumference, then the vertex is considered to be inside.
       url xfido.com/publicfile/codedoc/h4.txt
       url xfido.com/publicfile/codedoc/triangle_vex.png
       url: http://xfido.com/publicfile/codedoc/incircle.txt
       NOTE: Found bug => Found a vertex is inside the triangle
       [Vertex3 0.1 0.1 0.0, Vertex3 0.12 0.8 0.0, Vertex3 1.3 0.12 0.0] v=Vertex3 0.2 0.6 0.0
       v is inside the triangle
    --}
    let inCircleList = join $ filter (\x -> x /= []) $ map (\ls -> filter (\(b,_,_) -> b) ls ) isInCirList

    -- url http://xfido.com/publicfile/codedoc/trivex.txt
    let triVex = map (\(_, cx, v) -> (cx, v)) inCircleList
  
    -- url http://xfido.com/publicfile/codedoc/segtri.txt
    -- mapM_ (\([x, y, z], v) -> triangle red (x, y, z)) triVex

    let ranPts = map (\(Vertex3 x y z) -> Vertex3 (x - 1.5) y z) randomPts

    -- ERROR: A vertex is inside a triangle
    -- URL: http://xfido.com/publicfile/codedoc/tri_err.txt
  
    when False $ do
        let le = len randomPts
        let cvexls = convexHull le randomPts
        logFile2 "/tmp/x4" ["cvexls convexHull"]
        logFile2 "/tmp/x4" $ map show cvexls
    when False $ do
        let le = len randomPts
        let cvexls = convexHull2 le randomPts
        logFile2 "/tmp/x4" ["cvexls convexHull2"]
        logFile2 "/tmp/x4" $ map show cvexls

    when False $ do
        -- (m choose n)
        let ls = combin 2 randomPts
        pp "ok"
        drawAllSeg red ls
  
    when False $ do
        let ls = [(Vertex3 1.0 2.0 0, Vertex3 2.2 3.3 0) , (Vertex3 4.0 6.0 0, Vertex3 9.2 2.3 0)]
        let seg = (Vertex3 4.0 6.0 0, Vertex3 9.2 2.3 0)
        let cx = removeSeg seg ls
        pre $ cx == [(Vertex3 1.0 2.0 0, Vertex3 2.2 3.3 0)]
        pre cx
        threadDelay 0
        pp "removeSeg"
  
    when False $ do
        let ls = [(Vertex3 1.0 2.0 0, Vertex3 2.2 3.3 0, Vertex3 0.1 0.1 0.1) , (Vertex3 4.0 6.0 0, Vertex3 9.2 2.3 0, Vertex3 0.1 0.1 0.1)]
        let tri = (Vertex3 4.0 6.0 0, Vertex3 9.2 2.3 0, Vertex3 0.1 0.1 0.1)
        let cx = removeTri tri ls
        pre $ cx == [(Vertex3 1.0 2.0 0, Vertex3 2.2 3.3 0, Vertex3 0.1 0.1 0.1)]
        pre cx
        threadDelay 0
        pp "removeTri"
    when False $ do
        pp "sortVex ls'"
        let ls = [(Vertex3 1.0 2.0 0, Vertex3 2.2 3.3 0, Vertex3 0.1 0.1 0) , (Vertex3 4.0 6.0 0, Vertex3 9.2 2.3 0, Vertex3 0.1 0.1 0), (Vertex3 4.0 2.0 0, Vertex3 4.0 2.0 0, Vertex3 0.1 0.1 0)]
        let ls' = map tupTols3 ls
        let lt = sortVex ls'
        pre lt
        threadDelay 0
        pp "removeTri"

    when False $ do
        pp "removeTriWithSeg 1"
        let ls = [(Vertex3 1.0 2.0 0, Vertex3 2.2 3.3 0, Vertex3 0.1 0.1 0.1),
                  (Vertex3 4.0 6.0 0, Vertex3 9.2 2.3 0, Vertex3 0.1 0.1 0.1),
                  (Vertex3 4.0 2.0 0, Vertex3 4.0 2.0 1.0, Vertex3 0.1 0.1 0.1)]
        let lt = removeTriWithSeg (Vertex3 1.0 2.0 0, Vertex3 0.1 0.1 0.1) ls
        pre lt
        let lr = removeTriWithSeg (Vertex3 2.2 3.3 0, Vertex3 0.1 0.1 0.10) ls
        pre lr
        threadDelay 0
        pp "removeTri"
    -- END_triangulation

main = do
  argList <- getArgs
  if len argList > 0 then do
    case head argList of
      "-h" -> helpme
      _    -> do
        print $ "Wrong option => " ++ (head argList) ++ ", -h => Help"
  else mymain

{-|        
main = do
       pp "ok"
-}
