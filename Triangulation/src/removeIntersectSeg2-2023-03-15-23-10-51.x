removeIntersectSeg2 :: [Vertex3 GLfloat] -> Vertex3 GLfloat -> ([Vertex3 GLfloat], [Vertex3 GLfloat], [[Vertex3 GLfloat]])
removeIntersectSeg2 cx v = if | len cx /= 3 -> error "ERROR: removeIntersectSeg2 => List has to contain three (Vertex3 GLfloat)."
                              | [] /= filter (/= Nothing) lx -> (sortSeg [y, z], sortSeg [x, v], map sortSeg [[x, v, y], [x, v, z]])
                              | [] /= filter (/= Nothing) ly -> (sortSeg [x, z], sortSeg [y, v], map sortSeg [[y, v, x], [y, v, z]])
                              | [] /= filter (/= Nothing) lz -> (sortSeg [x, y], sortSeg [z, v], map sortSeg [[z, v, x], [z, v, y]])
                              | otherwise                         -> error $ "ERROR removeIntersectSegs => to remove segment" ++ (show [x, y, z]) ++ " v=" ++ show v
          where
            x = cx !! 0
            y = cx !! 1
            z = cx !! 2
            lx = map (\s -> interSeg (cx, v) s) cx
            ly = map (\s -> interSeg (cx, v) s) cx
            lz = map (\s -> interSeg (cx, v) s) cx
            s = [(x, y), (y, z), (x, z)]
            interSeg = intersectSegNoEndPt2
            sortSeg = qqsort cmpVex