-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
{-# LANGUAGE MultiWayIf #-}
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Data.Maybe(fromJust, fromMaybe)
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD
import qualified Data.Array as DR 
import qualified Data.List as L
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString as SB -- strict ByteString 
import qualified Data.ByteString.Lazy.UTF8 as BLU
import qualified Data.ByteString.UTF8 as BSU
import qualified Data.Text as DT
import qualified Data.Text.Lazy as DL 
import qualified Data.Map.Strict as M
import qualified Data.Aeson as DA
import qualified Data.Aeson.Text as AAT  -- encodeToLazyText

import Database.Redis
import Control.Monad.IO.Class

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close
-- Either String String isomorphic to Maybe String ? 
-- redisGet::String -> IO (Maybe String)
-- redisGet key = do
             -- conn <- connect defaultConnectInfo
             -- mValue <- runRedis conn $ do 
                        -- value <- get $ toSBS key 
                        -- let mayValue = case value of
                                    -- Left _ -> Nothing 
                                    -- -- Right v -> fromMaybe (BSU.fromString "Could not find key") v
                                    -- Right v -> v
                        -- return mayValue
                        -- -- liftIO::(MonadIO m) => IO a => m a 
             -- return $ toStr <$> mValue 


{-|
redisGet1::String -> IO (Maybe String)
redisGet1 key = do
             conn <- connect defaultConnectInfo
             mValue <- runRedis conn $ do 
                        value <- get $ toSBS key 
                        let mayValue = case value of
                                    Left _ -> Nothing 
                                    -- Right v -> fromMaybe (BSU.fromString "Could not find key") v
                                    Right v -> v
                        return mayValue
                        -- liftIO::(MonadIO m) => IO a => m a 
             return $ toStr <$> mValue 
-}

{-|
redisSet::String -> String -> IO ()
redisSet k v = do
                conn <- connect defaultConnectInfo
                let k' = toSBS k
                let v' = toSBS v
                runRedis conn $ do 
                        set k' v' 
                        liftIO $ print $ "Set k=" ++ k ++ " v=" ++ v
                return ()
-}

-- emacs        
main = do 
        conn <- connect defaultConnectInfo
        -- runRedis::Connection -> Redis a -> IO a
        -- newType Redis a = Redis (ReaderT RedisEnv IO a) deriving (Monad, MonadIO, Functor, Applicative)
        let prefix = "prefix"
        prefixMay <- redisGet prefix
        let preStr = case prefixMay of 
                          Just x -> x
                          _      -> "" 
        let input = "keykk"
        s <- redisGet input 
        let str = case s of
                       Just x -> x
                       _      -> "nothing"
        let ls = map escapeHtml $ lines str
        
        let renderStr = if | preStr == "k" -> let concat ls = concatStr ls "" in concat $ htmlTable $ tran [ls] 
                           | preStr == "i" -> let concat ls = concatStr ls "" in concat $ htmlTable $ tran [ls] 
                           -- It seems the newlines are not ignored..
                           | preStr == "n" -> "<pre>" ++ (concatStr ls "<br>") ++ "</pre>"
                           | otherwise -> concatStr ls ""

        -- let str1 = concat $ htmlTable $ tran [ls] where concat ls = concatStr ls ""
        redisSet "keytable" renderStr 
        -- javascript can retrieve the key 'keytable'
        pp "done"

