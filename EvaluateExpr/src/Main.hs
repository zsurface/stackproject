import AronSimple
-- KEY: evaluate expression
       
data Expr = INT Integer
          | Str String
          | Add Expr Expr
          | Sub Expr Expr
            
evaluate::Expr -> Integer
evaluate (INT x) = x
evaluate (Str x) = strToInteger x
evaluate (Add (INT x) (INT y)) = (evaluate $ INT x) + (evaluate $ INT y)
evaluate (Sub (INT x) (INT y)) = (evaluate $ INT x) - (evaluate $ INT y)
evaluate  _    = 1


-- + 3 4
-- Add (INT 3) (INT 4)
--
-- - (+ 3 4) (+ 1 2)
-- Sub (Add (INT 3) (INT 4)) (Add (INT 1) (INT 2))


main = do 
        old <- timeNowSecond
        print $ evaluate $ INT 3
        print $ evaluate $ Str "4"
        print $ evaluate $ Add (INT 3) (INT 4)
        print $ evaluate $ Sub (INT 3) (INT 4)
        new <- timeNowSecond
        print $ "sec=" ++ (show (new - old))
