{-# LANGUAGE RankNTypes #-}  -- forall
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module AronDevLib where

import Control.Monad
import Control.Applicative
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import Data.Typeable
import AronModule


class MyType a b c where
  rmInx :: a -> b -> c

instance MyType Int [a] [a] where
  rmInx n cx = removeIndex n cx

instance MyType Integer [a] [a] where
  rmInx n cx = removeIndex (fi n) cx


{-|
    KEY: Integer to Num
    Better name
-}   
integerToNum::(Integral a, Num b) => a -> b
integerToNum = fromIntegral

(÷)::forall a b c.(Typeable a, Num a, Typeable b, Num b, Fractional c) => a -> b -> c
(÷) x y = x' / y'
  where
    x' = case cast x of
           Just (x::Int) -> realToFrac x
           _             -> case cast x of
                               Just (x::Integer) -> realToFrac x
                               _                 -> case cast x of
                                                       Just (x::Float) -> realToFrac x
                                                       _               -> case cast x of
                                                                             Just (x::Double) -> realToFrac x
                                                                             _                -> error "Unknown type"
    y' = case cast y of
           Just (y::Int) -> realToFrac y
           _             -> case cast y of
                                Just (y::Integer) -> realToFrac y
                                _                 -> case cast y of
                                                        Just (y::Float) -> realToFrac y
                                                        _               -> case cast y of
                                                                              Just (y::Double) -> realToFrac y
                                                                              _                -> error "Unknown type"





(×)::forall a b c. (Typeable a, Num a, Typeable b, Num b, Fractional c) => a -> b -> c
(×) x y = x' * y'
   where
     x' = case cast x of
            Just (x::Float) -> realToFrac x
            _               -> case cast x of
                                 Just (x::Int) -> realToFrac x
                                 _             -> case cast x of
                                                   Just (x::Double) -> realToFrac x
                                                   _                -> error "Unknown type"

     y' = case cast y of
            Just (y::Float) -> realToFrac y
            _               -> case cast y of
                                 Just (y::Int) -> realToFrac y
                                 _             -> case cast y of
                                                   Just (y::Double) -> realToFrac y
                                                   _                -> error "Unknown type"


(↑)::Int -> [a] -> [a]
(↑) n cx = take n cx
(↓)::Int -> [a] -> [a]
(↓) n cx = drop n cx

ρ::[a]-> Int
ρ cx = len cx
           

