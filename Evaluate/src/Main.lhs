-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

>{-# LANGUAGE MultiWayIf        #-}
>{-# LANGUAGE QuasiQuotes       #-} -- support raw string [r|<p>dog</p> |]
>{-#  LANGUAGE GADTs #-}
>{-#  LANGUAGE KindSignatures #-}
>{-#  LANGUAGE TypeOperators #-}
>{-#  LANGUAGE DataKinds #-}

import Text.RawString.QQ       -- Need QuasiQuotes too 


-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


shell command template:
 
        argList <- getArgs
        if len argList == 2 then do
            let n = stringToInt $ head argList
            let s = last argList
            putStr $ drop (fromIntegral n) s
       else print "drop 2 'abcd'"

>import Prelude hiding(Maybe(..))
>import Data.Kind

>import AronModule 

>p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

 kind 
 MyNothhing has kind *
 MyJust has kind * -> *
 MyMaybe a => List<T> 
 MyMaybe Int => List<Integer>
 int fun(List<T> list){ 
        return list.size();
 }
 
 
 data MyMaybe a = Nothing | Just a
  Just a => Myabe a 
 Just (a) -> Maybe a
 zo - open
 za - close

 There are three Value constructor
 Expr => is type constructor
 I Int => is value constructor 
 Add Expr Expr is value constructor
 Mul Expr Expr is value constructor

 
Fractional is subtype of Num,  

class (Num a) => Fractional a where
        (/):: a -> a -> a 
        recip :: a -> a
        fromRational :: a -> a

instance Fractional Float
instance Fractional Double


Ints: * -> *
Ints: Int -> MyExpr Int



>data MyExpr a where
>       FI :: Float -> MyExpr Int
>       IF :: Int -> MyExpr Float
>       (:+) :: MyExpr Int -> MyExpr Int -> MyExpr Int
>       (:++) :: MyExpr a -> MyExpr a -> MyExpr a
>       Ints  :: Int -> MyExpr Int
>       Fl    :: Float -> MyExpr Float
>       Inte  :: Integer -> MyExpr Integer
>       MyAdd :: MyExpr Int -> MyExpr Int -> MyExpr Int       
>       MyMul :: MyExpr Int -> MyExpr Int -> MyExpr Int
>       MyDiv :: MyExpr Int -> MyExpr Int -> MyExpr Int



>myeval::MyExpr Int -> Int
>myeval (Ints n) = n
>myeval (Inte n) = fi n
>myeval (MyAdd e1 e2) = (myeval e1) + (myeval e2)
>myeval ((:+) e1 e2) = (myeval e1) + (myeval e2)
>myeval (MyMul e1 e2) = (myeval e1) * (myeval e2)
>myeval (MyDiv e1 e2) = div (myeval e1)  (myeval e2)
>myeval ((:++) e1 e2) = (myeval e1) + (myeval e2)

>myevalF::MyExpr Float -> Float
>myevalF (Fl n) = n
>myevalF ((:++) e1 e2) = (myevalF e1) + (myevalF e2)


fromIntegral(Num b, Integral a) => a -> b 

Num a=> a 
Fractional a =>Float, Double, Integer
rf = realToFrac(Real a, Integral b)=> a -> b


>myevalf::MyExpr Float -> Float
>myevalf (Ints n) = fi n

-- >myevalf (Ints n) = fi n
-- >myevalf (Fl n)  = n
-- >myevalf (Inte n) = fi n
-- >myevalf (MyAdd e1 e2) = (myevalf e1) + (myevalf e2)
-- >myevalf ((:+) e1 e2) = (myevalf e1) + (myevalf e2)
-- >myevalf (MyMul e1 e2) = (myeval e1) * (myeval e2)
-- >myevalf (MyDiv e1 e2) = div (myeval e1)  (myeval e2)





>data Term a where
>  Lit    :: Int -> Term Int
>  Succ   :: Term Int -> Term Int
>  IsZero :: Term Int -> Term Bool   
>  If     :: Term Bool -> Term a -> Term a -> Term a
>  Pair   :: Term a -> Term b -> Term (a,b)


>toInt::Float -> Int                                            
>toInt x = floor x

>data Expr = I Int 
>            | F Float
>            | D Double
>            | In Integer
>            | B Bool
>            | S String
>            | Add Expr Expr 
>            | Sub Expr Expr 
>            | Div Expr Expr 
>            | Mul Expr Expr deriving (Show)

we can pattern-match all the constructors

-- >evalI::Expr -> Int 
-- >evalI (I n) = n 
-- >evalI (F n) = floor n
-- >evalI (D n) = floor n
-- >evalI (In n) = fi n
-- >evalI (S s) = strToInt s 
-- >evalI (Add e1 e2) = floor $ (evalF e1) + (evalF e2)
-- >evalI (Sub e1 e2) = floor $ (evalF e1) - (evalF e2)
-- >evalI (Mul e1 e2) = floor $ (evalF e1) * (evalF e2)
-- >evalI (Div e1 e2) = floor $ (evalF e1) / (evalF e2)



-- >evalF::Expr -> Float
-- >evalF (I n) = fi n 
-- >evalF (In n)= rf n
-- >evalF (F n) = n
-- >evalF (D n) = rf n 
-- >evalF (S s) = (fi . strToInt) s 
-- >evalF (Add e1 e2) = (evalF e1) + (evalF e2) 
-- >evalF (Sub e1 e2) = (evalF e1) - (evalF e2) 
-- >evalF (Mul e1 e2) = (evalF e1) * (evalF e2) 
-- >evalF (Div e1 e2) = (evalF e1) / (evalF e2) 

-- >evalIn::Expr -> Integer
-- >evalIn (I n) = fi n
-- >evalIn (F n) = floor n
-- >evalIn (In n) = n
-- >evalIn (S s) = (fi . strToInt) s
-- >evalIn (Add e1 e2) = floor $ (evalF e1) + (evalF e2)
-- >evalIn (Sub e1 e2) = floor $ (evalF e1) - (evalF e2)
-- >evalIn (Mul e1 e2) = floor $ (evalF e1) * (evalF e2)
-- >evalIn (Div e1 e2) = floor $ (evalF e1) / (evalF e2)


>main = do 
>        pp " done  cool yes !"

-- >        let e1 = I 2
-- >        let e2 = I 3
-- >        let val =  evalI $ Add e1 e2
-- >        let val2 = evalI $ Mul e1 e2
-- >        let val3 = evalI $ Mul (Mul e1 e2) (Mul e1 e2)
-- >        let s1 = S "1"
-- >        let s2 = S "9"
-- >        let val4 = evalI $ Add s1 s2 
-- >        let val5 = evalI $ Div e1 e2 
-- >        let val6 = evalF $ Div e1 e2 
-- >        let val7 = evalF $ Div s1 s2 
-- >        let val8 = evalF $ Div (F 0.3) (F 0.4) 
-- >        let val9 = evalI $ Div  (F 2.3) (F 2.4) 
-- >        let val10 = evalF $ Add (F 2.3) (F 2.4) 
-- >        let val11 = evalF $ Sub (F 2.3) (F 2.4)
-- >        let val12 = evalF $ Sub (I 2) (F 2.4)
-- >        let val13 = evalF $ Div (In 2) (F 2.4)
-- >        let val14 = evalI $ Mul (In 4) (F 3.33)
-- >        let val15 = evalF $ Mul (In 4) (F 3.33)
-- >        let val16 = evalF $ Mul (F 4.301) (I 3)
-- >        let val17 = evalF $ Div (F 4.301) (I 3)
-- >        let val18 = evalI $ Div (F 4.301) (I 3)
-- >        let val19 = evalF $ Div (F 4.301) (D 3.3301)
-- >        let num = 3::Int
-- >        let val20 = evalI $ Div (F 3) (D 3.3301)
-- >        pw "val" val
-- >        pw "val2" val2
-- >        pw "val3" val3
-- >        pw "val4" val4
-- >        pw "val5" val5
-- >        pw "val6" val6
-- >        pw "val7" val7
-- >        pw "val8" val8
-- >        pw "val9" val9
-- >        pw "val10" val10
-- >        pw "val11" val11
-- >        pw "val12" val12
-- >        pw "val13" val13
-- >        pw "val14" val14
-- >        pw "val15" val15
-- >        pw "val16" val16
-- >        pw "val17" val17
-- >        pw "val18" val18
-- >        pw "val19" val19
-- >        pw "val20" val20

>        let e1 = Ints 1
>        let e2 = Ints 2
>        let e3 = Fl 1.02
>        let e4 = Fl 2.001
>        pp $ myeval $ e1 :+ e2
>        pp "aron5"
>        pp $ myeval $ e1 :++ e2
>        pp $ myevalF $ e3 :++ e4
>        timeNowSecond >>= \x -> pp x

