-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE QuasiQuotes #-} -- support raw string [r|<p>dog</p> |]
import Text.RawString.QQ       -- Need QuasiQuotes too 


-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import qualified System.Console.Pretty as SCP

import qualified Text.Regex.TDFA as TD

import System.Console.Haskeline
import qualified System.Console.ANSI as AN

import System.IO (stdin, hReady, hSetEcho, hSetBuffering, BufferMode(..))
import Control.Monad (when)

import Control.Monad.Trans
import System.Console.Haskeline

import Rainbow()
import System.Console.Pretty (Color (..), color)

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - openFunctor
-- za - close

{-|
   === KEY: read from pipe, read from stdin, read from pipe

   * Read from file or pipe
   @
   -- Read from file 
   readFilePipe "/tmp/a" 

   -- Read from pipe
   readFilePipe [] 
   @
-}
readFilePipe::String -> IO String
readFilePipe fp = if (not . null) fp then readFile fp else getContents 

main :: IO ()
main = do
    argList <- getArgs
    case len argList of
        var | var == 3 || var == 2 -> do
                let c = head argList 
                let n = let k = (head . tail) argList in read k :: Int
                let f1 = var == 3 ? last argList $ []
                -- From pipe
                --

                ls <- readFilePipe f1 >>= return . lines
                case c of
                    var | var == "-t" || var == "-take" -> do
                            let ls' = takeX (fi n) ls
                            pbcopy $ unlines ls'
                            mapM_ putStrLn ls'
                        | var == "-d" || var == "-drop" -> do
                            let ls' = dropX (fi n) ls
                            let lt  = takeX (len ls - fi n) ls
                            pbcopy $ unlines lt 
                            mapM_ putStrLn ls'

                        -- Expect n as an index of list 
                        | var == "-i" || var == "-index" -> do
                            let ix = n
                            let b = 0 <= ix && ix <= (len ls) - 1 
                            if b then do 
                                      let st = ls !! ix
                                      pbcopy st
                                      -- putStrLn $ "Copy to clipboard => " ++ st
                                      putStrLn st 
                                 else error $ "Invalid Index=" ++ show ix 

        var | var == 4 -> do
                let c = head argList 
                let n = let k = (head . tail) argList in read k :: Int
                let f1 = (last . init) argList 
                let f2 = last argList 
                -- From pipe
                ls <- readFilePipe f1 >>= return . lines
                case c of
                    var | var == "-t" || var == "-take" -> do
                            let ls' = takeX (fi n) ls
                            writeFileList f2 ls'
                        | var == "-d" || var == "-drop" -> do
                            let ls' = dropX (fi n) ls
                            writeFileList f2 ls'
            | otherwise -> do
                pp ""
                let nTab = 2
                let ppc1 s = let cs = colorfgStr 200 s in putStrLn $ "\t" ++ cs
                let ppc2 s = let cs = colorfgStr 100 s in putStrLn $ "\t" ++ cs
                ppc1 "Two arguments"
                ppc2 "cat /tmp/a   | takeDropLine -take  4                => take first 4 lines FROM /tmp/x => Stdout"
                ppc2 "echo \"cat\" | takeDropLine -index 1                => take index 1 lines FROM /tmp/x => Stdout"
                ppc2 "echo \"cat\" | takeDropLine -i     1                => take index 1 lines FROM /tmp/x => Stdout"
                ppc1 "Three arguments"
                ppc2 "takeDropLine -take  4 /tmp/x          => take first 4 lines FROM /tmp/x => Stdout"
                ppc1 "Four  arguments"
                ppc2 "takeDropLine -take  4 /tmp/x /tmp/x.c => take first 4 lines FROM /tmp/x => /tmp/x.c"
                ppc2 "takeDropLine -take -4 /tmp/x /tmp/x.c => take last  4 lines FROM /tmp/x => /tmp/x.c"
                ppc2 "takeDropLine -drop  4 /tmp/x /tmp/x.c => drop first 4 lines FROM /tmp/x => /tmp/x.c"
                ppc2 "takeDropLine -drop -4 /tmp/x /tmp/x.c => drop last  4 lines FROM /tmp/x => /tmp/x.c"
                ppc2 "takeDropLine -t    -4 /tmp/x /tmp/x.c => drop last  4 lines FROM /tmp/x => /tmp/x.c"
                ppc2 "takeDropLine -d    -4 /tmp/x /tmp/x.c => drop last  4 lines FROM /tmp/x => /tmp/x.c"
                ppc2 "takeDropLine -i    -4 /tmp/x /tmp/x.c => drop last  4 lines FROM /tmp/x => /tmp/x.c"

--    if len argList == 3 || len argList == 4 then do
--      let ls = len argList == 3 ? argList ++ [""] $ argList
--      let c = head ls
--      let n = (fi . strToInt) $ (head . tail) ls
--      let f1 = (last . init) ls
--      let f2 = last ls
--      takeDropLine c n f1 f2
--    else do
--      let nTab = 2
--      printBox nTab ["Three arguments"]
--      printBox nTab ["takeDropLine -take  4 /tmp/x          => take first 4 lines FROM /tmp/x => Stdout"]
--      printBox nTab ["Four  arguments"]
--      printBox nTab ["takeDropLine -take  4 /tmp/x /tmp/x.c => take first 4 lines FROM /tmp/x => /tmp/x.c"]
--      printBox nTab ["takeDropLine -take -4 /tmp/x /tmp/x.c => take last  4 lines FROM /tmp/x => /tmp/x.c"]
--      printBox nTab ["takeDropLine -drop  4 /tmp/x /tmp/x.c => drop first 4 lines FROM /tmp/x => /tmp/x.c"]
--      printBox nTab ["takeDropLine -drop -4 /tmp/x /tmp/x.c => drop last  4 lines FROM /tmp/x => /tmp/x.c"]
--      printBox nTab ["takeDropLine -t    -4 /tmp/x /tmp/x.c => drop last  4 lines FROM /tmp/x => /tmp/x.c"]
--      printBox nTab ["takeDropLine -d    -4 /tmp/x /tmp/x.c => drop last  4 lines FROM /tmp/x => /tmp/x.c"]

   
