{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_highlight (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/highlight/.stack-work/install/x86_64-osx/8791056ce80957aa87be87cdcf164fb90982e34e8f52d0f86047bb442b487fda/8.10.4/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/highlight/.stack-work/install/x86_64-osx/8791056ce80957aa87be87cdcf164fb90982e34e8f52d0f86047bb442b487fda/8.10.4/lib/x86_64-osx-ghc-8.10.4/highlight-0.1.0.0-Bp1fW0mc1WrGRhg8vbLB0Y-highlight"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/highlight/.stack-work/install/x86_64-osx/8791056ce80957aa87be87cdcf164fb90982e34e8f52d0f86047bb442b487fda/8.10.4/lib/x86_64-osx-ghc-8.10.4"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/highlight/.stack-work/install/x86_64-osx/8791056ce80957aa87be87cdcf164fb90982e34e8f52d0f86047bb442b487fda/8.10.4/share/x86_64-osx-ghc-8.10.4/highlight-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/highlight/.stack-work/install/x86_64-osx/8791056ce80957aa87be87cdcf164fb90982e34e8f52d0f86047bb442b487fda/8.10.4/libexec/x86_64-osx-ghc-8.10.4/highlight-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/highlight/.stack-work/install/x86_64-osx/8791056ce80957aa87be87cdcf164fb90982e34e8f52d0f86047bb442b487fda/8.10.4/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "highlight_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "highlight_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "highlight_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "highlight_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "highlight_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "highlight_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
