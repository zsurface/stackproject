{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
{-# LANGUAGE MultiWayIf       #-}

-- Sat 10 Dec 14:18:31 2022 
-- BUG: There is bug between CPP and Text.RawString.QQ 
-- {-# LANGUAGE CPP #-}
--
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-}
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
-- import Control.Monad
-- import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
-- import qualified Data.List as L
-- import Data.List.Split
-- import Data.Time
-- import Data.Time.Clock.POSIX
import Data.Char
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import Text.Printf
import Text.Format  
import Control.Monad (unless, when, join)
-- import System.Process
-- import Text.Read
-- import Text.Regex
-- import Text.Regex.Base
-- import Text.Regex.Base.RegexLike
-- import Text.Regex.Posix
-- import Data.IORef 
-- import Control.Concurrent 

-- import qualified Text.Regex.TDFA as TD


import AronModule hiding (CodeBlock(..))
import AronAlias
import AronHtml2
-- import AronHtml
import AronGraphic 
-- import GenePDFHtmlLib
-- import WaiLib 
-- import WaiConstant
import AronToken

-- import qualified Diagrams.Prelude as DIA
-- import Diagrams.Backend.SVG.CmdLine
-- import Diagrams.TrailLike
import Diagrams.Prelude hiding (pre, blue, trim, getSub)
import Diagrams.Backend.SVG.CmdLine
import Text.RawString.QQ
import qualified System.Console.Pretty as PRE


main = do
        argList <- getArgs
        let ln = len argList
        -- testAll
        if ln == 1 then do
            let s = head argList  
            let ls = tokenize s 
            let lt = colorToken ls 
            putStr $ concat lt
        else do
            putStrLn $ colorfgStr 200 "Need one argument"
            putStrLn $ colorfgStr 100 "cmd \"abc{dog}[cat]<cow> =>\""
