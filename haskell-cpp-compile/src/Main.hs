{-# LANGUAGE QuasiQuotes #-}
-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

import NeatInterpolation
import Data.Text (Text)

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

-- COMPILE: stack exec haskell-cpp-compile -- arg1 arg2
-- cmd="g++ -O3 -framework IOKit -F $framework  -std=c++14 -I $cLib -I $cppLib -I $boostInclude -lboost_filesystem -lboost_system -o $output  $1"

glutlib="/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/System/Library/Frameworks/GLUT.framework"
framework="/Library/Developer/CommandLineTools/SDKs/MacOSX.sdk/System/Library/Frameworks"

fun framework cLib cppLib boostInclude output arg0 = [text| 
                g++ -O3 -framework IOKit -F ${framework}  -std=c++14 -I ${cLib} -I ${cppLib} -I ${boostInclude} -lboost_filesystem -lboost_system -o $output  ${arg0}
                |]

main = do 
        argList <- getArgs
        pp $ length argList
        cppLib <- getEnv "b" >>= \x -> return $ toSText $ x </> "cpplib"
        cLib   <- getEnv "b" >>= \x -> return $ toSText $ x </> "clib"
        let boostInclude = "/usr/local/include"
        when (len argList > 0) $ do
           let arg = head argList
           let arg0 = toSText arg
           let output = toSText $ takeBaseName arg 
           let cmd = toStr $ fun framework cLib cppLib boostInclude output arg0
           case len argList of
                 v | v == 1 -> do 
                       s <- runCmd cmd 
                       runCmd $ "./" ++ toStr output
                       pre s
                   | v == 2 -> do 
                       let arg0 = toSText $ head argList
                       let arg1 = toSText $ last argList
                       s <- runCmd cmd 
                       if arg1 == "no" || arg1 == "NO" then do
                         pp "Not run" 
                       else do
                         pp "Run it"
                   | otherwise -> print "Need arguments"
        pp "done!"
