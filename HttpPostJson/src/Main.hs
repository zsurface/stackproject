-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

-- import           Data.Aeson
import qualified Data.ByteString.Char8 as S8
import qualified Data.Yaml             as Yaml
import           Network.HTTP.Simple
import Data.Aeson
import GHC.Generics


--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import qualified Data.Aeson as DA
import AronModule 
import Data.Typeable

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

-- KEY: Http post JSON, Http Json
       
{-|
    var obj = new Object()
    obj.pid = 0
    obj.begt = Date.now();
    obj.endt = 0
    obj.newcode = newcode 
    let data = JSON.stringify(obj)
    xhr.send(data)

data UpdateCodeBlock = UpdateCodeBlock{pid::Integer, newcode::String, begt::Integer, endt::Integer} deriving (GEN.Generic, Show)

  -- https://hackage.haskell.org/package/http-conduit-2.3.7.3/docs/Network-HTTP-Simple.html

-}

-- :NOTE:

url = "http://localhost:8080/insert"

data MyType = MyType{name::String} deriving (Generic, Show)
instance DA.FromJSON MyType
instance DA.ToJSON MyType where
    toEncoding = DA.genericToEncoding DA.defaultOptions

main :: IO ()
main = do
  argList <- getArgs
  str <- readFileStr "/tmp/xx1.x"
  let updateCodeBlock = UpdateCodeBlock{pid = 0, newcode = str , begt = 0, endt = 0}
  let request = setRequestBodyJSON updateCodeBlock url
  response <- httpJSON request :: IO (Response ())
  putStrLn $ show $ getResponseStatusCode response
  pp "Ok"

{--
  putStrLn $ "The status code was: " ++
             show (getResponseStatusCode response)
  print $ getResponseHeader "Content-Type" response
  let ss = getResponseBody response
  -- let en = DA.decode ss :: Maybe UpCodeBlock
  let en = DA.decode ss :: Maybe UpdateCodeBlock 
     
  pre $ typeOf en
  -- pre $ show en :: UpCodeBlock
  -- S8.putStrLn $ DA.encode (getResponseBody response :: Value)
--}
  -- putStrLn $ ok upCodeBlock
  -- putStrLn $ retcmd upCodeBlock
