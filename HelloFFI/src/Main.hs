-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE CPP #-} 
-- import Turtle
-- echo "turtle"

-- FFI
{-# LANGUAGE ForeignFunctionInterface #-}

-- ffi beg 
import Foreign
import Foreign.Storable  -- Storable
import Foreign.Ptr
import Foreign.C.Types
import Foreign.C.String
import System.IO.Unsafe
-- ffi end
  
import Text.Printf   -- C style printf
import Control.Monad

import Criterion.Main
import AronModule

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

foreign import ccall "increment" 
    c_increment :: CInt -> IO CInt

f_increment :: Int  -> IO Int
f_increment n = do 
                 x <- c_increment (fromIntegral n)
                 return  $ fromIntegral x

-- KEY: read and write C pointer in haskell code
-- KEY: peek or poke C pointer in haskell code
-- try to map C type => FFI type => Haskell Type
foreign import ccall "incrementPt" 
    c_incrementPt :: Ptr CInt -> IO () -- FFI type

f_incrementPt :: IO Int  -- Haskell type
f_incrementPt  = do
                 alloca $ \pt -> do  -- allocate a pointer pt
                   poke pt 5         -- write a value to a memory location
                   c_incrementPt pt  -- pass the pointer pt to c function
                   x <- peek pt      -- read a value from a memory location
                   return $ fromIntegral x  -- convert CInt to Int

foreign import ccall "setMyStruct"
    c_setMyStruct::Ptr MyStruct -> IO() -- FFI Type

-- Convert C MyStruct to Haskell MyStruct
f_setMyStruct::IO MyStruct
f_setMyStruct = do
  alloca $ \pt -> do
    c_setMyStruct pt
    d <- peekByteOff pt 0  -- 0-8   read d :: Double from MyStruct
    c <- peekByteOff pt 8  -- 8-12  read c :: Word8 from MyStruct
    i <- peekByteOff pt 12 -- 12-16 read i :: Int32 from MyStruct
    return $ MyStruct{ d  = d, c = c, i = i}

data Stuff = Stuff{n :: CInt}

data MyStruct = MyStruct
            { d :: Double  -- 8 bytes
            , c :: Word8   -- 1 bytes but padding 4 bytes here
            , i :: Int32   -- 4 bytes
            } deriving (Show)


-- 	struct tm {
-- 	int	tm_sec;		/* seconds after the minute [0-60] */
-- 	int	tm_min;		/* minutes after the hour [0-59] */
-- 	int	tm_hour;	/* hours since midnight [0-23] */
-- 	int	tm_mday;	/* day of the month [1-31] */
-- 	int	tm_mon;		/* months since January [0-11] */
-- 	int	tm_year;	/* years since 1900 */
-- 	int	tm_wday;	/* days since Sunday [0-6] */
-- 	int	tm_yday;	/* days since January 1 [0-365] */
-- 	int	tm_isdst;	/* Daylight Savings Time flag */
-- 	long	tm_gmtoff;	/* offset from UTC in seconds */
-- 	char	*tm_zone;	/* timezone abbreviation */
--      };


data TimeInfo = TimeInfo
            {
             tm_sec::CInt
            ,tm_min::CInt		
            ,tm_hour::CInt
            ,tm_mday::CInt
            ,tm_mon::CInt
            ,tm_year::CInt
            ,tm_wday::CInt
            ,tm_yday::CInt
            ,tm_isdst::CInt
            ,tm_gmtoff::CLong
            ,tm_zone::CString
            } deriving (Show)

foreign import ccall "get_time_struct"
    c_get_time_struct::Ptr TimeInfo -> IO ()

f_get_time_struct:: IO TimeInfo
f_get_time_struct = do
                    alloca $ \ptr -> do
                      c_get_time_struct ptr
                      sec <- peekByteOff ptr 0
                      min <- peekByteOff ptr 4
                      hour <- peekByteOff ptr 8
                      mday <- peekByteOff ptr 12
                      mon <- peekByteOff ptr 16
                      year <- peekByteOff ptr 20
                      wday <- peekByteOff ptr 24
                      yday <- peekByteOff ptr 28
                      isdst <- peekByteOff ptr 32
                      gmtoff <- peekByteOff ptr 40
                      -- gmtoff <- peekArray 8 (castPtr $ plusPtr ptr 36)
                      zone <- peekByteOff ptr 48
                      ss <- peekByteOff ptr 48 >>= peekCString
                      print $ "ss=" ++ ss
                      -- zone <- peekCAString (castPtr $ plusPtr ptr 44)
                      return (TimeInfo sec min hour mday mon year wday yday isdst gmtoff zone)

data MyThink = MyThink { nX :: CInt, ptrX :: Ptr CChar} deriving (Show)

data MyBox = MyBox {strX::String} deriving (Show)

foreign import ccall "&" foo :: Ptr CInt

foreign import ccall "setMyThink"
  c_setMyThink :: Ptr MyThink -> IO()

f_setMyThink :: IO MyThink
f_setMyThink = do
                alloca $ \pt -> do
                  c_setMyThink pt
                  n <- peekByteOff pt 0
                  chptr <- peekByteOff pt 8  -- NOTE: Why is 8 bytes here?
                  ss <- peekCString chptr
                  print $ "f_setMyThink=" ++ ss
                  return (MyThink n chptr)
  
f_setMyThink2 :: IO MyBox
f_setMyThink2 = do
                alloca $ \pt -> do
                  c_setMyThink pt
                  n <- (peekByteOff pt 0) :: IO Int
                  chptr <- peekByteOff pt 8  -- NOTE: Why is 8 bytes here?
                  ss <- peekCString chptr
                  print $ "f_setMyThink=" ++ ss
                  return (MyBox ss)
  
  
foreign import ccall "myArray"
    c_myArray::Ptr CInt -> IO()

f_myArray::IO [Int]
f_myArray = do
             alloca $ \pt -> do
               c_myArray pt
               arr <- peekArray 10 pt
               return $ map fromIntegral arr

{-|

  allocaArray :: forall a  b. Storable a => CInt -> (Ptr a -> IO b) -> IO b
                        ↑  ↑
                        a  b  are different type
-}
foreign import ccall "quick_sort"
    quick_sort_c :: Ptr CInt -> CInt -> CInt -> IO()

quick_sort_f::[Int] -> IO [Int]
quick_sort_f cx = do
  let lo' = fromIntegral 0
  let hi' = fromIntegral $ length cx - 1
  let len = length cx
  allocaArray len $ \pt -> do
    pokeArray pt (map fromIntegral cx)
    quick_sort_c pt lo' hi'
    arr <- peekArray len pt
    return $ map fromIntegral arr
  
foreign import ccall "myCStringToString"
    c_myCStringToString::Ptr CChar -> IO()

f_myCStringToString::String -> String
f_myCStringToString c = do
  unsafePerformIO $
    withCString c $ \cs ->
      c_myCStringToString cs >> peekCString cs
  
foreign import ccall "alloca_memory"
    c_alloca_memory::Ptr CInt -> CInt -> IO()

{-|
-- NOTE: Need to free(pt) here
f_alloca_memory::Int -> IO [Int]
f_alloca_memory n = do
  pt <- mallocBytes n
  c_alloca_memory pt (fromIntegral n)
  arr <- peekArray n pt
  return $ map fromIntegral arr
-}

-- pt is freed after (Ptr a -> IO b) is terminated
f_alloca_memory::Int -> IO [Int]
f_alloca_memory n = do
  allocaBytes n $ \pt -> do
    c_alloca_memory pt (fromIntegral n)
    arr <- peekArray n pt
    return $ map fromIntegral arr

instance Storable MyThink where
  alignment _ = 8
  sizeOf _ = 12
  peek ptr = MyThink
       <$> peekByteOff ptr 0
       <*> peekByteOff ptr 4
  poke ptr (MyThink nX ptrX) = do
    pokeByteOff ptr 0 nX
    pokeByteOff ptr 4 ptrX
    
  
{-|

   peek ptr = MyStruct <$> peekByteOff ptr 0
            = map MyStruct $ peekByteOff ptr 0
-}
instance Storable MyStruct where
        alignment _ = 8    -- lease common multiple of the alignments of struct fields.
        sizeOf _    = 16  -- Double 8 bytes, Word8 4 byte (padding), Int32 4 bytes = 16 bytes
        peek ptr    = MyStruct
            <$> peekByteOff ptr 0
            <*> peekByteOff ptr 8
            <*> peekByteOff ptr 12 -- skip padding bytes after "c"
        poke ptr (MyStruct d c i) = do
            pokeByteOff ptr 0 d  
            pokeByteOff ptr 8 c  -- 0 - 8   => 8 bytes d
            pokeByteOff ptr 12 i -- 4 - 12  => 4 bytes c
                                 -- 12 - 16 => 4 byte  i
instance Storable TimeInfo where
  alignment _ = 8
  sizeOf _ = 56
  peek ptr = TimeInfo
       <$> peekByteOff ptr 0
       <*> peekByteOff ptr 4
       <*> peekByteOff ptr 8
       <*> peekByteOff ptr 12
       <*> peekByteOff ptr 16
       <*> peekByteOff ptr 20
       <*> peekByteOff ptr 24
       <*> peekByteOff ptr 28
       <*> peekByteOff ptr 32
       <*> peekByteOff ptr 40
       <*> peekByteOff ptr 44
       -- <*> peekCAString (castPtr $ plusPtr ptr 44)
  poke ptr (TimeInfo sec min hour mday mon year wday yday isdst gmtoff zone) = do
       pokeByteOff ptr 0 sec
       pokeByteOff ptr 4 min
       pokeByteOff ptr 8 hour
       pokeByteOff ptr 12 mday
       pokeByteOff ptr 16 mon
       pokeByteOff ptr 20 year
       pokeByteOff ptr 24 wday
       pokeByteOff ptr 28 yday
       pokeByteOff ptr 32 isdst
       pokeByteOff ptr 40 gmtoff
       pokeByteOff ptr 44 zone

-- KEY: pass struct pointer to C function
instance Storable Stuff where
  sizeOf s = 4
  alignment _ = 4
  poke p stuff = undefined
  peek p = undefined


main = do 
        n <- f_increment 10 
        print n
        print "done!"
        m <- f_incrementPt
        print m
        mystruct <- f_setMyStruct
        print mystruct

        arr <- f_myArray
        print arr

        arr2 <- f_alloca_memory 12
        print arr2

        timeInfo <- f_get_time_struct
        print timeInfo
        ss <- peekCString $ tm_zone timeInfo
        print $ "tm_zone String=" ++ ss

        print $ f_myCStringToString "abcd efghijkl mnopq rst uvw xyz"
        think <- f_setMyThink
        -- print think
        rst <- peekCString $ ptrX think
        print $ "rst=" ++ rst

        th2 <- f_setMyThink2
        print th2
        n <- peekByteOff foo 0
        print "ok"
        print (n :: Int)

        when True $ do
          arr <- quick_sort_f [1]
          print arr
  
        when True $ do
          arr <- quick_sort_f [1, 3, 2]
          print arr
  
        when True $ do
          arr <- quick_sort_f [1, 3, 2, 4, 9, 0, 1, 1, 0]
          print arr
        m1 <- readFile2d "/tmp/a" :: IO[[Int]]
        let m2 = join m1
        when True $ do
          arr <- quick_sort_f m2
          print arr
        print "ok"

        when True $ do
          m1 <- readFile2d "/tmp/b" :: IO[[Int]]
          let m2 = join m1
          old <- timeNowSecond
          let qm = qs m2
          pp $ show . len $ qm
          new <- timeNowSecond
          let diff = new - old
          pp $ "qs m2 => diff=" ++ (show diff) ++ " qm=" ++ (show . len $ qm)
  
        when True $ do
          m1 <- readFile2d "/tmp/b" :: IO[[Int]]
          let m2 = join m1
          old <- timeNowSecond
          qm <- quick_sort_f m2
          pp $ show . len $ qm
          new <- timeNowSecond
          let diff = new - old
          pp $ "quick_sort_f m2 => diff=" ++ (show diff) ++ " qm=" ++ (show . len $ qm) 



qs::[Int] -> [Int]
qs []     = []
qs (x:cx) =  (qs l) ++ [x] ++ (qs r)
  where
    l = [ y | y <- cx, y <= x]
    r = [ y | y <- cx, y >  x]

fun99 x = x
fun88 x y = x + y
s = [2, 1]

#if 0
main = do
       m1 <- readFile2d "/tmp/b" :: IO[[Int]]
       let m2 = join m1
       defaultMain [
        bgroup "kk" [
             bench "fun99" $ whnf fun99 1
            ,bench "quick_sort_f m2" $ nfIO $ do
                                                s <- quick_sort_f m2
                                                print $ len s
            ,bench "qs m2" $ whnfIO $ do
                                       let ss = qs m2
                                       print $ len ss
                                  
                   ]

        ]

#endif
