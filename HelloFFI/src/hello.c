#include<stdio.h>
#include<stdint.h>
#include<stdlib.h>
#include<time.h>
#include<string.h>


int foo = 4;

int increment(int n) {
  return n + 1;
}

void incrementPt(int* pt){
  int n = *pt;
  *pt = n + 1;
}

typedef struct MyStuff{
  int n;
} Stuff;


typedef struct MyStruct{
  double d;
  char c;
  int i;
} MyStruct;

typedef struct MyThing{
  int n;
  // char arr[10];
  // NOTE: Need to free it somewhere in Haskell side
  // https://stackoverflow.com/questions/43372363/releasing-memory-allocated-by-c-runtime-from-haskell
  char* arr;
} MyThing;

void setMyThink(MyThing* ptr){
  ptr -> n = 100;
  // NOTE: Need to free it somewhere in Haskell side
  // https://stackoverflow.com/questions/43372363/releasing-memory-allocated-by-c-runtime-from-haskell
  ptr -> arr = (char*)malloc(sizeof(char)*10);
  strcpy(ptr -> arr, "abcd");
}

void free_setMyThink(MyThing* ptr){
  free(ptr -> arr);
  free(ptr);
}

void setStuff(Stuff *pt){
  pt -> n = 9;
}

// KEY: read struct
void setMyStruct(MyStruct *pt){
  pt -> d = 3.33;
  pt -> c = 'a';  // 97
  pt -> i = 31415;
}

// KEY: ffi read array
void myArray(int array[10]){
  for(int i = 0; i < 10; i++)
    array[i] = i;
}

// KEY: Mutate a Haskell String in C
void myCStringToString(char* arr){
  for(int i = 0; i < strlen(arr); i++){
	arr[i] = arr[i] + 1;
  }
}

void myDynamicPt(int* pt){
  int len = 10;
  pt = (int*)malloc(sizeof(int)*len);
  for(int i = 0; i < len; i++){
    pt[i] = i;
  }
  if(pt)
    free(pt);
}

// KEY: allocate and free memory in Haskell, set value in C function
void alloca_memory(int* pt, int len){
  for(int i = 0; i < len; i++){
    pt[i] = i;
  }
}


/**
    Use Haskell:
	allocaArray - allocate memory in Haskell
	quick Sort in C
	peekArray   - read n elements from allocaArray
 */
void swap(int* arr, int i, int j){
  int tmp = arr[i];
  arr[i] = arr[j];
  arr[j] = tmp;
}

/**
   Partition an array with pivot, return index of the pivot
   1, 2, 4, 3
   1 < 3
            1
   2 < 3
   1  2 
   4 > 3
   1   2    4
   3 == 3
         3
   1 2	 3  4

   x
   2 3
   .
     x
   2 3
     .
	 
   
   1 3 2
   x
     x
	   .
   1 2 3
       x
 */
int partition_array(int* arr, int lo, int hi){
  int bigInx = lo;
  int pivotInx = lo;
  int pivot = arr[hi];
  for(int i = lo; i <= hi; i++){
	if(arr[i] <= pivot){
	  swap(arr, bigInx, i);
	  pivotInx = bigInx;
	  bigInx++;
	}
  }
  return pivotInx;
}


void quick_sort(int* arr, int lo, int hi){
  if(lo < hi){
	int pivotInx = partition_array(arr, lo, hi);
	quick_sort(arr, lo, pivotInx - 1);
	quick_sort(arr, pivotInx + 1, hi);
  }
}


void get_time_struct(struct tm* st){
  	time_t rawtime;
	struct tm * timeinfo;

	time(&rawtime );
	timeinfo = localtime ( &rawtime );
	printf( "Current local time and date: %s", asctime (timeinfo) );
	printf("tm_sec=%d\n", timeinfo -> tm_sec);
	printf("tm_min=%d\n", timeinfo -> tm_min);
	printf("tm_hour=%d\n", timeinfo -> tm_hour);
	printf("tm_mday=%d\n", timeinfo -> tm_mday);
	printf("tm_mon=%d\n", timeinfo -> tm_mon);
	printf("tm_year=%d\n", timeinfo -> tm_year);
	printf("tm_wday=%d\n", timeinfo -> tm_wday);
	printf("tm_yday=%d\n", timeinfo -> tm_yday);
	printf("tm_isdst=%d\n", timeinfo -> tm_isdst);
	printf("tm_gmtoff=%ld\n", timeinfo -> tm_gmtoff);
	printf("tm_zone=%s\n", timeinfo -> tm_zone);

	st -> tm_sec = timeinfo -> tm_sec;
	st -> tm_min = timeinfo -> tm_min;
	st -> tm_hour = timeinfo -> tm_hour;
	st -> tm_mday = timeinfo -> tm_mday;
	st -> tm_mon = timeinfo -> tm_mon;
	st -> tm_year = timeinfo -> tm_year;
	st -> tm_wday = timeinfo -> tm_wday;
	st -> tm_yday = timeinfo -> tm_yday;
	st -> tm_isdst = timeinfo -> tm_isdst;
	st -> tm_gmtoff = timeinfo -> tm_gmtoff;

	// ERROR:
	// NOTE: Need to free it somewhere in Haskell
	st -> tm_zone = (char*)malloc(sizeof(char)*10);
	strcpy(st -> tm_zone, timeinfo -> tm_zone);
	printf("st -> tm_zone=%s\n", timeinfo -> tm_zone);
	// st -> tm_zone = timeinfo -> tm_zone;

	free(timeinfo);
}
