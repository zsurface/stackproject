{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_HelloFFI (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/HelloFFI/.stack-work/install/x86_64-osx/264c4daf1a1f22c7a990e1ba79cc8b60568c047743e377fea72aca935dfa2262/8.6.5/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/HelloFFI/.stack-work/install/x86_64-osx/264c4daf1a1f22c7a990e1ba79cc8b60568c047743e377fea72aca935dfa2262/8.6.5/lib/x86_64-osx-ghc-8.6.5/HelloFFI-0.1.0.0-sij6j5NUxp7MP2MxgARQ0-HelloFFI"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/HelloFFI/.stack-work/install/x86_64-osx/264c4daf1a1f22c7a990e1ba79cc8b60568c047743e377fea72aca935dfa2262/8.6.5/lib/x86_64-osx-ghc-8.6.5"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/HelloFFI/.stack-work/install/x86_64-osx/264c4daf1a1f22c7a990e1ba79cc8b60568c047743e377fea72aca935dfa2262/8.6.5/share/x86_64-osx-ghc-8.6.5/HelloFFI-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/HelloFFI/.stack-work/install/x86_64-osx/264c4daf1a1f22c7a990e1ba79cc8b60568c047743e377fea72aca935dfa2262/8.6.5/libexec/x86_64-osx-ghc-8.6.5/HelloFFI-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/HelloFFI/.stack-work/install/x86_64-osx/264c4daf1a1f22c7a990e1ba79cc8b60568c047743e377fea72aca935dfa2262/8.6.5/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "HelloFFI_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "HelloFFI_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "HelloFFI_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "HelloFFI_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "HelloFFI_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "HelloFFI_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
