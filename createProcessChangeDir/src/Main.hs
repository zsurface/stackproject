-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent
import System.Posix.Process
 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

{--
main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
--}

-- URL: https://stackoverflow.com/questions/53258843/system-process-how-to-keep-the-shell-session-instead-of-creating-a-new-process
-- KEY: change dir, change shell dir
  
ourShell :: CreateProcess
ourShell =  (proc "/usr/local/bin/bash" []) { std_in = CreatePipe, std_out = CreatePipe }

{--
main = do
  (Just bashIn, Just bashOut, Nothing, bashHandle) <- createProcess ourShell

  hSetBuffering bashIn NoBuffering
  hSetBuffering bashOut NoBuffering

  print "Sending pwd"
  hPutStrLn bashIn "pwd"
  print "reading response"
  hGetLine bashOut >>= print

  print "Sending cd /tmp"
  hPutStrLn bashIn "cd /tmp"
  print "reading response"
--  hGetLine bashOut >>= print you need to know there is no answer ....

  print "Sending pwd"
  hPutStrLn bashIn "pwd"
  print "reading response"
  hGetLine bashOut >>= print

  print "Run vim /tmp/x.x"
  hPutStrLn bashIn "vim /tmp/a.x"
  -- hGetLine bashOut >>= print
  
  -- pp "I will exit"
  -- hPutStrLn bashIn "exit"
  -- hGetContents bashOut >>= print
  ec <- waitForProcess bashHandle
  print ec
--}
main = do
     pp "Ok"
     s <- forkProcess $ executeFile "vim" True ["/tmp/a.x"] Nothing
     pp $ show s
