{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_try (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/try/.stack-work/install/x86_64-osx/326f03724afa4dabbd806931a68fc66e6bb04c0fa58dfb1d12dbc0c3a55d0044/8.10.3/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/try/.stack-work/install/x86_64-osx/326f03724afa4dabbd806931a68fc66e6bb04c0fa58dfb1d12dbc0c3a55d0044/8.10.3/lib/x86_64-osx-ghc-8.10.3/try-0.1.0.0-151mTHqUSNOBsVuSpPMoyO-try"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/try/.stack-work/install/x86_64-osx/326f03724afa4dabbd806931a68fc66e6bb04c0fa58dfb1d12dbc0c3a55d0044/8.10.3/lib/x86_64-osx-ghc-8.10.3"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/try/.stack-work/install/x86_64-osx/326f03724afa4dabbd806931a68fc66e6bb04c0fa58dfb1d12dbc0c3a55d0044/8.10.3/share/x86_64-osx-ghc-8.10.3/try-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/try/.stack-work/install/x86_64-osx/326f03724afa4dabbd806931a68fc66e6bb04c0fa58dfb1d12dbc0c3a55d0044/8.10.3/libexec/x86_64-osx-ghc-8.10.3/try-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/try/.stack-work/install/x86_64-osx/326f03724afa4dabbd806931a68fc66e6bb04c0fa58dfb1d12dbc0c3a55d0044/8.10.3/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "try_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "try_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "try_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "try_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "try_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "try_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
