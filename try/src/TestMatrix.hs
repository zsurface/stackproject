{-# LANGUAGE FlexibleContexts          #-}
{-# LANGUAGE MultiWayIf                #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings         #-}
{-# LANGUAGE QuasiQuotes               #-}
{-# LANGUAGE TypeFamilies              #-}
{-# LANGUAGE ScopedTypeVariables #-}

{-# LANGUAGE RankNTypes #-}   -- forall

module Main where

-- Sat 10 Dec 14:18:31 2022
-- BUG: There is bug between CPP and Text.RawString.QQ
-- {-# LANGUAGE CPP #-}
--
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-}
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List
-- import Control.Monad
-- import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
-- import qualified Data.List as L
-- import Data.List.Split
-- import Data.Time
-- import Data.Time.Clock.POSIX
-- /Users/aaa/myfile/bitbucket/stackproject/AronModuleLib/src/xx2-2023-02-27-02-10-34.x
import           Control.Monad                (join, unless, when)
import           Control.Monad.Primitive
import           Data.Char
import           System.Directory
import           System.Environment
import           System.Exit
import           System.FilePath.Posix
import           System.IO
import           System.Posix.Files
import           System.Posix.Unistd
import           Text.Format
import           Text.Printf
import qualified Data.List.Split as DLS
import Graphics.Rendering.OpenGL

-- import System.Process
-- import Text.Read
-- import Text.Regex
-- import Text.Regex.Base
-- import Text.Regex.Base.RegexLike
-- import Text.Regex.Posix
-- import Data.IORef
-- import Control.Concurrent



import           AronAlias
import           AronHtml2
-- import           AronDevLib                   
import           AronModule                   hiding (CodeBlock (..), watchDir)
-- import AronHtml
import           AronGraphic
import           AronOpenGL
import           AronSymbolicAlgebra
import           AronToken
import           AronFFI
  
-- import GenePDFHtmlLib
-- import WaiLib
-- import WaiConstant

-- import qualified Diagrams.Prelude as DIA
-- import Diagrams.Backend.SVG.CmdLine
-- import Diagrams.TrailLike
import           Control.Applicative
import qualified Data.Map.Strict              as M
import qualified Data.Text                    as TS
import           Diagrams.Backend.SVG.CmdLine
import           Diagrams.Prelude             hiding (blue, getSub, pre, trim, diff, rewrite)
import qualified System.Console.Pretty        as PRE
import           Text.RawString.QQ
import qualified Text.Regex.Applicative.Text  as TA
import qualified Text.Regex.TDFA              as TD

import Data.Functor.Identity

-- /Users/aaa/myfile/bitbucket/stackproject/AronModuleLib/src/xx-2023-02-27-02-06-14.x
import qualified Data.Vector                  as V
import qualified Data.Vector.Mutable          as MV

import Control.Monad.ST
import Data.Array.MArray
import Data.Array.IO


import System.FSNotify
import Control.Concurrent (threadDelay)
import Control.Monad (forever)
import qualified Numeric.Matrix as NM

main = do
       s1 <- geneRandMat (1000, 1000)
       s2 <- geneRandMat (1000, 1000)
       let m1 = NM.fromList s1 :: NM.Matrix Double
       let m2 = NM.fromList s2 :: NM.Matrix Double
       t_old <- timeNowMicro
       let m3 = NM.times m1 m2
       let ls = NM.toList m3
       wfl "/tmp/x.x" $ map show ls 
       t_new <- timeNowMicro
       let t_diff = t_new - t_old
       fw "t_diff"
       print t_diff

       pp "Ok"
