
  
-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE BangPatterns #-}


-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Maybe (fromJust, isJust, fromMaybe)
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import qualified System.IO.Streams as Streams 
import qualified System.IO.Streams.File as Streams 
import System.Posix.Files
import System.Posix.Unistd
import System.Process
-- import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
-- import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Monad.ST
import Control.Concurrent
import Numeric
import Text.Read(readMaybe)
import Graphics.Rendering.OpenGL hiding (get)


import           Data.Int (Int64)
import           Database.SQLite.Simple
import           Database.SQLite.Simple.FromRow
import           Database.SQLite.Simple.ToRow
import           Database.SQLite.Simple.FromField
import           Database.SQLite.Simple.ToField
import           Database.SQLite.Simple.Internal
import           Database.SQLite.Simple.Ok

import qualified Text.Printf as PR

import qualified Data.Aeson      as DA
import qualified GHC.Generics         as GEN
import qualified Data.Text as TS
import qualified Text.Regex.TDFA as TD

-- import Data.Array
-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

-- BEG Geometry Lib
import Control.Monad (unless, when)
import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S
import qualified Data.List as DL
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import Data.Typeable
import qualified Text.Printf as PR
import Text.RawString.QQ (r)         -- Need QuasiQuotes too 
-- import Language.Haskell.Interpreter 

import GHC.Float.RealFracMethods
import Control.Monad.State hiding (lift, modify, get)
import Control.Monad.Identity
import qualified Data.HashSet as DH (fromList, difference, intersection, toList)
import Data.String
import Text.Regex.TDFA

import Control.Monad.Trans
import System.Console.Haskeline

import qualified Data.ByteString           as BS  -- Strict ByteString
import qualified Data.ByteString.Char8     as BSC
import qualified Data.ByteString.UTF8      as BSU
  
import Data.CaseInsensitive (CI)
  

-- import Graphics.UI.GLUT
-- import Graphics.UI.GLUT.Callbacks.Global
import AronModule hiding (rw)
-- import AronOpenGL
import AronDevLib

import qualified Data.Vector as VU
import qualified Data.Vector.Mutable as DVM
import Data.Typeable
-- END Geometry Lib
  
import qualified Data.ByteString.Lazy.Char8 as L8
import           Network.HTTP.Simple
import qualified Data.ByteString.Lazy.Internal as LI

-- BEGIN_Haskeline test
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.State (StateT, evalStateT, get, modify)
import Data.List (isPrefixOf)
import System.Console.Haskeline

import qualified Control.Monad.State as CMS
import Control.Monad.IO.Class

import Control.Monad.ST
import Data.STRef
import Control.Monad
-- END_Haskeline test

import System.Random


import AronAlias
import AronModule
import AronDevLib
import AronFFI

main :: IO ()
main = do
       m <- geneRandMat (1000, 1000) 
       let m' = (map . map) fi m

--        old <- timeNowMicro
--        new <- timeNowMicro
--        let diff = new - old
--        let n = head $ join m1
--        print $ "n=" ++ (show n)
       
       old <- timeNowMicro
       -- let !m2 = multiMat m m
       m2 <- f_multiMat m' m'
       wfl "/tmp/x.x" $ map show m2
       new <- timeNowMicro
       let diff = new - old
       let k = (head . head) m2
       let u = (last . last) m2
       print $ "k=" ++ show k 
       print $ "u=" ++ show u 
       print $ " diff=" ++ (show diff)


















