module Main where

import Graphics.Rendering.Cairo


x0 = 0
y0 = 10
w0 = 500
h0 = 500

bg :: Render ()
bg = do
  setSourceRGBA 0 0 0 1
  rectangle x0 y0 w0 h0
  fill

drawSquare :: Render ()
drawSquare = do
  -- setSourceRGBA 1 1 0 1
  let px = x0 + (w0 / 2)
  let py = y0
  let h = 40
  let width = 10
  let height = 40
  let margin = 1
  
  mapM_ (\x -> do
           setSourceRGBA 1 (0.1*x) (0.2*x) 1
           rectangle (px - width/2) ((py + (h + margin) * x) + y0) width height
           stroke
        ) [0..3]
  
  setSourceRGBA 0.5 0.5 0 1
  setLineWidth 2
  
  moveTo (w0/2) y0
  lineTo ((w0/2) + (0.8*w0/2)) 100
  lineTo (w0/2) (y0 + 100 + 20)
  stroke
  setSourceRGBA 0.5 0.5 0 1
  moveTo (w0/2) y0
  lineTo ((w0/2) - (0.8*w0/2)) 100
  lineTo (w0/2) (y0 + 100 + 20)
  stroke
  
  setSourceRGBA 0 0.5 0.5 1
  rectangle (w0/4) (y0 + 80) 20 50
  stroke
  
  setSourceRGBA 0 0.5 0.5 1
  rectangle ((w0/4) + (w0/2)) (y0 + 80) 20 50
  stroke
  
  -- rectangle 10 10 50 50
  -- fill

sketch :: Render ()
sketch = bg >> drawSquare

main :: IO ()
main = do
  surface <- createImageSurface FormatARGB32 500 500
  renderWith surface sketch
  surfaceWriteToPNG surface "out.png"
