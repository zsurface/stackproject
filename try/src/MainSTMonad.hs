{-# LANGUAGE InstanceSigs #-}

module Main where


import Control.Monad.ST
import Data.STRef
import Control.Monad 
import Control.Monad.IO.Class
import Control.Lens

import Data.Array
import Data.List
import AronModule
import AronAlias
import AronFFI 

-- KEY: st monad

-- data ST s a
-- runST :: ST s a -> a
-- newSTRef :: a -> ST s (STRef s a)
-- modifySTRef :: STRef s a -> (a -> a) -> STRef s ()
-- readSTRef :: STRef s a -> ST s a

sumST :: (Num a) => [a] -> a 
sumST xs = runST $ do
    n <- newSTRef 1 
    forM_ xs $ \x -> do
      modifySTRef n (+x)
    readSTRef n


multiVecX ::(Num a) => [[a]] -> [a] -> [[a]]
multiVecX m v = tran $ [map (\r -> sum $ r * v) m]

multiVecX2 ::(Num a) => [[a]] -> [a] -> [a] 
multiVecX2 m v = map (\r -> sum $ r * v) m

main = do
  when True $ do
    ls <- geneRandMat (100, 100) <&> join
    t_old <- timeNowMicro
    let s = sumST ls 
    wfl "/tmp/x.x" [show s]
    t_new <- timeNowMicro
    let t_diff = t_new - t_old
    fw "t_diff"
    print t_diff 
  when True $ do
    let s = [1, 2, 3]
    let m1 = join $ multiVecX mat s
    fw "m1"
    print m1
    let ls = multiVecX2 mat s
    print ls 
  when True $ do
    t_old <- timeNowMicro
    forM_ [1..1000] $ \x -> do
      s <- f_dot [123.00, 4440.033, 3] [123.00, 4440.033, 3] 
      wfl "/tmp/x.x" [show s]
      return ()
    t_new <- timeNowMicro
    let t_diff = t_new - t_old
    fw "t_diff 1 = "
    print t_diff 

  when True $ do
    t_old <- timeNowMicro
    forM_ [1..1000] $ \x -> do
      let r = [123.00, 4440.033, 3] 
      let s = r * r
      wfl "/tmp/x.x" [show s]
      return ()
    t_new <- timeNowMicro
    let t_diff = t_new - t_old
    fw "t_diff 2 = "
    print t_diff 
    pp "Ok" 
