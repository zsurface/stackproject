-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
{-# LANGUAGE RankNTypes #-}

-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Maybe (fromJust, isJust, fromMaybe)
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import qualified System.IO.Streams as Streams 
import qualified System.IO.Streams.File as Streams 
import System.Posix.Files
import System.Posix.Unistd
import System.Process
-- import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
-- import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Monad.ST
import Control.Concurrent
import Numeric
import Text.Read(readMaybe)
import Graphics.Rendering.OpenGL hiding (get)


import           Data.Int (Int64)
import           Database.SQLite.Simple
import           Database.SQLite.Simple.FromRow
import           Database.SQLite.Simple.ToRow
import           Database.SQLite.Simple.FromField
import           Database.SQLite.Simple.ToField
import           Database.SQLite.Simple.Internal
import           Database.SQLite.Simple.Ok

import qualified Text.Printf as PR

import qualified Data.Aeson      as DA
import qualified GHC.Generics         as GEN
import qualified Data.Text as TS
import qualified Text.Regex.TDFA as TD

-- import Data.Array
-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

-- BEG Geometry Lib
import Control.Monad (unless, when)
import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S
import qualified Data.List as DL
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import Data.Typeable
import qualified Text.Printf as PR
import Text.RawString.QQ (r)         -- Need QuasiQuotes too 
-- import Language.Haskell.Interpreter 

import GHC.Float.RealFracMethods
import Control.Monad.State hiding (lift, modify, get)
import Control.Monad.Identity
import qualified Data.HashSet as DH (fromList, difference, intersection, toList)
import Data.String
import Text.Regex.TDFA

import Control.Monad.Trans
import System.Console.Haskeline

import qualified Data.ByteString           as BS  -- Strict ByteString
import qualified Data.ByteString.Char8     as BSC
import qualified Data.ByteString.UTF8      as BSU
  
import Data.CaseInsensitive (CI)
  

-- import Graphics.UI.GLUT
-- import Graphics.UI.GLUT.Callbacks.Global
import AronModule hiding (rw)
-- import AronOpenGL
import AronDevLib

import qualified Data.Vector as VU
import qualified Data.Vector.Mutable as DVM
import Data.Typeable
-- END Geometry Lib
  
import qualified Data.ByteString.Lazy.Char8 as L8
import           Network.HTTP.Simple
import qualified Data.ByteString.Lazy.Internal as LI

-- BEGIN_Haskeline test
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.State (StateT, evalStateT, get, modify)
import Data.List (isPrefixOf)
import System.Console.Haskeline

import Control.Monad.ST
import Data.STRef
import Control.Monad
-- END_Haskeline test


import AronAlias
import AronModule
import AronDevLib

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

#if 0 
-- zo - open
-- za - close
loop::Int -> IO()
loop x = do
  if x > 0 then do
    pp x
    loop (x - 1)
    else return ()


f::() -> Int -> Int
f () a = a + 1

infix 1 †
(†)::Bool -> a -> a -> a
(True  † a)  _  = a
(False † _)  b  = b

-- (ρ)::() -> Int -> Int
-- (ρ) () a = a + 1
-- f:: (a -> b -> c) -> [(Int, a)] -> [(Int, b)] -> [[c]]
-- f f z1 z2 =
--  where
--    a1 = fst z1
--    b1 = fst z2
--    a2 = snd z1
--    b2 = snd z2
--    ss = [[(a1, b1, f a2 b2)
                  
(⊗)::[a] -> [b] -> [[(Int, Int, (a, b))]]
(⊗) cx cy = ss
  where
    z1 = zip [0..] cx
    z2 = zip [0..] cy
    ss = [[let a1 = fst a
               b1 = fst b
               a2 = snd a
               b2 = snd b
           in (a1, b1, (a2, b2)) | a <- z1] | b <- z2]

 
(⊖)::(Int -> Int -> Bool) -> [[(Int, Int, b)]] -> [(Int, Int, b)]
(⊖) f cx = ls
  where
    ls = filter (\(a, b, _) -> f a b) $ join cx

(⊗⊖) :: Int -> Int -> Int
(⊗⊖) a b = 4114

strToListFloat::String -> [Float]
strToListFloat s = map(\x -> read x :: Float) $ splitStr "[, ]" s

splitListBefore::RegexStr -> [String] -> [[String]]
splitListBefore rx cx = tx
  where
  tup = map(\(n, s) -> if matchTest (mkRegex rx) s then (n, True) else (n, False)) $ zip [0..] cx
  ft = filter (\(_, t) -> t) tup
  h = head ft
  ft' = if fst h /= 0 then (0, True):ft else ft
  tu = zip (init ft') (tail ft')
  tu' = map(\(a, b) -> (fst a, fst b)) tu
  tx = map(\(a, b) -> takeIndexBetweenInc (a, b-1) cx) tu'

  
validateFormat::String -> Bool
validateFormat s = if len ls > 1 && (len . sp . head) ls > 2 then True else False
  where
    ls = lines s
    sp e = splitStr ":" e                   


sumList::[Int] -> Int -> Int
sumList [] s = s
sumList (x:cx) s = if x < 0 then s else sumList cx (s + x)

sumTilNegative::[Int] -> Int
sumTilNegative = go 0
  where
    go !total rest =
      case rest of
        [] -> total
        x:cx
         | x < 0 -> total
         | otherwise -> go (total + x) cx

--                 cx       init   sum
sumUntilNegative2::[Int] -> Int -> Int
sumUntilNegative2 [] n = n
sumUntilNegative2 (x:cx) n = sumUntilNegative2 cx (sum n x) 
  where
    sum total e =
      if e < 0 then total
      else (total + e)     
    
-- 10-02-2021
-- KEY: monad transform
--
-- f::(b -> a -> Either b b)
-- foldl::(a -> a -> a) -> ini -> [a] -> a
-- foldl(\a acc -> a + acc) ini [1, 2]
-- foldl(\a acc -> a - acc) 0 [1, 2] => -3
--
sumTilNegative'::[Int] -> Int
sumTilNegative' = foldTerminate f 0
  where
    f accum x
      | x < 0     = Left accum
      | otherwise = Right (accum + x)

sumTilGreater5::[Int] -> Int
sumTilGreater5 =
  foldTerminate f 0
  where
    f accum x
      | x < 5     = Left accum
      | otherwise = Right (accum + x)

foldTillA::[String] -> String
foldTillA =
  foldTerminate f ""
  where
    f acc x
      | x == "A"  = Left acc
      | otherwise = Right (acc ++ x)
    
--          f::(b -> a -> Either b b) go :: b -> [a] -> b
foldTerminate::(b -> a -> Either b b) -> b -> [a] -> b
foldTerminate f = go
  where
--  go::  b   [a]
    go !accum rest =
      case rest of
        []   -> accum
        x:cx ->
          case f accum x of
            Left accum' -> accum'
            Right accum' -> go accum' cx 


foldTerminate2::(b -> a -> Either b b) -> b -> [a] -> b
foldTerminate2 f accum list0 =
  either id id (go accum list0)
  where
    go !accum rest = do  -- Either b (), NOT IO ()
      (x, xs) <-
        case rest of
          [] -> Left accum
          x:xs -> Right (x, xs)
      accum' <- f accum x
      go accum' xs
            
foldTerm::(b -> a -> Maybe b) -> b -> [a] -> b
foldTerm f = go
  where
    go !accum rest =
      case rest of
        []   -> accum
        x:cx ->
          case f accum x of
            Nothing -> accum
            Just accum' -> go accum' cx
    
-- s0 -> s1 -> s2
--     = do
--    (a1, s1) <- random s0
--    (a2, s2) <- random s1
--    return (a2, s2)
-- modify'::(Monad m) => (s -> s) -> m (a, s)
-- newtype State s = StateT s Identity
-- runStateEither :: StateEither s e a -> s -> m(s, Either e a)
-- newtype StateEither s e a = StateEither {runStateEither :: s -> m (s, Either e a)}

foldTill::(String -> Bool) -> [String] -> String
foldTill g =
  foldTerm f ""
  where
    f acc x
      | g x == True = Nothing
      | otherwise = Just (acc ++ x)
    
f1::Int -> (Int, Int)
f1 x = let x1 = x + 1
           x2 = x + 2 in (x1, x2)

f2::Int -> Identity(Int, Int)
f2 x = do
  let x1 = x1 + 1
  let x2 = x1 + 2
  return $ (x1, x2)
                         
f3::Int -> Identity(Int, Int)
f3 x = do
  x1 <- return (x + 1)
  x2 <- return (x + 2)
  return (x1, x2)


-- newtype Identity a = Identity { runIdentity :: a}
--                                 runIdentity :: Identity a -> a

-- newtype StateT s m a = StateT {runStateT:: s -> m(a, s)}
--                        runStateT::StateT s m a -> s -> m(a, s)
--                        StateT s m a => m (a, s)
--                        s -> m(a, s) => StateT s m a
--
--                        StateT(\s -> m (a, s + 1)) => StateT s m a
--                        runStateT::StateT s m a -> s -> m(a, s)

-- type State s = StateT s Identity

-- modify'::(Monad m) => (s -> s) -> StateT s m ()
-- evalState::State s a -> s -> a
-- execState::State s a -> s -> s
--            ( ↑      )
--              – - - StateT s Identity a
--
foldState::(b -> a -> b) -> b -> [a] -> b
foldState f accum0 list0 =
    execState (mapM_ go list0) accum0
--     go x = (mapM_ modify' (\accum -> f accum x) list0)
--  execState (mapM_ modify' (\accum -> f accum x) list0) accum0
  where
    go x = modify' (\accum -> f accum x)
--        |----->  State s m () <--------|
--  go 2 = modify' (\3 -> (+) 3 2) => State 5 m ()
--                 
type MyM a = Maybe a
    
-- data Either a b = Left a | Right b

-- [{ab}
-- 
fun3::String -> Int
fun3 [] = 0
fun3 (x:cx) = if x == '[' then 1 + (fun3 cx) else fun3 cx



data Personx = Personx{alive::Bool, age::Int} deriving (Show)

input::String -> String -> Either String Personx
input alive ageStr = do
  alive <- case readMaybe alive of
           Nothing -> Left "Invalid Alive"
           Just x  -> return x

  age <- case readMaybe ageStr of
           Nothing -> Left "Invalid Age"
           Just x -> return x

  if age < 0 then Left "Age can not be negative."
                  else return ()

  return Personx{alive, age}


removeIndex2::(Int, Int) -> a -> [[a]] -> [[a]]
removeIndex2 (cInx, rInx) a = undefined

{-|

   @
   const::a -> b -> a
   const 2 => 2

   const x :: b -> x

   const 2 => 2
   else x


   [1 2 3]
   [3 4 5]       c = 1
   [4 5 9]

   replaceMe p (const 2) cx

   (const 2) :: b -> 2

   replaceMe2d v (c, r) cxx = replaceMe replaceMe c (const (replaceMe c ))

   p == c
        f x
        replaceMe r (const v)
          p == r
              b -> v
   @
-}
replaceMe p f cx = [if p == i then f x else x | (x, i) <- zip cx [0..]]

replaceMe2d v (c, r) = replaceMe c (replaceMe r (const v))

data MyParse a = MyParse{myparse::String -> (a, String)}
-- myparse :: MyParse a ->  (String -> (a, String))

-- NOTE: It DOES NOT works so far.
{-|
-}
findIdentBlock::[String] -> [String]
findIdentBlock [] = [] 
findIdentBlock (x:cs) | fst (isUnGuardedRhs x) = findIdentBlock cs
                      | fst (isIdent x)        = x : allIdent cs
                      | isOtherType x    = findIdentBlock cs
                      | isLeftBracket x  = findIdentBlock cs
                      | otherwise        = []

allBlocks::[String] -> [[String]]
allBlocks cx = zb
  where
    blocks = blockUnGuardedRhs cx
    bb = reverse $ blocks ++ [[]]
    b1 = init bb
    b2 = tail bb
    zb = zipWith (\x2 x1 -> dropEnd (len x1) x2) b2 b1

{-|
   ["a", "ab", "abc"]

   ["a",   "ab"]
   ["ab", "abc"]


   ["", "a", "ab", "abc"]

   ["",  "a", "ab"]
   ["a", "ab", "abc"]

   ["a" - "", "ab" - "a", "abc" - "ab"]

   ["a", "b", "c"]

   [("ab" - "a"), ("abc" - "ab")]
   ["b", "c"]
-}
blockUnGuardedRhs::[String] -> [[String]]
blockUnGuardedRhs [] = []
blockUnGuardedRhs (x:cs) = if bo then let f = innerUnGuardedRhs cs
                                          n = fst f
                                          s = snd f
                                      in if n == 0 then s:blockUnGuardedRhs cs else (x:s):(blockUnGuardedRhs cs)
                                 else blockUnGuardedRhs cs
  where
    bo = fst (isUnGuardedRhs x)

{-|
   http://localhost/image/hparse.png
-}
blockIdent::[String] -> [[String]]
blockIdent [] = []
blockIdent (x:cs) = [[]]
  

innerIdent::[String] -> (Int, [String])
innerIdent [] = (0, [])
innerIdent (x:cs) | isLeftBracket x =  ((fst fc) + 1, x:(snd fc))
                  | isRightBracket x = ((fst fc) - 1, x:(snd fc))
                  | otherwise = fc
  where
    fc = innerIdent cs

{-|
blockUnGuardedRhsList::[String] -> [[String]]
blockUnGuardedRhsList [] = []
blockUnGuardedRhsList (x:cs) = if bo then 
  where
    bo = fst (isUnGuardedRhs x) 
-}  
innerUnGuardedRhs::[String] -> (Int, [String])
innerUnGuardedRhs [] = (0, [])
innerUnGuardedRhs (x:cs) | isLeftBracket x =  ((fst fc) + 1, x:(snd fc))
                         | isRightBracket x = ((fst fc) - 1, x:(snd fc))
                         | otherwise = fc
  where
    fc = innerUnGuardedRhs cs
    
allIdent::[String] -> [String]
allIdent (x:cs) = if  (not . isLeftBracket) x then x : allIdent cs else  x : findIdentBlock cs

-- ) anything => True
isRightBracket::String -> Bool
isRightBracket [] = False
isRightBracket s = head s' == ')'
  where
    s' = trim s
  
-- ( anything => True
isLeftBracket::String -> Bool
isLeftBracket [] = False
isLeftBracket s = head s' == '('
  where
    s' = trim s
  
-- ( MyType => True
isOtherType::String -> Bool
isOtherType s = bracket == '(' && isMat
  where
    s' = trim s
    bracket = head s'
    ts = (trim . tail) s
    isMat = matchTest (mkRegex "[A-Z][[:alnum:]]+") ts

-- ( Ident  => True
isIdent::String -> (Bool, Char)
isIdent s = if len s > 0 then (bo, '(') else (False, '(')
  where
    s' = trim s
    bo = (s' !! 0) == '(' && matchTest (mkRegex "Ident" )  (trim $ drop 1 s')


-- ( UnGuardedRhs  => True
isUnGuardedRhs::String -> (Bool, Char)
isUnGuardedRhs s = if len s > 0 then (bo, '(') else (False, '(')
  where
    s' = trim s
    bo = (s' !! 0) == '(' && matchTest (mkRegex "UnGuardedRhs" )  (trim $ drop 1 s')

  
-- | Quaternion Implementation
data Quaternion = Quaternion{a::Float, b::Float, c::Float, d::Float} deriving (Show)

instance Num Quaternion where
    (Quaternion a1 b1 c1 d1) + (Quaternion a2 b2 c2 d2) = Quaternion(a1 + a2) (b1 + b2) (c1 + c2) (d1 + d2)
    (Quaternion a1 b1 c1 d1) - (Quaternion a2 b2 c2 d2) = Quaternion(a1 - a2) (b1 - b2) (c1 - c2) (d1 - d2)
    (Quaternion a  b  c  d)  * (Quaternion e  f  g  h)  = Quaternion(a*e - b*f - c*g -e*h) (b*e + a*f -e*g + c*h) (c*e + e*f + a*g -b*h) (d*e -c*f + b*g + a*h)
    abs _                                               = undefined
    signum _                                            = undefined
    fromInteger _                                       = undefined


{-|
    === KEY: quaternion conjugate

    \[
        q  = a + b i + c j + d k \\
        q* = a - b i - c j - d k \\
    \]
-}
conjugateQ::Quaternion -> Quaternion
conjugateQ q = Quaternion{a = a q, b = -(b q), c = -(c q), d = -(d q)}

unitQ::Quaternion -> Quaternion
unitQ q = Quaternion{a = a'/l, b = b'/l, c = c'/l, d = d'/l}
  where
    a' = a q
    b' = b q
    c' = c q
    d' = d q
    l  = sqrt $ a'^2 + b'^2 + c'^2 + d'^2
    
qToPureQ::Quaternion -> Quaternion
qToPureQ q = Quaternion{a = 0, b = b q, c = c q, d = d q}

inverseQ::Quaternion -> Quaternion
inverseQ q = Quaternion{a = (a q')/ds, b = (b q')/ds, c = (c q')/ds, d = (d q')/ds}
  where
    q' = conjugateQ q
    ds = (a q)^2 + (b q)^2 + (c q)^2 + (d q)^2

normQ::Quaternion -> Float
normQ q = sqrt $ a'^2 + b'^2 + c'^2 + d'^2
  where
    a' = a q
    b' = b q
    c' = c q
    d' = d q

data MyType = T1 (Integer -> Integer)

{-|
instance Num MyType where
  ....
-}

fun1::Integer -> IO()
fun1 n = print n
         
fun2::String -> IO()
fun2 s = print s

fun33::Integer -> Integer
fun33 x = x + 1

refillEmptyTitle:: String -> String -> String
refillEmptyTitle t u = if (null . trim) t then
                         if (upperStr $ takeEnd 5 u) == ".HTML" then takeName $ dropEnd 5 u else takeName u
                       else
                         t


{-|
    === KEY:

   
   
   @

   Input: "(  )"
   
   alg ')' (Nothing, []) => (Just 1, ")", [])
    alg '(' (Just 1, ")", []) => (Nothing, ('(':")":[]))) => (Nothing, [["()"]])


  
              

                            alg                   (Just (1, ")"), [])
                     ')'       (Nothing, [])

                                   alg                      (Nothing, ('(':x)):xs) => (Nothing, "()")
                            '('        (Just (1, ")"), [])

   Input: "()()"
   alg ')' (Nothing, []) => (Just 1, ")", [])
     alg '(' (Just 1, ")", []) => (Nothing, ('(':")":[]))) => (Nothing, [["()"]])
       alg ')' (Nothing, [["()"]]) => (Just (1, ")"), [["()"]])
         alg '(' (Just (1, ")"), [["()"]]) => (Nothing, ('(':")"):[["()"]]) => (Nothing, [["()"], ["()"]]


  print $ bb "()("

   @
   
-}
bb::String -> (Maybe (Integer, String), [String])
bb = foldr alg (Nothing, [])
 where
  -- alg '(' (Just (1, x), xs) = (Nothing, ('(':x):xs)
  alg '(' (Just (n, x), xs) = let k = n - 1 in if k == 0 then (Just (k, []), ('(':x):xs) else (Just (k, '(':x), xs)
  alg ')' (Nothing, xs)     = (Just (1, ")"), xs)
  alg ')' (Just (n, x), xs) = (Just (n + 1, ')':x), xs)
  alg '(' (Nothing, xs)     = (Just (100000, "("), xs)   -- Never be balanced, cheating
  alg _   (y, xs)           = (y, xs)

checkBalance::String -> Bool
checkBalance s = case may of
                    Just x -> fst x == 0
                    Nothing -> False
  where
    (may, str) = bb s
    tup = fromJust may

checkBalanceList::[String] -> [(Bool, String)]
checkBalanceList [] = [(True, [])]
checkBalanceList (x:cs) = case may of
                            Just (n, s) -> if n /= 0 then (False, s):(checkBalanceList cs) else (True, s):(checkBalanceList cs)
                            Nothing     -> undefined
  where
    (may, str) = bb x

-- collectBracket::[String
-- collectBracket [] = []
-- collectBracket (x:cx) = 

-- {{{
{-|
    === KEY: connect island problem
     0 1 0 0
     0 1 1 0
     0 0 0 1

                   (c-1) r
       (c (r -1))  (c r)     c  (r + 1)
                   (c+1) r

-}
connectCount::(Num a, Eq a) => [[a]] -> Int
connectCount cx = f 0 0 cx
  where
    f c r cy = e == 1 ? c1 + c2 + c3 + c4 + 1 $ 0
      where
        c1 = c - 1 >= 0 ? (f (c-1) r cy') $ 0
        c2 = c + 1 <  h ? (f (c+1) r cy') $ 0
        c3 = r - 1 >= 0 ? (f c (r-1) cy') $ 0
        c4 = r + 1 <  w ? (f c (r+1) cy') $ 0
        h = len cy
        w = len (cy ! 0)
        cy' = replace2d 0 (c, r) cy
        e = cy ! c ! r
        (!) = (!!)

-- }}}
  

main = do
  -- pp "hi"
  -- pp $ 1 ↑ [1, 2]
  -- pp $ 2 ↓ [2, 3, 5]
  -- s <- putStr "proceed continue, continuous, Everret, population 98k"
  -- case s of
       -- () -> print "unit type"
  -- pp $ f () 3
  -- pp $ ρ [1, 2]
  -- 3 == 4 ? print "ok" $ print "not"
  -- let ss = (⊗) [0, 1, 2] [0, 1, 2]
  -- pre ss
  -- fl
  -- pp $ (⊖) (\x y -> x > y) ss
  -- fl
  -- pp $ (⊗⊖) 1 1
  -- pp $ strToListFloat "1 2 3 0.0"
  -- pp $ takeIndexBetweenInc (3, 4) [0..10]
  -- ls <- rfl "/tmp/what.org"
  -- let t = map(\(n, s) -> if matchTest (mkRegex "^\\*\\*\\*") s then (n, s, 1) else (n, s, 0)) $ zip [0..] ls
  -- let ft = filter(\(n, s, i) -> i == 1) t
  -- let h = head ft
  -- let ft' = if t1 h /= 0 then (0, "kk", 1):ft else ft
  -- fl
  -- pp t
  -- pp "ok"
  -- fl      
  -- pp ft'
  -- let tu = zip (init ft') (tail ft')
  -- let tu' = map(\(a, b) -> (t1 a, t1 b)) tu
  -- let tx = map(\(a, b) -> takeIndexBetweenInc (a, b-1) ls) tu'
  -- fl
  -- pp tu
  -- fl
  -- pp tu'
  
  -- fl
  -- pre tx
  -- let tx' = map(\x -> x ++ [""]) tx
  -- wfl "/tmp/k.org" $ join tx'
  -- let sortls = qqsort (\x y -> head x <= head y) tx'
  -- wfl "/tmp/k1.org" $ join sortls
  -- pp "ok"
  -- getTime >>= \x -> printBox 4 [x]

  -- fl
  -- let str = "mytest_test:*: test it\nline on here"
  -- pp $ lines str
  -- pp $ validateFormat str
  -- let sub = dropEnd 1 $ drop 1 $ show "\\"
  -- pp $ len sub
  -- ls <- replaceFileLine "abc" sub  "/tmp/11.x"
  -- pre ls
  -- fl
  -- writeFileList "/tmp/22.x" ls
  {-|
  pp "hi"
  putStrLn $ show $ sumList [1, 2, (-3)] 0
  pp $ sumTilNegative [1, 2, 99, (-3), 4]
  pp $ runIdentity $ f3 1
  fl
  pp $ runIdentity $ f3 2
  pp $ foldState (+) 0 [1, 2]
    
  -- modifyIORef :: IORef a -> (a -> a) -> IO ()
  -- modify'     ::(Monad m) => (s -> s) -> StateT s m ()
  let go f x = modify'(\accum -> f accum x)
  mapM_ putStrLn ["desiccant", "Bovine -> cattle group and buffaloes or buffalos or bison"]
  pp "done"
  ref <- newIORef 3
  num <- readIORef ref
  pp num
  modifyIORef ref (+1)
  num' <- readIORef ref
  pp num'
  pp $ sumUntilNegative2 [1, 2, -1, 4] 0
  pp $ sumTilGreater5 [5, 6, 7, 1, 9]
  pp $ foldTillA ["a", "b", "A", "c"]
  pp $ foldTill (\x -> x == "A") ["a", "b", "A", "Bovine"]

  -- text <- readFile "/tmp/apl.el"
  -- writeFile "/tmp/x2.x" (unescape text)
  let skk = unescape "\\x2206"
  putStrLn skk
  let ch = chr $ read ("0x" ++ "2206")
  print [ch]
  pp $ hexToInt "2206"
  pp $ intToHex 8710
  fl

  pp $ (isBalanced3 "[" "" ) == False
  pp $ (isBalanced3 "[]" "" ) == True
  pp $ (isBalanced3 "[[]" "" ) == False
  pp $ (isBalanced3 "[[]]" "" ) == True
  pp $ (isBalanced3 "[[[]]" "" ) == False
  fl
  pp $ (isBalanced3 "[{" "" ) == False
  pp $ (isBalanced3 "[{}]" "" ) == True
  pp $ (isBalanced3 "[{]}" "" ) == False
  pp $ (isBalanced3 "{[[]}]" "" ) == False
  pp $ (isBalanced3 "[[[]]}" "" ) == False
  fl
  pp $ input "true" "4"
  pp $ input "True" "4"
  print "ok"
  pp $ showIntAtBaseX 2 intToDigit 10 ""
  pp $ showIntAtBaseX 10 intToDigit 10 ""
  pp $ showIntAtBaseX 8 intToDigit 10 ""
  pp $ showIntAtBaseX 16 intToDigit 32 ""
  pp $ showIntAtBaseX 16 intToDigit 0 ""
  pp $ showIntAtBaseX 16 intToDigit (-1) ""
  -}
  
  print "ok"
  let q1 = Quaternion{a = 1, b = 1, c = 1, d = 1}
  print q1
  let uq = conjugateQ q1
  print $ "uq = " ++ show uq
  pp "uq * q1"
  print $ show $ uq * q1
  print $ show $ unitQ q1
  print $ show $ qToPureQ q1
  pp "inverseQ"
  print $ inverseQ q1
  print $ show $ q1 * (inverseQ q1)
  fl
  let v1 = (Vector3 1.0 1.0 1.0) :: Vector3 GLfloat
  let v2 = (Vector3 1.0 1.0 1.0) :: Vector3 GLfloat
  print $ replaceMe 1 (const 2) [1, 1, 1, 1] == [1, 2, 1,1]
  print $ replaceMe 5 (const 2) [1, 1, 1, 1] == [1, 1, 1, 1]
  print $ replaceMe2d 5 (2, 1) [[1, 1, 1], [1, 1, 1], [1, 1, 1]] == [[1, 1, 1], [1,1,1], [1,5,1]]

  let fun x = if | x < 0   ->  -1
                 | x == 0  ->  0
                 | otherwise -> 1

  print "ok"
  let ls2 = [[1, 1],
             [0, 1]
            ]
  print $ connectCount ls2 == 3
  
  let ls1 = [[0]]
  print $ connectCount ls1 == 0
  
  let ls3 = [[1]]
  print $ connectCount ls3 == 1

  let title = "xfido"
  let url = "http://what.com/nice/file.html"
  pp "none empty title"
  print $ refillEmptyTitle title url
  fl
  pp "empty title"
  print $ refillEmptyTitle " " url
  fl
  pp "not html"
  let nohtml = "http://what.com/nice/file"
  print $ refillEmptyTitle " " nohtml
  fl
  
  let nohtml = "http://what.com/nice/file.js"
  print $ refillEmptyTitle "" nohtml
  print "--"
  
  -- ls <- readFileList "/tmp/x.x"
  -- pre ls
  -- let lss = filter (\x -> fst (isUnGuardedRhs x)) ls
  -- let lst = filter (\x -> fst (isIdent x)) ls
  -- pre $ findIdentBlock ls
  -- fl
  -- let zb = allBlocks ls
  -- fl
  -- fw "zb"
  -- pre zb
  
  print $ fileSizeStrToNum "3M"
  print $ fileSizeStrToNum "3K"
  fw "bb"
  print $ bb "(a)"
  print $ bb "(a(b))"
  print $ bb "(())() ((()))"
  print $ bb "(()"
  print $ bb "))"
  print $ bb "())"
  print $ bb "()("
  
  -- print $ checkBalanceList ["(", ")"]
  -- print $ checkBalanceList ["(", "("]
  -- print $ checkBalanceList ["(", "x"]
  
  fw "checkBalance"
  print $ checkBalance "()" == True
  print $ checkBalance "()a" == True
  print $ checkBalance "() " == True
  print $ checkBalance "()(" == False
  print $ checkBalance "() )" == False
  print $ checkBalance "(())" == True
  print $ checkBalance "()())" == False
  print $ checkBalance "()(())" == True
  print $ checkBalance ")(" == False
  print $ checkBalance "(()(" == False
  print $ checkBalance "(()())" == True
  fw "bb"
  print $ bb "()("
  fw "end checkBalance"
  
  let c = C{x = 1, y = 3}
  let c2 = c + c
  
  print c2
  print $ c * c
  print $ mag (c * c)

  home <- getEnv "HOME"
  osMap <- confMap configFile
  let userinputdb = lookupJust dbname osMap
  pp userinputdb
  let host         = lookupJust "host" osMap  -- localhost
  let snippet      = lookupJust "snippetpath" osMap
  let portStr      = lookupJust "port" osMap  -- 8081
  let useSnippet   = lookupJust "readSnippetFile" osMap
  let datadir      = lookupJust "datadir" osMap
  let datadirlatex = lookupJust "datadirlatex" osMap
  let hostName     = "http://" ++ host ++ ":" ++ portStr  -- http://localhost:8081
  let port = read portStr :: Int
  putStrLn userinputdb
  conn <- open $ home </> userinputdb 
  -- createCodeBlockTable conn
  let query = Query{ fromQuery = strToStrictText "CREATE TABLE IF NOT EXISTS CodeBlock (id INTEGER PRIMARY KEY AUTOINCREMENT, header TEXT, codeblock TEXT, addedtime DATETIME DEFAULT (strftime('%s', 'now')), score INTEGER DEFAULT 0)"}
  -- let query = Query{fromQuery = strToStrictText "CREATE TABLE IF NOT EXISTS CodeBlock (id INTEGER PRIMARY KEY AUTOINCREMENT, header TEXT, codeblock TEXT)"}
  execute_ conn query
  pp "ok"

  
  let par = MyParse(\x -> (x, "a"))
  let fun = myparse par
  pp "done"

  let s9 = ["dog", "cat"]
  forM_ s9 $ (\x -> do
                putStrLn x
             ) 
             
  s10 <-  forM s9 $ (\x -> do
                       return x
                    ) 
  pre s10
  pp "ok"

#endif

--{-|
--    === Read table like string from a file 
---}
--readTable::FilePath -> IO [[String]]
--readTable fp = rfl fp >>= return . alignTable 
--
--{-|
--    === KEY: alignment, column table, align column, format table
--
--    @
--    table 1:
--    "a  b   c"
--    "e f  g"
--      ↓ 
--
--    table 1:
--    "a" "b" "c"
--    "e" "f" "g"
--
--    @
---}
--alignTable::[String] -> [[String]]
--alignTable cx = tran m
--    where
--        ls = ft (\u -> len u > 0) $ map (\t -> ft (\x -> lent x > 0) $ splitSPC t) cx 
--        ls' = tran ls
--        ml s = foldr (\a b -> max a b) 0 s
--        ms = map (\r -> ml r ) $ (map . map) len ls'
--        pad n s = let d = n - len s in s ++ rep d ' '
--        m = map (\r -> let n = ml $ len <$> r in map(\s -> pad (n + 1) s) r) ls'
--

data MyBox = MyBox{
  editorbeg::Integer,
  editorend::Integer,
  editorfile::String,
  myarray::[String]
} deriving (Show, GEN.Generic)

instance DA.ToJSON MyBox where
  toEncoding = DA.genericToEncoding DA.defaultOptions

instance DA.FromJSON MyBox

{-|
main = do
  when True $ do
    pp "okkk"  
    decodeStr <- jsonToRecord "/tmp/x.json" :: IO (Maybe MyBox)
    case decodeStr of
      Nothing -> Prelude.putStrLn "Not a Valid JSON file"
      (Just x) -> Prelude.putStrLn $ show x
-}

{-|
main = do
        print "ok"
        let cx = ["a", "b"]
        let cy = ["a", "c"]
        let s1 = DH.fromList cx 
        let s2 = DH.fromList cy 
        let ix = DH.intersection s1 s2
        let ls = DH.toList $ (DH.difference s1 ix) :: [String]
        let lt = DH.toList $ (DH.difference s2 ix) :: [String]
        pre ls
        pre lt
        fw "diffList"
        pre $ diffList cx cy
-}

mytest :: ST s (DVM.MVector s Int)
mytest = do
  v <- DVM.new 10
  DVM.write v 0 100
  DVM.write v 1 200
  return v

test2 :: ST s Int
test2 = do
  v <- mytest
  DVM.read v 0

  
{--
main = do
        -- let b = "ok" =~ s :: TS.Text 
        -- pre b
        -- pre $ "alexis-de-tocqueville" TD.=~ "[a-z]+" :: String
        -- d
        pp "kk"
        let vec = runST test2
        print $ typeOf vec
        print vec
--}

{--
main = do
    hSetBuffering stdin NoBuffering
    getUntilEsc ""

getUntilEsc :: String -> IO ()
getUntilEsc acc = do
    c <- getChar
    case c of
      '\ESC' -> return ()
      '\n' -> do putStrLn $ "You entered [" ++ reverse acc ++ "]"

                 getUntilEsc ""
      _ -> getUntilEsc (c:acc)

--}
  


type Repl a = InputT IO a

process :: [String] -> IO ()
process cx = mapM_ putStrLn cx

repl :: [String] -> Repl ()
repl acc = do
  minput <- getInputLine "> "
  case minput of
    Nothing -> outputStrLn "Goodbye."
    Just s -> do
                  case s of
                       v | v == "ok" -> outputStrLn "ok, done"
                         | v == ":run" -> (liftIO $ process $ reverse acc) >> repl []
                         | v == ":cr" -> repl []
                         | v == ":ls" -> do
                                         ls <- liftIO $ run "ls";
                                         mapM_ put ls;
                                         repl []
                         | v == ":fi" -> do
                                         ls <- liftIO $ readFileList "/tmp/a.x"
                                         mapM_ put ls
                                         repl []
                         | containPrefix ":hsc" v -> do
                                         let s = trim $ drop (len ":hsh") v
                                         cx <- liftIO $ run $ "hsc " + s
                                         mapM_ put cx
                                         repl []
  
                         | otherwise -> repl (s:acc)
  where
    (+) = (++)
    put = outputStrLn

{--
main :: IO ()
main = do
       runInputT defaultSettings $ repl []
--}       

splitChar :: (Eq a) => a -> [a] -> [[a]]
splitChar c s = case break (== c) s of
                 (ls, []) -> [ls]
                 (ls, x:cx) -> ls : splitChar x cx

{--
main :: IO ()
main = do
       let s = "abcd efgh ijkl mnop qrst uvwx yz"
       pp $ splitPrefix (\x -> x /= ' ') s 
       pp $ splitChar ' ' s 
       case break (== ' ') s of
          (v, w) | v == "a" -> print v
                 | otherwise -> print "not ok"

--}

--    [Input Stream]  <-> [ Output Stream ]
-- list of lines as input stream
-- connect the input stream to output stream

{-|
  
 KEY: it is based on stream library
 https://hackage.haskell.org/package/io-streams-1.5.2.2/docs/System-IO-Streams-Tutorial.html
-}


grepLinex :: (String -> Bool) -> FilePath -> IO()  -- grepLine (hasStr "abc") "/tmp/a.x"
grepLinex f fp = withFile fp ReadMode $ \h -> do
                 is <- Streams.handleToInputStream h >>= Streams.lines >>= Streams.filter f'
                 os <- Streams.unlines Streams.stdout
                 Streams.connect is os
                 s <- hTell h
                 print s
    where
      f' x = f $ toStr x


{--
catx :: FilePath -> IO ()
catx file = withFile file ReadMode $ \h -> do
    is <- Streams.handleToInputStream h
    Streams.connect is Streams.stdout
--}

{--
main :: IO ()
main = do
       -- grepLine "/tmp/a.x" $ hasStr "abc" 
       let fp = "/tmp/a.x"
       let p = "abc"
       grepLinex (\x -> x == p) fp
--}

ss = "\9017\9018\9071\9037\9025\9056\9027\9016\9028\9072\9020\9044\9026\9043\9019\9036\247\9050\8900\9048\9066\8594\8867\8722\8802\8800\9049\9035\8710\8802\9068\9070\8592\8804\8801\8805\9065\9053\9014\9014\9055\8854\9061\9033\9052\9675\8868\168\9067\8739\9078\9078\9082\8892\8743\8592\9024\8869\9109\8594\8743\9066\9066\8968\9033\8854\9061\9022\9055\9021\9052\9052\9675\9066\9053\9023\8869\9058\9042\9067\9035\9049\9049\8710\8711\8801\8867\168\9050\9050\8900\8757\8835\247\9017\8595\8744\8746\8868\9046\9073\9062\9057\9045\9041\9046\8595\168\8834\8868\8712\9079\9079\8712\236\9024\8902\9079\8593\8970\9045\9058\8592\8594\9042\9035\8968\9065\8805\9061\9060\9075\9015\9075\8745\8745\8744\9080\9080\9075\8592\9060\9051\9051\8728\9066\9053\8592\8834\8867\9029\9063\9029\8804\8970\8866\9055\9055\9017\9017\8801\8968\8712\8712\8722\8970\8711\9074\8728\8722\9073\8800\8802\8764\9060\9051\9081\9081\9077\8893\8744\8728\8834\8757\9675\8835\8902\9032\9026\9020\9056\9037\9044\9018\9017\9047\9036\9016\9028\9019\9031\9027\9044\9071\9072\9032\9025\9040\9043\9031\9027\9028\9109\9054\9048\9048\9060\247\9023\9023/\8868\9076\8739\9021\9076\8594\8835\8866\9030}\9030\8728?\8854\9021\9024\9070\9076\215\9023\9023\9024\9064\9057\9059\9015\9015\9059\8902\9069\8739\8593\9064\8764\215\9033\8712\8746\8593\8743\8745\8869\9039\9074\9053\9038\9034\9039\9077\215\9068\8866\9069\9064\9074\9073\8764"

{--
main :: IO ()
main = do
       -- let ls = filter (\(a, _) -> a == '/') $ map(\x -> (x, charToInt x)) ss
       let ls = map(\(n, x) -> (n, x, [chr $ ord x])) $ zip [1..] ss
       pre ls
       writeFileList "/tmp/k.x" $ [show ls]
--}  

{--
main :: IO ()
main = do
    req <- parseRequest "https://httpbin.org/get"
    response <- httpLBS req
    pp "ok"

    let content_type =  Data.String.fromString "Content-Type" :: CI BS.ByteString
    print content_type
    pp "Ok"
    
    putStrLn $ "The status code was: " ++ show (getResponseStatusCode response)
    
    print $ getResponseHeader content_type response
    L8.putStrLn $ getResponseBody response


getContentsCTRLD :: IO String;
getContentsCTRLD = do
                   getChar >>= \c -> 
                     if c == '\EOT' 
                         then return "" 
                         else getContentsCTRLD >>= \s -> 
                         return (c:s)

    
  
main :: IO ()
main = do
       pp "ok"
       s <- getContentsCTRLD
       pp s



hasSubstr :: String -> String -> Bool
hasSubstr a s = S.member a set 
    where
        set = S.fromList $ head $ substr (len a) s 

--}

data TodoRec = TodoRec { pid :: Integer, time :: Integer, todo :: String } deriving (Show, Eq)

sumST :: Num a => [a] -> a
sumST xs = runST $ do
  n <- newSTRef 0
  forM_ xs $ \x -> do
    modifySTRef n (+x)
  readSTRef n

{--
main :: IO ()
main = do
       pp "ok"
       let s = [1, 2, 3] :: [Int]
       pre $ sumST s
       print $ hasSubstr "ab" "xxabb" == True
       print $ hasSubstr "axb" "xxabb" == False 
       print $ hasSubstr "" "xxabb" == True 
       print $ hasSubstr "xab" "xxabb" == True 
--}

type StateData = [String]


replx :: StateData -> IO ()
replx startState
  = flip evalStateT startState
  $ runInputT replSettings loop
  where
    loop :: InputT (StateT StateData IO) ()
    loop = do
      inputL <- getInputLine "> "
      case inputL of
        Nothing -> pure ()
        Just "quit" -> outputStrLn "--Exited--"
        Just ipt -> do
          -- Just add each entry to the state directly.
          lift $ modify (ipt :)
          loop

replSettings :: Settings (StateT StateData IO)
replSettings = Settings
  { complete       = replCompletion
  , historyFile    = Just "history.txt"
  , autoAddHistory = True
  }

replCompletion :: CompletionFunc (StateT StateData IO)
replCompletion = completeWordWithPrev Nothing " " completionGenerator

completionGenerator :: String -> String -> StateT StateData IO [Completion]
completionGenerator prefix suffix = do
  st <- get
  -- Trivial completion that just ignores the suffix.
  pure $ fmap (\ s -> Completion s s True)
    $ filter (prefix `isPrefixOf`) st
{--
main :: IO ()
main = replx []
--}

pop :: StateT [Integer] IO Integer
pop = do
      (x:xs) <- get
      put xs 
      return x

-- liftIO ::(MonadIO m) => IO a -> m a
io :: IO a -> StateT [Integer] IO a 
io = liftIO

code :: StateT [Integer] IO () 
code = do
       x <- pop
       io $ print x
       y <- pop 
       io $ print y
       return ()

main :: IO ()
main = do 
       pp "ok"
