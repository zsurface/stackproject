-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE RankNTypes #-} 
{-# LANGUAGE ScopedTypeVariables #-}
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

import AronModule 

import           Codec.Picture
import           Codec.Picture.Extra hiding (rotateLeft90, rotateRight90)
import           Codec.Picture.Drawing
import           Codec.Picture.Types
import           Control.Monad.Primitive

main :: IO ()
main = do
  args <- getArgs
  case len args of
       v | v == 1 -> case head args of
                           "-h" -> do
                                   print "imageSize -h => help"
                                   print "imageSize /tmp/img.png => 100 300"
                                   print "imageSize -s /tmp/img.png => (w=100, h=300)"
                           
                           _    -> do
                                   (w, h) <- imageSize (head args)
                                   putStrLn $ (show w) ++ " " ++ (show h)
                     
         | v == 2 -> case head args of
                           "-s" -> imageSize (last args) >>= \(w, h) -> print $ "width=" ++ show w ++ " " ++ "height=" ++ show h
                           _    -> print "Unknown option"
         | otherwise -> print "Need one or two argument"
