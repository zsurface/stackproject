
{-# LANGUAGE TemplateHaskell, QuasiQuotes #-}
module Main where
import qualified Language.C.Inline as C


-- KEY: haskell c code
C.include "<stdio.h>"

main :: IO ()
main = do
     putStrLn "Pick your lucky number"
     number <- readLn
     [C.block| void {
         int n = 3;
         int m = 4;
         printf("Lucky number: %i\n", $(int number));
         printf("Add number: %i\n", n + m);
     } |]
     putStrLn "Wasn't that nice?"
