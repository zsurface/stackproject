
1. Append current cmd to [String]
2. Filter out [String] which only contains current prefix
--------------------------------------------------------------------------------
lift $ modify (ipt :) 

completionGenerator :: String -> String -> StateT StateData IO [Completion]
completionGenerator prefix suffix = do
  st <- get
  -- Trivial completion that just ignores the suffix.
  pure $ fmap (\ s -> Completion s s True)
    $ filter (prefix `isPrefixOf`) st
