
  
-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE BangPatterns #-}


-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Maybe (fromJust, isJust, fromMaybe)
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import qualified System.IO.Streams as Streams 
import qualified System.IO.Streams.File as Streams 
import System.Posix.Files
import System.Posix.Unistd
import System.Process
-- import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
-- import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Monad.ST
import Control.Concurrent
import Control.Lens
import Numeric
import Text.Read(readMaybe)
import Graphics.Rendering.OpenGL hiding (get)


import           Data.Int (Int64)
import           Database.SQLite.Simple
import           Database.SQLite.Simple.FromRow
import           Database.SQLite.Simple.ToRow
import           Database.SQLite.Simple.FromField
import           Database.SQLite.Simple.ToField
import           Database.SQLite.Simple.Internal
import           Database.SQLite.Simple.Ok

import qualified Text.Printf as PR

import qualified Data.Aeson      as DA
import qualified GHC.Generics         as GEN
import qualified Data.Text as TS
import qualified Text.Regex.TDFA as TD

-- import Data.Array
-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

-- BEG Geometry Lib
import Control.Monad (unless, when)
import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S
import qualified Data.List as DL
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import Data.Typeable
import qualified Text.Printf as PR
import Text.RawString.QQ (r)         -- Need QuasiQuotes too 
-- import Language.Haskell.Interpreter 

import GHC.Float.RealFracMethods
import Control.Monad.State hiding (lift, modify, get)
import Control.Monad.Identity
import qualified Data.HashSet as DH (fromList, difference, intersection, toList)
import Data.String
import Text.Regex.TDFA

import Control.Monad.Trans
import System.Console.Haskeline

import qualified Data.ByteString           as BS  -- Strict ByteString
import qualified Data.ByteString.Char8     as BSC
import qualified Data.ByteString.UTF8      as BSU
  
import Data.CaseInsensitive (CI)
  

-- import Graphics.UI.GLUT
-- import Graphics.UI.GLUT.Callbacks.Global
import AronModule hiding (rw)
-- import AronOpenGL
import AronDevLib

import qualified Data.Vector as VU
import qualified Data.Vector.Mutable as DVM
import Data.Typeable
-- END Geometry Lib
  
import qualified Data.ByteString.Lazy.Char8 as L8
import           Network.HTTP.Simple
import qualified Data.ByteString.Lazy.Internal as LI

-- BEGIN_Haskeline test
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.State (StateT, evalStateT, get, modify)
import Data.List (isPrefixOf)
import System.Console.Haskeline

import qualified Control.Monad.State as CMS
import Control.Monad.IO.Class

import Control.Monad.ST
import Data.STRef
import Control.Monad
-- END_Haskeline test

import System.Random


import AronAlias
import AronModule
import AronDevLib
import AronFFI
import Data.Array 
import Data.Ix

matMult         :: (Ix a, Ix b, Ix c, Num d) =>
                   Array (a,b) d -> Array (b,c) d -> Array (a,c) d
matMult x y     =  array resultBounds
                         [((i,j), sum [x!(i,k) * y!(k,j) | k <- range (lj,uj)])
                                       | i <- range (li,ui),
                                         j <- range (lj',uj') ]
        where ((li,lj),(ui,uj))         =  bounds x
              ((li',lj'),(ui',uj'))     =  bounds y
              resultBounds
                | (lj,uj)==(li',ui')    =  ((li,lj'),(ui,uj'))
                | otherwise             = error "matMult: incompatible bounds"


main :: IO ()
main = do
       let ls = range (1, 4)
       print ls
       print "ok"
       when True $ do
         -- Create one dim array
         let s = array bnd [(i, i + 1) | i <- range bnd] where bnd = (1, 4)
         fw "Create one dim array"
         print s
         fw "index element"
         print $ s ! 1
       when True $ do
         fw "Create two dim array"
         let ls = array ((r0, r1), (s0, s1)) [((i, j), i + j) | i <- range(r0, s0), j <- range(r1, s1)] 
                  -- (1, 1), (4, 4)
                  -- [1..4] x [1..4] 
                    where
                    r0 = 1
                    r1 = 1
                    s0 = 4
                    s1 = 4
         print ls
         fw "index elem"
         print $ ls ! (1, 1) 
       when True $ do
         let ln = 100
         m1 <- geneRandMat (ln, ln) <&> join
         let ar1 = array ((0, 0),(ln - 1, ln - 1)) [((i, j), let ix = i * ln + j in m1 !! ix)
                                                    | i <- range (0, ln - 1),
                                                      j <- range (0, ln - 1)]

         let a0 = array ((0, 0), (1, 1)) [((i, j), i + j) | i <- range (0, 1), j <- range (0, 1)]
         let a1 = array ((0, 0), (1, 1)) [((i, j), i + j) | i <- range (0, 1), j <- range (0, 1)]
         let m = multiMatArr a0 a1
         print a0
         fw ""
         print a1
         fw "m"
         print m
         fw "ar1 x ar1"
         old <- timeNowMicro
         let m2 = multiMatArr ar1 ar1
         wfl "/tmp/x.x" $ map show $ elems m2
         new <- timeNowMicro
         let diff = new - old
         fw "diff="
         print diff
         -- print m2


       


















