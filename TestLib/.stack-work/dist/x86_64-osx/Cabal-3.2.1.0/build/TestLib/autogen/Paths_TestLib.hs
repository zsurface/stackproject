{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_TestLib (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/TestLib/.stack-work/install/x86_64-osx/98292e32af43bbd3109f2c3a836e226295705f8235a95bd05f8fe88db40a67eb/8.10.3/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/TestLib/.stack-work/install/x86_64-osx/98292e32af43bbd3109f2c3a836e226295705f8235a95bd05f8fe88db40a67eb/8.10.3/lib/x86_64-osx-ghc-8.10.3/TestLib-0.1.0.0-ISXs2hFwjcUAAX8CaT5gvJ-TestLib"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/TestLib/.stack-work/install/x86_64-osx/98292e32af43bbd3109f2c3a836e226295705f8235a95bd05f8fe88db40a67eb/8.10.3/lib/x86_64-osx-ghc-8.10.3"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/TestLib/.stack-work/install/x86_64-osx/98292e32af43bbd3109f2c3a836e226295705f8235a95bd05f8fe88db40a67eb/8.10.3/share/x86_64-osx-ghc-8.10.3/TestLib-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/TestLib/.stack-work/install/x86_64-osx/98292e32af43bbd3109f2c3a836e226295705f8235a95bd05f8fe88db40a67eb/8.10.3/libexec/x86_64-osx-ghc-8.10.3/TestLib-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/TestLib/.stack-work/install/x86_64-osx/98292e32af43bbd3109f2c3a836e226295705f8235a95bd05f8fe88db40a67eb/8.10.3/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "TestLib_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "TestLib_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "TestLib_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "TestLib_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "TestLib_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "TestLib_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
