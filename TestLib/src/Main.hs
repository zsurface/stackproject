-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"
{-# LANGUAGE QuasiQuotes       #-}
import Text.RawString.QQ (r)  

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import Data.Maybe (fromJust, isJust, fromMaybe)
       
import           Data.Int (Int64)
import           Database.SQLite.Simple
import           Database.SQLite.Simple.FromRow
import           Database.SQLite.Simple.FromField
import           Database.SQLite.Simple.ToField
import           Database.SQLite.Simple.Internal
import           Database.SQLite.Simple.Ok
                 
import qualified Data.HashMap.Strict as M

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 
import WaiLib
import Test.HUnit
import AronModule
import AronGraphic

type HMap2 = M.HashMap String [([String], Integer)]

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

-- | All the AronModule test cases are here.
-- | -------------------------------------------------------------------------------- 
-- | HUnit for unit testing 
-- | mergeSort
allTests = runTestTT tests 
        where
            test1 = TestCase (assertEqual "arithmetic test" 7 (3+4)) 
            test2 = TestCase (assertEqual "mergeSort" [3, 4] $ mergeSort [4, 3]) 
            test3 = TestCase (assertEqual "mergeSort" [3, 4] $ mergeSort [3, 4]) 
            test4 = TestCase (assertEqual "mergeSort" [] $ mergeSort []) 
            test5 = TestCase (assertEqual "mergeSort" [1] $ mergeSort [1]) 
            test6 = TestCase (assertEqual "mergeSort" [0..5] $ mergeSort [5, 3, 2, 4, 0, 1]) 

-- | The RIGHT boundary of interval will NOT be evaluated. [-2, 2) => 2 is NOT evaluated by the function
-- | This is why (-2.0) is only root can be found in f(x) = x^2 - 4 = 0 where x0 = -2, x1 = 2 . (f(x) = 0 has two roots: -2 and 2)
            test7 = TestCase (assertEqual "one root " (Just (-2.0)) $ let 
                                                                        f = \x-> x^2 -4; x0 = (-2); x1 = 2; eps = 0.001
                                                                    in 
                                                                        oneRoot f x0 x1 eps) 

            test8 = TestCase (assertEqual "one root " Nothing $ let 
                                                                        f = \x-> x^2 -4; x0 = (-4); x1 = (-2); eps = 0.001
                                                                    in 
                                                                        oneRoot f x0 x1 eps) 
            test9 = TestCase (assertEqual "one root " (Just 2)       $ let 
                                                                        f = \x-> x^2 -4; x0 = 2; x1 = 4; eps = 0.001
                                                                    in 
                                                                        oneRoot f x0 x1 eps)
            tests = TestList [
                            TestLabel "test1" test1,
                            TestLabel "test2" test2,
                            TestLabel "test3" test3,
                            TestLabel "test4" test4,
                            TestLabel "test5" test5,
                            TestLabel "test6" test6,
                            TestLabel "test7" test7,
                            TestLabel "test8" test8,
                            TestLabel "test9" test9
                            ] 
myTests = runTestTT tests 
        where
              -- See /Users/cat/myfile/bitbucket/database/insertData_CodeBlock.sql
            {--
            test5 = TestCase ( do ref <- newIORef M.empty
                                  home <- getEnv "HOME"
                                  let sqliteTestdb = "myfile/bitbucket/database/insertData_CodeBlock.db"
                                  conn <- open $ home </> sqliteTestdb
                                  newList <- readDatabaseCodeBlock3 conn
                                  fw "newList"
                                  pre newList
                                  let expected = [(["abc", "d"], (["line 1"], 1, 0, 0))]
                                  assertEqual "readDatabaseCodeBlock3" newList expected)

            -- See /Users/cat/myfile/bitbucket/database/insertData_CodeBlock2.db
            -- See /Users/cat/myfile/bitbucket/database/insertData_CodeBlock2.sql
            test6 = TestCase ( do ref <- newIORef M.empty
                                  home <- getEnv "HOME"
                                  let sqliteTestdb = "myfile/bitbucket/database/insertData_CodeBlock2.db"
                                  conn <- open $ home </> sqliteTestdb
                                  newList <- readDatabaseCodeBlock3 conn
                                  fw "newList"
                                  pre newList
                                  let expected = [(["abc", "d"], (["line 1"], 1, 0, 0)), (["abc", "e", "ab"], (["line 2"], 2, 0, 0))]
                                  assertEqual "readDatabaseCodeBlock3" newList expected)
            --}
            tests = TestList [
                            -- TestLabel "test5" test5,
                            -- TestLabel "test6" test6
                            ]

myTests1 = runTestTT tests 
        where
            test0 = TestCase ( do let strHTML = hiddenForm2 1 "hi"
                                  -- putStr strHTML
                                  let expHTML = [r|<form action="/update" name="Update" class="hf" id='f1' method="POST"><textarea name="header" rows="20" class="hide">hi</textarea><textarea name="myblock" spellcheck="false" autofocus="true" onfocus="textAreaAdjust(this);" id= 't1' class="hide">hi</textarea><div class="butcen">
        <input type="submit" name="update" value="update" id='b1' class="submitButton"> 
        <input type="submit" name="add"    value="add"    id='a1' class="submitButton"> 
        <input type="submit" name="delete" value="delete" id='d1' class="submitButton">
        <input type="button" name="myupdate" onclick="testfun('k1');"  value="myupdate" >
        </div></form>|]
                                  -- putStrLn strHTML
                                  assertEqual "The hiddenForm2 need to be updated, it has changed. [28-08-2020]" strHTML expHTML)
                                  -- assertEqual "hiddenForm2" 0 0)                                
            
            test1 = TestCase ( do let f = id
                                  let ls = [(["a"], 1, 0, 0), (["b"], 2, 0, 0)]
                                  let result = foldListList2 f ls
                                  fw "foldListList2 - result"
                                  putStrLn result
                                  assertEqual "foldListList2" 0 0)
            tests = TestList [
                            TestLabel "test0" test0,
                            TestLabel "test1" test1
                            ]
            
myTest2 = runTestTT tests 
        where
            gcdList_test0 = TestCase (assertEqual "gcdList0" 0 $ gcdList [])
            gcdList_test1 = TestCase (assertEqual "gcdList1" 1 $ gcdList [1])
            gcdList_test2 = TestCase (assertEqual "gcdList2" 2 $ gcdList [4, 2])
            gcdList_test3 = TestCase (assertEqual "gcdList3" 4 $ gcdList [4, 0])
            gcdList_test4 = TestCase (assertEqual "gcdList4" 2 $ gcdList [4, 2, 4])
            gcdList_test5 = TestCase (assertEqual "gcdList5" 2 $ gcdList [4, 2, 4])
            gcdList_test6 = TestCase (assertEqual "gcdList6" 1 $ gcdList [4, 2, 9])
            
            tests = TestList [
                            TestLabel "gcdList0" gcdList_test0,
                            TestLabel "gcdList1" gcdList_test1,
                            TestLabel "gcdList2" gcdList_test2,
                            TestLabel "gcdList3" gcdList_test3,
                            TestLabel "gcdList4" gcdList_test4,
                            TestLabel "gcdList5" gcdList_test5,
                            TestLabel "gcdList6" gcdList_test6
                            ]

-- type HMap2 = M.HashMap String [([String], Integer)]
myTest3 = runTestTT tests
        where
            test0 = TestCase( do
                                let x = 3
                                let y = 4
                                -- let s = [(["dog"], (["line1"], 3))]
                                let s = [(["dog"], (["line1"], 3, 9, 2)), (["dogs"], (["line2"], 4, 8, 3))]
                                let hmap = M.insert "dog" [(["a"], 1)]
                                ref <- newIORef M.empty
                                listToPrefixMap s ref
                                newmap <- readIORef ref
                                pre newmap
                                let mls = M.toList newmap
                                let expectedList = [
                                                    ("dog", [(["line1"], 3, 0, 0)]),
                                                    ("do",  [(["line1"], 3, 0, 0)]),
                                                    ("d",   [(["line1"], 3, 0, 0)])
                                                   ]
                                assertEqual "test0" expectedList mls
                                pp "ok"
                            )   
            tests = TestList [
                               TestLabel "listToPrefixMap" test0
                             ]
            
main = do
  -- myTests1
  -- myTests
  allTests
  myTest2
  myTest3
  print "done"
