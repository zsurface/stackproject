-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
{-# LANGUAGE CPP #-}
{-# LANGUAGE MultiWayIf #-}
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import qualified Data.Set as S 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

--removeFromList::String -> [String] -> [String]
--removeFromList s cx = left ++ right 
--    where
--        left = takeWhile (\x -> x /= s) cx
--        right = tail $ dropWhile (\x -> x /= s) cx

writeToHtmlTitle::String -> [String] -> IO ()
writeToHtmlTitle title cx = do
    let title' = removeSpace title
    let newName = "/tmp" </> "htmlTitle.txt"
    let htmlFile = "index" ++ title' ++ ".html"
    print $ "Generate Html file => " ++ htmlFile
    fullPath <-  getEnv "www" >>= \x -> return $ x </> "html" </> htmlFile
    p <- getEnv "sp" >>= \x -> return $ x ++ "/GeneXfidoHtml/src/htmlTitle.txt"
    copyFile p newName 
    removeFile p 
    writeFileList p cx 
    system  "GeneXfidohtml l"
    print p 
    system $ "echo " ++ fullPath ++ " | pbcopy"
    print $ "copy " ++ fullPath ++ " => clipboard"

--    printBox 2 ["vim " ++ fullPath ++ " ?  y => yes, ow No"] 
--    rs <- getLineX
--    when (rs == "y") $ do 
--        system $ "vim " ++ fullPath
--        pp "done"
--    pp "done!"

readHtmlTitle::IO[String]
readHtmlTitle = do 
        p <- getEnv "sp" >>= \x -> return $ x ++ "/GeneXfidoHtml/src/htmlTitle.txt"
        ls <- readFileList p
        return ls
main = do 
        argList <- getArgs
        pp $ length argList
        let n = len argList
        if | n == 2 -> do
                pp "2"
                let opt = head argList
                let title = last argList
                ls <- readHtmlTitle 
                if | opt == "-a" -> do
                    let lstr = (lowerStr . removeSpace) title
                    let bo = isInList lstr $ map (lowerStr . removeSpace) ls 
                    let ls' = insertIndexAt 0 title ls
                    let title' = removeSpace title
                    if bo then do 
                        error $ "ERROR: The title is duplicated =>" ++ title ++ " in htmlTitle.txt"
                        else do 
                            writeToHtmlTitle title ls'              
                   | opt == "-d" -> do                       
                        let ls' = removeFromList title ls
                        writeToHtmlTitle title ls' 
                        let base = "index" ++ removeSpace title
                        let htmlFile = base ++ ".html"
                        let orgFile  = base ++ ".org"
                        html <- getEnv "www" >>= \x -> return $ x </> "html"
                        let htmlPath = html </> htmlFile
                        let orgPath = html </> orgFile
                        mapM_ removeFile [htmlPath, orgPath]

                   | otherwise -> error $ "Invalid Option => " ++ opt

           | otherwise -> do
                printBox 2 ["-a => add"]
                printBox 2 ["-d => delete"]
                printBox 2 ["Need two arguments:  cmd [-a, -d] 'My New Page' "]

