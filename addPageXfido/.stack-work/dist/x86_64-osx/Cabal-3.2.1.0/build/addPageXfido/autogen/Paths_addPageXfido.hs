{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_addPageXfido (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/addPageXfido/.stack-work/install/x86_64-osx/9258dc9ee1958e214ca7fae1648f414aebd8be57d3f7163e2582076258199466/8.10.4/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/addPageXfido/.stack-work/install/x86_64-osx/9258dc9ee1958e214ca7fae1648f414aebd8be57d3f7163e2582076258199466/8.10.4/lib/x86_64-osx-ghc-8.10.4/addPageXfido-0.1.0.0-4NTrLyoj3XqD8RfJAEtIRy-addPageXfido"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/addPageXfido/.stack-work/install/x86_64-osx/9258dc9ee1958e214ca7fae1648f414aebd8be57d3f7163e2582076258199466/8.10.4/lib/x86_64-osx-ghc-8.10.4"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/addPageXfido/.stack-work/install/x86_64-osx/9258dc9ee1958e214ca7fae1648f414aebd8be57d3f7163e2582076258199466/8.10.4/share/x86_64-osx-ghc-8.10.4/addPageXfido-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/addPageXfido/.stack-work/install/x86_64-osx/9258dc9ee1958e214ca7fae1648f414aebd8be57d3f7163e2582076258199466/8.10.4/libexec/x86_64-osx-ghc-8.10.4/addPageXfido-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/addPageXfido/.stack-work/install/x86_64-osx/9258dc9ee1958e214ca7fae1648f414aebd8be57d3f7163e2582076258199466/8.10.4/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "addPageXfido_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "addPageXfido_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "addPageXfido_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "addPageXfido_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "addPageXfido_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "addPageXfido_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
