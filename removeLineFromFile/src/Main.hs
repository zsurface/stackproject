-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close


removeNonAsciiFromFile::FilePath -> IO()
removeNonAsciiFromFile fp = do
               let fn  = takeName fp 
               let tup = splitStrTuple "\\." fn
               let tmpFile = (dropName fp) + (fst tup) + "_tmp." + snd tup
               let copyFile = (dropName fp) + (fst tup) + "_copy." + snd tup
               pre tmpFile
               ls <- readFileLatin1ToList $ fp
               let fs = filter (\x -> containAscii x) ls
               writeFileList tmpFile fs
               exitCode <- system $ "cp " + fp + " " + copyFile
               if exitCode == ExitSuccess then do
                 run $ "rm " + fp
                 print "done"
                 run $ "mv " + tmpFile + " " + fp
                 pre $ "len=" + (show $ len fs)
                 print $ "copy of " + fp + " is in " + copyFile
               else print $ "Error: copy file" + fp + " to " + "/tmp"
        where
          (+) = (++)

main = do 
        argList <- getArgs
        let agLen = len argList
        if agLen == 1 || agLen == 2
        then do
                let fullName = head argList 
                let fn   = takeName fullName 
                let tup = splitStrTuple "\\." fn
                let tmpFile = (dropName fullName) + (fst tup) + "_tmp." + snd tup
                let copyFile = (dropName fullName) + (fst tup) + "_copy." + snd tup
                pre tmpFile
                ls <- readFileLatin1ToList $ head argList
                let fs = filter (\x ->
                                if agLen == 2 then (not $ matchTest (mkRegex (argList !! 1)) x) && containAscii x
                                              else containAscii x 
                                ) ls
                writeFileList tmpFile fs
                exitCode <- system $ "cp " + fullName + " " +copyFile
                if exitCode == ExitSuccess then do
                  run $ "rm " + fullName
                  print "done"
                  run $ "mv " + tmpFile + " " + fullName
                  pre $ "len=" + (show $ len fs)
                  print $ "copy of " + fullName + " is in " + copyFile
                else print $ "Error: copy file" + fullName + " to " + "/tmp"
        else do
           print "Need one or two arguments"
           print "removeLineFromFile /tmp/f1.x"
           print "removeLineFromFile /tmp/f1.x   'dog'"
           print "removeLineFromFile /tmp/f1.x   '^dog'"
           print "removeLineFromFile /tmp/f1.x   'dog$'"
           print "removeLineFromFile /tmp/f1.x   '[[:digit:]]+'"
 
   where
     (+) = (++)
