-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
-- import Data.List.Split hiding(oneOf)
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
-- import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import Text.ParserCombinators.Parsec hiding (spaces)
import Text.Parsec.String
import System.Environment
import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

-- write a parser in 48 hours
-- https://en.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours/Parsing

symbol::Parser Char
symbol = oneOf "!#+/*-" 

-- many      => 0 or more
-- skipMany1 => skip one or more
spaces::Parser()
spaces = skipMany1 space

readExpr::String -> String
readExpr s = case parse parseExpr "lisp" s of
        Left err -> "error" ++ show err
        Right x  -> "Found Value"

data LispVal = Atom String
              | List [LispVal]
              | DottedList [LispVal] LispVal
              | Number Integer
              | String String
              | Bool Bool

-- data ListVal = String String
parseString::Parser LispVal
parseString = do
              char '"'
              x <- many (noneOf "\"")
              char '"'
              return $ String x

{-| 
    @
    #t -> return (Bool True)
    #f -> return (Bool False) 
    _  -> return (Atom atom)

    data ListVal = Atom String
    @
-} 
parseAtom::Parser LispVal 
parseAtom = do
             first <- letter <|> symbol
             rest <- many (letter <|> digit <|> symbol)
             let atom = first : rest   -- atom => String
             return $ case atom of
                                "#t" -> Bool True
                                "#f" -> Bool False
                                _    -> Atom atom

{-| 
    liftM :: Monad m => (a -> b) -> (m a -> m b)
    many1 => one or more
    read :: Read a => String -> a 
    
    > read "123" :: Integer
    > 123

    > read "1.23" :: Float
    > 1.23

    -- data LispVal = Number Integer
-} 
parseNumber::Parser LispVal
parseNumber = liftM (Number . read) $ many1 digit


{-| 
data LispVal = Atom String 
               | List [lispVal] 
               | DottedList [LispVal] LispVal
               | Number Integer
               | String String
               | Bool Bool
-} 
parseExpr::Parser LispVal
parseExpr = parseAtom
            <|> parseString
            <|> parseNumber

{-| 
    data LispVal = List [LispVal]
-} 
parseList::Parser LispVal
parseList = liftM List $ sepBy parseExpr spaces

-- data LispVal = DottedList [LispVal] LispVal
parseDottedList :: Parser LispVal  
parseDottedList = do
                  head <- endBy parseExpr spaces
                  tail <- char '.' >> spaces >> parseExpr 
                  return $ DottedList head tail
    
main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp $ readExpr "+"
        pp $ readExpr "<"
        pp $ readExpr " #"
        pp $ readExpr "  #"
        pp "done!"



