-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE CPP #-} 
-- {-# LANGUAGE BlockArguments #-} 
-- import Turtle
-- echo "turtle"

{-# LANGUAGE MultiWayIf        #-}
{-# LANGUAGE QuasiQuotes       #-} -- support raw string [r|<p>dog</p> |]
import Text.RawString.QQ       -- Need QuasiQuotes too 


-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent
import qualified System.Console.Pretty as SCP

import qualified Text.Regex.TDFA as TD
import qualified Data.Array.IO as AIO
import Data.Array.IO
import qualified Data.Array.Base as DB
import qualified Data.Set as S
import qualified Data.HashMap.Strict as M
import qualified ASCII.Char as ASC
  
--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 


--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

-- shell command template:
-- 
--        argList <- getArgs
--        if len argList == 2 then do
--            let n = stringToInt $ head argList
--            let s = last argList
--            putStr $ drop (fromIntegral n) s
--        else print "drop 2 'abcd'"


import AronModule
import AronDevLib

p1 = "/Users/aaa/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

#if 0
main = do 
        home <- getEnv "HOME"
        -- configMap <- readConfig "./config.txt"
        osv <- getEnv "OSTYPE"
        let os = if | containStr "darwin" osv  -> "darwin"
                    | containStr "freebsd" osv -> "freebsd"
                    | otherwise                -> error "unknown"
        print "Hello World"
        argList <- getArgs
        pp $ length argList
		let colorStr = SCP.color SCP.Red "COLOR"
		pp colorStr

        pp "done!"
#endif


{-|
   KEY: display mutable array, print mutable array

   @
   arr1 <- newListArray (1, 3) [1, 2, 3] :: IO (AIO.IOArray Int Int)
   ar <- takeArr 2 arr1
   disp ar
   @
-}
disp::(Show a) => IOArray Int a -> IO ()
disp  x = do
  ls <- getElems x
  pre ls

{-|

   @
   arr1 <- newListArray (1, 3) [1, 2, 3] :: IO (AIO.IOArray Int Int)
   ar <- takeArr 2 arr1
   disp ar
   @
-}
takeArr::Int -> IOArray Int a -> IO (IOArray Int a)
takeArr n arr = do
  l <- getBounds arr >>= return . fst
  mapIndices (l, l + n - 1) id arr

dropArr::Int -> IOArray Int a -> IO (IOArray Int a)
dropArr n arr = do
  b <- getBounds arr
  let l = fst b
  mapIndices (l + n, snd b) id arr

headArr::IOArray Int a -> IO a
headArr arr = readArray arr 1

lastArr::IOArray Int a -> IO a
lastArr arr = getBounds arr >>= (readArray arr) . snd

-- filterArr::(a -> b) -> IOArray Int a -> IO (IOArray Int a)
-- filterArr f arr = do
  -- b <- getBounds arr
  -- na <- newArray b 0

filterArr::(a -> Bool) -> IOArray Int a -> IO (IOArray Int a)
filterArr f arr = do
  ls <- getElems arr
  let s = filter f ls
  newListArray (1, len s) s
  
getPart::(a -> Bool) -> Int -> [Int] -> IOArray Int a -> IO [Int]
getPart f n cx arr = do
  b <- getBounds arr >>= return . snd
  if n <= b then do
    x <- readArray arr n
    if f x then getPart f (n+1) (n:cx) arr
      else do
        return cx
  else do
    return cx
  

swap::IOArray Int a -> Int -> Int -> IO (IOArray Int a)
swap arr i j = do
  it <- readArray arr i
  jt <- readArray arr j
  writeArray arr i jt
  writeArray arr j it
  return arr

partArr::(Ord a) => IOArray Int a -> Int -> Int -> IO (IOArray Int a, Int)
partArr arr lo hi = loop arr lo hi lo
  
loop arr lo hi j = do
  p <- readArray arr hi
  if lo <= hi then do
    ix <- readArray arr lo
    if ix <= p then do
        na <- swap arr lo j
        loop na (lo+1) hi (j + 1)
      else do
        loop arr (lo+1) hi j
  else return (arr, j <= hi ? j $ hi)


qsArr::(Ord a)=>IOArray Int a -> Int -> Int -> IO (IOArray Int a)
qsArr arr lo hi = do
  if lo < hi then do
    (a, px) <- loop arr lo hi lo
    qsArr a lo (px - 1)
    qsArr a (px + 1) hi
  else do
    return arr
#if 0
main = do
        let ls = [1, 2, 3]
        arr <- newArray (1,10) 37 :: IO (AIO.IOArray Int Int)
        a <- readArray arr 1
        writeArray arr 1 64
        b <- readArray arr 1 
        print (a,b)
        arr1 <- newListArray (1, 3) [1, 2, 3] :: IO (AIO.IOArray Int Int)
        c <- readArray arr1 2
        disp arr1
        b <- getBounds arr1
        pre b
        arr2 <- mapArray (\x -> x + 1) arr1
        nArr <- mapIndices (3, 3) (\x -> x) arr1
        ar <- takeArr 2 arr1
        fw "takeArr 2"
        disp ar
        fw "dropArr 2"
        a1 <- dropArr 2 arr1
        disp a1
        ls <- getElems nArr
        fl
        fw "headArr arr1"
        headArr arr1 >>= print
        fw "lastArr arr1"
        lastArr arr1 >>= print
  
        readArray arr2 1 >>= \x -> print $ "a=" ++ show x 
        pp $ "c=" ++ show c
        print "ok"
#endif 


main = do
        let ls = [1, 2, 3]
        pre ls
#if 0
  x
  3 1 2
  i

  x
  3 1 2
    i
    x
  1 3 2
      i
      x
  1 2 3
        i


  x
  2 1 3
  i
    x
  2 1 3
    i
  
      j
  2 1 3
      i
#endif
        when True $ do
          arr4 <- newListArray (1, 3) [3, 1, 2] :: IO (AIO.IOArray Int Int)
          b <- getBounds arr4
          let lo = fst b
          let hi = snd b
          let j = lo
          (myarr, px) <- loop arr4 lo hi j
          disp myarr
          print $ "px=" ++ show px

        when True $ do
          arr4 <- newListArray (1, 1) [3] :: IO (AIO.IOArray Int Int)
          b <- getBounds arr4
          let lo = fst b
          let hi = snd b
          let j = lo
          (myarr, px) <- loop arr4 lo hi j
          disp myarr
          print $ "px=" ++ show px
        when True $ do
          arr4 <- newListArray (1, 2) [2, 1] :: IO (AIO.IOArray Int Int)
          b <- getBounds arr4
          let lo = fst b
          let hi = snd b
          let j = lo
          (myarr, px) <- loop arr4 lo hi j
          disp myarr
          print $ "px=" ++ show px
        when True $ do
          arr <- newArray (2, 1) 37 :: IO (AIO.IOArray Int Int)
          disp arr

        when True $ do
          arr <- newListArray (1, 1) [1] :: IO (AIO.IOArray Int Int)
          fw "arr 1"
          b <- getBounds arr
          let lo = fst b
          let hi = snd b
          arr1 <- qsArr arr lo hi
          fw "qsArr 1"
          disp arr1

        when True $ do
          arr <- newListArray (1, 2) [2, 1] :: IO (AIO.IOArray Int Int)
          fw "arr 2"
          b <- getBounds arr
          let lo = fst b
          let hi = snd b
          arr1 <- qsArr arr lo hi
          fw "qsArr 2"
          disp arr1

        when True $ do
          arr <- newListArray (1, 3) [2, 1, 3] :: IO (AIO.IOArray Int Int)
          b <- getBounds arr
          let lo = fst b
          let hi = snd b
          (a, px) <- partArr arr lo hi
          fw "partArr 1"
          disp a
          print $ "px=" ++ show (px == 3)

        when True $ do
          arr <- newListArray (1, 1) [2] :: IO (AIO.IOArray Int Int)
          b <- getBounds arr
          let lo = fst b
          let hi = snd b
          (a, px) <- partArr arr lo hi
          disp a
          print $ "px=" ++ show (px == 1)

        when True $ do
          let lo = 1
          arr <- newListArray (1, 2) [2, 1] :: IO (AIO.IOArray Int Int)
          b <- getBounds arr
          let lo = fst b
          let hi = snd b
          let j = lo
          (a, px) <- loop arr lo hi j
          fw "loop arr lo lo 1"
          disp a
          print $ "px=" ++ show px
          print $ "px=" ++ show (px == 2)
#if 1
        when True $ do
          expectedArr <- newListArray (1, 2) [1, 2] :: IO (AIO.IOArray Int Int)
          arr <- newListArray (1, 2) [2, 1] :: IO (AIO.IOArray Int Int)
          fw "arr 3"
          b <- getBounds arr
          let lo = fst b
          let hi = snd b
          arr1 <- qsArr arr lo hi
          fw "qsArr 3"
          disp arr1
          print $ show (arr1 == expectedArr)
#endif
  
#if 1
        when True $ do
          expectedArr <- newListArray (1, 4) [1, 1, 2, 3] :: IO (AIO.IOArray Int Int)
          arr <- newListArray (1, 4) [2, 1, 1, 3] :: IO (AIO.IOArray Int Int)
          fw "arr 4"
          b <- getBounds arr
          let lo = fst b
          let hi = snd b
          arr1 <- qsArr arr lo hi
          fw "qsArr 4"
          disp arr1
          print $ arr1 == expectedArr
#endif
#if 1
        when True $ do
          arr <- newListArray (1, 4) [1, 1, 2, 3] :: IO (AIO.IOArray Int Int)
          fw "arr 4"
          b <- getBounds arr
          let lo = fst b
          let hi = snd b
          arr1 <- qsArr arr lo hi
          fw "qsArr 4"
          disp arr1
#endif
#if 1
        when True $ do
          fw "xxx 1"
#endif

           

#if 0
        when True $ do
          m1 <- readFile2d "/tmp/b" :: IO[[Int]]
          let m2 = join m1
          let le = len m2
          let lo = 1
          let hi = le
          arr <- newListArray (1, le) m2 :: IO (AIO.IOArray Int Int)
          old <- timeNowSecond
          qm <- qsArr arr lo hi
          ls <- getElems qm
          new <- timeNowSecond
          print $ "len=" ++ show (len ls)
          print $ "head=" ++ show (head ls)
          print $ "last=" ++ show (last ls)
          let diff = new - old
          print $ "diff=" ++ (show diff)
#endif
  
#if 1
        when True $ do
          fw "getPart 1"
          arr <- newListArray (1, 3) [1, 2, 3] :: IO (AIO.IOArray Int Int)
          let n = 1
          s <- getPart (\x -> x < 2) n [] arr
          pre s
          print $ "diff="
#endif
#if 1
        when True $ do
          fw "filterArr 1"
          arr <- newListArray (1, 3) [1, 2, 3] :: IO (AIO.IOArray Int Int)
          let n = 1
          s <- filterArr (\x -> x < 2) arr
          disp s
#endif
#if 1
        when True $ do
          fw "filterArr 2"
          arr <- newListArray (1, 3) [1, 2, 3] :: IO (AIO.IOArray Int Int)
          let n = 1
          s <- filterArr (\x -> x > 2) arr
          disp s
          let n = 3::Int
          let m = 4::Float
          let l = (÷) n m
          print $ "l=" ++ (show l)
#endif
#if 1
        when True $ do
          let n = 3::Integer
          let m = 4::Float
          let l = (÷) n m
          print $ "l=" ++ (show l)
#endif
#if 1
        when True $ do
          let n = 3::Integer
          let m = 4::Int
          let l = (÷) n m
          print $ "l=" ++ (show l)
#endif
#if 1
        when True $ do
          let n = 3::Double
          let m = 4::Int
          let l = (÷) n m
          print $ "l=" ++ (show l)
#endif
#if 1
        when True $ do
          let n = 3::Integer
          let m = 4::Float
          let l = (÷) m n ::Float
          print $ show $ typeOf l
          print $ "l=" ++ (show l)
          let k = 0.3::Float
          let p = 4::Int
          print ((×) l k)
          let q = (×) k k
          print "ok"
          print $ (×) l (0.3 :: Float)
          print $ (×) l (4::Int)
          print $ (÷) (20::Int) (((×) (4::Int) (0.6::Double))::Float)
         
#endif
        when True $ do
          fw "filterArr 2"

        when True $ do
          fw "combin"
          pre $ combin 2 [1, 2, 3]
          pre $ combin 1 [1, 2, 3]
          pre $ combin 0 [1, 2, 3]
          pre $ combin 3 [1, 2, 3]

        when True $ do
          env <- getEnv "jlib"
          let path = env ++ "/Aron.java"         
          -- let p = "/tmp/a.x"
          let p = "/Users/aaa/.emacs.d/init.el"
          hmap <- splitFileBlock path
          fw "hmap"
          pre hmap
        when True $ do
          fw "redisSet"
          redisSet "try000xxx" "try000value"
          pp "setkey"

        when True $ do
          fw "redisSet"
          let s = isPun "ab,Stilmond,Elkmonad."
          pre s
        when True $ do
          s <- readFileFloat2d "/tmp/y"
          pre s
          fw "redisSet"


--readFileFloat2d::FilePath -> IO[[Float]]
--readFileFloat2d fp = do
--    s <- readFileList fp >>= \str -> return $ map (splitSPC) str 
--    let s' = filter (\e -> len e > 0) $ map (\ls -> filter (\a -> len a > 0) ls) $ (map . map) (trim) s
--    let ns = (map . map) (\x -> read x :: Float) s'
--    pre ns
--    return ns 

{-|
   KEY: split list of string

   @
   splitRex = "BEG_[[:print:]]*|END_[[:print:]]*"
   nameRex = "(.*BEG_)([[:print:]]+)(.*)"
   @
-}
splitBlockRegex::String -> [String] -> Either String (M.HashMap String [String])
splitBlockRegex rgx cs = either
  where
    splitRex = "BEG_[[:print:]]*|END_[[:print:]]*"
    nameRex = "(.*BEG_)([[:print:]]+)(.*)"
    s1 = map (hasStr rgx) cs
    z = zip s1 cs
    tls = filter (\(a,_) -> a) z
    block = splitBlock cs splitRex
    b = let n1 = len tls; n2 = len block in n1 == n2 - 1 && (mod n1 2 == 0)
    block' = map (\[a, b] -> (a, b)) $ partList 2 $ init block
    tls' = map (\(_,(_,b)) -> b) $ filter (\(a, b) -> even a) $ zip [0..] tls
    rex = mkRegex nameRex
    ztup = zipWith(\b (_, c) -> (subRegex rex b "\\2", c)) tls' block'
    hmap = M.fromList ztup
    either = b ? Right hmap $ Left "ERROR"

-- BEG_splitfileblock
{-|
   KEY: split file and capture code block
   @

   BEG_XXX
   code1
   END_XXX

   splitRex = "BEG_[[:print:]]*|END_[[:print:]]*"
   nameRex = "(.*BEG_)([[:print:]]+)(.*)"
   @
-}    
splitFileBlock::FilePath -> IO (M.HashMap String [String])
splitFileBlock p = do
          let spRgx = "BEG_[-_[:alnum:]]+|END_[-_[:alnum:]]+"
          let groupRgx = "(.*BEG_)([-_[:alnum:]]+)(.*)"
          ls <- readFileList p
          let ss = map (hasStr spRgx) ls
          let z = zip ss ls
          let tls = filter (\(a,_) -> a) z
          let block = splitBlock ls spRgx
          unless (let n1 = len tls; n2 = len block in n1 == n2 - 1 && (mod n1 2 == 0)) $ do
            pp "ERROR"
            pp $ "len tls=" ++ (show $ len tls)
            pp $ "len block=" ++ (show $ len block)
            pp $ "(len tls) /= (len block) + 1" 
          let block' = map (\[a, b] -> (a, b)) $ partList 2 $ init block
          let tls' = map (\(_,(_,b)) -> b) $ filter (\(a, b) -> even a) $ zip [0..] tls
          pre block'
          let rex = mkRegex groupRgx
          let ztup = zipWith(\b (_, c) -> (subRegex rex b "\\2", c)) tls' block'
          let hmap = M.fromList ztup
          return hmap
-- END_splitfileblock

data MyType = MyType Int Int deriving(Show)

permx :: [a] -> Int -> [[a]]
permx []  _      = [[]]
permx ls@(x:xs) n = concatMap ((rotations len).(x:)) (permx xs n)
                  where len = length ls
                        -- iterate (\(y:ys) -> ys ++ [y]) [1, 2, 3]
                        -- [1, 2, 3], [2, 3, 1], [3, 1, 2] ...
                        rotations :: Int -> [a] -> [[a]]
                        rotations len xs = take len (iterate (\(y:ys) -> ys ++ [y]) xs)

-- XXX
data PType = STR | LETTER | PUN | NONE deriving(Show)

isPun::String -> [(String, String, PType)]
isPun []   = [([], [], NONE)]
isPun (x:cx) = if | isPunctuation x -> ([x], cx, PUN) : isPun cx
                  | isLetter x -> ([x], cx, LETTER) : isPun cx 
                  | otherwise -> ("", "", NONE) : isPun cx

myStr::String -> String -> (String, String, PType)
myStr [] _ = ([], [], NONE)
myStr (x:y:cx) s = if | isLetter x && (not . isLetter) y -> (x:s, (y:cx), STR)
                      | isLetter x -> myStr (y:cx) (x:s)                
                      | otherwise -> (s, (x:y:cx), NONE)
