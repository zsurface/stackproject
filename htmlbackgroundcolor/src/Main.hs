-- {{{ begin_fold
{-# LANGUAGE QuasiQuotes       #-}
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
-- {-# LANGUAGE QuasiQuotes       #-}
import Text.RawString.QQ (r)         -- Need QuasiQuotes too
       
import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule
import AronHtml2

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close
data CSSColor = CSSColor{color_ :: String, bgColor_ :: String} deriving (Show)
       

htmlHB sty s  = [r|
            <HTML>   
            <HEAD>   
            <meta charset="utf-8">
            <TITLE>Search Code Snippet</TITLE>|]
            <> sty <>
            [r|</HEAD><BODY>|] <> s <> [r| <div id="searchdata">stackproject/htmlbackgroundcolor</div> </BODY></HTML>|]


-- width: 30px;
-- border: 15px solid green;
-- padding: 50px;
-- margin: 20px;

                
main = do
        p <- getEnv "b"
        let path = p </> "publicfile" </> "backgroundcolor.html"
        -- r1 <- mapM (\_ -> randomInteger 0    ((16^3) - 1)) [1..200]
        -- r2 <- mapM (\_ -> randomInteger (16^3) ((16^6) - 1)) [1..200]
        r1 <- mapM (\_ -> randomInteger 0    ((16^3) - 1)) [1..2000]  -- background   0 0 F F F F  0 0 G G B B
        r2 <- mapM (\_ -> randomInteger 0    ((16^6) - 1)) [1..2000]  -- color        F F F F F F  R R G G B B
        let tu = filter(\(x, y) -> abs (x - y) > 16^2 ) $ zip r1 r2
        let rr = map(\(x, y) -> [
                            ("background-color", "#" + (cssColor x)),
                            ("color", "#" + (cssColor y)           ),
                            ("width", "600px"                      ),
                            ("height", "100px"                     ),
                            ("border", "2px solid green"           ),
                            ("margin", "4px"                       ),
                            ("white-space", "pre-wrap"             )
                            ]
                         ) tu
        let ss = map(\x -> "{" + (concatStyle x) + "}" + "\n") rr
        let tt = zipWith(\x y -> (".b" + (show x), y)) [1..] ss
        let qq = map(\x -> (fst x) + " " + (snd x)) tt
        let hh = "<style>" + (concatStr qq []) + "</style>"
        
        writeFileList path qq
        pp $ div_ (class_ [hh]) "mystr"
        let vv = map(\x -> pre_ (class_ [drop 1 (fst x)]) (snd x)) tt
        let pv = partList 3 vv
        let tv = htmlTable pv
        writeFileList path [htmlHB ("\n" + hh + "\n") (concatStr (htmlTable pv) [])]
        fw "ss"
        pp ss
        fl
        pp tv
        fl
        pp pv
        fl
        pp vv
        fl
        pp tt
        fl
        pp qq
        fl
        pp ss
        fl
        pp rr
        pp $ htmlBox [("width", "20px"), ("height", "30px")] "Hello World"
        pp "done!"
  where
    (+) = (++)
    css x = "#" + (integerToHex x) + ";"
    bgc = "background-clor:"
    color = "color:"
    i2s = integerToString
    i2h = integerToHex
    cssColor n = let s = integerToHex n; diff = 6 - len s in (take diff "000000") + s
    
    mystyle_::[String] -> String
    mystyle_ cs = if len str > 0 then " style='{" + str + "}' " else ""
      where
        str = concatStr cs []
