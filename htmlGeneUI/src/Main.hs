#!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
{-# LANGUAGE QuasiQuotes       #-}
-- {{{ begin_fold
-- script
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
-- {-# LANGUAGE QuasiQuotes       #-}
import Text.RawString.QQ (r)         -- Need QuasiQuotes too 

--import Data.Array
--import Text.Regex.TDFA -- the module will conflict with Text.Regex

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule
import Network.HTTP
import AronHtml2

main = do
       let style = ["background:green;", "color:blue;"]
       let myid =  ["myid"]
       let myclass = ["myclass"]
       let ls = ["d1", "d2", "d3"]
       
       let lss = [["a1", "a2", "a3"],["b1", "b2", "b3"]]
       let row ls = (ccat (map (\x -> td "" x) ls) "")
       let rowCol lss = ccat (map (\r-> tr "" (row r)) lss) ""
       let style1 = ["color:green; background:black"]
       let tab = table (attrStyle style1) (rowCol lss)
       let str = htmlBody
                     $ textAreaNew (attrStyle style) "this is textarea 1."
                     ++ htmlbr
                     ++ textAreaNew (attrId myid) "this is textarea 2."
                     ++ htmlbr
                     ++ textAreaNew (attrClass myclass) "this is textarea 3."
                     ++ htmlbr
                     ++ table (attrStyle style) "my table"
                     ++ td (attrStyle style) "my td"
                     ++ tr (attrStyle style) "my tr"
                     ++ tr (attrStyle style)  (td (attrStyle []) "row 1")
                     ++ tr (attrStyle style)  (row ls)
                     ++ ccat  (map(\r -> tr (attrStyle style) (row r)) lss) ""
                     ++ tab
                     ++ htmlbr
                     ++ pre_ (attrStyle style) "This is my code"
                     ++ pre_ (attrStyle [])    "Style should be empty"
                     ++ htmlbr
                     ++ div_ (attrStyle style) (pre_ "" "my code")
                     ++ htmlbr
                     ++ label_ (attrStyle style ) "My Label"
                     ++ htmlbr
                     ++ input_ (attr ["type='text'"]) "my input"
                     ++ htmlbr
                     ++ div_ (attrStyle ["background:cyan;"]) (input_ (attr ["name='First Name'", "type='text'", "value='My Value'"]) "my input")
                     ++ div_ (attrStyle []) "try div"
                     ++ div_ (attrStyle []) (unlines ["Justin Bieber", "Justin Trudeau"])
       pre str
       writeFile "/tmp/try.html" str
       -- sys "open /tmp/try.html"
       pp "done"

       
