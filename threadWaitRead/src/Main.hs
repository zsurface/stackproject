-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Posix (fdToHandle)
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import qualified System.Console.ANSI as AN
-- import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

-- zo - open
-- za - close

{--
main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
--}

import Control.Monad (forever)
import GHC.Conc (threadWaitRead)
import System.IO (openFile, stdin, IOMode(..), BufferMode(..), hSetBuffering)
import System.Posix (FdOption(..), setFdOption, fdRead, handleToFd)
import System.Posix.Types
import System.Exit (exitFailure)
import System.Environment (getArgs)

loop :: [String] -> String -> Fd -> Int -> IO()
loop s cur ttyFd x = do
      -- putStrLn "Calling threadWaitRead"
      threadWaitRead ttyFd
      -- putStrLn "Calling fdRead"
      (b, byteCount) <- fdRead ttyFd 16
      -- putStrLn $ show b
      case b of
           "\n" -> do
                   -- pp "new line"
                   -- putStrLn s 
                   AN.cursorDownLine 1
                   loop (cur:s) [] ttyFd 0
           "\ESC[A" -> do
                   pre s
                   -- putStr $ "kk" <> b
                   AN.cursorUpLine 1
                   -- putStr "OK"
                   loop s [] ttyFd 0
                   
           "\ESC[D" -> do
                   AN.cursorBackward 1 
                   loop s cur ttyFd (x + 1)
           _    -> do
                   AN.cursorForward 1 
                   putStr b
                   -- putStrLn $ "read: " <> show b <> "ByteCount=" <> show byteCount

                   loop s (cur <> b) ttyFd (x + 1)


main :: IO ()
main = do
  args <- getArgs
  handle <- case args of
      ["stdin"] -> return stdin
      ["tty"]   -> openFile "/dev/tty" ReadMode
      _         -> putStrLn "Usage: minimal <stdin|tty>" >> exitFailure

  hSetBuffering handle NoBuffering
  ttyFd <- handleToFd handle
  setFdOption ttyFd NonBlockingRead False
  loop [] [] ttyFd 0 

{--
  forever $ do
      putStrLn "Calling threadWaitRead"
      threadWaitRead ttyFd
      putStrLn "Calling fdRead"
      (b, byteCount) <- fdRead ttyFd 16
      putStrLn $ "read: " <> show b <> "ByteCount=" <> show byteCount
--}
