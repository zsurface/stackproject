{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_threadWaitRead (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/threadWaitRead/.stack-work/install/x86_64-osx/5050d4c17fb8dca4a9a81d415347c61f6a3a357b577d222bcc8d6b35e01917ad/8.10.3/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/threadWaitRead/.stack-work/install/x86_64-osx/5050d4c17fb8dca4a9a81d415347c61f6a3a357b577d222bcc8d6b35e01917ad/8.10.3/lib/x86_64-osx-ghc-8.10.3/threadWaitRead-0.1.0.0-44EysbvHOcbJ9WfPjITL9X-threadWaitRead"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/threadWaitRead/.stack-work/install/x86_64-osx/5050d4c17fb8dca4a9a81d415347c61f6a3a357b577d222bcc8d6b35e01917ad/8.10.3/lib/x86_64-osx-ghc-8.10.3"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/threadWaitRead/.stack-work/install/x86_64-osx/5050d4c17fb8dca4a9a81d415347c61f6a3a357b577d222bcc8d6b35e01917ad/8.10.3/share/x86_64-osx-ghc-8.10.3/threadWaitRead-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/threadWaitRead/.stack-work/install/x86_64-osx/5050d4c17fb8dca4a9a81d415347c61f6a3a357b577d222bcc8d6b35e01917ad/8.10.3/libexec/x86_64-osx-ghc-8.10.3/threadWaitRead-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/threadWaitRead/.stack-work/install/x86_64-osx/5050d4c17fb8dca4a9a81d415347c61f6a3a357b577d222bcc8d6b35e01917ad/8.10.3/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "threadWaitRead_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "threadWaitRead_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "threadWaitRead_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "threadWaitRead_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "threadWaitRead_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "threadWaitRead_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
