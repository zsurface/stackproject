{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_maxlen (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/maxlen/.stack-work/install/x86_64-osx/5050d4c17fb8dca4a9a81d415347c61f6a3a357b577d222bcc8d6b35e01917ad/8.10.3/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/maxlen/.stack-work/install/x86_64-osx/5050d4c17fb8dca4a9a81d415347c61f6a3a357b577d222bcc8d6b35e01917ad/8.10.3/lib/x86_64-osx-ghc-8.10.3/maxlen-0.1.0.0-Cpv8gXYgRWI6AlVAqns0Gq-maxlen"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/maxlen/.stack-work/install/x86_64-osx/5050d4c17fb8dca4a9a81d415347c61f6a3a357b577d222bcc8d6b35e01917ad/8.10.3/lib/x86_64-osx-ghc-8.10.3"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/maxlen/.stack-work/install/x86_64-osx/5050d4c17fb8dca4a9a81d415347c61f6a3a357b577d222bcc8d6b35e01917ad/8.10.3/share/x86_64-osx-ghc-8.10.3/maxlen-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/maxlen/.stack-work/install/x86_64-osx/5050d4c17fb8dca4a9a81d415347c61f6a3a357b577d222bcc8d6b35e01917ad/8.10.3/libexec/x86_64-osx-ghc-8.10.3/maxlen-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/maxlen/.stack-work/install/x86_64-osx/5050d4c17fb8dca4a9a81d415347c61f6a3a357b577d222bcc8d6b35e01917ad/8.10.3/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "maxlen_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "maxlen_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "maxlen_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "maxlen_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "maxlen_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "maxlen_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
