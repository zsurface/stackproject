module Main where

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import qualified Graphics.UI.GLFW as G
import Graphics.Rendering.OpenGL.GLU.Matrix as GM
-- import qualified Graphics.Rendering.FTGL as FTGL
import qualified Graphics.UI.GLUT as GLUT

import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S
import qualified Data.List as DL
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import Data.Typeable
import qualified Text.Printf as PR
import Language.Haskell.Interpreter 

import GHC.Float.RealFracMethods

-- import Graphics.UI.GLUT
-- import Graphics.UI.GLUT.Callbacks.Global
import AronModule hiding (rw)
import AronGraphic hiding (dist)
import AronOpenGL
import AronDevLib

import qualified Data.Vector as VU

-- | -------------------------------------------------------------------------------- 
-- | compile: run.sh  
-- | ghc -i/Users/cat/myfile/bitbucket/haskelllib -o file file.hs
-- | 
-- | KEY: keyboard example, keypress example, modifyIORef example,  
-- | -------------------------------------------------------------------------------- 


tmpfile = "/tmp/tmpfile.txt"


errorString :: InterpreterError -> String
errorString (WontCompile es) = DL.intercalate "\n" (header : map unbox es)
  where
    header = "ERROR: Won't compile:"
    unbox (GhcError e) = e
errorString e = show e

say::String -> Interpreter()
say = liftIO . putStrLn

emptyLine::Interpreter()
emptyLine = say ""

colorls = VU.fromList [
  Color3 0.1 0.1 0.1,
  Color3 0.3 0.1 0.9,
  Color3 0.3 0.7 0.9,
  Color3 0.4 0.1 0.1,
  Color3 0.3 0.6 0.9,
  Color3 0.4 0.1 0.3,
  Color3 0.3 0.4 0.9,
  Color3 0.6 0.1 0.9,
  Color3 0.3 0.7 0.9,
  Color3 0.4 0.1 0.5,
  Color3 0.3 0.8 0.9,
  Color3 0.1 0.1 0.4
  ]
  

testHint::[String] -> [String] -> Interpreter()
testHint xs expls = do
        loadModules ["/Users/cat/myfile/bitbucket/stackproject/HelloSimple/src/AronSimple.hs"]
        setImportsQ [("Prelude", Nothing), ("Data.Map", Just "M"), ("AronSimple", Nothing)]        
        let stmts = xs
        forM_ stmts $ \s -> do
          say $ " " ++ s
          runStmt s        
        emptyLine


        -- let expr3 = "\\(x, y) -> x + 1 + y"
        VU.mapM_ (\(exp, c) -> do
                  fun <- interpret exp (as :: GLfloat -> GLfloat)
                  -- say $ show $ fun 3.141569
                  liftIO $ drawCurveV fun (-1.0, 1.0) c
              ) $ VU.zip (VU.fromList expls) colorls

        emptyLine
            

          
mc::(GLfloat, GLfloat) -> (GLfloat, GLfloat) -> (GLfloat, GLfloat)
mc (a, b) (a', b') = (a*a' - b*b', a*b' + a'*b)

-- (a + bi)(a' + b'i)
-- (aa' - bb') + (ab' + ba')i
-- (a, b) * (a', b') = (aa' - bb', ab' + a'b)

-- conform= [(0.1 * (fst $ mc c c'), 0.1* (snd $ mc c c'), 0) 
--            | c <- zip (map(*0.01) [1..aa]) (map(*0.01) [1..bb]), c' <- zip (map(*0.01) [1..aa]) (map(*0.01) [1..bb])]
conform::[[Vertex3 GLfloat]]
conform= [[Vertex3 (fst $ mc c1 c2) (snd $ mc c1 c2)  0 | c1 <- c] | c2 <- c] 
--conform= [[(fst $ c1, snd $ c1 , 0) | c1 <- c] | c2 <- c] 
        where 
            fa = 0.1 
            aa = map(\x -> fa * x) [1..10]
            bb = map(\x -> fa * x) [1..3]
            c = foldr(++) [] $ [[(a, b) | a <- aa] | b <- bb]

grid::[[[Vertex3 GLfloat]]]
grid =[[[Vertex3 a b (a*a - b*b) | a <- aa] | b <- bb] | c <- cc] 
        where 
            n  = 10
            fa = 1/n 
            aa = map(\x -> fa * x) [1..n]
            bb = map(\x -> fa * x) [1..n]
            cc = map(\x -> fa * x) [1..n]

grid2::[[Vertex3 GLfloat]]
grid2 =[[Vertex3 a b (a*a - b*b) | a <- aa] | b <- bb] 
        where 
            n  = 30
            fa = 1/(2*n)
            aa = map(\x -> fa * x) [-n..n]
            bb = map(\x -> fa * x) [-n..n]

-- f z = z*z
-- 
-- a    => x
-- b    => y
-- re c => z
-- im c => color
grid4::[[(Vertex3 GLfloat, GLfloat)]]
grid4 =[[ let c = (C a b)*(C a b) in (Vertex3 a b (re c), im c) | a <- aa] | b <- bb]
        where 
            ne = [[ let c = sqrtC' (C a b) in (Vertex3 a b (re c), im c) | a <- aa] | b <- bb]
            n  = 20 
            fa = 1/(1.5*n)
            aa = map(\x -> fa * x) [-n..n]
            bb = map(\x -> fa * x) [-n..n]

grid4' = (map . map) (\x -> fst x) grid4

trig s1 s2 = map(\x -> foldr(++) [] x) $ (zipWith . zipWith)(\x y -> [x, y]) (init s1) (tail s2)

segment1 = zipWith(\x y -> [Vertex3 (-1) 0 0, y]) [1..] test_circle

test_circle::[Vertex3 GLfloat]
test_circle=[ let x = (1-t*t)/(1 + t*t); 
                  y = (2*t)/(1 + t*t) in Vertex3 x y 0 | t <- aa]
        where 
            n  = 50 
            fa = 1/(0.1*n)
            aa = map(\x -> fa * x) [-n..n]

splitPt::Int->[(GLfloat, GLfloat, GLfloat)]->[[(GLfloat, GLfloat, GLfloat)]]
splitPt _ [] = []
splitPt n xs = take n xs : (splitPt n $ drop n xs)


mergeChunk::Int->[(GLfloat, GLfloat, GLfloat)]->[(GLfloat, GLfloat, GLfloat)]
mergeChunk n c = mergeList  (take n c)  (take n $ drop n c) 

bigChunk::Int->[(GLfloat, GLfloat, GLfloat)]->[[(GLfloat, GLfloat, GLfloat)]]
bigChunk n xs = splitPt n xs 

renderSurface::[(GLfloat, GLfloat, GLfloat)]->IO()
renderSurface xs = do 
        iterateList (bigChunk 80 xs) (\chunk -> do
                        let len = length chunk
                        let n = div len 2 
                        let c = mergeChunk n chunk
                        let cc = zipWith(\x y -> (x, y)) c [1..]
                        renderPrimitive TriangleStrip $ mapM_(\((x, y, z), n) -> do 
                            case mod n 3 of 
                                0 -> do 
                                        color(Color3 0.8 1 0 :: Color3 GLdouble) 
                                1 -> do 
                                        color(Color3 0 0.5 1 :: Color3 GLdouble) 
                                _ -> do 
                                        color(Color3 1 0 0.7 :: Color3 GLdouble) 
                            normal $ (Normal3 x y z::Normal3 GLfloat)
                            vertex $ Vertex4 x y z 0.8) cc  
                            )

type Vex3 = Vertex3 GLfloat
type V3d = Vertex3 GLdouble
         
drawHistogram::IO()
drawHistogram = do
  preservingMatrix $ do
    translate (Vector3 0.0 0 0 :: Vector3 GLdouble)
    drawRect (Vertex3 (-0.1) (-0.1) 0, Vertex3 0.1 0.1 0)


drawHistogram2::[GLfloat] -> IO()
drawHistogram2 cx = do
  let n = len cx
  let δ = 1 / rf n
  let w = δ - 0.002
  let zz = map(\(a, b) -> (rf a, b)) $ zip cx [0..]
  mapM (\(h, c) -> do
           pp h
           pp c
           let off = rf $ c * δ
           preservingMatrix $ do
             translate (Vector3 off (h/2) 0 :: Vector3 GLdouble)
             -- drawRect2d w (rf h)
             drawRectFill2d white (w, (rf h))
         
             -- translate (Vector3 off (-0.3) 0 :: Vector3 GLdouble)
           preservingMatrix $ do
             let strNum = PR.printf "%.1f" h :: String
             strWidth <- GLUT.stringWidth GLUT.Roman strNum
             -- strHeight <- GLUT.stringHeight GLUT.Roman str
             -- 1000 => 1000 pixel
             print $ "strWidth=" ++ (show $ rf strWidth/scaleFont)
             let cen = off - ((rf strWidth) /(scaleFont*2.0))
             print $ "cen=" ++ (show cen)
             print $ "off=" ++ (show off)
             translate (Vector3 cen (-0.1) 0 :: Vector3 GLdouble)
             renderText strNum
         
           return ()
             
       ) zz
  return ()



drawHis::[GLfloat] -> IO()
drawHis cx = do
  preservingMatrix $ do
    translate (Vector3 (-0.5) 0 0 ::Vector3 GLdouble)
    drawHistogram2 cx


bstr::String -> String
bstr s = "[" ++ s ++ "]"

{-|
   === Move object alone X-Axis

   1. move drawRect2d to the right in x

   @
   moveToX drawRect2d (w, h) x    -- move to the right
   moveToX drawRect2d (w, h) (-x) -- move to the left 
   @

       drawRect2d w h

           ↑ y
           |
         ⌜---⌝
  ← -x   |   |  → x
         | + |–--->      X-Axis
         |   |
         ⌞---⌟

-}
moveToX::((GLfloat, GLfloat) -> IO()) -> (GLfloat, GLfloat) -> GLdouble -> IO()  -- moveToX drawRect2d (w, h) x  -- move to the (x) right, (-1) left
moveToX f (w, h) x = do
  preservingMatrix $ do
    translate (Vector3 x 0 0 ::Vector3 GLdouble)
    f (w, h)

moveToY::((GLfloat, GLfloat) -> IO()) -> (GLfloat, GLfloat) -> GLdouble -> IO()
moveToY f (w, h) y = do
  preservingMatrix $ do
    translate (Vector3 0 y 0 ::Vector3 GLdouble)
    f (w, h)
    
moveToZ::((GLfloat, GLfloat) -> IO()) -> (GLfloat, GLfloat) -> GLdouble -> IO()
moveToZ f (w, h) z = do
      preservingMatrix $ do
        translate (Vector3 0 0 z ::Vector3 GLdouble)
        f (w, h)


renderText::String -> IO()
renderText str = do
  preservingMatrix $ do
    -- rotate (60)$ (Vector3 0 0 1 :: Vector3 GLdouble)
    -- translate (Vector3 0 (-0.1) 0 ::Vector3 GLdouble)
    GL.scale (1/scaleFont :: GL.GLdouble) (1/scaleFont) 1
    GLUT.renderString GLUT.Roman str
    
    -- KEY: string width, string height, font width, font height
    -- strWidth <- GLUT.stringWidth GLUT.Roman str
    -- strHeight <- GLUT.stringHeight GLUT.Roman str

{-|   
renderText :: GLUT.Font -> String -> IO ()
renderText font str = do
    GL.scale (1/64 :: GL.GLdouble) (1/64) 1
    GLUT.renderString GLUT.Roman str
-}    
mymain :: IO ()
mymain = do
  -- finally add double buffer
  successfulInit <- G.init
  G.windowHint (G.WindowHint'DoubleBuffer True)
  -- if init failed, we exit the program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 1000 1000 "Simple example, haskell style" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          ref <- newIORef initCam 
          refStep <- newIORef initStep

          mainLoop window ref refStep
          G.destroyWindow window
          G.terminate
          exitSuccess

lsfun = [          
  "\\x -> x*x",
  "\\x -> 2*x*x",
  "\\x -> 3*x*x",
  "\\x -> 4*x*x",
  "\\x -> 5*x*x",
  "\\x -> 6*x*x",
  "\\x -> 7*x*x",
  "\\x -> 8*x*x",
  "\\x -> 9*x*x"
  ]

strToTriple::String -> (GLfloat, GLfloat, GLfloat)
strToTriple str = tri
  where
    s = splitSPC str
    tri = if (ρ s) == 3 then let a = read (s ! 0)::GLfloat
                                 b = read (s ! 1)::GLfloat
                                 c = read (s ! 2)::GLfloat
                                 (!) = (!!)
                             in (a, b, c)
          else error $ "Error1: String should contain three GLfloat. str=" + bstr str + " s=" + (show s)
    (+) = (++)
vertex3ToTriple::Vertex3 GLfloat -> (GLfloat, GLfloat, GLfloat)
vertex3ToTriple (Vertex3 a b c) = (a, b, c)

tripleToVertex3::(GLfloat, GLfloat, GLfloat) -> Vertex3 GLfloat
tripleToVertex3 (a, b, c) = Vertex3 a b c
    
strToVertex3::String -> Vertex3 GLfloat
strToVertex3 str = vex
  where
    s = splitSPC str
    vex = if ρ s == 3 then let a = read (s ! 0)::GLfloat
                               b = read (s ! 1)::GLfloat
                               c = read (s ! 2)::GLfloat
                               (!) = (!!)
                           in Vertex3 a b c
          else error $ "Error2: String should contain three GLfloat. str=" + bstr str + " s=" + (show s)
    (+) = (++)
{-|
    KEY: convert 'String' to 'Vertex3' 'GLfloat'

    @
     Input:
     segment
     0.3 0.4 0.0
     0.2 0.3 0.0
     0.1 0.2 0.0
     endsegment

     => [Vertex3 0.3 0.4 0.0, Vertex3 0.2 0.3 0.0, Vertex3 0.1 0.2 0.0]
    @
-}
takeSegment::[String] -> [Vertex3 GLfloat]
takeSegment [] = []
takeSegment cx = cs
  where
    beg = "segment"
    end = "endsegment"
    ss = filter(\x -> (len . trim) x > 0) $ takeBetweenExc beg end cx
    cs = map(\x -> strToVertex3 x ) ss

{-|
    KEY: convert 'String' to 'Vertex3' 'GLfloat'

    @
     Input:
     point
     0.3 0.4 0.0
     0.2 0.3 0.0
     endpoint

     => [Vertex3 0.3 0.4 0.0, Vertex3 0.2 0.3 0.0]
    @
-}
takePoint::[String] -> [Vertex3 GLfloat]
takePoint [] = []
takePoint cx = cs
  where
    beg = "point"
    end = "endpoint"
    ss = filter(\x -> (len . trim) x > 0) $ takeBetweenExc beg end cx
    cs = map(\x -> strToVertex3 x ) ss
    
    
{-|
    KEY: convert 'String' to 'Vertex3' 'GLfloat'

    @
     Input:
     triangle
     0.3 0.4 0.0
     0.2 0.3 0.0
     0.1 0.2 0.0
     endtriangle

     => [Vertex3 0.3 0.4 0.0, Vertex3 0.2 0.3 0.0, Vertex3 0.1 0.2 0.0]
    @
-}    
takeTriangleVex::[String] -> [Vertex3 GLfloat]
takeTriangleVex [] = []
takeTriangleVex cx = xs
  where
    beg = "triangle"
    end = "endtriangle"
    ss = filter(\x -> (len . trim) x > 0) $ takeBetweenExc beg end cx
    xs = map(\x -> strToVertex3 x ) ss


vertex3Triple::[Vertex3 GLfloat] -> [(Vertex3 GLfloat, Vertex3 GLfloat, Vertex3 GLfloat)]
vertex3Triple cx = ts
  where
    ls = let s = partList 3 cx in if (len . last) s == 3 then s else init s
    ts = map(\(a:b:c:_) -> (a, b, c)) ls


{-|
    === KEY: read file to load geometry(ies)

    1. "--" will be ignored
    2. Emtpy line will be ignored

    Support following geometries so far

    @
      point
      0.1 0.1 0.1
      0.2 0.2 0.2
      0.3 0.3 0.3
      endpoint

      segment
      0.1 0.1 0.1
      0.2 0.2 0.2
      0.3 0.3 0.3
      0.4 0.4 0.4
      endsegment

      triangle
      0.1 0.1 0.1
      0.2 0.2 0.2
      0.3 0.3 0.3
      0.4 0.4 0.4
      0.5 5.5 0.5
      0.6 0.6 0.6
      endtriangle
    @
-}
readGLScript::FilePath -> IO [String]
readGLScript fp = readFileList fp >>= \ls -> return $ filter(\x -> let f = not $ hasPrefix "--" x
                                                                       g = (len . trim) x > 0
                                                                   in f && g) ls    

{-|
    === KEY: Convert a list vertices to tuple3 vertices

    @
     [Vertex3 0.1 0.1 0.1, Vertex3 0.2 0.2 0.2, Vertex3 0.3 0.3 0.3, Vertex3 0.4 0.4 0.4]

     => [(Vertex3 0.1 0.1 0.1, Vertex3 0.2 0.2 0.2, Vertex3 0.3 0.3 03)]
    @
-}
listToTuple3Vex::[Vertex3 GLfloat] -> [(Vertex3 GLfloat, Vertex3 GLfloat, Vertex3 GLfloat)]
listToTuple3Vex cx = let lss = partList 3 cx
                         lst = if len lss > 0 then (let s = last lss
                                                    in len s == 3 ? lss $ init lss)
                               else lss
                     in map(\x -> let a = x ! 0
                                      b = x ! 1
                                      c = x ! 2
                                      (!) = (!!)
                                  in (a, b, c)) lst :: [(Vertex3 GLfloat, Vertex3 GLfloat, Vertex3 GLfloat)]

{-|
    === KEY: Convert a list vertices to tuple2 vertices

    @
     [Vertex3 0.1 0.1 0.1, Vertex3 0.2 0.2 0.2, Vertex3 0.3 0.3 0.3, Vertex3 0.4 0.4 0.4]

     => [(Vertex3 0.1 0.1 0.1, Vertex3 0.2 0.2 0.2),
         (Vertex3 0.3 0.3 0.3,  Vertex3 0.4 0.4 0.4)
        ]
    @
-}                                                    
listToTuple2Vex::[Vertex3 GLfloat] -> [(Vertex3 GLfloat, Vertex3 GLfloat)]
listToTuple2Vex cx = let lss = partList num cx
                         lst = if ρ lss > 0 then (let s = last lss
                                                    in ρ s == num ? lss $ init lss)
                               else lss
                     in map(\x -> let a = x ! 0
                                      b = x ! 1
                                      (!) = (!!)
                                  in (a, b)) lst :: [(Vertex3 GLfloat, Vertex3 GLfloat)]
  where
    num = 2
mainLoop :: G.Window -> IORef Cam -> IORef Step -> IO ()
mainLoop w ref refStep = unless' (G.windowShouldClose w) $ do
    (width, height) <- G.getFramebufferSize w
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    GL.clear [ColorBuffer, DepthBuffer]
    G.setKeyCallback w (Just $ keyBoardCallBack refStep)  -- AronOpenGL
    
    -- lightingInfo
    loadIdentity
    -- view matrix: http://localhost/html/indexUnderstandOpenGL.html
    -- matrixMode $= Modelview 0
    matrixMode $= Projection
    loadIdentity
    let fovy = 80.0; aspect = 1.0; zNear = 2.0; zFar = (-2)
      in GM.perspective fovy aspect zNear zFar

    matrixMode $= Modelview 0
    loadIdentity
    GM.lookAt (Vertex3 0.0 0.0 1.0::Vertex3 GLdouble) (Vertex3 0 0 0:: Vertex3 GLdouble) (Vector3 0 1 0 :: Vector3 GLdouble)
    -- GL.lookAt (Vertex3 1.1 0 0::Vertex3 GLdouble) (Vertex3 0 0 0:: Vertex3 GLdouble) (Vector3 0 1 0 :: Vector3 GLdouble)

    keyboardRot ref refStep (fromIntegral width) (fromIntegral height)
    renderCoordinates

--    drawTriangle

    -- render code here

--    drawPrimitive LineLoop red torus 
--    mapM_ (\lo -> drawPrimitive LineLoop red lo) $ torusR 0.1 0.2 
--    mapM_ (\lo -> drawPrimitive LineLoop blue lo) $ tran $ torusR 0.1 0.2 
--    mapM_ (\lo -> drawPrimitive LineLoop blue lo) $ surface1  
--    mapM_ (\lo -> drawPrimitive LineLoop red lo) $ tran $ surface1  
--    mapM_ (\lo -> drawPrimitive LineLoop green lo) $ surface2  
--    mapM_ (\lo -> drawPrimitive LineLoop blue lo) $ tran $ surface2  

    -- mapM_ renderSurface $ surface3
    -- renderSurface $ torusR 0.5 1.0 
    -- drawSurfaceR(\x y -> 2*(x^2) + 3*(y^2)) 4
    -- drawParamSurf(\u v -> u*v) (\u -> u) (\v ->v)
    -- (1 - cos(β))² + sin(β)² = 1
    --
    -- torus ⇒ (⃝¹ × ⃝¹)
    -- circle ⃝ ⇒ x = 1 - cos(α)
    --             y = sin(α)
    --            x² + y² = 1
    --         ⇒  (1 - cos(α))² + sin(α)²     = 1  
    --         ⇒  (1 - cos(α))² + sin(α)² - 1 = 0
    --
    -- torus ⇒ {(1 - cos(α))² + sin(α)² - 1} × {(2 - cos(β))² + sin(β)² - 0.5} = 0 
    -- drawSurface(\α β -> ((0.2 - cos(α))² + sin(α)² - 0.5) * (cos(β)² + (sin(β)²) - 0.3) )
    -- drawTorus2

    
--    http://localhost/image/torusred.png
--    Torus
--     let δ = rf(2*pi/(n-1))
--         n = 40 
--         r = 0.1
--         br = 0.2
-- 
--         fx = \i j -> (br + r*cos(δ*i))*cos(δ*j)
--         fy = \i j -> sin(rf δ*i)
--         fz = \i j -> (br + r*cos(rf δ*i))*sin(rf δ*j)
--         in drawParamSurface fx fy fz


-- Sphere  
--     let δ = rf(2*pi/(n-1))
--         n = 40 
--         r = 0.1
--         br = 0.2
-- 
--         fx = \i j -> cos(δ*i)*cos(δ*j)
--         fy = \i j -> sin(rf δ*i)
--         fz = \i j -> cos(rf δ*i)*sin(rf δ*j)
--         in drawParamSurface fx fy fz n
    
-- Sphere
--    let δ = rf(2*pi/(n-1))
--        n = 40 
--        r = 0.1
--        br = 0.2
--
--        fx::Int -> Int -> GLfloat
--        fx i j = let i' = fi i
--                     j' = fi j
--                 in cos(δ*i')*cos(δ*j')
--        
--        fy::Int -> Int -> GLfloat
--        fy i j = let i' = fi i
--                     j' = fi j
--                 in sin( δ*i')
--        
--        fz::Int -> Int -> GLfloat
--        -- fz i j = cos(rf δ*i)*sin(rf δ*j)
--        fz i j = let i' = fi i
--                     j' = fi j
--                 in cos( δ*i')*sin(δ*j')
--        in drawParamSurface fx fy fz

    
 -- Cylinder
   
    {-|
    let n = 40::Int
        δ = (2*pi)/(rf(n-1)) :: Float
        r = 0.3
        br = 0.2
        σ = 1/rf(n-1)

        fx::Int -> Int -> GLfloat
        fx i j = let i' = rf i
                     j' = rf j
                 in r * cos(δ * i')
        
        fy::Int -> Int -> GLfloat
        fy i j = let i' = rf i
                     j' = rf j
                     n = 3
                 in r * sin(δ * i')
        
        fz::Int -> Int -> GLfloat
        fz i j = let i' = rf i
                     j' = rf j
                 in 0.0
      in drawParamSurfaceN fx fy fz n
    
    let n = 40::Int
        δ = (2*pi)/(rf(n-1)) :: Float
        r = 0.3
        br = 0.2
        σ = 1/rf(n-1)

        fx::Int -> Int -> GLfloat
        fx i j = let i' = rf i
                     j' = rf j
                 in σ * i'
        fy::Int -> Int -> GLfloat
        fy i j = let i' = rf i
                     j' = rf j
                     n = 3
                 in r * cos(δ * i')
        
        fz::Int -> Int -> GLfloat
        fz i j = let i' = rf i
                     j' = rf j
                 in r*sin(δ * i')
      in drawParamSurfaceN fx fy fz n
    -}
    --let fovy = 60.0; aspect = 1.0; zNear = 4.0; zFar = (-2)
    --  in GM.perspective fovy aspect zNear zFar
    --
    {-|
    let x0 = -0.5::GLfloat
        y0 = -0.5::GLfloat
        z0 = 0.0::GLfloat
        x1 = 0.5::GLfloat
        y1 = 0.5::GLfloat
        z1 = 0.0::GLfloat in drawRect ((Vertex3 x0 y0 z0), (Vertex3 x1 y1 z1))
    -}

    -- $f(x) = \lim_{x \rightarrow \infty} e^x dx$
    -- drawHistogram2 [0.1, 0.2, 0.3, 0.2, 0.1, 0.2, 0.9, 0.3, (-0.2)]
    -- drawRect2d 0.3 0.2
    -- fstr <- readFileList tmpfile
    let fstr = lsfun
    -- let numls = map (\x -> read x :: Float) $ filter (\x -> (len . trim) x > 0 ) $ splitStr "[[:space:]]+" fstr
    -- pre numls    
    rls <- randomFloat 10
    let cx = [Vertex3 0.1 0.1 0, Vertex3 0.2 0.1 0, Vertex3 0.2 (-0.1) 0, Vertex3 (-0.1) (-0.1) 0]
    -- drawQuads cx
    -- drawHis [0.1, 0.2, 0.7, 0.2, 0.3, 0.4, 0.8,0.1, 0.1, 0.3, 0.7, 0.6, 0.5, 0.2, 0.23, 0.22, 0.11, 0.33, 0.39]
    -- drawHis numls
    -- moveToX drawRect2d (0.2, 0.4) 0.2
    -- renderText $ show 3.1415
    -- drawCurve (\x -> x*x) (0.0, 1.0) red
    
    -- drawRectFill2d blue (0.4, 0.3)

    let input = "let f x = x + 1 in f 3"
    {-|
    r <- runInterpreter $ testHint [input] fstr
    case r of
      Left err -> putStrLn $ errorString err
      Right () -> return ()

    VU.mapM_ (\(exp, c) -> do
                 fun <- interpret exp (as :: GLfloat -> GLfloat)
                   -- say $ show $ fun 3.141569
                   drawCurveV fun (-1.0, 1.0) c
             ) $ VU.zip (VU.fromList expls) colorls
    -}

    
    -- drawCurve (\x -> x*x) (-1.0, 1.0) green
    -- drawCurve (\x -> 2*x*x) (-1.0, 1.0) green
    -- drawCurve (\x -> 3*x*x) (-1.0, 1.0) red
    -- drawCurve (\x -> 4*x*x) (-1.0, 1.0) green
    -- drawCurve (\x -> 5*x*x) (-1.0, 1.0) blue
    -- drawCurve (\x -> 6*x*x) (-1.0, 1.0) white
    -- drawCurve (\x -> 7*x*x) (-1.0, 1.0) green
    -- drawCurve (\x -> 8*x*x) (-1.0, 1.0) red
    -- drawCurve (\x -> 9*x*x) (-1.0, 1.0) green

    {-|
                    p2

               p0       p1
    -}
    -- let p0 = Vertex3 0.0 0.0 0.0
        -- p1 = Vertex3 0.4 0.0 0.0
        -- p2 = Vertex3 0.4 0.4 0.0
        -- p3 = Vertex3 (rf $ (0.0 + 0.4 + 0.4)/3) (rf (0.0 + 0.0 + 0.4)/3) 0.0
        -- in do
           -- drawDot (Vertex3 0.0 0.0 0.0) -- p0
           -- drawDot (Vertex3 0.4 0.0 0.0) -- p1
           -- drawDot (Vertex3 0.4 0.4 0.0) -- p2
           -- drawDot p3

    ls3 <- (en "b" >>= \x -> readGLScript (x ++ "/tmp/draw.x"))

    -- pre ls3
    -- drawSegments blue (takeSegment ls3)
    let lsVex = takeTriangleVex ls3
    -- fl
    pre lsVex
    
    mapM_ (\x -> drawTriangleVex blue x) $ listToTuple3Vex lsVex
    mapM_ (\x -> drawDot x ) $ takePoint ls3
    mapM_ (\x -> drawSegment green x) $ listToTuple2Vex $ takeSegment ls3
    
    drawCircle2 (Vertex3 0.1 0.1 0.1) 0.4
    -- let vx = [Vertex3 0.1 0.1 0.1
              -- Vertex3 0.2 0.2 0.2
              -- Vertex3 0.3 0.3 0.3]

    -- else return ()


    G.swapBuffers w
    G.pollEvents
    mainLoop w ref refStep
main = mymain

