-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 
import System.Console.Haskeline
import Language.Haskell.Interpreter(interpret, as, Interpreter)

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - clse

{-|
main :: IO ()
main = runInputT defaultSettings loop
    where 
        loop :: InputT IO ()
        loop = do
               minput <- getInputLine "% "
               case minput of
                    Nothing -> return ()
                    Just "quit" -> return ()
                    Just input -> do outputStrLn $ "Input was: " ++ input
               loop
-}

{-|
main :: IO ()
main = do hSetBuffering stdin NoBuffering
          inputLoop
-}

inputLoop :: IO ()
inputLoop = do i <- getContents
               mapM_ putChar $ takeWhile ((/= 27) . ord) i

myGetLine :: IO String
myGetLine = do
    c <- getChar
    case c of
        '\n' -> return "new line" -- don't echo newlines
        _ -> do
            putChar c -- do echo everything else
            fmap (c:) myGetLine


readEvalPrintLoop :: IO()
readEvalPrintLoop = do
    line <- myGetLine -- getLine changed to myGetLine
    case line of
            "bye" -> return ()
            line   -> do putStrLn $ "line=" ++ line
                         readEvalPrintLoop

main = do
    -- hSetEcho stdin False
    hSetEcho stdin True 
    hSetBuffering stdin NoBuffering
    readEvalPrintLoop
