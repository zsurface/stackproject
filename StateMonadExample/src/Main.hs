-- import AronSimple
       
import Control.Monad
import Control.Applicative


-- main = do 
--         old <- timeNowSecond
--         print "hi" 
--         print "hi" 
--         print "hi" 
--         new <- timeNowSecond
--         print $ "sec=" ++ (show (new - old))
type S = String

data SM a = SM (S -> (a,S))  -- The monadic type


-- bind::(Monad m)=> m a -> (a -> m b) -> m b
-- m a `bind` f = m (f a)
-- m a >>= \x -> return f x 

instance Monad SM where
  -- defines state propagation
  SM c1 >>= fc2         =  SM (\s0 -> let (r,s1) = c1 s0 -- c1 = (S -> (a, S))
                                                         -- s0 = S
                                                         -- (r, s1) = c1 s0
                                          SM c2 = fc2 r in
                                      c2 s1)
                     -- =  SM (\s0 -> let (r, s1) = c1 s0; SM c2 = fc2 r in c2 s1)
  return k              =  SM (\s -> (k,s))


 -- extracts the state from the monad
readSM                  :: SM S
readSM                  =  SM (\s -> (s,s))

 -- updates the state of the monad by applying 'f'
updateSM                :: (S -> S) -> SM ()  -- alters the state
updateSM f              =  SM (\s -> ((), f s)) 

-- run a computation in the SM monad
runSM                   :: S -> SM a -> (a,S)
runSM s0 (SM c)         =  c s0




doSomething = do
	updateSM ((flip (++)) " world")
	
main = do
	let (_, thing) = runSM "hello" doSomething
	putStrLn thing
