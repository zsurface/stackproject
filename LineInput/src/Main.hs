-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE MultiWayIf #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Trans.Class
import Control.Concurrent


import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}


import System.Console.Haskeline
import System.Environment
import qualified Data.Text                 as TS
import           Data.Int (Int64)
import           Database.SQLite.Simple hiding (bind)
import           Database.SQLite.Simple.FromRow
import           Database.SQLite.Simple.ToRow
import           Database.SQLite.Simple.FromField
import           Database.SQLite.Simple.ToField
import           Database.SQLite.Simple.Internal
import           Database.SQLite.Simple.Ok
  
import AronModule

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close


--main :: IO ()
--main = runInputT defaultSettings loop
--   where
--       loop :: InputT IO ()
--       loop = do
--           minput <- getInputLine "% "
--           ls <- getInputLineWithInitial "prompt> " ("left", "") 
--           -- ls <- getInputLineWithInitial "prompt> " ("left ", "right") -- The cursor starts before the second word.
--           let str = case ls of
--                          Just x -> x
--                          Nothing -> "nothing"
--               
--           pre str 
--           case minput of
--               Nothing -> return ()
--               Just "quit" -> return ()
--               Just input -> do outputStrLn $ "Input was: " ++ input
--                                loop

{--
Testing the line-input functions and their interaction with ctrl-c signals.
Usage:
./Test          (line input)
./Test chars    (character input)
./Test password (no masking characters)
./Test password \*
./Test initial  (use initial text in the prompt)
--}

mySettings :: Settings IO
mySettings = defaultSettings {historyFile = Just "myhist"}

fun::Maybe String
fun = Just "hi"
      
myrun::String -> IO (Maybe String)
myrun cmd = do
    ls <- run cmd
    let str = concat ls
    if len str > 0 then return (Just str) else return Nothing
  
bmfile h = h </> "Library/Application Support/Firefox/Profiles/sk75a0xs.default-release-1/places.sqlite"

f9:: String -> IO()
f9 x = putStrLn x
  
-- KEY: getline editor, command line editor, edit line, haskell line, console line, getchar, getline
main :: IO ()
main = do
        args <- getArgs
        pre args
        home <- getEnv "HOME"
        clear
        let dbFile = bmfile home
        let ln = len args
        conn <- open dbFile
  
        args <- getArgs
        let inputFunc = case args of
                ["chars"] -> fmap (fmap (\c -> [c])) . getInputChar
                ["password"] -> getPassword Nothing
                ["password", [c]] -> getPassword (Just c)
                ["initial"] -> flip getInputLineWithInitial ("left ", "right")
                -- getInputLine :: (MonadIO m, MonadMask m) => String -> InputT m (Maybe String)
                _ -> getInputLine
        runInputT mySettings $ withInterrupt $ loop inputFunc 0
    where
        loop :: (String -> InputT IO (Maybe String)) -> Int -> InputT IO ()
        loop inputFunc n = do
            liftIO $ setCursorPos 30 10
            minput <-  handleInterrupt (return (Just "Caught interrupted"))
                        $ inputFunc (show n ++ ":")
            case minput of
                Nothing -> return ()
                Just "quit" -> return ()
                Just "q" -> return ()
                Just s -> do
                            outputStrLn ("line " ++ show n ++ ":" ++ s)
                          
                            -- outputStrLn $ liftIO $ putStrLn "How to use liftIO :: (MonadIO m) => IO a -> m a"
                            -- liftIO :: (MonadIO m) => IO a -> m a
                            -- InputT IO x
                            -- [   m   ] x
                            -- InputT IO is the instance of MonadIO
                            -- [   m   ]
                            -- liftIO $ putStrLn ⇒ liftIO (IO a) ⇒ (InputT IO) a
                            --                             IO a -> [    m    ] a
                            liftIO $ setCursorPos 32 10 
                            case s of
                              var | hasPrefix "-h" s -> do
                                      outputStrLn "help"
                                  | otherwise -> do
                                      outputStrLn "otherwise"
                            loop inputFunc (n+1)
