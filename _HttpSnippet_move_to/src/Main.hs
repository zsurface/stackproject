-- {{{ begin_fold
-- script
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"
{-# LANGUAGE MultiWayIf #-}

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import Network.HTTP
import Network.URI.Encode
import Rainbow
import System.Console.Pretty (Color (..), Style (..), bgColor, color, style, supportsPretty)

-- https://hackage.haskell.org/package/ansi-terminal-0.10.3/docs/System-Console-ANSI.html
import System.IO (hFlush, stdout)
import qualified System.Console.ANSI as AN
       
import AronModule

-- KEY: Get data from Wai Http Server, query snippet from httpd, console color, ansi color
-- The code is use vimscript to call Wail Server through 8080
-- /Users/cat/myfile/bitbucket/vim/vim.vimrc:274
-- 
-- Server: /Users/cat/myfile/bitbucket/haskell_webapp/wai.hs
--
-- Wed May  8 15:52:21 2019 
-- Snippet:         "s regex" => Snippet
-- Aron.java:       "j list" => Java Aron.*
-- Print.java:      "p pr"   => Java Print.*
-- AronModule.hs    "h list" => Haskell
--    var | var == "x " -> responseXCmd s         -- forget what it is for.
--        | var == "c " -> responseCmd conn s     -- Shell commands
--        | var == "j " -> responseJavaHtml s     -- Java AronLib.java with Html, css.
--        | var == "h " -> responseHaskellHtml s  -- Haskell AronModule.hs with Html, css.
--        | var == "k " -> queryLibHaskell s    -- Haskell AronModule.hs
--        | var == "i " -> queryLibJava s       -- Java $b/javalib/AronLib.java
--        | var == "p " -> queryLibJavaPackage "Print." s -- Java $b/javalib/Print.java
--        | var == "n " -> responseSnippetTxt s ref  -- Snippet with NO Html, css.
--        | otherwise   -> responseSnippetHtml conn s ref  -- Snippet with Html, css.
--
-- Sun May 12 11:58:33 2019  
-- Remove code from Html to Text
-- return Text file from Wai server directly
-------------------------------------------------------------------------------- 
-- url = "http://127.0.0.1:8080/snippet?id=s+emacs"
-- url = "http://127.0.0.1:8080/snippet?id="
url = "http://xfido.com:8081/snippet?id="



{-| 
    * Optional argument
    * `NoHead` => "-nh" => no header
    * `Color`  => "-co" => escape sequence ascii color 
    * `High`  => "-an" =>  Highlight word 
-} 
data Option = NoHead | Color  | High | Page

opt::Option -> String 
opt  NoHead = "-nh"
opt  Color  = "-co"
opt  High   = "-hi"
opt  Page   = "-p"

esc_SEQ="\x1b["                   
fg_BLACK=       esc_SEQ   ++ "30m"
fg_RED=         esc_SEQ   ++ "31m"
fg_GREEN=       esc_SEQ   ++ "32m"
fg_YELLOW=      esc_SEQ   ++ "33m"
fg_BLUE=        esc_SEQ   ++ "34m"
fg_MAGENTA=     esc_SEQ   ++ "35m"
fg_CYAN=        esc_SEQ   ++ "36m"
fg_WHITE=       esc_SEQ   ++ "37m"
fg_BR_BLACK=    esc_SEQ   ++ "90m"
fg_BR_RED=      esc_SEQ   ++ "91m"
fg_BR_GREEN=    esc_SEQ   ++ "92m"
fg_BR_YELLOW=   esc_SEQ   ++ "93m"
fg_BR_BLUE=     esc_SEQ   ++ "94m"
fg_BR_MAGENTA=  esc_SEQ   ++ "95m"
fg_BR_CYAN=     esc_SEQ   ++ "96m"
fg_BR_WHITE=    esc_SEQ   ++ "97m"
                                  
fg_END=         "\x1b[0m"         

                

{-| 
    === line with color
    psTab 5 (color Green   (replicate 80 '-'))
-} 
lineColor::IO()
lineColor = psTab 5 (color Green   (replicate 80 '-'))

{-| 
    * Make http call with following options,

    [Default option] include header and no color

    [-nh] no header
    [-co] escape ascii color


    >-nh  => no header
    >-co  => no escape ascii(console) color
    >
    >
    > HttpSnippet s java

    *  no option

    >
    > HttpSnippet -nh s java

    * header is removed, only code block is left 

    @
    & HttpSnippet -nh s java

    java_regex:*: java
    line 1
    line 2

    output:
    line 1
    line 2
    @

    
    * HttpSnippet -nh -co s java
    * no header with color 

    >
    > HttpSnippet -co s java

    * [block code] includes escape ascii(console) color

    @
    & HttpSnippet -co s java

    java_regex:*: java
    line 1
    line 2

    output: 
    java_regex:*: java
    [ascii] line 1 [ascii]
    [ascii] line 2 [ascii]
    @

    * HttpSnippet -co -nh  s java
    * opList = ["-co", "-nh", "-hi"]
    * -nh  => no header
    * -co  => no escape ascii (console) color
    * -hi  => highlight word 
    * param = "s+java"
-} 
main = do
        line <- getArgs 
        bo <- supportsPretty
        -- if bo then print "support pretty" else print "not support pretty"
        
        if length line > 0 then do
            let opList = takeWhile(\x -> head x == '-') line
            let param = concatStr (dropWhile(\x -> head x == '-') line) "+"
            -- let param = init $ foldr (\x y -> x ++ "+" ++ y) [] line
            -- putStrLn param
            let req = url ++ param 
            -- pp $ "[" <<< req <<< "]"
            -- TODO: get text without any Html
            rsp <- Network.HTTP.simpleHTTP (getRequest req)
            -- fetch document and return it (as a 'String'.)
            -- s <- fmap (take 100000) (getResponseBody rsp)
            s <- getResponseBody rsp
            -- remove the top or head line
            let s1 = map (\x -> searchReplace x "sed"   (color Red "\\0") ) $ lines s
            let s2 = map (\x -> searchReplace x "grep"  (color White "\\0") ) s1 
            let s3 = map (\x -> searchReplace x "awk"   (color Red "\\0") ) s2 
            let s4 = map (\x -> searchReplaceAny x "->" (color White "\\0") ) s3
            let s5 = map (\x -> searchReplaceAny x "=>" (color Red "\\0") ) s4
--            let s3 = map (\x -> searchReplace x "awk" "\x1b[32m" ++ "\\0" ++ "\x1b[0m") s2
            -- let ls = map(\x -> if len x > 0 then unlines $ (if (opt NoHead) `elem` opList then tail x  else x) else "" ) $ splitListEmptyLine $ lines s
            let ls = map(\x -> if len x > 0 then unlines $ (if (opt NoHead) `elem` opList then tail x  else x) else "" ) $ splitListEmptyLine s5
            
            -- color is NOT working so far.. 
            -- let diffColorList = map(\x -> unlines $ map(\(n, b) -> if div n 2 == 0 then color Red b else color Pink b ) (zip [0..] x) ) $ splitListEmptyLine $ lines s
            -- let diffColorList = map(\x -> unlines $ map(\(n, b) -> color Red b) (zip [0..] x) ) $ splitListEmptyLine $ lines s
            let diffColorList = map(\x -> unlines $ map(\(n, b) -> b) (zip [0..] x) ) $ splitListEmptyLine $ lines s
            -- pre diffColorList

            -- let lsm = if (opt Color) `elem` opList then lss else ls 
            let outList = if | opt NoHead `elem` opList -> map(\x -> if len x > 0 then unlines $ tail x else "") $ splitListEmptyLine s5
                             | opt High   `elem` opList -> map(\x -> if len x > 0 then unlines x else "") $ splitListEmptyLine s5 
                             | opt Color  `elem` opList -> diffColorList 
                             | otherwise                -> map(\x -> if len x > 0 then unlines x else "" ) $ splitListEmptyLine $ lines s 

            -- mapM_ (\x -> putStrLn x) lsm

            let zoutList = zip [1..] outList
            mapM_ (\(n, x) -> do
                              -- show code in page style
                              if opt Page `elem` opList then do
                                clear
                                setCursorPos 2 5
                                mapM_ (psTab 5) $ lines x   -- lines x => [String]
                                tmp <- getEnv "t"
                                writeFile tmp x
                                setCursorPos (2 + (len $ lines x) + 1) 5
                                psTab 5 $ "[" ++ (show $ len outList) ++ "] => " ++ (show n)
                                setCursorPos (2 + (len $ lines x) + 1) 20 
                                psTab 5 $ "Code => [" ++ tmp ++ "]"
                                bo <- AN.hSupportsANSI stdout 
                                if bo then print "YES" else print "NO"
                                size <- getTerminalSize
                                psTab 5 $ "size=" ++ (show size)
                                _ <- getLine
                                clear
                                setCursorPos 2 5
                                psTab 5 (color Red $ replicate 80 '-')
                              else do
                              -- show all code at once (no page style)
                                putStrLn x
                         ) zoutList  -- outList => [String] 

            -- putStr $ unlines ls
            -- writeFile "/tmp/xx.html" s
            -- html to text file
            -- text <- run "w3m -dump /tmp/xx.html"
            -- pp text
            -- mapM putStrLn text 
            -- putStr "END" 
        else do
            psTab 5 (color Yellow "The  HttpSnippet DOES NOT connect to Redis.")
            psTab 5 (color Yellow "HttpSnippet => redis_query.hs Aron.java")
            psTab 5 (color Yellow "HttpSnippet => redis_query.hs Print.java")
            psTab 5 (color Yellow "HttpSnippet => redis_query.hs AronModule.hs")
            lineColor
            psTab 5 (color Red "HttpSnippet n emacs (NoHtml )=> snippet => snippet.hs")
            psTab 5 (color Red "HttpSnippet k list  (NoHtml )=> Haskell => AronModule.hs")
            psTab 5 (color Red "HttpSnippet i list  (NoHtml )=> Java    => Aron.java")
            psTab 5 (color White $ replicate 80 '-')
            psTab 5 (color White "HttpSnippet -p n latex => pager")
            psTab 5 (color White "HttpSnippet -co n latex => color")
            psTab 5 (color White "HttpSnippet -hi n latex => highlight")
            lineColor
            psTab 5 (color Yellow "HttpSnippet j list  (Html   )=> Java    => Aron.java")
            psTab 5 (color Yellow "HttpSnippet s regex (Html   )=> Snippet => snippet.hs")
            lineColor
            psTab 5 (color Yellow "HttpSnippet h list  (Html   )=> Haskell => AronModule.hs")
            lineColor
            psTab 5 (color Yellow "HttpSnippet p pr    (NoHtml )=> Java    => Print.java")

        -- run "haskellbin httpSnippet.hs httpSnippet" 
