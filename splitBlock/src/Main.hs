-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE MultiWayIf #-}
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import Text.RawString.QQ

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 
import AronAlias

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

-- fileBlock "/x.x" "===" -drophead
-- fileBlock "/x.x" "===" -droplast
-- fileBlock "/x.x" "===" -append  "str"
-- fileBlock "/x.x" "===" -prepend "str"
-- fileBlock "/x.x" "===" -size         => 3

defaultDel = replicate 10 '='


-- ["", "", "a", " ", " "] => ["a"]
trimList::[String] -> [String]
trimList lss = let f x = dropWhile (\x -> (len . trim) x == 0) x 
                   rev = reverse
               in  (rev . f . rev . f) lss 

main = do 
        argList <- getArgs
        -- pp $ length argList
        let lena = len argList
        if lena == 3 || lena == 4 then do
            let path = head argList
            let separator = (head . tail) argList
            let opt = trim $ argList !! 2 
            fs <- rfl path
            -- Drop all empty lines from the end because the last line is the delimiter
            let fs' = reverse $ dropWhile (\x -> (len . trim) x == 0) $ reverse fs
            fw "fs'"
            pre fs'
            if len fs' >= 0 then do
                let delimiter = len fs' > 0 ? last fs' $ defaultDel
                let ls = filter (\x -> len x > 0 ) $ splitBlock fs' separator 
                fw "ls"
                pre ls
                -- ls => [[String]]
                if len ls >= 0 then do
                    let ls' =  len ls == 0 ? ls $ let s = concat $ map (\x -> trim x) $ last ls in (len s == 0 ? init ls $ ls) 
                    fw "ls'"
                    pre ls'

                    case opt of
                        v | v == "-drophead" && lena == 3 -> do 
                                        let ss = map (\x -> trimList x ++ [delimiter] ) ls'
                                        let s = len ss > 0 ? tail (map (trim . unlines) ss) $ [] 
                                        wfl path s
                                        pp "-drophead"
                        v | v == "-droplast" && lena == 3 -> do
                                        let ss = map (\x -> trimList x ++ [delimiter] ) ls'
                                        let s = init $ map (trim . unlines) ss 
                                        wfl path s
                                        pp "-droplast"
                        v | v == "-dropindex" && lena == 4 -> do
                                        let ss = map (\x -> trimList x ++ [delimiter] ) ls'
                                        let inx = strToInt $ last argList
                                        let st = removeIndex inx $ map (trim . unlines) ss
                                        wfl path st
                                        pp "-dropindex"
                        v | v == "-printindex" && lena == 4 -> do
                                        let ss = map (\x -> trimList x) ls'
                                        let inx = strToInt $ last argList
                                        let st = 0 <= inx && inx < len ss ? ss !! inx $ error ("ERROR: len =" ++ (show . len) ss ++ " index=" ++ show inx)
                                        putStr $ unlines st
                                        pp "-printindex"

                        v | v == "-printhead" && lena == 3 -> do
                                        let ss = map (\x -> trimList x) ls'
                                        let inx = strToInt $ last argList
                                        let st = len ss > 0 ? head ss $ error ("ERROR: len =" ++ (show . len) ss ++ " index=" ++ show inx)
                                        putStr $ unlines st
                                        pp "-printhead"
                        v | v == "-printlast" && lena == 3 -> do
                                        let ss = map (\x -> trimList x) ls'
                                        let inx = strToInt $ last argList
                                        let st = len ss > 0 ? last ss $ error ("ERROR: len =" ++ (show . len) ss ++ " index=" ++ show inx)
                                        putStr $ unlines st
                                        pp "-printlast"

                        v | v == "-append" && lena == 4 -> do
                                        let str = last argList 
                                        let la = (map trimList ls') ++ [lines str] 
                                        let tt = map (trim . unlines) $ map (\x -> x ++ [delimiter] ) la 
                                        wfl path tt 
                                        pp "-append"

                        v | v == "-appendlist" && lena == 4 -> do
                                        let str = last argList 
                                        let hls = read str :: [String] 
                                        let la = (map trimList ls') ++ [hls] 
                                        let tt = map (trim . unlines) $ map (\x -> x ++ [delimiter] ) la 
                                        wfl path tt 
                                        pp "-appendlist"

                        v | v == "-prepend" && lena == 4 -> do
                                        let str = last argList 
                                        let la = [lines str] ++ ls'
                                        let tt = map (trim . unlines) $ map (\x -> trimList x ++ [delimiter] ) la 
                                        wfl path tt 
                                        pp "-prepend"
                        v | v == "-size" && lena == 3 -> do
                                        putStr $ (show . len) ls'

                        _           -> do 
                                        pp "Invalid option"


                    -- pre ls
                    -- pre ls' 
                    mapM_ (\x -> do 
                                 putStrLn $ show x
                                 -- putStr " "

                          ) $ map unlines ls'
                else do
                    print "File is empty?"
            else do
                print "Only empty lines in File?"
        else do
            putStrLn $ [r| splitBlock "/tmp/x.x" "==" => "line1" "line2\nline3\n" "line4"|] 
            pp ""
            putStrLn $ [r| Split file /tmp/x.x to different blocks with Separator === |]
















