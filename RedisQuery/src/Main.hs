-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- import Turtle
-- echo "turtle"
{-# LANGUAGE DuplicateRecordFields #-} 
-- {-# LANGUAGE OverloadedStrings, DeriveGeneric, DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}
-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import Data.Char
import Data.Maybe(fromJust, fromMaybe)
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
--import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Data.Typeable (typeOf)
import Control.Monad (unless, when, liftM, liftM2, liftM3)
import Control.Monad.IO.Class
import Control.Concurrent 
import Database.Redis
import GHC.Generics

import qualified Text.Regex.TDFA as TD
import qualified Data.Array as DR 
import qualified Data.List as L
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Char8 as BS
import qualified Data.ByteString as SB -- strict ByteString 
import qualified Data.ByteString.Lazy.UTF8 as BLU
import qualified Data.ByteString.UTF8 as BSU
import qualified Data.Text as DT
import qualified Data.Text.Lazy as DL 
import qualified Data.Map.Strict as M
import qualified Data.Aeson as DA
import qualified Data.Aeson.Text as AAT  -- encodeToLazyText

import AronModule 

{-| 
    1 Read Java and Haskell library
    2 Parse all functions/methods from Aron.java and AronModule.hs
    3 Build prefix string from name of functions/methods
-} 
type MMap a b = M.Map a [b]

printBlock::[[DL.Text]] -> IO ()
printBlock cx = (mapM_ . mapM_) (putStrLn . lazyTextToStr) cx 


fname = "/Users/cat/myfile/bitbucket/testfile/test.txt"
jname = "/Users/cat/myfile/bitbucket/javalib/Aron.java"
hname = "/Users/cat/myfile/bitbucket/haskelllib/AronModule.hs"
-- hname = "/Users/cat/myfile/bitbucket/testfile/AronModule_test.hs"

-- geneMap2 bs ws ["line1", "line2"] -> [([k0, k1], 1, ["line1"])]
geneMap2::String->String-> [String] -> [([String], Integer, [String])]
geneMap2 _ _ [] = [] 
geneMap2 bs ws cx = zblock 
    where
     block = filter(\x -> len x > 0) $ splitBlock2 cx bs 
     sblock = map(\k -> (unique $ join $ map(\x -> filter(\e -> len e > 0 && isWord e) $ splitStrChar ws x) k, k)) block
     zblock = zipWith(\x y -> (fst y, x, snd y)) [0..] sblock


splitBlock2::[String] -> String -> [[String]]
splitBlock2 [] _ = []
splitBlock2 cx pat = splitWhen (\x -> matchTest (mkRegex pat) x) cx

{-| 
    [([k0, k1], a0)] ,  Map => k0 -> a0
                               k1 -> a0
-} 
addMore::(Ord e)=>[([e], a)] -> M.Map e [a]-> M.Map e [a] 
addMore [] m = m
addMore (s:cs) m = addMore cs $ add s m 

{-| 
    ===
    ([k0, k1], a0) , Map => k0 -> a0
                            k1 -> a0
-} 
add::(Ord e)=>([e], a) -> M.Map e [a] -> M.Map e [a] 
add ([], _)   m = m
add (s:cs, n) m = case ls of 
                       Just x -> add (cs, n) $ M.insert s (n:x) m  -- ^ contains s
                       _      -> add (cs, n) $ M.insert s [n] m    -- ^ not contains s
    where
        ls = M.lookup s m -- Maybe a

data Block = Block{bblock::[DL.Text]} deriving (Generic, Show)
data MBlock = MBlock{mblock::[Integer]} deriving (Generic, Show)
-- data Block = Block{bblock::[[ByteString]]} deriving (Generic, Show)

instance DA.FromJSON Block 
instance DA.ToJSON Block where
    -- No need to provide a toJSON implementation.

    -- For efficiency, we write a simple toEncoding implementation, as
    -- the default version uses toJSON.
    toEncoding = DA.genericToEncoding DA.defaultOptions

instance DA.FromJSON MBlock 
instance DA.ToJSON MBlock where
    -- No need to provide a toJSON implementation.

    -- For efficiency, we write a simple toEncoding implementation, as
    -- the default version uses toJSON.
    toEncoding = DA.genericToEncoding DA.defaultOptions

{-| 
    @
    import qualified Data.ByteString.Char8 as BS
    String to ByteString
    @
-} 
strToBS::String -> SB.ByteString
strToBS s = BS.pack s

fun::SB.ByteString -> Redis SB.ByteString
fun key = do
    result <- get key
    case result of
        Left _ -> return $ BSU.fromString "Some error occurred"
        Right v -> return $ fromMaybe (BSU.fromString "Could not find key") v

enMapBlock1::M.Map String [Integer] -> [(SB.ByteString, SB.ByteString)]
enMapBlock1 gmap = L.map(\s -> 
                            (toSBS $ fst s, 
                             toSBS $ DA.encode $ MBlock{mblock = snd s}
                            )
                        ) $ M.toList gmap -- > [(ByteString, [[ByteString]])]

enMapBlock2::M.Map Integer [String] -> [(SB.ByteString, SB.ByteString)]
enMapBlock2 gmap = L.map(\s -> 
                            (toSBS $ intToString $ fst s,
                             
                             toSBS $ DA.encode $ Block{bblock = (L.map) (\c -> DL.pack c) $ snd s}
                            )
                        ) $ M.toList gmap -- > [(ByteString, [[ByteString]])]

mapPair::String -> String -> [String] ->(M.Map String [Integer], M.Map Integer [String])
mapPair _ _ [] = (M.empty, M.empty)
mapPair ws bs cx = (map1, map2)
    where
       bb = geneMap2 bs ws cx 
       ls = map(\x -> (t1 x, t2 x)) bb 
       lt = map(\x -> (t2 x, t3 x)) bb 
       map1 = addMore ls M.empty
       map2 = M.fromList lt 

helpme::IO()
helpme = do
         fl
         putStrLn "Need one argument" 
         putStrLn "RedisQuery Aron.list"
         putStrLn "RedisQuery Print.p"
         fl


redisQuery::String -> IO [[DL.Text]]
redisQuery input = do
           let bs = "^[[:space:]]*(---){1,}[[:space:]]*" -- block delimiter
           let ws = "[,.<>;()/\\ ]"                      -- ws: word delimiter 
           conn <- connect defaultConnectInfo
           -- runRedis::Connection -> Redis a -> IO a
           -- newType Redis a = Redis (ReaderT RedisEnv IO a) deriving (Monad, MonadIO, Functor, Applicative)
           s <- runRedis conn $ do 
                    ret1 <- get $ toSBS input
                    let key2 = case ret1 of
                                Left _ -> BSU.fromString "Some error occurred"
                                Right v -> fromMaybe (BSU.fromString "Could not find key") v
                    -- liftIO $ pw "key2" key2
                    -- data MBlock = MBlock{mblock::[Integer]} deriving (Generic, Show)
                    let may = DA.decode $ BL.fromStrict key2 :: Maybe MBlock
                    let ls = case may of 
                                (Just x) -> mblock x
                                _        -> []
                    -- liftIO $ print ls
                    let bs = L.map(\x -> BSU.fromString $ intToString x) ls 
                    ret2 <- mget bs

                    -- key3: [ByteString]
                    let key3 = case ret2 of
                                Left _  -> [BSU.fromString "Error occurred"] 
                                Right v -> map(\x -> fromMaybe (BSU.fromString "can not find key") x) v 
                    -- liftIO $ pw "key3" key3
                    let rblock = map(\r -> DA.decode r) $ map BL.fromStrict key3 :: [Maybe Block] 
                    let lsblock = map(\x -> case x of
                                                 (Just e) -> bblock e
                                                 _        -> []
                                          ) rblock 
                    -- liftIO $ pw "rblock" rblock
                    -- liftIO change IO a to monadIO a
                    -- liftIO::(MonadIO m) => IO a => m a 
                    -- liftIO $ printBlock lsblock
                    return lsblock
           return s

main = do 
       argList <- getArgs 
       if len argList == 0 then helpme else do
           let input = head argList

           let bs = "^[[:space:]]*(---){1,}[[:space:]]*" -- block delimiter
           let ws = "[,.<>;()/\\ ]"                      -- ws: word delimiter 
           conn <- connect defaultConnectInfo
           -- runRedis::Connection -> Redis a -> IO a
           -- newType Redis a = Redis (ReaderT RedisEnv IO a) deriving (Monad, MonadIO, Functor, Applicative)
           runRedis conn $ do 
                ret1 <- get $ toSBS input
                let key2 = case ret1 of
                            Left _ -> BSU.fromString "Some error occurred"
                            Right v -> fromMaybe (BSU.fromString "Could not find key") v
                -- liftIO $ pw "key2" key2
                let may = DA.decode $ BL.fromStrict key2 :: Maybe MBlock
                let ls = case may of 
                            (Just x) -> mblock x
                            _        -> []
                -- liftIO $ print ls
                let bs = L.map(\x -> BSU.fromString $ intToString x) ls 
                ret2 <- mget bs

                -- key3: [ByteString]
                let key3 = case ret2 of
                            Left _  -> [BSU.fromString "Error occurred"] 
                            Right v -> map(\x -> fromMaybe (BSU.fromString "can not find key") x) v 
                -- liftIO $ pw "key3" key3
                let rblock = map(\r -> DA.decode r) $ map BL.fromStrict key3 :: [Maybe Block] 
                let lsblock = map(\x -> case x of
                                             (Just e) -> bblock e
                                             _        -> []
                                      ) rblock 
                -- liftIO $ pw "rblock" rblock
                -- liftIO change IO a to monadIO a
                -- liftIO::(MonadIO m) => IO a => m a 
                liftIO $ printBlock lsblock













