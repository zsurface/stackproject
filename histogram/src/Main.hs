-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

import AronModule 

import           Codec.Picture
import           Codec.Picture.Drawing
import           Codec.Picture.Types
import           Control.Monad.Primitive


geneImg::IORef Int -> Int -> Int -> IO()
geneImg ref w h = do
    n <- readIORef ref
    let pngName = "00" ++ (show n) ++ ".png"
    img <- withMutableImage w h (PixelRGB8 150 0 0) $ \m -> do
      -- A green diagonal line
      drawLine m 0 0 (w - 1) (h - 1) (PixelRGB8 0 255 0)

      -- A blue square at a 45-degree angle
      drawPolygon m [(50, 50), (75, 75), (100, 50), (75, 25), (50, 50)] (PixelRGB8 0 0 255)

      -- An orange bounding rectangle
      drawRectangle m 0 0 (w - 1) (h - 1) (PixelRGB8 255 150 0)

      -- A mangenta filled rectangle
      fillRectangle m (200 + n) 30 (250 + n) 130 (PixelRGB8 255 0 255)
  
      modifyIORef ref (+1)
      -- A dark green filled triangle
      fillTriangle m 49 200 250 300 70 350 (PixelRGB8 0 150 50)
      ls <- randIntList 100 (1, 100)
      drawRectBar m (10, 20) (20, 30) 100 ls (PixelRGB8 0 255 0)

      -- A blue pentagon
      drawPolygon m
          [ (340, 80)
          , (245, 149)
          , (281, 261)
          , (399, 261)
          , (435, 149)
          , (340, 80)
          ]
          (PixelRGB8 0 0 255)
    writePng pngName img


main :: IO ()
main = do
    argList <- getArgs
    if (len argList == 1) then do
        let fname = head argList
        histogram fname
        print "gene png file => /tmp/histogram.png"
    else print "ERROR: need a file"
