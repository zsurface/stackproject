{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_JuicyPixelExample (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/JuicyPixelExample/.stack-work/install/x86_64-osx/7c621c7b9e3b35fe3f85b2af9dfde17f433b7f43ac795ac5159c22c877402f68/8.10.4/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/JuicyPixelExample/.stack-work/install/x86_64-osx/7c621c7b9e3b35fe3f85b2af9dfde17f433b7f43ac795ac5159c22c877402f68/8.10.4/lib/x86_64-osx-ghc-8.10.4/JuicyPixelExample-0.1.0.0-EQZpS1juEKBLXCecjoe7Rp-JuicyPixelExample"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/JuicyPixelExample/.stack-work/install/x86_64-osx/7c621c7b9e3b35fe3f85b2af9dfde17f433b7f43ac795ac5159c22c877402f68/8.10.4/lib/x86_64-osx-ghc-8.10.4"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/JuicyPixelExample/.stack-work/install/x86_64-osx/7c621c7b9e3b35fe3f85b2af9dfde17f433b7f43ac795ac5159c22c877402f68/8.10.4/share/x86_64-osx-ghc-8.10.4/JuicyPixelExample-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/JuicyPixelExample/.stack-work/install/x86_64-osx/7c621c7b9e3b35fe3f85b2af9dfde17f433b7f43ac795ac5159c22c877402f68/8.10.4/libexec/x86_64-osx-ghc-8.10.4/JuicyPixelExample-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/JuicyPixelExample/.stack-work/install/x86_64-osx/7c621c7b9e3b35fe3f85b2af9dfde17f433b7f43ac795ac5159c22c877402f68/8.10.4/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "JuicyPixelExample_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "JuicyPixelExample_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "JuicyPixelExample_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "JuicyPixelExample_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "JuicyPixelExample_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "JuicyPixelExample_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
