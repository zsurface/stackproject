-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
{-# LANGUAGE MultiWayIf #-}  --
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}
import Text.RawString.QQ
import Turtle (shell, empty)
import Control.Monad
import Data.Char
import Data.Default
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Tuple(swap)
import Data.Maybe (fromJust)
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Posix.Types
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef
import Control.Monad (unless, when)
import Control.Concurrent(threadDelay)
import Foreign.C.Types
import Control.Concurrent

import AronModule

macOS = "darwin"
freeBSD = "freebsd"


{-|
    === Watch files and raise event

    * The code use file modificationTime to get the diff of timestamp of a file.
    * Sun Feb  2 13:08:27 2020
    * Fixed 'createProcess' error
-}

xhlib        = "myfile/bitbucket/haskelllib"
xhgraphic    = "myfile/bitbucket/haskelllib/AronGraphic.hs"
xaronmodule  = "myfile/bitbucket/haskelllib/AronModule.hs"
xwailib      = "myfile/bitbucket/haskelllib/WaiLib.hs"
xsnippeths   = "myfile/bitbucket/snippets/snippet.hs"
xlatexnum    = "myfile/bitbucket/math/number.tex"
xgraphictest = "myfile/bitbucket/haskell/AronGraphicTest.hs"
xarongl      = "myfile/bitbucket/haskelllib/AronOpenGL.hs"
xaronjava    = "myfile/bitbucket/javalib/Aron.java"
xpassdir     = "myfile/password"
xhlibdir     = "myfile/bitbucket/haskelllib"
xdothistory  = "myfile/bitbucket/shell/dot_bash_history"
               
xAronModuleLib  = "myfile/bitbucket/stackproject/AronModuleLib"
          
main = do
        let (+) = (++)
        home <- getEnv "HOME"
        configMap <- readConfig $ home </> "myfile/bitbucket/publicfile/osconfig.txt"
        osv <- getEnv "OSTYPE"

        let os = if | containStr macOS   osv -> macOS
                    | containStr freeBSD osv -> freeBSD
                    | otherwise              -> error "Unknow OS"

        let hlibPath       = home </> xhlib
        let hGraphic       = home </> xhgraphic
        let hAronModule    = home </> xaronmodule
        let waiLib         = home </> xwailib
        let snippet        = home </> xsnippeths
        let pdf            = home </> xlatexnum
        let aronGraphicTest= home </> xgraphictest
        let aronopengl     = home </> xarongl
        let javalib        = home </> xaronjava
        let passworddir    = home </> xpassdir
        let haskelllib     = home </> xhlibdir
        let dothistory     = home </> xdothistory
        let spAronModuledir = home </> xAronModuleLib

        -- let javadoc= "cd $b/javalib && javadoc -cp $(ls -d $jlib/jar/*.jar | tr '\n' ':') -noqualifier all -d /Library/WebServer/Documents/xfido/htmljavadoc  *.java"
        forever $ do
            nvar1 <- getFileStatus hGraphic    >>= return . modificationTime
            nvar2 <- getFileStatus hAronModule >>= return . modificationTime
            nvar3 <- getFileStatus snippet     >>= return . modificationTime
            nvar4 <- getFileStatus pdf         >>= return . modificationTime
            nvar5 <- getFileStatus aronopengl  >>= return . modificationTime
            nvarJavalib <- getFileStatus javalib     >>= return . modificationTime
            nvar7 <- if os == macOS then (getFileStatus passworddir >>= return . modificationTime) else return (CTime 0)
            nvar8 <- getFileStatus waiLib      >>= return . modificationTime
            nvar9 <- getFileStatus haskelllib  >>= return . modificationTime
            nvar10 <- getFileStatus dothistory  >>= return . modificationTime
            prevDirMo <- dirModified haskelllib

            nowTime <- timeNowSecond
            sleepSec 4

            now1 <- getFileStatus hGraphic    >>= return . modificationTime
            now2 <- getFileStatus hAronModule >>= return . modificationTime
            now3 <- getFileStatus snippet     >>= return . modificationTime
            now4 <- getFileStatus pdf         >>= return . modificationTime
            now5 <- getFileStatus aronopengl  >>= return . modificationTime
            nowJavalib <- getFileStatus javalib     >>= return . modificationTime
            -- now7 <- getFileStatus passworddir >>= return . modificationTime
            now7 <- if os == macOS then (getFileStatus passworddir >>= return . modificationTime) else return (CTime 0)
            now8 <- getFileStatus waiLib      >>= return . modificationTime
            now9 <- getFileStatus haskelllib  >>= return . modificationTime
            now10 <- getFileStatus dothistory  >>= return . modificationTime
            currDirMo <- dirModified haskelllib
            -- bad idea here using prevDirmo currDirmo
            -- or [ 1 0 0 1 1 0 0 ] => 1
            let dirMod = or $ zipWith(\x y -> x < y) prevDirMo currDirMo
            if | now1 > nvar1 -> return ()
               | now2 > nvar2 -> do
                    let okMsg = [r|"OK => run.sh => AronModuleLib"|]
                    let errMsg = [r|"ERROR => run.sh => AronModuleLib"|]
                    cd spAronModuledir
                    sys "./run.sh c" >>= \x -> if x == ExitSuccess then appNotify okMsg else appNotify errMsg
                    return ()
        
               | dirMod || (now9 > nvar9) -> do
                    let rmsg = [r|"OK => Run RedisInsert"|]
                    let rError = [r|"Error => RedisInsert"|]
                    sys "RedisInsert" >>= \x -> if x == ExitSuccess then appNotify rmsg else appNotify rError -- insert data to redis server,                     

                    ex <- sys $ "gene_haskell_haddock"
                    ex <- sys $ "runh2 " + aronGraphicTest
                    ex <- sys $ dropExtension aronGraphicTest
                    cd hlibPath
                    ex <- sys $ "gg p 'add'"    -- push to repo.
                    let msg =  [r|"gene_haskell, runh2 aronGraphicTest, runh2 AronModule.hs"|]
                    appNotify msg

                    print ex
                    return ()
               | now3 > nvar3 -> do
                    pp "restart haskellwebapp2.sh"
                    let killapp2 = [r|"ps aux | grep haskellwebapp2 | awk '{print $2}' | line 'x -> kill -9 x'"|]
                    sys "ps aux | grep haskellwebapp2 | awk '{print $2}' | line 'x -> kill -9 x'"
                    sleepSec 2
                    sys "ps aux | grep haskellwebapp2.sh | awk '{print $2}' | line 'x -> kill -9 x'"
                    sleepSec 3
                    sys "haskellwebapp2.sh &"
                    return ()
               | now4 > nvar4 -> do
                    let outdir = " -outdir=" + (takeDirectory pdf)
                    let cmd = "latexmk -pdf -synctex=1 -file-line-error " + outdir + pdf
                    out <- sys $ "latexmk -pdf -synctex=1 -file-line-error " + outdir + pdf
                    pp out
                    -- following line is from ~/.latexmk
                    out <- sys $ "open -a /Applications/Skim.app/Contents/MacOS/Skim"
                    let pf = (dropExtension pdf) + ".pdf" -- .tex => .pdf
                    out <- sys $ "open -a /Applications/Skim.app/Contents/MacOS/Skim " + pf
                    return ()

               | now5 - nvar5 > 10000 -> sys "gene_haskell_haddock" >>= \x -> print x
               | nowJavalib - nvarJavalib > 10000 -> do
                    docMVar <- newEmptyMVar
                    comMVar <- newEmptyMVar
                    forkIO(do
                         exitCode <- sys "java_genedoc.sh"
                         putMVar docMVar exitCode
                         )
                    forkIO(do
                         exitCode <- compileJava javalib
                         putMVar comMVar exitCode
                         )
                    docExitCode <- takeMVar docMVar
                    case docExitCode of
                         ExitSuccess -> appNotify "\"java_genedoc.sh => ExitSuccess\""
                         ExitFailure x -> appNotify "\"java_genedoc.sh => ExitFailure\""
                    comExitCode <- takeMVar comMVar
                    case comExitCode of
                         ExitSuccess -> print "Nice ExitSuccess" >> appNotify "\"compile ExitSuccess\""
                         ExitFailure x -> print "ExitFailure" >> appNotify "\"compile ExitFailure \""

                    pp "done"
               | os == macOS && now7 > nvar7 -> do
                      txtList <- lsRegex passworddir "\\.txt$|\\.pass$"
                      if len txtList > 0
                      then do
                        let errorFile = home </> "myfile/bitbucket/testfile/error.txt"
                        date <- getLocalDate
                        pp $ "Error: Remove txt file from " + passworddir
                        -- use run MacOS script to send notification
                        let str = concatStr txtList " "
                        let msg = [r|"Urgent: Remove txt or pass file from password dir:"|] <> str 
                        appNotify msg 
                        let txtError = date + " -> " + str 
                        putStrLn (unlines txtList)
                        writeToFile errorFile [txtError]
                        pp txtList
                      else pp $ "Ok in " + passworddir
               | now8 > nvar8 -> sys "gene_haskell_haddock" >> return ()
               -- | now10 > nvar10 -> sys "InsectHistoryToSqlite3" >> appNotify "'InsectHistoryToSqlite3'"
               | now10 - nvar10 > 10 -> sys "InsectHistoryToSqlite3" >>  pp ("now10=" + (show now10) + " nvar10=" + (show nvar10) + " now10 - nvar10 = " + (show (now10 - nvar10))) >>  return () 
               | otherwise -> return ()
        pp "done!"

