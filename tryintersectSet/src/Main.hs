import AronSimple

import qualified Data.HashSet as HS
import qualified Data.Hashable as DH

intersectSet::(Eq a, DH.Hashable a) => [a] -> [a] -> [a]
intersectSet s1 s2 = ar
  where
    a1 = HS.fromList s1
    a2 = HS.fromList s2
    ar = HS.toList $ HS.intersection a1 a2


intersectFile::FilePath -> FilePath -> IO [String]
intersectFile f1 f2 = do
      s1 <- readFileLatin1ToList f1
      s2 <- readFileLatin1ToList f2
      return $ intersectSet s1 s2

main = do 
        old <- timeNowSecond
        new <- timeNowSecond
        print $ "sec=" ++ (show (new - old))
        let s1 = [1, 2]::[Integer]
        let s2 = [2, 3]::[Integer]
        let a1 = ["Chateau", "dog"]
        let a2 = ["a", "dog"]
        pre $ intersectSet s1 s2
        pre $ intersectSet a1 a2
        s <- intersectFile "/tmp/f.x" "/tmp/f.x"
        pre s
