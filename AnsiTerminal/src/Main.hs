-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent
import System.Console.ANSI

import qualified Text.Regex.TDFA as TD
import Control.Monad.Trans
import System.Console.Haskeline


import System.IO (stdin, hSetEcho, hSetBuffering, BufferMode(NoBuffering))
import Control.Monad (when)

import System.IO (stdin, hReady)



--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close





{-|     
printTable::Int -> Int -> [String] -> IO()
printTable h w cx = do
  let zz = zip [0..] cx
  mapM_ (\(n, x) -> do
           setCursorPosition (h + n) w
           putStrLn x) zz

main :: IO ()
main = do
  clear
  setSGR [SetColor Foreground Vivid Red]
  setSGR [SetColor Background Vivid Blue]
  putStrLn "Red-On-Blue"
  setSGR [Reset]  -- Reset to default colour scheme
  putStrLn "Default colors."
  setCursorPosition 10 10
  putStrLn "cursorPos"

  forever $ do
    setCursorPosition 10 10
    ch <- getChar
    print ch
    cmd <- getLine
    if cmd == "q" then exitSuccess else do
      clear
      (e2, so, si2) <- runSh $ toSText cmd
      if e2 == ExitSuccess then do
                                setCursorPosition 10 40
                                let ls = lines $ toStr so
                                let lm = map(\x -> (replicate 10 '-') ++ x) ls
                                pp "(10 40)"
                                -- pre lm
                                printTable 10 40 lm
                                printTable 10 80 lm


                                
      else do
           pp e2
           pp si2
           pp "Exit Error Zoom What is the meeting ID"

  pp "DONE"
  -- pp "DONE"
  -- setCursorPosition 10 10
  -- pp "(10, 10)"
-}

{-|
type Repl a = InputT IO a
  
process :: String -> IO ()
process = putStrLn

repl :: Repl ()
repl = do
  minput <- getInputLine "> "
  case minput of
    Nothing -> outputStrLn "Goodbye."
    Just input -> (liftIO $ process input) >> repl

main :: IO ()
main = runInputT defaultSettings repl
-}

getKey :: IO [Char]
getKey = reverse <$> getKey' ""
  where getKey' chars = do
          char <- getChar
          more <- hReady stdin
          (if more then getKey' else return) (char:chars)


-- Simple menu controller

inputBox::IO String
inputBox = do
  clear
  let x = 0
  inputStr <- newIORef (""::String)
  h <- newIORef (10::Int)
  w <- newIORef (10::Int)
  hSetBuffering stdin NoBuffering
  hSetEcho stdin False
  forever $ do

    hFlush stdout
    key <- getKey
    when (key /= "\ESC") $ do
      case key of
        "\ESC[A" -> do
          -- putStrLn "↑"
          h' <- readIORef h
          w' <- readIORef w
          -- putStr $ "w=" ++ (show w')
          let h1 = h' - 1
          setCursorPos h1 w'
          -- putStr $ "w1=" ++ (show w1)
          -- cursorForward w1
          -- setCursorColumn w1
          writeIORef h h1
    
        "\ESC[B" -> do
          -- putStrLn "↓"
          h' <- readIORef h
          w' <- readIORef w
          -- putStr $ "w=" ++ (show w')
          let h1 = h' + 1
          setCursorPos h1 w'
          -- putStr $ "w1=" ++ (show w1)
          -- cursorForward w1
          -- setCursorColumn w1
          writeIORef h h1
          
        "\ESC[C" -> do
          h' <- readIORef h
          w' <- readIORef w
          -- putStr $ "w=" ++ (show w')
          let w1 = w' + 1
          setCursorPos h' w1
          -- putStr $ "w1=" ++ (show w1)
          -- cursorForward w1
          -- setCursorColumn w1
          writeIORef w w1

          -- putStrLn "→"

        "\ESC[D" -> do
          -- putStrLn "←"
          h' <- readIORef h
          w' <- readIORef w
          -- putStr $ "w=" ++ (show w')
          let w1 = w' - 1
          setCursorPos h' w1
          -- putStr $ "w1=" ++ (show w1)
          -- cursorForward w1
          -- setCursorColumn w1
          writeIORef w w1
          
        "\n"     -> do
          putStrLn "σ"
    
        "\DEL"   -> putStrLn "⎋"
        _        -> do
          h' <- readIORef h
          w' <- readIORef w
          let w1 = w' + 1
          setCursorPos h' w1
          inputStr' <- readIORef inputStr
          putStr key
          writeIORef inputStr (inputStr' ++ key)
          -- putStr $ "w1=" ++ (show w1)
          -- cursorForward w1
          -- setCursorColumn w1
          writeIORef w w1
  s1 <- readIORef inputStr
  return s1


main = do
  inputBox


{-|
xcursorUp = "\ESC[A"
goto:: Int -> Int -> String
goto x y = '\ESC':'[':(show y ++(';':show x ++ "H"))
main = do
  pp "ok"
  putStr $ goto 40 10
  s <- getLineFlush
  putStr "DONE"
  main
-}
