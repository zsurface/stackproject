{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_OpenGLFont (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLFont/.stack-work/install/x86_64-osx/735434ae5515bc5cf8943d3cce7ab1f65331c20dc0bb8ed9b5fe2f74a02e5c8d/8.6.5/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLFont/.stack-work/install/x86_64-osx/735434ae5515bc5cf8943d3cce7ab1f65331c20dc0bb8ed9b5fe2f74a02e5c8d/8.6.5/lib/x86_64-osx-ghc-8.6.5/OpenGLFont-0.1.0.0-KNVJd3riTgRLIfPQhF1N43-OpenGLFont"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLFont/.stack-work/install/x86_64-osx/735434ae5515bc5cf8943d3cce7ab1f65331c20dc0bb8ed9b5fe2f74a02e5c8d/8.6.5/lib/x86_64-osx-ghc-8.6.5"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLFont/.stack-work/install/x86_64-osx/735434ae5515bc5cf8943d3cce7ab1f65331c20dc0bb8ed9b5fe2f74a02e5c8d/8.6.5/share/x86_64-osx-ghc-8.6.5/OpenGLFont-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLFont/.stack-work/install/x86_64-osx/735434ae5515bc5cf8943d3cce7ab1f65331c20dc0bb8ed9b5fe2f74a02e5c8d/8.6.5/libexec/x86_64-osx-ghc-8.6.5/OpenGLFont-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLFont/.stack-work/install/x86_64-osx/735434ae5515bc5cf8943d3cce7ab1f65331c20dc0bb8ed9b5fe2f74a02e5c8d/8.6.5/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "OpenGLFont_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "OpenGLFont_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "OpenGLFont_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "OpenGLFont_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "OpenGLFont_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "OpenGLFont_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
