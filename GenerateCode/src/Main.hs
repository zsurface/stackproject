import System.Environment
import System.Directory 
import Text.Regex
import Text.Regex.Posix
import Data.Bits
import System.FilePath.Posix
import System.IO
import Control.Monad 

import AronModule

-- Fri Jul 13 00:04:12 PDT 2018
-- add c file type
-- Tue Dec  4 17:49:29 2018
-- Add haskellbin generatecode.hs gc
-- Add html template 08-12-2020
--
-- KEY: genrate code template
--
helpme::IO ()
helpme = do
        p "--------------------------------------------------------------------------------" 
        p "Last Updated: Thu Feb 27 11:35:20 2020"
        p "Last Updated:               08-12-2020"
        p "Last Updated: Sun 23 Jul 00:14:00 2023"
        p "--------------------------------------------------------------------------------" 
        p "generate [h]     [try1] -> ./try1.hs"
        p "generate [ho]    [try1] -> ./try1.hs  Haskell OpenGL"
        p "generate [ht]    [try1] -> ./try1.html"
        p "generate [j]     [try1] -> ./try1.java"
        p "generate [l]     [try1] -> ./try1.tex"
        p "generate [c]     [try1] -> ./try1.c"
        p "generate [cpp]   [try1] -> ./try1.cpp"
        p "generate [s]     [try1] -> ./try1.sh"
        p "generate [g]     [try1] -> ./try1.groovy"
        p "generate [p]     [try1] -> ./try1.py"
        p "--------------------------------------------------------------------------------"
        p "haskellbin generatecode.hs gc => create binary code/symbol link in mybin and $sym" 
        p "--------------------------------------------------------------------------------"
    where
        p s = putStrLn $ "\t" ++ s

--haskellPath = "/Users/cat/myfile/bitbucket/haskell/text/haskell.txt"
--javaPath   = "/Users/cat/myfile/bitbucket/haskell/text/java.txt"
--latexPath  = "/Users/cat/myfile/bitbucket/haskell/text/latex.txt"
--ccodePath  = "/Users/cat/myfile/bitbucket/haskell/text/c.txt"
--cppCodePath  = "/Users/cat/myfile/bitbucket/haskell/text/cpp.txt"
--shPath     = "/Users/cat/myfile/bitbucket/haskell/text/sh.txt"
--haskellOpenGLPath     = "/Users/cat/myfile/bitbucket/haskell/text/haskell_opengl.txt"
--groovyPath     = "/Users/cat/myfile/bitbucket/haskell/text/groovy.groovy"
--
-- KEY: generate code, code generator
-- UPDATE: Sun 18 Sep 09:47:21 2022 
-- SLINK: $sym/gco
--
main = do 
        argList <- getArgs 
        progName <- getProgName
        home <- getEnv "HOME"
        let haskellPath       = home </> "myfile/bitbucket/publicfile/text/haskell.txt"
        let javaPath          = home </> "myfile/bitbucket/publicfile/text/java.txt"
        let latexPath         = home </> "myfile/bitbucket/publicfile/text/latex.txt"
        let ccodePath         = home </> "myfile/bitbucket/publicfile/text/c.txt"
        let cppCodePath       = home </> "myfile/bitbucket/publicfile/text/cpp.txt"
        let shPath            = home </> "myfile/bitbucket/publicfile/text/sh.txt"
        let haskellOpenGLPath = home </> "myfile/bitbucket/publicfile/text/haskell_opengl.txt"
        let groovyPath        = home </> "myfile/bitbucket/publicfile/text/groovy.groovy"
        let pythonPath        = home </> "myfile/bitbucket/publicfile/text/python.txt"
        let htmlPath          = home </> "myfile/bitbucket/publicfile/text/html.html"

        let hpath     = home </> "myfile/bitbucket/haskell/"
        let jpath     = home </> "myfile/bitbucket/java/"
        let lpath     = home </> "myfile/bitbucket/math/"
        let cpath     = home </> "myfile/bitbucket/c99/"
        let cpppath   = home </> "myfile/bitbucket/cpp/"
        let spath     = home </> "myfile/bitbucket/script/"
        let gpath     = home </> "myfile/bitbucket/groovy/"
        let ppath     = home </> "myfile/bitbucket/python/"
        let htpath    = home </> "myfile/bitbucket/html/"

        let lenArg = length argList
        let fileType = if lenArg > 0 then head argList else "help"
        putStrLn $ "\t type=" + fileType
        putStrLn $ "\t len=" + (show $ lenArg)

        case argList of
            []      -> do
                    helpme
            _       -> do
                    case (lenArg >= 1) of
                           True    -> do 
                                case fileType of 
                                    "help" -> do 
                                            helpme
                                    "h" -> do 
                                            list <-  readFileLatin1ToList haskellPath 
                                            let name = if lenArg > 1 then (head $ tail argList) else "try1"
                                            let full = hpath + name + ".hs"
                                            pp full
                                            isThere <- doesFileExist full
                                            unless isThere $ writeToFile full list
                                            --if isThere == False then (writeToFile full list) else (pp "file exists")
                                    "ho" -> do 
                                            list <- readFileLatin1ToList haskellOpenGLPath 
                                            let name = if lenArg > 1 then (head $ tail argList) else "try1"
                                            let full = hpath + name + ".hs"
                                            pp full
                                            isThere <- doesFileExist full
                                            unless isThere $ writeToFile full list
                                    "ht" -> do 
                                            list <- readFileLatin1ToList htmlPath 
                                            let name = if lenArg > 1 then (head $ tail argList) else "try1"
                                            let full = htpath + name + ".html"
                                            pp full
                                            isThere <- doesFileExist full
                                            unless isThere $ writeToFile full list
                                            --if isThere == False then (writeToFile full list) else (pp "file exists")
                                    "l" -> do
                                            list <- readFileLatin1ToList latexPath 
                                            let name = if lenArg > 1 then (head $ tail argList) else "try1"
                                            let full = lpath + name + ".tex"
                                            pp full
                                            isThere <- doesFileExist full
                                            unless isThere $ writeToFile full list
                                            -- if isThere == False then (writeToFile full list) else (pp "file exists")
                                    "j" -> do 
                                            list <- readFileLatin1ToList javaPath 
                                            let name = if lenArg > 1 then (head $ tail argList) else "try1"
                                            let full = jpath + name + ".java"
                                            pp full
                                            isThere <- doesFileExist full

                                            let r1 = mkRegex "Program"
                                            -- mapM pp list
                                            let newList = map(\x -> subRegex r1 x name ) list
                                            -- unless isThere $ writeToFile full newList
                                            if isThere == False 
                                                then (writeToFile full newList) 
                                                else (pp $ "file exists:[" + name + ".java] Plz change new name.")
                                    "s" -> do 
                                            list <- readFileLatin1ToList shPath 
                                            let name = if lenArg > 1 then (head $ tail argList) else "try1"
                                            let full = spath + name + ".sh"
                                            pp full
                                            isThere <- doesFileExist full
                                            if isThere == False then do 
                                                writeToFile full list 
                                                -- setFileMode filePath ownerExecuteMode
                                                -- SEE: System.Posix.Files
                                                -- chmod 64
                                                run $ "chmod 755 " ++ full
                                                pp ""
                                            else (pp "file exists")
                                    "c" -> do 
                                            list <- readFileLatin1ToList ccodePath 
                                            let name = if lenArg > 1 then (head $ tail argList) else "try1"
                                            let full = cpath + name + ".c"
                                            pp full
                                            isThere <- doesFileExist full

                                            let r1 = mkRegex "Program"
                                            mapM pp list
                                            let newList = map(\x -> subRegex r1 x name ) list
                                            unless isThere $ writeToFile full list
                                            -- if isThere == False then (writeToFile full newList) else (pp "file exists")
                                    "cpp" -> do 
                                            list <- readFileLatin1ToList cppCodePath 
                                            let name = if lenArg > 1 then (head $ tail argList) else "try1"
                                            let full = cpppath + name + ".cpp"
                                            pp full
                                            isThere <- doesFileExist full

                                            let r1 = mkRegex "Program"
                                            mapM pp list
                                            let newList = map(\x -> subRegex r1 x name ) list
                                            unless isThere $ writeToFile full list
                                    "g" -> do 
                                            list <- readFileLatin1ToList groovyPath 
                                            let name = if lenArg > 1 then (head $ tail argList) else "try1"
                                            let full = gpath + name + ".groovy"
                                            pp full
                                            isThere <- doesFileExist full
                                            unless isThere $ writeToFile full list
                                    "p" -> do 
                                            list <- readFileLatin1ToList pythonPath 
                                            let name = if lenArg > 1 then (head $ tail argList) else "try1"
                                            let full = ppath + name + ".py"
                                            pp full
                                            isThere <- doesFileExist full
                                            unless isThere $ writeToFile full list
                                    _ -> putStrLn "Invalid option. [h => haskell, j => java]. [ge h myhaskell or ge j Myjava]"
                           False  -> do
                                    pp "Invalid args"
        if progName /= "gc" 
        then do 
             -- run $ "haskellbin " + progName + ".hs gc" -- kind of Hask 
             pp $ "haskellbin " + progName + ".hs gc"
        else
            pp "done!"
  where
    (+) = (++)
