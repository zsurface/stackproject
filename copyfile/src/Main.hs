{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes       #-}

import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import qualified Data.Text                 as TS
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent
import System.Console.ANSI

import Text.RawString.QQ
-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

import AronModule 

-- pdf = "/Library/WebServer/Documents/xfido/pdf"
-- img = "/Library/WebServer/Documents/xfido/image"

r_ = system

box = printBox 2

main = do
        try <- getEnv "tt"
        www <- getEnv "www"
        home <- getEnv "HOME"
        let pdf = www </> "pdf"
        let img = www </> "image"
        let try = home </> "try"
        let down = home </> "Downloads"
        
        let helpme = box ["copyfile f.pdf pdf :" + pdf] >> 
                     box ["copyfile f.png img :" + img]
        
        pwd
        helpme
        argList <- getArgs
        curr <- getPwd 
        if (len argList) == 2 
        then case head $ drop 1 argList of 
            "pdf" -> do 
                     r_ cmd >>= \x -> pp x 
                     r_ ("ls " + fn) >>= \x -> pp x 
                     r_ ([r| echo |] <> fn <> [r| | pbcopy |]) >>= \x -> pp x
                     box ["copy " + fn + " to clipboard"]
                     return ()
                        where 
                            fn = head argList; 
                            cmd = "cp " + (curr </> fn) + " " ++ pdf 
            "img" -> do 
                     r_ cmd >>= \x -> pp x
                     r_ ("ls " + fn) >>= \x -> pp x 
                     r_ ([r| echo |] <> fn <> [r| | pbcopy |]) >>= \x -> pp x
                     box ["copy " + fn + " to clipboard"]
                     return ()
                        where 
                            fn = head argList
                            cmd = "cp " + (curr </> fn) + " " ++ img 
            _     -> box ["Run pdf or img  file"]
        else if (len argList) == 1
        then case head argList of
                  var | hasPrefix (lowerStr var) "downloads" -> do
                         let down = home </> "Downloads"
                         let target = www </> "image"
                         lss <- lsTable down
                         let fname = (head . head) $ getColumn lss 9
                         let source = down </> fname
                         when (isImage fname) $ do
                           box ["copy " + source + " => " + target]
                           box ["Enter => yes, otherwise no"]
                           putStr "\t\t|\n\n"
                           cursorUpLine 2
                           setCursorColumn 20

                           s <- getLineX
                           if s == "yes"
                           then do
                                box $ [concatStr ["copy", fname, source, "=>", target] " "]
                                cp source target
                                box ["Copy"]
                           else do
                                box ["Not copy"]

                  var | hasPrefix (lowerStr var) "try" -> do
                         lss <- lsTable try
                         let fname = (head . head) $ getColumn lss 9
                         let source = try </> fname
                         let target = www </> "image"
                         box ["copy " + source + " => " + target]
                         box ["Enter => yes, otherwise no"]
                         -- setCursorPosition 40 1
                         putStr "\t\t|\n\n"
                         cursorUpLine 2
                         setCursorColumn 20
                         
                         s <- getLineX
                         if s == "yes"
                         then do
                              box $ [concatStr ["copy", fname, source, "=>", target] " "]
                              cp source target
                              box ["Copy"]
                         else do
                              box ["Not copy"]

                  var | hasPrefix (lowerStr var) "downloads" -> box ["downloads"]
                      | otherwise -> box ["bad"]

        else do
             box ["Need One or Two arguments"]
             box ["copyfile f.png img => " + www </> "pdf"]
             box ["copyfile f.pdf pdf => " + www </> "image"]
             box ["copyfile try       => copy the newest png/jpeg/jpg file => " + www </> "image"]
             box ["copyfile down      => copy the newest " + down + " png/jpeg/jpg file => " + www </> "image"]

  where
    (+) = (++)
