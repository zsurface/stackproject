### copy image and pdf files somewhere
### Friday, 05 November 2021 01:53 PDT
### copy file from $tt and $HOME/Downloads to $www/image $www/pdf

``` bash
   copy f.png img  # copy f.png to $www/image
   copy f.pdf pdf  # copy f.pdf to $www/pdf
   
   copy tr     # copy the newest png/jpeg/jpg file to $www/image
   copy down   # copy the newest png/jpeg/jpg file to $www/image
   copy try    # same
```

