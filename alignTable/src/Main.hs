-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
{-# LANGUAGE MultiWayIf #-}
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 
import AronAlias

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

-- a b c d  e f g h  i j k l   m n  o p q
-- rst uvw xyz
--
-- a b c d e
-- f g h i j
-- k l m n o
-- p q r s t
-- u v w x y 
-- z
--
-- a b c d e f
-- g h i j k l
-- m n o p q r 
-- s t u v w x
-- y z
--
-- a b c d e f g 
-- h i j k l m n
-- o p q r s t u 
-- v w x y z
--

helpMe::IO()
helpMe = do
  printBox 3 ["1. alignTable /tmp/a.x"]
  printBox 3 ["2. cat /tmp/a.x | alignTable"]

main = do 
    argList <- getArgs
    case len argList of
        var | var == 0 -> do
                -- ls = filter (\u -> len u > 0) $ map (\t -> filter (\x -> (len . trim) x > 0) $ splitSPC t) cx 

                input <- getContents
                let ls = filter (\x -> lent x > 0) $ lines input 
                let lu = filter (\u -> len u > 0) $ map (\t -> filter (\x -> (len . trim) x > 0) $ splitSPC t) ls 
                -- pre lu
                let lr = map len lu
                let isSameCol = foldr (\a b -> a && b) True $ map (\e -> e == x) lr where x = head lr 
                pre isSameCol 
                mapM_ putStrLn $ map (\x -> concatStr x "") $ alignTable ls
            | var == 1 -> do
                let fp = head argList
                if fp /= "-h" then do
                  m <- readTable fp
                  mapM_(\r -> putStrLn $ foldr (\a b -> a ++ b) "" r) m
                  else helpMe
            | otherwise -> do
                helpMe
