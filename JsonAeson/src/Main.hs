{-# LANGUAGE OverloadedStrings, DeriveGeneric, DeriveAnyClass #-}
{-# LANGUAGE DuplicateRecordFields #-} 

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO hiding (putStrLn)
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import qualified Data.Aeson as DA

import GHC.Generics
import Data.Text.Lazy (Text)
import qualified Data.Text.Lazy.IO as I
import Data.Aeson.Text (encodeToLazyText)
import Data.Aeson (ToJSON, decode, encode)

import qualified Data.ByteString               as B
import qualified Data.ByteString.Internal      as BI
import qualified Data.ByteString.Lazy          as BL
import qualified Data.ByteString.Lazy.Internal as BLI

import qualified Network.Wai.Handler.Warp as WARP
import Network.HTTP.Types
import Network.HTTP.Types.Status (statusCode)
import Network.HTTP.Types.Header
import Network.Wai.Handler.Warp (run)
import Network.HTTP.Client

import AronModule 
import AronAlias

{-| 
    Convert Lazy ByteString to Strict ByteString
-} 

{-| 
    Json exmaple, Json tutorial, serialize Json, deserialize Json
-} 
data Person = Person {
      name :: Text
    , age  :: Int
    , list  :: [String] 
    } deriving (Generic, Show)

instance DA.ToJSON Person where
    toEncoding = DA.genericToEncoding DA.defaultOptions

instance DA.FromJSON Person
    -- No need to provide a parseJSON implementation.
-- read and write json

--instance DA.ToJSON Person where
--    -- this generates a Value
--    toJSON (Person name age) =
--        object ["name" .= name, "age" .= age]
--
--    -- this encodes directly to a bytestring Builder
--    toEncoding (Person name age) =
--        pairs ("name" .= name <> "age" .= age)

-- data EditorCode = EditorCode{
  -- editorbeg::Integer,
  -- editorend::Integer,
  -- editorfile::String,
  -- editorcmd::String,
  -- editorcode::String,
  -- mycat::Cat
  -- } deriving (Show, Generic)

data EditorCode = EditorCode{
  editorbeg::Integer,
  editorend::Integer,
  editorfile::String,
  editorcmd::String,
  editorcode::String,
  editortheme::String,
  editormode::String,
  mycat :: Cat
  } deriving (Generic, Show)


instance DA.ToJSON EditorCode where
  toEncoding = DA.genericToEncoding DA.defaultOptions

instance DA.FromJSON EditorCode

data Cat = Cat { name :: Text, 
                 age :: Int, 
                 list :: [String] 
               } deriving (Show, Generic)

instance DA.ToJSON Cat where
  toEncoding = DA.genericToEncoding DA.defaultOptions

instance DA.FromJSON Cat

meowmers = Cat { name = "meowmers", 
                 age = 1, 
                 list = ["dog", "cat", "independency Injection"] 
               }

editCode = EditorCode {
   editorbeg = 100,
   editorend = 200,
   editorfile = "try919591",
   editorcmd = "save",
   editorcode = "Hello World",
   editortheme = "mytheme",
   editormode = "my mode",
   mycat = meowmers
 }

-- {-|
    -- === KEY: String to JSON

    -- See 'toLBS'

    -- @
    -- toLBS::Typeable a => a -> BL.ByteString
    -- @
-- -}
-- jsonDecode::(DA.FromJSON a) => String -> Maybe a
-- jsonDecode = DA.decode . toLBS


-- {-|
    -- === KEY: json to record, json file to record

    -- @
    -- {"editorbeg":100,
     -- "editorend":200,
     -- "editorfile":"try919591",
     -- "editorcmd":"save",
     -- "editorcode":"Hello World",

     -- "mycat":{"name":"meowmers",
              -- "age":1,"list":["dog","cat","independency Injection"]
             -- }
    -- }

    -- data EditorCode = EditorCode{
      -- editorbeg::Integer,
      -- editorend::Integer,
      -- editorfile::String,
      -- editorcmd::String,
      -- editorcode::String,
      -- mycat::Cat
      -- } deriving (Show, Generic)

    -- instance DA.ToJSON EditorCode where
      -- toEncoding = DA.genericToEncoding DA.defaultOptions

    -- instance DA.FromJSON EditorCode

    -- data Cat = Cat { name :: Text, 
                     -- age :: Int, 
                     -- list :: [String] 
                   -- } deriving (Show, Generic)

    -- instance DA.ToJSON Cat where
      -- toEncoding = DA.genericToEncoding DA.defaultOptions

    -- instance DA.FromJSON Cat

    -- meowmers = Cat { name = "meowmers", 
                     -- age = 1, 
                     -- list = ["dog", "cat", "independency Injection"] 
                   -- }

    -- editCode = EditorCode {
      -- editorbeg = 100,
      -- editorend = 200,
      -- editorfile = "try919591",
      -- editorcmd = "save",
      -- editorcode = "Hello World",
      -- mycat = meowmers
    -- }

    -- decodeStr <- jsonToRecord "/tmp/json.json" :: IO (Maybe EditorCode)
    -- case decodeStr of
      -- Nothing -> Prelude.putStrLn "Not a Valid JSON file"
      -- (Just x) -> Prelude.putStrLn $ show x
    -- @
-- -}
-- jsonToRecord::(DA.FromJSON a) => FilePath -> IO (Maybe a)
-- jsonToRecord fp = readFileStr fp >>= \x -> return $ (DA.decode . toLBS) x

-- KEY: haskell json, write json to file, aeson to file, read json file to record, object inside object

-- UpdateCodeBlock{pid = 0, newcode="no code", begt=0, endt=0} 

--data UpdateCodeBlock = UpdateCodeBlock{pid::Integer, newcode::String, begt::Integer, endt::Integer} deriving (GEN.Generic, Show)
--instance DA.FromJSON UpdateCodeBlock
--instance DA.ToJSON UpdateCodeBlock where
--    toEncoding = DA.genericToEncoding DA.defaultOptions

{-|
main = do
    let arr = [meowmers]
    I.writeFile "/tmp/arr.json" (encodeToLazyText arr)
    I.writeFile "/tmp/json.json" (encodeToLazyText editCode)
    -- encode: Record to Json
    print $ DA.encode (Person {name = "Joe", age = 12, list = ["dog", "cat"]})
    -- decode Json to Record
    let mp = DA.decode "{\"name\":\"Joe\",\"age\":12, \"list\":[\"dog\", \"cow\"]}" :: Maybe Person 
    case mp of
        Nothing -> return ()
        (Just p) -> print p
    -- jstr <- readFileStr "/tmp/json.json"
    -- let decodeStr = jsonDecode jstr :: Maybe EditorCode
    decodeStr <- jsonToRecord "/tmp/x.json" :: IO (Maybe EditorCode)
    case decodeStr of
      Nothing -> Prelude.putStrLn "Not a Valid JSON file"
      (Just ec) -> do
        let s = editorcode ec
        Prelude.putStrLn $ show $ s
        writeFileStr "/tmp/x1.x" s
        ls <- readFileList "/tmp/x2.x"
        let twoList = splitListWhen (\x -> trim x == "latexcode_replace123") ls
        pp "s="
        pre s
        fl
        fl
        pre twoList
        fl
        let mylist = (head twoList) ++ [s] ++ (last twoList)
        fl
        pre mylist
        fl
        writeFileList "/tmp/x3.x" mylist
-}

main = do
        argList <- getArgs
        case len argList of
              var | var == 2 -> do
                        pp "2"
                        let url = head argList 
                        let tmpfile = last argList
                        ls <- rfl tmpfile 
                        pre ls
                        let lt = trimList ls
                        pre lt
                        if len lt >= 1 then do
                            let he = head lt
                            let s = trimList $ splitStr "@=" he
                            if len s == 2 then do
                                manager <- newManager defaultManagerSettings
                                let pid = read (last s) :: Integer 
                                pre pid
                                pp "Nice"
                                let tt = tail lt 
                                fw "tt"
                                pre tt
                                let code = unlines $ (init s) ++ tt 
                                fw "code"
                                pre code 
                                let block = UpdateCodeBlock{pid = pid, newcode = code, begt = 0, endt = 0} 
                                fw "block"
                                pre block
                                -- initReq <- parseRequest "http://localhost:8081/updatecode"
                                initReq <- parseRequest url 
                                let req = initReq { method = "POST", requestBody = RequestBodyLBS $ encode block}
                                response <- httpLbs req manager
                                putStrLn $ "The status code was: " ++ (show $ statusCode $ responseStatus response)
                                print $ responseBody response
                            else do
                                pp "Invalid Snippet format"
                            pre s
                        else do
                            pp "At least two lines for Snippet/CodeBlock"
                  | var == 1 -> do
                        let url = head argList 
                        pp "1"
                        ls <- getContents >>= return . lines 
                        fw "ls"
                        pre ls
                        let lt = trimList ls
                        fw "lt"
                        pre lt
                  | otherwise -> do
                        pp "2 or 1 numbers of arguments"
        pp "done"
{-|
        argList <- getArgs
        if len argList == 2 then do
            let url = head argList 
            let tmpfile = last argList
            ls <- rfl tmpfile 
            pre ls
            let lt = trimList ls
            pre lt
            if len lt >= 1 then do
                let he = head lt
                let s = trimList $ splitStr "@=" he
                if len s == 2 then do
                    manager <- newManager defaultManagerSettings
                    let pid = read (last s) :: Integer 
                    pre pid
                    pp "Nice"
                    let tt = tail lt 
                    fw "tt"
                    pre tt
                    let code = unlines $ (init s) ++ tt 
                    fw "code"
                    pre code 
                    let block = UpdateCodeBlock{pid = pid, newcode = code, begt = 0, endt = 0} 
                    fw "block"
                    pre block
                    -- initReq <- parseRequest "http://localhost:8081/updatecode"
                    initReq <- parseRequest url 
                    let req = initReq { method = "POST", requestBody = RequestBodyLBS $ encode block}
                    response <- httpLbs req manager
                    putStrLn $ "The status code was: " ++ (show $ statusCode $ responseStatus response)
                    print $ responseBody response
                else if len s == 1 then do
                        ls <- getContents >>= return . lines
                        pre ls
                        pre lt
                    else do
                        pp ""

                pre s
            else do
                pp "At least two lines for Snippet/CodeBlock"
        else do
            putStrLn "postSnippet http://localhost:8081/updatecode  /tmp/x1.x                                 "
            putStrLn "                     ↑                            ↑                                     "
            putStrLn "                     + -> Update code URL          + -> tmp file store edited snippet    "
            putStrLn ""
            putStrLn "There is at least two lines in the file"
-}








        
    
