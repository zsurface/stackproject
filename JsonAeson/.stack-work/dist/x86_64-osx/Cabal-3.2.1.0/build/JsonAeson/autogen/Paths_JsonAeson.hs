{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_JsonAeson (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/JsonAeson/.stack-work/install/x86_64-osx/8791056ce80957aa87be87cdcf164fb90982e34e8f52d0f86047bb442b487fda/8.10.4/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/JsonAeson/.stack-work/install/x86_64-osx/8791056ce80957aa87be87cdcf164fb90982e34e8f52d0f86047bb442b487fda/8.10.4/lib/x86_64-osx-ghc-8.10.4/JsonAeson-0.1.0.0-1nSJ7ezUyxi9AmGCHw6C8X-JsonAeson"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/JsonAeson/.stack-work/install/x86_64-osx/8791056ce80957aa87be87cdcf164fb90982e34e8f52d0f86047bb442b487fda/8.10.4/lib/x86_64-osx-ghc-8.10.4"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/JsonAeson/.stack-work/install/x86_64-osx/8791056ce80957aa87be87cdcf164fb90982e34e8f52d0f86047bb442b487fda/8.10.4/share/x86_64-osx-ghc-8.10.4/JsonAeson-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/JsonAeson/.stack-work/install/x86_64-osx/8791056ce80957aa87be87cdcf164fb90982e34e8f52d0f86047bb442b487fda/8.10.4/libexec/x86_64-osx-ghc-8.10.4/JsonAeson-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/JsonAeson/.stack-work/install/x86_64-osx/8791056ce80957aa87be87cdcf164fb90982e34e8f52d0f86047bb442b487fda/8.10.4/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "JsonAeson_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "JsonAeson_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "JsonAeson_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "JsonAeson_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "JsonAeson_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "JsonAeson_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
