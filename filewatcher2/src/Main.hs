-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}  --
{-# LANGUAGE MultiWayIf #-}  --
{-# LANGUAGE QuasiQuotes       #-} -- RawString QQ, Quasi 
-- 
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Turtle (shell, empty)                      
import Control.Monad
import Data.Char
import Data.Default
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Tuple(swap)
import Data.Maybe (fromJust)
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Posix.Types
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent(threadDelay)
import Foreign.C.Types

-- {-# LANGUAGE QuasiQuotes       #-} -- RawString QQ, Quasi 
import Text.RawString.QQ          -- QuasiQuotes needs for Text.RawString.QQ 

import AronModule 

macOS = "darwin"
freeBSD = "freebsd"

haskellDir= "/Users/cat/myfile/bitbucket/haskell" 

monitorHaskellDir::IO()
monitorHaskellDir = do
                    let file = "AronModule.hs"
                    ls <- lsFile haskellDir 
                    let n = len $ filter (\x -> x == file) ls
                    if n > 0 
                    then do 
                         pp $ "Error message: remove =>" ++ file
                         let str = [r| "Urgent: remove file=>"|] <> (haskellDir <> "=>" <> file)
                         -- system ("notify.sh " <> str) >>= \x -> print x
                         pp "run it"
                    else return ()

-- monitorHaskellDir::IO()
-- monitorHaskellDir = do
--                     let file = "AronModule.hs"
--                     ls <- runRawCmd "ls" [haskellDir]
--                     pp ls
--                     let n = 1
--                     if n > 0 
--                     then do 
--                          pp $ "Error message: remove =>" ++ file
--                          let str = [r| "Urgent: remove file=>"|] <> (haskellDir <> "=>" <> file)
--                          -- system ("notify.sh " <> str) >>= \x -> print x
--                          pp "run it"
--                     else return ()

main = do 
        forever $ do
            nvar1 <- getFileStatus haskellDir >>= return . modificationTime
            sleepSec 1 
            now1 <- getFileStatus haskellDir >>= return . modificationTime
            pp $ show now1 
            pp $ show nvar1

            monitorHaskellDir
            if | now1 > nvar1 -> do 
                                 pp $ "change " ++ haskellDir
               | otherwise -> return ()
        pp "done!"

