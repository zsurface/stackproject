-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close
type Lock = MVar()

newLock :: IO Lock
newLock = newMVar()

withLock :: Lock -> IO a -> IO a
withLock x = withMVar x . const

-- create a box
-- do some calculation and put the result to the box
-- if there is thing in the box, take it out and print it to console
main = do
        print "Hello World"
        exCode <- compileJava "/Users/cat/myfile/bitbucket/java/try_kkf.java"
        pp $ "exCode=" ++ (show exCode)
        -- result is empty
        result4 <- newEmptyMVar
        result6 <- newEmptyMVar
        -- create a thread from a main thread
        forkIO(do
                  sleepSec 1
                  pp "cal4"
                  -- result has value
                  putMVar result4 44
              )
        forkIO(do
                sleepSec 1
                pp "cal6"
                -- result has value
                putMVar result6 66
            )

        pp "waiting result4"
        -- if result has value, then take it out, otherwise just waiting, or block
        value4 <- takeMVar result4
        pp $ "value4=" <<< value4
        value6 <- takeMVar result6
        pp $ "value6=" <<< value6
        pp "done"
