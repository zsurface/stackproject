{-# LANGUAGE QuasiQuotes #-}
-- base
import Control.Monad (when)
import Control.Monad.Except
import Control.Exception (bracket)
import Foreign -- includes many sub-modules
import Foreign.C.String (withCAStringLen, newCString)
-- GLFW-b
import qualified Graphics.UI.GLFW as GLFW
-- gl
import Graphics.GL.Core33
import Graphics.GL.Types
-- raw-strings-qq
import Text.RawString.QQ
-- JuicyPixels
import Codec.Picture (readImage, generateImage, convertRGB8, DynamicImage(..), Image(..), PixelRGB8(..))
-- vector
import qualified Data.Vector.Storable as VS


-- KEY: opengl texture, combine texture
-- https://lokathor.gitbooks.io/using-haskell/content/opengl/textures-blend.hs

winWidth = 800

winHeight = 600

winTitle = "Textures"

-- | Ensures that we only run GLFW code while it's initialized, and also that we
-- always terminate it when we're done. Also, this function should only be used
-- from the main thread.
bracketGLFW :: IO () -> IO ()
bracketGLFW act = bracket GLFW.init (const GLFW.terminate) $ \initWorked ->
    when initWorked act

-- type KeyCallback = Window -> Key -> Int -> KeyState -> ModifierKeys -> IO ()
callback :: GLFW.KeyCallback
callback window key scanCode keyState modKeys = do
    print key
    when (key == GLFW.Key'Escape && keyState == GLFW.KeyState'Pressed)
        (GLFW.setWindowShouldClose window True)

vertexShaderSource :: String
vertexShaderSource = [r|
    #version 330 core
    layout (location = 0) in vec3 position;
    layout (location = 1) in vec3 color;
    layout (location = 2) in vec2 texCoord;

    out vec3 ourColor;
    out vec2 TexCoord;

    void main()
    {
        gl_Position = vec4(position, 1.0f);
        ourColor = color;
        // We swap the y-axis by substracing our coordinates from 1.
        // This is done because most images have the top y-axis inversed with OpenGL's top y-axis.
        TexCoord = vec2(texCoord.x, 1.0 - texCoord.y);
    }
    |]

fragmentShaderSource :: String
fragmentShaderSource = [r|
    #version 330 core
    in vec3 ourColor;
    in vec2 TexCoord;

    out vec4 color;

    // Texture samplers
    uniform sampler2D ourTexture0;
    uniform sampler2D ourTexture1;

    void main()
    {
        // Linearly interpolate between both textures (second texture is only slightly combined)
        color = mix(texture(ourTexture0, TexCoord), texture(ourTexture1, TexCoord), 0.2);
    }
    |]

-- | Given a shader type and a shader source, it gives you (Right id) of the
-- successfully compiled shader, or (Left err) with the error message. In the
-- error case, the shader id is deleted before the function returns to avoid
-- accidentally leaking shader objects.
loadShader :: GLenum -> String -> IO (Either String GLuint)
loadShader shaderType source = do
    -- new shader object
    shaderID <- glCreateShader shaderType
    -- assign the source to the shader object
    withCAStringLen source $ \(strP, strLen) ->
        withArray [strP] $ \linesPtrsPtr ->
            withArray [fromIntegral strLen] $ \lengthsPtr ->
                glShaderSource shaderID 1 linesPtrsPtr lengthsPtr
    -- compile and check success
    glCompileShader shaderID
    success <- alloca $ \successP -> do
        glGetShaderiv shaderID GL_COMPILE_STATUS successP
        peek successP
    if success == GL_TRUE
        -- success: we're done
        then return (Right shaderID)
        -- failure: we get the log, delete the shader, and return the log.
        else do
            -- how many bytes the info log should be (including the '\0')
            logLen <- alloca $ \logLenP -> do
                glGetShaderiv shaderID GL_INFO_LOG_LENGTH logLenP
                peek logLenP
            -- space for the info log
            logBytes <- allocaBytes (fromIntegral logLen) $ \logP -> do
                -- space for the log reading result
                alloca $ \resultP -> do
                    -- Try to obtain the log bytes
                    glGetShaderInfoLog shaderID logLen resultP logP
                    -- this is how many bytes we actually got
                    result <- fromIntegral <$> peek resultP
                    peekArray result logP
            -- delete the shader object and return the log
            glDeleteShader shaderID
            let prefix = case shaderType of
                    GL_VERTEX_SHADER -> "Vertex"
                    GL_GEOMETRY_SHADER -> "Geometry"
                    GL_FRAGMENT_SHADER -> "Fragment"
                    _ -> "Unknown Type"
            return $ Left $
                prefix ++ " Shader Error:" ++
                    (map (toEnum.fromEnum) logBytes)

-- | Given a vertex shader object and a fragment shader object, this will link
-- them into a new program, giving you (Right id). If there's a linking error
-- the error log is retrieved, the program deleted, and (Left err) is returned.
linkProgram :: GLuint -> GLuint -> IO (Either String GLuint)
linkProgram vertexID fragmentID = do
    programID <- glCreateProgram
    glAttachShader programID vertexID
    glAttachShader programID fragmentID
    glLinkProgram programID
    success <- alloca $ \successP -> do
        glGetProgramiv programID GL_LINK_STATUS successP
        peek successP
    if success == GL_TRUE
        -- success: we're done
        then return (Right programID)
        -- failure: we get the log, delete the shader, and return the log.
        else do
            -- how many bytes the info log should be (including the '\0')
            logLen <- alloca $ \logLenP -> do
                glGetProgramiv programID GL_INFO_LOG_LENGTH logLenP
                peek logLenP
            -- space for the info log
            logBytes <- allocaBytes (fromIntegral logLen) $ \logP -> do
                -- space for the log reading result
                alloca $ \resultP -> do
                    -- Try to obtain the log bytes
                    glGetProgramInfoLog programID logLen resultP logP
                    -- this is how many bytes we actually got
                    result <- fromIntegral <$> peek resultP
                    peekArray result logP
            -- delete the program object and return the log
            glDeleteProgram programID
            return $ Left $ "Program Link Error: " ++
                (map (toEnum.fromEnum) logBytes)

-- | Given the source for the vertex shader and the fragment shader, compiles
-- both and links them into a single program. If all of that is successful, the
-- intermediate shaders are deleted before the final value is returned.
programFromSources :: String -> String -> IO (Either String GLuint)
programFromSources vertexSource fragmentSource = runExceptT $ do
    v <- ExceptT $ loadShader GL_VERTEX_SHADER vertexSource
    f <- ExceptT $ loadShader GL_FRAGMENT_SHADER fragmentSource
    p <- ExceptT $ linkProgram v f
    glDeleteShader v
    glDeleteShader f
    return p

main :: IO ()
main = bracketGLFW $ do
    {-|
    GLFW.windowHint (GLFW.WindowHint'ContextVersionMajor 3)
    GLFW.windowHint (GLFW.WindowHint'ContextVersionMinor 3)
    GLFW.windowHint (GLFW.WindowHint'OpenGLProfile GLFW.OpenGLProfile'Core)
    GLFW.windowHint (GLFW.WindowHint'Resizable False)
    -}
    GLFW.windowHint (GLFW.WindowHint'ContextVersionMajor 3)
    GLFW.windowHint (GLFW.WindowHint'ContextVersionMinor 2)
    GLFW.windowHint (GLFW.WindowHint'OpenGLProfile GLFW.OpenGLProfile'Core)
    GLFW.windowHint (GLFW.WindowHint'OpenGLForwardCompat True)
    GLFW.windowHint (GLFW.WindowHint'DoubleBuffer True)
  
    maybeWindow <- GLFW.createWindow winWidth winHeight winTitle Nothing Nothing
    case maybeWindow of
        Nothing -> putStrLn "Failed to create a GLFW window!"
        Just window -> do
            -- enable keys
            GLFW.setKeyCallback window (Just callback)

            -- calibrate the viewport
            GLFW.makeContextCurrent (Just window)
            (x,y) <- GLFW.getFramebufferSize window
            glViewport 0 0 (fromIntegral x) (fromIntegral y)

            -- ready and use our program
            eErrP <- programFromSources vertexShaderSource fragmentShaderSource
            shaderProgram <- case eErrP of
                Left e -> putStrLn e >> return 0
                Right p -> return p
            glUseProgram shaderProgram

            -- ready our texture0
            texture0P <- malloc
            glGenTextures 1 texture0P
            texture0 <- peek texture0P
            glBindTexture GL_TEXTURE_2D texture0
            -- wrapping and filtering params would go here.
            eErrDI0 <- readImage "texture-demo.jpg"
            dyImage0 <- case eErrDI0 of
                Left e -> do
                    putStrLn e
                    return $ ImageRGB8 $ generateImage (\x y ->
                        let x' = fromIntegral x in PixelRGB8 x' x' x') 800 600
                Right di -> return di
            let ipixelrgb80 = convertRGB8 dyImage0
                iWidth0 = fromIntegral $ imageWidth ipixelrgb80
                iHeight0 = fromIntegral $ imageHeight ipixelrgb80
                iData0 = imageData ipixelrgb80
            VS.unsafeWith iData0 $ \dataP ->
                glTexImage2D GL_TEXTURE_2D 0 GL_RGB iWidth0 iHeight0 0 GL_RGB GL_UNSIGNED_BYTE (castPtr dataP)
            glGenerateMipmap GL_TEXTURE_2D
            glBindTexture GL_TEXTURE_2D 0

            -- ready our texture1
            texture1P <- malloc
            glGenTextures 1 texture1P
            texture1 <- peek texture1P
            glBindTexture GL_TEXTURE_2D texture1
            -- wrapping and filtering params would go here.
            eErrDI1 <- readImage "texture-demo2.jpg"
            dyImage1 <- case eErrDI1 of
                Left e -> do
                    putStrLn e
                    return $ ImageRGB8 $ generateImage (\x y ->
                        let x' = fromIntegral x in PixelRGB8 x' x' x') 800 600
                Right di -> return di

            -- convertRGB8::DynamicImage -> Image PixelRGB8
            {-|
            data Image a = Image
              { -- | Width of the image in pixels
                imageWidth  :: {-# UNPACK #-} !Int
                -- | Height of the image in pixels.
              , imageHeight :: {-# UNPACK #-} !Int

                -- | Image pixel data. To extract pixels at a given position
                -- you should use the helper functions.
                --
                -- Internally pixel data is stored as consecutively packed
                -- lines from top to bottom, scanned from left to right
                -- within individual lines, from first to last color
                -- component within each pixel.
              , imageData   :: V.Vector (PixelBaseComponent a)
              }
              deriving (Typeable)
            -}
  
            let ipixelrgb81 = convertRGB8 dyImage1
                iWidth1 = fromIntegral $ imageWidth ipixelrgb81
                iHeight1 = fromIntegral $ imageHeight ipixelrgb81
                iData1 = imageData ipixelrgb81
            VS.unsafeWith iData1 $ \dataP ->
                glTexImage2D GL_TEXTURE_2D 0 GL_RGB iWidth0 iHeight0 0 GL_RGB GL_UNSIGNED_BYTE (castPtr dataP)
            glGenerateMipmap GL_TEXTURE_2D
            glBindTexture GL_TEXTURE_2D 0

            -- setup our verticies
            let verticies = [
                    -- Positions      // Colors        // Texture Coords
                    0.5,  0.5, 0.0,   1.0, 0.0, 0.0,   1.0, 1.0,   -- Top Right
                    0.5, -0.5, 0.0,   0.0, 1.0, 0.0,   1.0, 0.0,   -- Bottom Right
                    -0.5, -0.5, 0.0,   0.0, 0.0, 1.0,   0.0, 0.0,  -- Bottom Left
                    -0.5,  0.5, 0.0,   1.0, 1.0, 0.0,   0.0, 1.0   -- Top Left 
                    ] :: [GLfloat]
            let verticesSize = fromIntegral $ sizeOf (0.0 :: GLfloat) * (length verticies)
            verticesP <- newArray verticies

            -- setup the indexes
            let indices = [  -- Note that we start from 0!
                    0, 1, 3, -- First Triangle
                    1, 2, 3  -- Second Triangle
                    ] :: [GLuint]
            let indicesSize = fromIntegral $ sizeOf (0 :: GLuint) * (length indices)
            indicesP <- newArray indices

            -- setup a vertex array object
            vaoP <- malloc
            glGenVertexArrays 1 vaoP
            vao <- peek vaoP
            glBindVertexArray vao

            -- setup a vertex buffer object and send it data
            vboP <- malloc
            glGenBuffers 1 vboP
            vbo <- peek vboP
            glBindBuffer GL_ARRAY_BUFFER vbo
            glBufferData GL_ARRAY_BUFFER verticesSize (castPtr verticesP) GL_STATIC_DRAW

            -- setup an element buffer object and send it data
            eboP <- malloc
            glGenBuffers 1 eboP
            ebo <- peek eboP
            glBindBuffer GL_ELEMENT_ARRAY_BUFFER ebo
            glBufferData GL_ELEMENT_ARRAY_BUFFER indicesSize (castPtr indicesP) GL_STATIC_DRAW

            -- assign the attribute pointer information
            let floatSize = (fromIntegral $ sizeOf (0.0::GLfloat)) :: GLsizei
            -- position attribute
            glVertexAttribPointer 0 3 GL_FLOAT GL_FALSE (8*floatSize) nullPtr
            glEnableVertexAttribArray 0
            -- color attribute
            let threeFloatOffset = castPtr $ plusPtr nullPtr (fromIntegral $ 3*floatSize)
            glVertexAttribPointer 1 3 GL_FLOAT GL_FALSE (8*floatSize) threeFloatOffset
            glEnableVertexAttribArray 1
            -- texture information
            let sixFloatOffset = castPtr $ plusPtr nullPtr (fromIntegral $ 6*floatSize)
            glVertexAttribPointer 2 2 GL_FLOAT GL_FALSE (8*floatSize) sixFloatOffset
            glEnableVertexAttribArray 2

            -- unbind our vertex array object to prevent accidental changes in
            -- between our draw calls.
            glBindVertexArray 0

            -- Uncomment this line for "wireframe mode"
            -- glPolygonMode GL_FRONT_AND_BACK GL_LINE

            -- the name of our uniforms
            -- NOTE: all the variables are from vertex shader and fragment shader
            ourColor <- newCString "ourColor"
            ourTexture0 <- newCString "ourTexture0"
            ourTexture1 <- newCString "ourTexture1"

            -- enter our main loop
            let loop = do
                    shouldContinue <- not <$> GLFW.windowShouldClose window
                    when shouldContinue $ do
                        -- event poll
                        GLFW.pollEvents
                        -- clear the screen
                        glClearColor 0.2 0.3 0.3 1.0
                        glClear GL_COLOR_BUFFER_BIT
                        -- bind textures using texture units
                        glActiveTexture GL_TEXTURE0
                        glBindTexture GL_TEXTURE_2D texture0
                        our0Loc <- glGetUniformLocation shaderProgram ourTexture0
                        glUniform1i our0Loc 0
                        glActiveTexture GL_TEXTURE1
                        glBindTexture GL_TEXTURE_2D texture1
                        our1Loc <- glGetUniformLocation shaderProgram ourTexture1
                        glUniform1i our1Loc 1
                        -- draw our rectangle with the textures on it
                        glBindVertexArray vao
                        glDrawElements GL_TRIANGLES 6 GL_UNSIGNED_INT nullPtr
                        glBindVertexArray 0
                        -- swap buffers and go again
                        GLFW.swapBuffers window
                        loop
            loop
--
