{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_randomList (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/randomList/.stack-work/install/x86_64-osx/b83baa9c5c76621301de80f4b5a4524b03e5e3c6ffcf08c98a5ff0c3c07e3916/8.10.4/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/randomList/.stack-work/install/x86_64-osx/b83baa9c5c76621301de80f4b5a4524b03e5e3c6ffcf08c98a5ff0c3c07e3916/8.10.4/lib/x86_64-osx-ghc-8.10.4/randomList-0.1.0.0-54bfIhGlRalEuVzEmm60rv-randomList"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/randomList/.stack-work/install/x86_64-osx/b83baa9c5c76621301de80f4b5a4524b03e5e3c6ffcf08c98a5ff0c3c07e3916/8.10.4/lib/x86_64-osx-ghc-8.10.4"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/randomList/.stack-work/install/x86_64-osx/b83baa9c5c76621301de80f4b5a4524b03e5e3c6ffcf08c98a5ff0c3c07e3916/8.10.4/share/x86_64-osx-ghc-8.10.4/randomList-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/randomList/.stack-work/install/x86_64-osx/b83baa9c5c76621301de80f4b5a4524b03e5e3c6ffcf08c98a5ff0c3c07e3916/8.10.4/libexec/x86_64-osx-ghc-8.10.4/randomList-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/randomList/.stack-work/install/x86_64-osx/b83baa9c5c76621301de80f4b5a4524b03e5e3c6ffcf08c98a5ff0c3c07e3916/8.10.4/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "randomList_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "randomList_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "randomList_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "randomList_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "randomList_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "randomList_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
