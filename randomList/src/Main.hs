{-# LANGUAGE CPP #-}
{-# LANGUAGE MultiWayIf #-}
-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import Text.Printf

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

#if 0
    randomInt -n 10 2 100
#endif

main = do 
        let toF = sciToFloat 3
        argList <- getArgs
        let (optls, numls) = pick (hasStr "-") argList
        if | len argList == 2 -> do
              if | containAll ["-f"] optls -> do
                   if | (len numls == 1) -> do
                        let n0 = read $ head numls :: Int
                        ls <- randomFloat n0
                        mapM_ putStrLn $ map toF ls
                      | otherwise -> print "Invalid Options"
                 | otherwise -> print "Invalid Options" 

           | len argList == 3 -> do
              let n0 = read $ head numls :: Int
              ls <- randomFloat n0
              let s = tail $ foldl (\a b -> a ++ " " ++ b) [] $ map toF ls
              if | containAll ["-h"] optls -> putStrLn s
                 | otherwise               -> mapM_ putStrLn $ map toF ls

           | len argList == 4 -> do
              let n0 = read $ head numls :: Int
              let n1 = read $ (head . tail) numls :: Int
              let n2 = read $ last numls :: Int
              if | containAll ["-n"] optls -> do
                   ls <- randIntList n0 (n1, n2)
                   mapM_ putStrLn $ map show ls 
                 | otherwise        -> print "Invalid Options"

           | len argList == 5 && len optls == 2 -> do
              if | containAll ["-h"] optls -> do
                     if | containAll ["-n"] optls -> do
                          let n0 = read $ head numls :: Int            -- count 
                          let n1 = read $ (head . tail) numls :: Int   --  lower bound
                          let n2 = read $ last numls :: Int            --  lower bound
                          ls <- randIntList n0 (n1, n2) 
                          let s = tail $ foldl (\a b -> a ++ " " ++ b) [] $ map show ls
                          putStrLn s
                        | containAll ["-f"] optls -> do
                            let n0 = read $ head numls :: Int           
                            ls <- randomFloat n0            
                            let s = tail $ foldl (\a b -> a ++ " " ++ b) [] $ map toF ls
                            putStrLn s
                        | otherwise -> do 
                                         putStrLn "Invalid Options" 
                                         pre optls
                  | otherwise -> print "Invalid Options"                    

            | otherwise -> do  
              printBox 4 ["-n => Int, -f => Float, -h => print horizontally                                 "]
              printBox 4 [""]
              printBox 4 ["randomList -f 5         => 5 random number Float within (0, 1)                   "]
              printBox 4 ["randomList -n 5 1 10    => 5 random number Int within [1, 10], print vertically  "]
              printBox 4 ["randomList -n -h 5 1 10 => 5 random number Int within [1, 10], print horizontally"]
              printBox 4 ["randomList -n 5 -h 1 10 => 5 random number Int within [1, 10], print horizontally"]
              printBox 4 [""]
              printBox 4 ["randomList 10 1 10 -n => OK, options can be any order"]
              printBox 4 ["randomList 5 -f       => OK, options can be any order"]
