-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

-- import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close
------------------------------------------------------------------
-- KEY: Combine and simplify commands 
-- | Sat Aug 25 13:46:13 2018  
-- | todo: add tar -xf file.tar.xz
------------------------------------------------------------------ 
--grep::String->String
--grep s = "grep --color=always --include=\"" ++ s ++ "\" -Hnris " 

grepCmdStr::String -> String -> String
grepCmdStr f s = "/usr/bin/grep --color=always --include=\"" ++ f ++ "\" -Hnris -A 5 " ++ "\"" ++ s ++ "\"" ++ " . "

grepCmdStrNew::String -> String -> String -> String
grepCmdStrNew cmd f s = cmd ++ " --color=always --include=\"" ++ f ++ "\" -Hnris -A 5 " ++ "\"" ++ s ++ "\"" ++ " . "


whichCmd::String -> IO[String]
whichCmd s = run $ "which " ++ s
  
-- Process is stuck, not sure why?
grepProcess::String -> String -> IO ()
grepProcess s1 s2 = do
                    ls <- whichCmd "grep"
                    if len ls > 0 then do
                       let grep = head ls
                       (Nothing, Just hout, Nothing, ph) <- createProcess (shell $ grepCmdStrNew grep s1 s2 )
                                                                            { std_in  = Inherit
                                                                            , std_out = CreatePipe
                                                                            , std_err = Inherit
                                                                            }
                       ec <- waitForProcess ph
                       out <- hGetContents hout 
                       mapM_ putStrLn $ lines out
                       else do
                        print "ERROR: grep is NOT FOUND"

findProcess::String->IO()
findProcess s = do
                ls <- whichCmd "find"
                if len ls > 0 then do
                  let find = head ls
                  let cmd = find ++ " $PWD -iname \"" ++ s ++ "\" -print"
                  (Nothing, Just hout, Nothing, ph) <- createProcess (shell cmd)
                                                                      { std_in  = Inherit
                                                                      , std_out = CreatePipe
                                                                      , std_err = Inherit
                                                                      }
                  ec <- waitForProcess ph
                  out <- hGetContents hout 
                  mapM_ putStrLn $ lines out
                else do
                 print "ERROR: find is NOT FOUND."

myhelp::IO()
myhelp = do
        p "Update: Sunday, 26 September 2021 22:53 PDT                                               "
        p "Wed 19 Jul 14:29:27 2023                                                                  "
        p "------------------------------------------                                                "
        p "cmd [zip]  [foo]                     => zip -r foo.zip foo                                "
        p "cmd [zip]  [foo] [foo.zip]           => zip -r foo.zip foo                                "
        p "cmd [gz]   [foo.txt]                 => gzip foo  => foo.gz                               "
        p "cmd [tar]  [foo.txt]                 => tar -czvf foo.tar.gz                              "
        p "cmd [bz2]  [foo.bz2]                 => bzip2 -d foo.bz2  => foo                          "
        p "------------------------------------------                                                "
        p "cmd uzip file.txt.zip                => unzip foo.zip foo                                 "
        p "cmd ugz  file.txt.gz                 => gunzip foo.gz foo                                 "
        p "cmd utar file.txt.tar.gz             => file.txt                                          "
        p "cmd utar file.txt.tar                => file.txt                                          "
        p "------------------------------------------                                                "
        p "cmd grep '*.hs' pattern              => grep --color --include=\"*.hs\"   -Hnris pattern ."
        p "cmd grep h pattern                   => grep --color --include=\"*.hs\"   -Hnris pattern ."
        p "cmd grep j pattern                   => grep --color --include=\"*.java\" -Hnris pattern ."
        p "------------------------------------------                                                "
        p "cmd find '*.hs'                      => find . -iname \"*.hs\" -print                     "
        p "cmd find 'gene*.hs'                                                                       "
        p "cmd find '*profile*'                 => find file name contains 'profile'                 "
        p "cmd find a1                          => find . -iname [\"*.hs\"] -print                   "
        p "cmd ssh                              => ssh-keygen -C noname                              "
        p "------------------------------------------"
  where 
    p = putStrLn

shellCmd::[String] -> IO () 
shellCmd x = case x of 
        [op]     -> case op of
                         "h"  -> myhelp
                         "k"  -> createProcess (proc "ssh-keygen" ["-C", "noname"]){ cwd = Just "." } >>= \_ -> return ()
                         "ssh" -> do
                                    print cmd
                                    (Nothing, Just hout, Nothing, ph) <- createProcess p
                                    out <- hGetContents hout 
                                    mapM_ putStrLn $ lines out
                                    where 
                                        p = (shell cmd)
                                            { std_in  = Inherit
                                            , std_out = CreatePipe
                                            , std_err = Inherit
                                            }
                                        cmd = "ssh-keygen -C noname"
                         _    -> pp "Invalid option"
        [op, a1] -> case op of
                         "zip" -> do
                           (ex, stdout, stderr) <- runShell "which zip"  -- Use whichCmd instead
                           if ex == ExitSuccess then let sp = init $ toStr stdout in createProcess (proc sp ["-r", (a1 ++ ".zip"), a1])      { cwd = Just "." } >>= \_ -> return ()
                             else putStrLn $ toStr stderr
                         "gz"  -> do
                           (ex, stdout, stderr) <- runShell "which gzip"  -- Use whichCmd instead
                           if ex == ExitSuccess then let sp = init $ toStr stdout in createProcess (proc sp [a1])                            { cwd = Just "." } >>= \_ -> return ()
                             else putStrLn $ toStr stderr
                         "tar" -> createProcess (proc "/usr/bin/tar"    ["-czvf", (a1 ++ ".tar.gz"), a1]){ cwd = Just "." } >>= \_ -> return ()
                         "bz2" -> do
                           (ex, stdout, stderr) <- runShell "which bzip2"
                           if ex == ExitSuccess then let sp = init $ toStr stdout in createProcess (proc sp ["-d", a1]){ cwd = Just "." } >>= \_ -> return ()
                             else putStrLn $ toStr stderr
                         "utar"-> createProcess (proc "/usr/bin/tar"    ["-xvf", a1])                   { cwd = Just "." } >>= \_ -> return () 
                         "uzip"-> createProcess (proc "/usr/bin/unzip"  [a1])                            { cwd = Just "." } >>= \_ -> return () 
                         "ugz" -> createProcess (proc "/usr/bin/gunzip" [a1])                            { cwd = Just "." } >>= \_ -> return () 
                         "find"-> do
                                    findProcess a1
                         _     -> print $ "[" ++ op ++ "][" ++ a1 ++ "]"

        [op, a1, a2] -> case op of
                 "zip" -> createProcess (proc "/usr/bin/zip" ["-r", a2, a1]){ cwd = Just "." } >>= \_ -> return () 
                 "grep"-> do
                            case a1 of
                                "h" -> do 
                                        -- grepProcess does not work here, not sure why?
                                        let f = "*.hs"
                                        let cmd = "/usr/bin/grep --color=always --include=\'" ++ f ++ "\' -Hnris -A 5 " ++ a2 ++ " $PWD "
                                        pp cmd
                                        _ <- sys $ grepCmdStr "*.hs" a2
                                        pp "done"
                                "j" -> do 
                                        _ <- sys $ grepCmdStr "*.java" a2
                                        pp "done"
                                _   -> do 
                                        _ <- sys $ grepCmdStr a1 a2
                                        pp "done"
                 _     -> print $ "[" ++ op ++ "][" ++ a1 ++ "][" ++ a2 ++ "]"

        _ -> myhelp 


main =  do
        argList <- getArgs
        curr <- getPwd 
        cd curr
        pp curr
        s <- shellCmd argList
        pp s
