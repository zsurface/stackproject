module AronGraphic where

import           Control.Concurrent (threadDelay)
import           Control.Monad                        (unless, when)
import           Control.Monad
import           Data.IORef
import           Data.Maybe
import           Data.Set                             (Set, delete, fromList)
                 
import qualified                            Data.Set         as S
import qualified                            Data.List        as L
import Graphics.Rendering.OpenGL            as               GL
import Graphics.Rendering.OpenGL.GLU.Matrix as               GM
import qualified                            Graphics.UI.GLFW as FW
import qualified                            Graphics.UI.GLUT as GLUT

import           System.Exit
import           System.IO
import           System.Random
import           GHC.Real

import qualified Data.Vector as VU
import           AronModule
                 
-- import qualified Data.Vector.Unboxed as VU
-- Unboxed only support
{-|
Unboxed Arrays: Data.Vector.Unboxed

    Bool
    ()
    Char
    Double
    Float
    Int
    Int8, 16, 32, 64
    Word
    Word8, 16, 32, 64
    Complex a's, where 'a' is in Unbox
    Tuple types, where the elements are unboxable
-}



--import Linear.V3
--import Linear.V3(cross)
--import Linear.Vector
--import Linear.Matrix
--import Linear.Projection as P
--import Linear.Metric(norm, signorm)



lightDiffuse ::Color4 GLfloat
lightDiffuse = Color4 0.6 1.0 0.5 0.6

lightAmbient ::Color4 GLfloat
lightAmbient = Color4 0.0 0.0 1.0 1.0

lightPosition ::Vertex4 GLfloat
lightPosition = Vertex4 1.0 1.0 1.2 0.0

lightSpecular ::Color4 GLfloat
lightSpecular = Color4 1.0 0.7 1.0 0.8

{-|
    === KEY: draw string, render string

    @
    GL.scale (1/scaleFont :: GL.GLdouble) (1/scaleFont) 1
    GLUT.renderString GLUT.Roman str

    strWidth <- GLUT.stringWidth GLUT.Roman str
    strHeight <- GLUT.stringHeight GLUT.Roman str
    @
-}
scaleFont::GLdouble
scaleFont = 6000.0
                
-- | --------------------------------------------------------------------------------
-- | Fri Dec  7 14:35:38 2018
-- | three colors: data Color3 a = Color3 !a !a !a
red   = Color3 1 0 0 :: Color3 GLdouble
green = Color3 0 1 0 :: Color3 GLdouble
blue  = Color3 0 0 1 :: Color3 GLdouble
white = Color3 1 1 1 :: Color3 GLdouble
black = Color3 0 0 0 :: Color3 GLdouble

data SegEndPt = No      -- no pt, just a segment
                | End   -- end pt
                | Beg   -- begin pt
                | Both  -- begin and end pts
                | Cen   -- center pt
                | All   -- all pts: begin, end and ceneter

--   /\
--   | ccw
--    -->
--   | cw
--   V
-- | counter clockwise | clock wise
data NormalDir = NCCW | NCW deriving (Eq)


{-|
    === Compute the distance between two points

    \( v = (x, y, z) \)

    \( \| v \| = \sqrt{ x^2 + y^2 + z^2} = \sqrt{ v \cdot v} \)

    >let v1 = Vertex3 1 2 3
    >let v2 = Vertex3 2 3 4
    >dist v1 v2
-}
dist::Vertex3 GLfloat -> Vertex3 GLfloat -> GLfloat
dist (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) = sqrt $ (x1 - x0)**2 + (y1 - y0)**2 + (z1 - z0)**2


{-|
    === Compute the norm-squared

    \( v = (x, y, z)\)

    \( |v|^2 = x^2 + y^2 + z^2 \)

    >let v1 = Vertex3 1 2 3
    >let v2 = Vertex3 2 3 4
    >sqdist v1 v2
-}
sqdist::Vertex3 GLfloat-> Vertex3 GLfloat-> GLfloat
sqdist (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) = (x1 - x0)**2 + (y1 - y0)**2 + (z1 - z0)**2

-- | dot product for Vector3
dot3ve::(Num a)=>Vector3 a-> Vector3 a-> a
dot3ve v1 v2 = dot3vx (v2x v1) (v2x v2)

-- | dot product for Vertex3
dot3vx::(Num a)=>Vertex3 a-> Vertex3 a-> a
dot3vx (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) = x0*x1 + y0*y1 + z0*z1

(+:)::(Num a)=>Vertex3 a-> Vector3 a-> Vertex3 a
(+:) (Vertex3 x0 y0 z0) (Vector3 x1 y1 z1) = Vertex3 (x0 + x1) (y0 + y1) (z0 + z1)

(-:)::(Num a)=>Vertex3 a-> Vertex3 a-> Vector3 a
(-:) (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) = Vector3 (x0 - x1) (y0 - y1) (z0 - z1)

(⊥)::Vector3 GLfloat -> Vector3 GLfloat
(⊥) (Vector3 x y z) = Vector3 y (-x) z

perpcw::(Num a)=>Vector3 a->Vector3 a
perpcw (Vector3 x y z) = Vector3 y (-x) z

perpccw::(Num a)=>Vector3 a->Vector3 a
perpccw (Vector3 x y z) = Vector3 (-y) x z

(*:)::(Num a)=>a-> Vector3 a-> Vector3 a
(*:) k (Vector3 x y z) = Vector3 (k*x) (k*y) (k*z)

(**:)::GLfloat -> Vertex3 GLfloat-> Vertex3 GLfloat
(**:) t (Vertex3 x0 y0 z0) = Vertex3 (t*x0) (t*y0) (t*z0)

(*>:)::GLfloat -> Vertex3 GLfloat -> Vertex3 GLfloat
(*>:) k (Vertex3 x y z) = Vertex3 (k*x) (k*y) (k*z)

(>>:)::GLfloat -> Vector3 GLfloat -> Vector3 GLfloat
(>>:) k (Vector3 x y z) = Vector3 (k*x) (k*y) (k*z)

(.>)::Int
(.>) = 3

-- | dot product  odot
(⊙)::Vector3 GLfloat -> Vector3 GLfloat -> GLfloat
(⊙) (Vector3 x0 y0 z0) (Vector3 x1 y1 z1) = (x0*x1) + (y0*y1) + (z0*z1)

(∈)::(Num a, Ord a)=>a -> [a]-> Bool
(∈) a [b, c] = a >= b && a <= c

(∘)::(Num a)=>a-> Vector3 a-> Vector3 a
(∘) k (Vector3 x y z) = Vector3 (k*x) (k*y) (k*z)


(∎)::GLfloat -> Vector3 GLfloat -> Vector3 GLfloat
(∎) k (Vector3 x y z) = Vector3 (k*z) (k*y) (k*z)

v2x::(Num a)=>Vector3 a-> Vertex3 a
v2x (Vector3 x y z) = Vertex3 x y z

x2v::(Num a)=>Vertex3 a-> Vector3 a
x2v (Vertex3 x y z) = Vector3 x y z

-- Tue Jan  1 17:57:32 2019
-- remove it
--smulv::GLfloat -> Vector3 GLfloat ->Vector3 GLfloat
--smulv k (Vector3 x y z) = Vector3 (k*x) (k*y) (k*z)

-- | Shorten realToFrac
--rf::(Real a, Fractional b) => a -> b
--rf = realToFrac



{-|
    === Form a __skew symmetric matrix__ from \( \color{red}{Vertex3}  \)
    __cross product__ of a x b = [a] b where [a] is skew symmetric matrix from vector a
    colIndex x rowIndex = [c][r]

    >Vertex3 a1 a2 a3

    \[
     \begin{bmatrix}
            0    & -a_3 & a_2 \\
            a_3  & 0    & -a_1 \\
            -a_2 & a_1  & 0 \\
     \end{bmatrix}
    \]

-}
skew::(Num a)=>Vertex3 a->[[a]]
skew (Vertex3 a1 a2 a3) = [
                            [0,     -a3, a2],
                            [a3,    0,  -a1],
                            [-a2,   a1,   0]
                          ]


{-|
    === Form a __skew symmetric matrix__ from \( \color{red}{Vector3}  \)
    __cross product__ of a x b = [a] b where [a] is skew symmetric matrix from vector a
    colIndex x rowIndex = [c][r]

    >Vertex3 a1 a2 a3

    \[
     \begin{bmatrix}
            0    & -a_3 & a_2 \\
            a_3  & 0    & -a_1 \\
            -a_2 & a_1  & 0 \\
     \end{bmatrix}
    \]
-}
skew'::(Num a)=>Vector3 a->[[a]]
skew' (Vector3 a1 a2 a3) = [
                            [0,     -a3, a2],
                            [a3,    0,  -a1],
                            [-a2,   a1,   0]
                           ]


{-|
  === Cross product of two vectors.
  * The directin is determinated by the the Right Hadle Rule
-}
(⊗)::Vector3 GLfloat -> Vector3 GLfloat -> Vector3 GLfloat
(⊗) v0@(Vector3 a1 a2 a3) v1@(Vector3 b1 b2 b3) = Vector3 x y z
                    where
                        vs = [[b1], [b2], [b3]]
                        -- form a skew matrix from v0
                        sm = skew' v0
                        -- cross product: v0 X v1
                        vc  = multiMat sm vs
                        vp = Vector3 (head . head)

                        -- vc = [[x], [y], [z]]
                        x  = (head . head) vc
                        y  = (head . head) $ tail vc
                        z  = (last . last) vc


-- | check whether four points are coplanar
--
-- | /Users/cat/myfile/bitbucket/math/coplanar.pdf
--
isCoplanar::Vertex3 GLfloat ->Vertex3 GLfloat ->Vertex3 GLfloat ->Vertex3 GLfloat -> Bool
isCoplanar p0 p1 q0 q1 = if nis0 && nis1 then norm (cross v01 v12) == 0.0 else False
                where
                    nis0 = not $ isColinear p0 q0 q1
                    nis1 = not $ isColinear p1 q0 q1
                    v0  = q0 -: p0 -- p0 -> q0
                    v1  = q1 -: p0 -- p0 -> q1
                    v2  = q0 -: p1 -- p1 -> q0
                    v3  = q1 -: p1 -- p1 -> q1
                    v01 = cross v0 v1 -- v0 X v1
                    v12 = cross v2 v3 -- v2 X v3
                    norm v = sqrt $ dot3ve v v

cross::Vector3 GLfloat -> Vector3 GLfloat -> Vector3 GLfloat
cross u v = u ⊗ v


{-|
    === KEY: compute the normal of three points
-}            
normal3::Vertex3 GLfloat ->Vertex3 GLfloat ->Vertex3 GLfloat ->Maybe (Vector3 GLfloat)
normal3 p0 q0 q1 = if isc then Nothing else (Just nr)
                where
                    isc = isColinear p0 q0 q1
                    v0  = q0 -: p0
                    v1  = q1 -: p0
                    nr  = v0 ⊗ v1

{-|
    === KEY: compute the normal of plane with two given vectors
-}
normal3'::Vector3 GLfloat -> Vector3 GLfloat -> Vector3 GLfloat
normal3' v0 v1 = v0 ⊗ v1

-- | (Vertex3 a) operator: (-) (+)
instance (Num a)=>Num(Vertex3 a) where
    (-) (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) = Vertex3 (x0 - x1) (y0 - y1) (z0 - z1)
    (+) (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) = Vertex3 (x0 + x1) (y0 + y1) (z0 + z1)
    negate (Vertex3 x0 y0 z0)                 = Vertex3 (-x0) (-y0) (-z0)
    abs (Vertex3 x0 y0 z0)                    = Vertex3 (abs x0) (abs y0) (abs z0)
    signum _                                  = undefined
    (*) _ _                                   = undefined
    fromInteger _                             = undefined

-- | (Vector3 a) operator: (-) (+)
instance (Num a)=>Num(Vector3 a) where
    (-) (Vector3 x0 y0 z0) (Vector3 x1 y1 z1) = Vector3 (x0 - x1) (y0 - y1) (z0 - z1)
    (+) (Vector3 x0 y0 z0) (Vector3 x1 y1 z1) = Vector3 (x0 + x1) (y0 + y1) (z0 + z1)
    negate (Vector3 x0 y0 z0)                 = Vector3 (-x0) (-y0) (-z0)
    abs    (Vector3 x0 y0 z0)                 = Vector3 (abs x0) (abs y0) (abs z0)
    signum _                                  = undefined
    (*) _ _                                   = undefined
    fromInteger _                             = undefined

-- | Vector: p0 -> p1 = p1 - p0
vec::Vertex3 GLfloat -> Vertex3 GLfloat -> Vector3 GLfloat
vec p0 p1 = p1 -: p0

-- | Given a point and vector,
--
-- | draw a line passes the point and alone the vector
--
-- | r(t) = p0 + t(p1 - p0)
--
ray::Vertex3 GLfloat -> GLfloat -> Vector3 GLfloat -> Vertex3 GLfloat
ray p0 t v = p0 +: (t *: v)

-- | Given s, t, p0 and p1
--   1. c₀ = center p₀ p₁
--   2. p1' = p0 + s(⊥ p0 p1), p2' = p0 + t(⊥ p0 p1)
--   3. draw line from p1' to p2'
perpenLine::GLfloat -> GLfloat -> Vertex3 GLfloat -> Vertex3 GLfloat -> [Vertex3 GLfloat]
perpenLine s t p0 p1 = [c0 +: (s *: v), c0 +: (t *: v)]
           where
             v = perpcw $ vec p0 p1 -- p₀ → p₁
             c0= cen p0 p1

curvePt::(GLfloat -> GLfloat)->(GLfloat, GLfloat)->[Vertex3 GLfloat]
curvePt f (a, b) = [Vertex3 x (f x) 0 | x <- let n = 100; d = (b - a)/n; in map(\x -> a + x*d) [0..n]]

curvePtV::(GLfloat -> GLfloat)->(GLfloat, GLfloat)-> VU.Vector (Vertex3 GLfloat)
-- curvePtV f (a, b) = VU.fromList [Vertex3 0.1 10.1 10.0]
curvePtV f (a, b) = VU.map(\x -> Vertex3 x (f x) 0 ) $ let n = 100; d = (b - a)/(rf n) in VU.map(\x -> a + x*d) $ VU.enumFromN 0 n


{-| 
    === Given a function \(f\), interval \( (a, b) \)
    draw the curve from \(a\) to \(b\)

    >mapM_ (\n -> drawCurve (\x -> x^n) (-1.0, 1.0) green) [1..20] 
-} 
drawCurve::(GLfloat -> GLfloat) -> (GLfloat, GLfloat) ->Color3 GLdouble  -> IO()
drawCurve f (a, b) c = renderPrimitive LineStrip $ mapM_(\vx -> do
                                            color c
                                            vertex $ vx) $ curvePt f (a, b)

drawCurveV::(GLfloat -> GLfloat) -> (GLfloat, GLfloat) ->Color3 GLdouble  -> IO()
drawCurveV f (a, b) c = renderPrimitive LineStrip $ VU.mapM_(\vx -> do
                                            color c
                                            vertex $ vx) $ curvePtV f (a, b)

{-| 
    === draw Surface for equation \( f(x, y) = x^2 + y^2 \) form

    Draw \( f (x, y) = x^2 + y^2 \)

    > drawSurface (\x y -> x^2 + y^2)
-} 
drawSurface::(GLfloat -> GLfloat -> GLfloat) -> IO()
drawSurface f = do
    mapM_ (\row -> drawSegmentFromTo red row ) $ grid2 f 
    mapM_ (\row -> drawSegmentFromTo blue row ) $ tran $ grid2 f 

{-| 
    === draw Surface for equation \( f(x, y) = x^2 + y^2 \) form

    Draw \( f (x, y) = x^2 + y^2 \)

    > r = 2 => 1/(r*n) 
    > drawSurfaceR (\x y -> x^2 + y^2) r 
-} 
drawSurfaceR::(GLfloat -> GLfloat -> GLfloat) -> GLfloat -> IO()
drawSurfaceR f r = do
    mapM_ (\row -> drawSegmentFromTo red row ) $ grid2Ratio f r
    mapM_ (\row -> drawSegmentFromTo red row ) $ tran $ grid2Ratio f r


    

--drawParamSurf::(GLfloat -> GLfloat) -> (GLfloat -> GLfloat) -> (GLfloat -> GLfloat) -> IO() 
--drawParamSurf fx fy fz = do 
--    mapM_ (\row -> drawSegmentFromTo red row ) $ pts 
--    mapM_ (\row -> drawSegmentFromTo red row ) $ tran pts 
--        where 
--            n  = 10 
--            fa = 1/(1.5*n)
--            t = map(\x -> rf $ fa * x) [-n..n]
--            pts = [[ Vertex3 (fx t) (fy t) (fz t)]]


{-| 
    === Plot all pts

    <http://localhost/image/opengl_plot_pt.png Plot_Points>

    >interval from [-len/2.. len/2]
    >let pts = [0.3, 0.4, 0.1, 0.2] 
    >plotPts red pts 
    >plotPts green $ quickSort1 pts 
-} 
plotPts::Color3 GLdouble -> [GLfloat] ->IO()
plotPts co cx = do
                let n = div (length cx) 2
                let xx = let del = 1/(rf n) in map(\x -> del*(rf x)) [-n..n]
                let xx' = map(\x -> rf x) xx
                let vl = zipWith(\x y -> Vertex3 x y 0) xx' cx 
                let pair = join $ zipWith(\x y -> [x, y]) (init vl) (tail vl)
                mapM_ (\x -> drawCircleColor x red 0.01) vl 
                drawPrimitive' Lines co pair 

{-|
  Draw a line that is tangent at pt (c, f(c)) where x in [x0, x1]

  <http://localhost/image/tangleline.png tangentLine>

  1. Given a function f, and x0 on x-Axis

  2. Derive a tangent line at (c, f(c)) with slop = f'(c)

  3. Interpolate (x0, f(x0)) and (x1, f(x1))

-}
tangentLine::(Fractional a)=>(a->a)->(a, a)->a->[Vertex3 a]
tangentLine f (x0, x1) c = interpolate' 1 p0 p1
        where
            y0 = tangent f x0 c  -- y = slop(x - c) + (f x0)
            y1 = tangent f x1 c  -- y = slop(x - c) + (f x1)
            p0 = Vertex3 x0 y0 0
            p1 = Vertex3 x1 y1 0

interpolate'::(Fractional a)=>Integer -> Vertex3 a->Vertex3 a->[Vertex3 a]
interpolate' n p0@(Vertex3 x0 y0 z0) p1@(Vertex3 x1 y1 z1) = map(\k -> let t = d*(fromIntegral k) in p0 +: (t *: ve)) [0..n]
                    where
                        ve = p1 -: p0 -- vector: p0 -> p1
                        d = 1/(fromInteger n)

{-|
    === Given a function f, interval (a, b),
        draw a tangent line at (x0, f(x0)) from a to b
    >f::x -> y

    (a, b) is interval

    differentiate at (x0, f x0)
-}
drawTangentLine::(GLfloat->GLfloat)->(GLfloat, GLfloat) -> GLfloat -> Color3 GLdouble -> IO()
drawTangentLine f (a, b) x0 c = renderPrimitive LineStrip $ mapM_(\vx -> do
                                            color c
                                            vertex $ vx) $ pts
                    where
                        pts = tangentLine f (a, b) x0

{-|
 === NormalDir is either counter clockwise or clockwise for normal

 (x0, x1) is interval for the normal
 f is  any function
 x=c is the tangent point at (c, f c)

 >data NormalDir = NCCW | NCW deriving (Eq)
-}
normalLine::NormalDir -> (GLfloat->GLfloat)->(GLfloat, GLfloat)->GLfloat->[Vertex3 GLfloat]
normalLine n f (x0, x1) c = interpolate' 1 p0 p1'
        where
            y0 = tangent f x0 c
            y1 = tangent f x1 c
            p0 = Vertex3 x0 y0 0
            p1 = Vertex3 x1 y1 0
            ve = p1 -: p0  -- p0 -> p1
            nr = if n == NCW then perpccw ve else perpcw ve
            p1'= p0 +: nr

normalLineNew::NormalDir -> (GLfloat->GLfloat)->GLfloat->GLfloat->[Vertex3 GLfloat]
normalLineNew n f x0 c = interpolate' 1 p0 p1'
        where
            y0 = tangent f x0 c
            y1 = tangent f (x0 + len) c
            p0 = Vertex3 x0 y0 0
            p1 = Vertex3 (x0 + len) y1 0
            ve = p1 -: p0  -- p0 -> p1
            nr = if n == NCW then perpccw ve else perpcw ve
            p1'= p0 +: nr
            len = 0.1

drawNormalLine::(GLfloat->GLfloat)->(GLfloat, GLfloat) -> GLfloat -> Color3 GLdouble -> IO()
drawNormalLine f (a, b) x0 c = renderPrimitive LineStrip $ mapM_(\vx -> do
                                            color c
                                            vertex $ vx) $ pts
                    where
                        pts = normalLine NCCW f (a, b) x0

drawNormalLineNew::(GLfloat->GLfloat)->GLfloat -> GLfloat -> Color3 GLdouble -> IO()
drawNormalLineNew f a x0 c = renderPrimitive LineStrip $ mapM_(\vx -> do
                                            color c
                                            vertex $ vx) $ pts
                    where
                        pts = normalLineNew NCCW f a x0
                        b = 0.1

-- | compute the center of two vertices/vertex
cen::Vertex3 GLfloat -> Vertex3 GLfloat -> Vertex3 GLfloat
cen (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) = Vertex3 ((x0 + x1)/2) ((y0 + y1)/2) ((z0 + z1)/2)

-- | check two vectors whether they are perpendicular
isPerpen::Vector3 GLfloat -> Vector3 GLfloat -> Bool
isPerpen v1 v2 =  v1 ⊙ v2 == 0

{-|
    === Check whether three pts are colinear
    __NOTE__

    * If two points or three points are overlapped, then they are still colinear

    @
    f(t) = p0 + t(p1 - p0)
    f(s) = p0 + s(p2 - p0)
    f(t) = f(s) =>
    t(p1 - p0) = s(p2 - p0)
    t v1 = s v2, let A = [v1 v2]
    det A =? 0
    det A = 0 => p0 p1 p2 are colinear
    @
-}
isColinear::Vertex3 GLfloat ->Vertex3 GLfloat ->Vertex3 GLfloat -> Bool
isColinear (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) (Vertex3 x2 y2 z2) = det == 0
                where
                    -- v01 = Vertex3 (x1 - x0) (y1 - y0) (z1 - z0)
                    -- v02 = Vertex3 (x2 - x0) (y2 - y0) (z2 - z0)
                    det = (x1 - x0)*(y2 - y0) - (x2 - x0)*(y1 - y0)

{-|
    === Check whether a given point is inside the segment

    __NOTE__ use 'ptOnSegment' instead

    * Assume three pts are different pts

    * Given p0, q0 q1, check whether p0 is inside the segment of q0 q1

    * __If__ they are colinear
    * __then__ check the distance \( \overline{p_0 q_0} \)  \( \overline{p_0 q_1} \) and \( \overline{q_0 q_1} \)

    Using following __Affine combination on points__ formula

    === Linear combination on vector \( \{ x_1, x_2, \dots \} \)
    \[
        \begin{aligned}
            \sum_{i=1}^{n} \alpha_{i} x_i
        \end{aligned}
    \]

    If the sum of all the coefficients is 1
    \[
        \begin{aligned}
            \sum_{i=1}^{n} \alpha_{i} &= 1
        \end{aligned}
    \]
    is called __Affine Combination__ of \( \{x_1, x_2, \dots \} \)

    __NOTE__
    If we extend above definition to highter power on \(t\),
    \(t\) can have any power \( n > 0 \) or \( t^{n} \)

    \( [(1 - t) + t]^{n} \) is just Binomial Expansion

    <http://localhost/html/indexBezierCurve.html Bezier_Curve>

    \[
        \begin{aligned}
            1 &= [(1 - t) + t]^{1} \\
            Q &= (1 - t)p_0 + t p_1 \\ \\
            1 &= [(1 - t) + t]^{2} = (1 - t)^{2} + 2t(1-t) + t^2 \\
            Q &= (1-t)^{2} p_0 + 2t(1-t) p_1 + t^2 p_2  \\
        \end{aligned}
    \]



    In out case, there are only two points \( p_0, p_1 \)
    so \( p \) is the __Affine Combination__ of \( p_0, p_1 \)
    \[
        \begin{aligned}
            p &= (1 - t) p_0 + t p_1 \\
        \end{aligned}
    \]
    If \( 0 < t < 1 \) then the point on the segment \( \overline{p_0 p_1} \) but not overlapping on \( p_0, p_1 \)

    If \( t = 0 \) or \( t = 1 \) then the point \(p\) is overlapping \( p_0 \) or \( p_1 \)

    If \( t < 0 \) or \( t > 1 \) then the point \(p\) is NOT on the segment \( \overline{p_0 p_1} \)

    >p0 = Vertex3 0.5 0.5 0
    >q0 = Vertex3 1 1 0
    >q1 = Vertex3 0 0 0
    >isInSegment p0 q0 q1
    >Just True
-}
isInSegment::Vertex3 GLfloat ->Vertex3 GLfloat ->Vertex3 GLfloat -> Maybe Bool
isInSegment p0 q0 q1 = if is then (if d1 > ds || d2 > ds then Just False else Just True) else Nothing
                where
                    is = isColinear p0 q0 q1 -- has to be colinear
                    ds = dist q0 q1
                    d1 = dist p0 q0
                    d2 = dist p0 q1

data PtSeg = OnEndPt
             | InSeg
             | OutSeg  deriving (Eq, Show)

{-|
    === Better version of 'isInSegment'

    (1) If \(p_0\) is overlapped with \(q_0, q_1\) then OnPt
    (2) If \(\overline{p_0 q_0} + \overline{p_0 q_1} > 0\) then OutSeg
    (3) Else InSeg

    @
    data PtSeg = OnEndPt -- ^ Overlapped pt
                 | InSeg   -- ^ Inside the segment
                 | OutSeg  -- ^ Out the segment
    @

    Maybe use better name:
    crossSegments - No endpt is overlapped

   >1. Three pts are colinear
   >    1. one endpts is overlapped
   >    2. No endpt is overlapped, one endpt is "inside" the segment
   >    3. Not intersected
   >2. Four pts are colinear
   >     1. No endpts is overlapped
   >         1. one segment "inside" the other segment
   >         2. one segment "outside" the other segment
   >     2. One endpts is overlapped
   >         1. one endpt is "outside" a segment
   >         2. one endpt is "inside" a segment
   >     3. two endpts are overlapped => same segment
   >3. No three pts are colinear
   >    1. If two segment is intersected, one must cross other segment
   >    2. Two segments DO NOT intersect

-}
ptOnSegment::Vertex3 GLfloat ->Vertex3 GLfloat ->Vertex3 GLfloat -> PtSeg
ptOnSegment p0 q0 q1 = if is then OnEndPt else (if d1 + d2 > ds then OutSeg else InSeg)
    where
        is = containPt p0 [q0, q1]
        ds = dist q0 q1
        d1 = dist p0 q0
        d2 = dist p0 q1

{-|
    === Draw primitives with a list of triple '(GLfloat, GLfloat, GLfloat)' such lines, points, lineloop etc

    <http://localhost/html/indexHaskellOpenGLPrimitiveMode.html PrimitiveMode>

    @
    PrimitiveMode
    Lines
    LineStrip
    LineLoop
    TriangleStrip
    TriangleFan
    Quad
    QuadStrip
    Polygon
    Patches
    @

    drawPrimitive Lines green [(0.1, 0.2, 0.0), (0.2, 0.4, 0.0)]
-}
drawPrimitive::PrimitiveMode -> Color3 GLdouble -> [(GLfloat, GLfloat, GLfloat)]->IO()
drawPrimitive m c list = do
    renderPrimitive m $ mapM_(\(x, y, z) -> do
        color c
        vertex $ Vertex3 x y z) list

{-|
    === KEY: Draw primitives with a list of 'Vertex3 GLfloat'

    <http://localhost/html/indexHaskellOpenGLPrimitiveMode.html PrimitiveMode>

    @
    PrimitiveMode
    Lines
    LineStrip
    LineLoop
    TriangleStrip
    TriangleFan
    Quad
    QuadStrip
    Polygon
    Patches
    @

    drawPrimitiveVex Lines green [Vertex3 0.1, 0.2, 0.0, Vertex3 0.2, 0.4, 0.0]
-}    
drawPrimitiveVex::PrimitiveMode -> Color3 GLdouble -> [Vertex3 GLfloat]->IO()
drawPrimitiveVex m c list = do
    renderPrimitive m $ mapM_(\vx -> do
        color c
        vertex $ vx) list
    
drawTriangleStrip:: Color3 GLdouble -> [Vertex3 GLfloat] -> IO ()
drawTriangleStrip c cx = drawPrimitiveVex TriangleStrip c cx

{-|
    KEY: Draw ONE triangle ONLY from '(Vertex3 GLfloat, Vertex3 GLfloat, Vertex3 GLfloat)'
-}
drawTriangleVex:: Color3 GLdouble -> (Vertex3 GLfloat, Vertex3 GLfloat, Vertex3 GLfloat) -> IO ()
drawTriangleVex co (a, b, c) = drawPrimitiveVex TriangleStrip co [a, b, c]
                           
{-| 
    === draw primitive such lines, points, lineloop etc

    <http://localhost/html/indexHaskellOpenGLPrimitiveMode.html PrimitiveMode>

    @
    Lines
    LineStrip
    LineLoop
    TriangleStrip
    TriangleFan
    Quad
    QuadStrip
    Polygon
    Patches
    @

    'AronOpenGL.randomVertex'

    >list <- randomVertex 60
    >drawPrimitive' Lines red list
-} 
drawPrimitive'::PrimitiveMode -> Color3 GLdouble -> [Vertex3 GLfloat]->IO()
drawPrimitive' m c list = do
    renderPrimitive m $ mapM_(\v3 -> do
        color c
        vertex $ v3) list

drawLines::Color3 GLdouble -> [Vertex3 GLfloat]->IO()
drawLines c list = do
    renderPrimitive Lines $ mapM_(\v3 -> do
        color c
        vertex v3) list
    mapM_ (\(x, y) -> if (mod x 2) == 1 then drawCircleColor' red 0.01 y else return () ) $ zip [1..] list
    mapM_ (\(x, y) -> if (mod x 2) == 0 then drawCircleColor' green 0.015 y else return () ) $ zip [1..] list


{-|
    === Draw Segment with Two End Pts

    >v = [Vertex3 0.1 0.1 0.2, Vertex3 0.4 0.2 0.7]
    >drawSegmentWithEndPt red v
    >
    >v = [[Vertex3 0.1 0.1 0.2, Vertex3 0.4 0.2 0.7]]
    >mapM_(\x -> drawSegmentWithEndPt red x) vv
-}
drawSegmentWithEndPt::Color3 GLdouble -> [Vertex3 GLfloat]->IO()
drawSegmentWithEndPt c list = do
    renderPrimitive Lines $ mapM_(\v3 -> do
        color c
        vertex v3) list
    mapM_ (\(x, y) -> if (mod x 2) == 1 then drawCircleColor' red 0.01 y else return () ) $ zip [1..] list
    mapM_ (\(x, y) -> if (mod x 2) == 0 then drawCircleColor' green 0.015 y else return () ) $ zip [1..] list

drawSegmentNoEndPt::Color3 GLdouble -> [Vertex3 GLfloat]->IO()
drawSegmentNoEndPt c list = do
    renderPrimitive Lines $ mapM_(\v3 -> do
        color c
        vertex v3) list

{-|
    === Draw a set segment from each pair of points(segment)

    Segment contains two end points, one is begin point, other is end point

    > [p0, p1, p2] = p0 -> p1    (p2 is ignored)
    > [p0, p1, p2, p3] = p0 -> p1
    >                    p2 -> p3
-}
drawSegments::Color3 GLdouble -> [Vertex3 GLfloat] -> IO()
drawSegments c list = do
    renderPrimitive Lines $ mapM_(\v3 -> do
        color c
        vertex v3) list
    mapM_ (\(x, y) -> if (mod x 2) == 1 then drawCircleColor' red 0.01 y else return () ) $ zip [1..] list
    mapM_ (\(x, y) -> if (mod x 2) == 0 then drawCircleColor' green 0.015 y else return () ) $ zip [1..] list

-- | Draw one segment from p0 to p1
-- | drawSegment color (p0, p1)
-- |
drawSegment::Color3 GLdouble -> (Vertex3 GLfloat, Vertex3 GLfloat)->IO()
drawSegment c (p0, p1) = do
    renderPrimitive Lines $ mapM_(\v3 -> do
        color c
        vertex v3) list
    mapM_ (\(x, y) -> if (mod x 2) == 1 then drawCircleColor' red 0.01 y else return () ) $ zip [1..] list
    mapM_ (\(x, y) -> if (mod x 2) == 0 then drawCircleColor' green 0.015 y else return () ) $ zip [1..] list
        where
            list = [p0, p1]

{-| 
    === Draw one segment with no endpt
-} 
drawSegmentNoEnd::Color3 GLdouble -> (Vertex3 GLfloat, Vertex3 GLfloat)->IO()
drawSegmentNoEnd c (p0, p1) = do
    renderPrimitive Lines $ mapM_(\v3 -> do
        color c
        vertex v3) list
        where
            list = [p0, p1]

-- | definition:
--   Segment contains two end points, one is begin point, other is end point
--   [p0, p1] = p0 -> p1
--
--data SegEndPt = No      -- no pt, just a segment
--                | End   -- end pt
--                | Beg   -- begin pt
--                | Both  -- begin and end pts
--                | Cen   -- center pt
--                | All   -- all pts: begin, end and ceneter
drawSegment'::SegEndPt -> Color3 GLdouble -> [Vertex3 GLfloat]->IO()
drawSegment' endpt c list = do
    renderPrimitive Lines $ mapM_(\v3 -> do
        color c
        vertex v3) list
    let bList = mapM_ (\(x, y) -> if (mod x 2) == 1 then drawCircleColor' red 0.01 y else return () ) $ zip [1..] list
        eList = mapM_ (\(x, y) -> if (mod x 2) == 0 then drawCircleColor' green 0.015 y else return () ) $ zip [1..] list
        ols = odds  list
        els = evens list
        cls = zipWith(\x y -> pure(/2) <*> (pure(+) <*> x <*> y)) ols els
        cList = mapM_ (drawCircleColor' blue 0.012) cls
        in case endpt of
            Beg -> bList -- begin pt
            End -> eList -- end pt
            Cen -> cList -- center pt
            Both -> do
                bList
                eList
            All -> do
                bList
                eList
                cList
            _   -> return ()

-- | Draw intersection pt from two segments
-- | Two segments need not have an intersection.
--
drawSegmentArg::Vertex3 GLfloat ->
                Vertex3 GLfloat ->
                Vertex3 GLfloat ->
                Vertex3 GLfloat -> IO()
drawSegmentArg p0 p1 q0 q1 = do
            drawLines green [p0, ps]
            if not $ s ∈ [0, 1]
            then if s > 1
                then drawLines blue  [p1, ps] -- (p0 --> p1) --> ps
                else drawLines green [p0, ps] -- ps <-- (p0 --> p1
            else return ()
            if t > 1 || t < 0
            then if t > 1
                then drawLines green [q1, qt] -- (q0 --> q1) --> qt
                else drawLines blue  [q0, qt] -- qt <-- (q0 --> q1)
            else return ()
            where
                tup = fromJust $ intersectLine  p0 p1 q0 q1
                vx  = fst tup
                s   = (head . head) $ snd tup
                t   = (last . last) $ snd tup
                ps  = p0 +: (s *: (p1 -: p0))
                qt  = q0 +: (t *: (q1 -: q0))

{-|
  === draw sphere center at c with radius r
  * β is in x-y plane, it rotates around z-Axis. ϕ is x-z plane, it rotates aroound y-Axis
  * \( r*\cos(ϕ) \) is the radius of circle cut through x-y plane

-}
drawSpherePt::Vertex3 GLfloat -> GLfloat -> [Vertex3 GLfloat]
drawSpherePt c@(Vertex3 x0 y0 z0) r = pp
        where
            fx ϕ β = r*cos(ϕ)*cos(β) + x0
            fy ϕ β = r*cos(ϕ)*sin(β) + y0
            fz ϕ β = r*sin(ϕ) + z0
            n = 50
            π = pi
            δ = 2*π/n
            ll= map(\x -> x*δ) [1..n]
            ls= map(\x -> x*δ) [1..n]
            pp=[ Vertex3 (fx ϕ β) (fy ϕ β) (fz ϕ β)  | ϕ <- let d = π/10 in map (*d) [0..10], β <- ls]



-- | draw plane with three points
-- | no check whether they are co-linear
--
drawPlane::Color3 GLdouble ->
           Vertex3 GLfloat ->
           Vertex3 GLfloat ->
           Vertex3 GLfloat -> IO()
drawPlane c q0 q1 q2 = do
  renderPrimitive Lines $ mapM_ (\x -> do
                                    color c
                                    vertex x) [q0, q1, q0, q2, q1, q2]
{-|
    === Generate a circle 

    > let radius = 0.1
    > let pts = circlePt (Vertex3 0 0 0) radius

    See 'circleN' $n$ segments
-}
circlePt::Vertex3 GLfloat -> Double -> [Vertex3 GLfloat]
circlePt (Vertex3 x0 y0 z0) r =[let alpha = (pi2*n)/num in Vertex3 ((rf r)*sin(alpha) + x0) ((rf r)*cos(alpha) + y0) (0 + z0) | n <- [1..num]]
        where
            num = 4 
            pi2 = 2*pi::Float

{-|
    \(\color{red}{Deprecated} \) Use 'circlePt'

    === Fri Feb 15 11:13:17 2019 

    === Draw xy-plane circle
-}
circle'::Vertex3 GLfloat -> Double -> [Vertex3 GLfloat]
circle' (Vertex3 x0 y0 z0) r =[let alpha = (pi2*n)/num in Vertex3 ((rf r)*sin(alpha) + x0) ((rf r)*cos(alpha) + y0) (0 + z0) | n <- [1..num]]
        where
            num = 4
            pi2 = 2*pi::Float
            
{-|
    === Draw xy-plane circle with $n$ segment

    See 'circlePt' 
-}
circleN::Vertex3 GLfloat -> Double -> Integer -> [Vertex3 GLfloat]
circleN (Vertex3 x0 y0 z0) r num =[let alpha = (pi2*(rf n))/(rf num) in Vertex3 ((rf r)*sin(alpha) + x0) ((rf r)*cos(alpha) + y0) (0 + z0) | n <- [1..num]]
        where
            pi2 = 2*pi::Float

{-|
    === Draw xz-plane circle
-}
circleXY::Vertex3 GLfloat -> Double -> [Vertex3 GLfloat]
circleXY (Vertex3 x0 y0 z0) r =[let alpha = (pi2*n)/num in Vertex3 ((rf r)*cos(alpha) + x0) ((rf r)*sin(alpha) + y0) (0 + z0) | n <- [1..num]]
        where
            num = 4
            pi2 = 2*pi::Float

{-|
    === Draw xz-plane circle
-}
circleXZ::Vertex3 GLfloat -> Double -> [Vertex3 GLfloat]
circleXZ (Vertex3 x0 y0 z0) r =[let alpha = (pi2*n)/num in Vertex3 ((rf r)*cos(alpha) + x0) (0 + y0) ((rf r)*sin(alpha) + z0) | n <- [1..num]]
        where
            num = 4
            pi2 = 2*pi::Float

{-|
    === Draw yz-plane circle
-}
circleYZ::Vertex3 GLfloat -> Double -> [Vertex3 GLfloat]
circleYZ (Vertex3 x0 y0 z0) r =[let alpha = (pi2*n)/num in Vertex3  (0 + x0) ((rf r)*cos(alpha) + y0) ((rf r)*sin(alpha) + z0) | n <- [1..num]]
        where
            num = 4
            pi2 = 2*pi::Float


-- | draw dot, small circle
drawDot::Vertex3 GLfloat -> IO()
drawDot cen = drawPrimitive' LineLoop c $ circle' cen r
        where
            r = 0.01
            c = red


-- | Given an Vector3 x y z or Vertex3 x y z
-- | Convert Cartesian Coordinates to Polar Coordinates
-- | beta  x-y plane,
-- | alpha x-z plane
-- |
-- |             y  z
-- |             | /
-- |          ---|/--- x
-- |             /
--
cartToPolar::(Vector3 GLfloat) -> (GLfloat, GLfloat)
cartToPolar (Vector3 x y z) = (atan(y/x), asin(z/rxz))
        where
            rxy = sqrt $ dot3ve (Vector3 x y 0.0) (Vector3 x y 0.0)
            rxz = sqrt $ dot3ve (Vector3 x 0.0 z) (Vector3 x 0.0 z)
            -- | at  = if y < 0 && x < 0 then pi + atan(y/x) else atan(y/x)

-- | right hand coordinates system in OpenGL
-- |
-- | x-z plane
roty α =[[cos α,          0.0,  sin α]
        ,[ 0.0 ,          1.0,  0.0  ]
        ,[negate $ sin α, 0.0,  cos α]
         ]

-- | x-y plane
rotz ϕ = [ [cos ϕ,          negate $ sin ϕ, 0.0]
         ,[ sin ϕ,          cos ϕ, 0.0]
          ,[ 0.0 ,            0.0, 1.0]
          ]


coordTip:: Color3 GLdouble -> IO()
coordTip c = do
    renderPrimitive Lines $ mapM_(\(x, y, z) -> do
        color c
        vertex (Vertex3 x y z ::Vertex3 GLfloat)
        ) conic

coordTipX::Color3 GLdouble ->  GLdouble -> IO()
coordTipX c u = do
    preservingMatrix $ do
        translate (Vector3 u 0 0 :: Vector3 GLdouble)
        --rotate (90)$ (Vector3 0 0 1 :: Vector3 GLdouble)
        coordTip c
    preservingMatrix $ do
        translate (Vector3 (u/2.0) 0 0 :: Vector3 GLdouble)
        GL.scale (1/scaleFont :: GL.GLdouble) (1/scaleFont) 1
        GLUT.renderString GLUT.Roman "X"

coordTipY:: Color3 GLdouble ->  GLdouble -> IO()
coordTipY c u = do
    preservingMatrix $ do
        translate (Vector3 0 u 0 :: Vector3 GLdouble)
        rotate (90)$ (Vector3 0 0 1 :: Vector3 GLdouble)
        coordTip c
    preservingMatrix $ do
        translate (Vector3 0 (u/2.0) 0 :: Vector3 GLdouble)
        rotate (90)$ (Vector3 0 0 1 :: Vector3 GLdouble)
        GL.scale (1/scaleFont :: GL.GLdouble) (1/scaleFont) 1
        GLUT.renderString GLUT.Roman "Y"

coordTipZ::Color3 GLdouble -> GLdouble -> IO()
coordTipZ c u = do
    preservingMatrix $ do
        translate (Vector3 0 0 u :: Vector3 GLdouble)
        rotate (-90)$ (Vector3 0 1 0 :: Vector3 GLdouble)
        coordTip c
    preservingMatrix $ do
        translate (Vector3 0 0 (u/2.0) :: Vector3 GLdouble)
        rotate (-90)$ (Vector3 0 1 0 :: Vector3 GLdouble)
        GL.scale (1/scaleFont :: GL.GLdouble) (1/scaleFont) 1
        GLUT.renderString GLUT.Roman "Z"

{-|
    === Coordinate with tips

    positive dir is the tips dir
-}
renderCoordinates::IO()
renderCoordinates = do
    let u = 1.0
    coordTipX red   u
    coordTipY green u
    coordTipZ blue  u
    renderPrimitive Lines $ do
        -- x-Axis
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 (-u) 0 0 ::Vertex3 GLdouble)
        color  (Color3 1 0 0 :: Color3 GLdouble)
        vertex (Vertex3 u 0 0 :: Vertex3 GLdouble)
        -- y-Axis
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 0 (-u) 0 :: Vertex3 GLdouble)
        color  (Color3 0 1 0 :: Color3 GLdouble)
        vertex (Vertex3 0 u 0 :: Vertex3 GLdouble)
        -- z-Axis
        color  (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 0 0 (-u) :: Vertex3 GLdouble)
        color  (Color3 0 0 1 :: Color3 GLdouble)
        vertex (Vertex3 0 0 u :: Vertex3 GLdouble)

-- | Given an radius r, center Vertex3 vx, and Vector ve
--   Draw circle(r, vx) which is perpendicular to ve
-- |
drawCircleVec::GLfloat -> Vertex3 GLfloat -> Vector3 GLfloat -> IO()
drawCircleVec r c v = do
  mapM_ drawDot $ map (matVx) ls'
  drawLines blue [c, c +: v]
  where
    (ϕ, α) = cartToPolar v
    circ = [[[0.0], [r*(cos β)], [r*(sin β)]] | β <- let π = 3.1415; d = 2*π/100 in map(*d) [1..100]]
    -- | circ = [[[0.5], [0.5], [0.0]]]
    ls   = map(multiMat (rotz $ ϕ)) circ -- rotate x-y plane
    ls'  = map(multiMat (roty $ α)) ls   -- rotate x-z plane
    -- |  v'   = multiMat (rotz $ ϕ) (veMat v)
    -- |  v''  = multiMat (roty $ α) v'
    -- | matVx [[x], [y], [z]] = Vertex3 x y z
    -- | veMat (Vector3 x y z) = [[x], [y], [z]]
    -- | ls   = map(multiMat (rotz $ 0.0)) circ
    ls1  = map(multiMat (rotz $ pi/4)) circ
    ls2  = map(multiMat (rotz $ 2*pi/4)) circ
    ls3  = map(multiMat (rotz $ 3*pi/4)) circ
    ls4  = map(multiMat (rotz $ 4*pi/4)) circ
    ls5  = map(multiMat (rotz $ 5*pi/4)) circ
    ls6  = map(multiMat (rotz $ 6*pi/4)) circ
    ls7  = map(multiMat (rotz $ 7*pi/4)) circ
    ls8  = map(multiMat (rotz $ 8*pi/4)) circ

-- | matrix to Vertex
matVx [[x], [y], [z]] = Vertex3 x y z
-- | matrix to Vector
matVe [[x], [y], [z]] = Vector3 x y z
-- | Vector to matrix
veMat (Vector3 x y z) = [[x], [y], [z]]

        

{-|
    === KEY: rectangle with leftTop and bottomRight
    @
    let     x0 = -0.5::GLfloat
            y0 = -0.5::GLfloat                                                     
            z0 = 0.0::GLfloat                                                      
            x1 = 0.5::GLfloat                                                      
            y1 = 0.5::GLfloat                                                      
            z1 = 0.0::GLfloat  in drawRect ((Vertex3 x0 y0 z0), (Vertex3 x1 y1 z1))

              Y
         p₀   ↑
         ↓    |    th
         ⌜----|----⌝
         |    |    |  rv
         |    |    |
    lv   |    /----- → x
         |   /     |
         ⌞--/------⌟ ← p₁  
           z        

             bh   
    @
-}                             
drawRect::(Vertex3 GLfloat, Vertex3 GLfloat) -> IO()
drawRect (p0@(Vertex3 x0 y0 z0), p1@(Vertex3 x1 y1 z1)) = do
        drawSegmentNoEnd red (Vertex3 x0 y0 z0, Vertex3 x1 y0 z0)  --  top horizontal 
        drawSegmentNoEnd red (Vertex3 x0 y0 z0, Vertex3 x0 y1 z0)  --  left vertical  
        drawSegmentNoEnd red (Vertex3 x1 y0 z0, Vertex3 x1 y1 z1)  --  right vertical
        drawSegmentNoEnd red (Vertex3 x0 y1 z0, Vertex3 x1 y1 z1)  --  bottom horizontal

{-|
    === Draw Rectangle with Width and Height

           w
       ⌜--------⌝
       |        |  
       |        | 
       |   +    | h
       |        |
       |        |
       ⌞--------⌟

-}
drawRect2d::(GLfloat, GLfloat) -> IO()
drawRect2d (w, h) = do
  drawRect (p0, p1)
  where
    x0 = w/2
    y0 = h/2
    p0 = Vertex3 (-x0) y0    0
    p1 = Vertex3 x0    (-y0) 0
        
drawQuads::[Vertex3 GLfloat] -> IO()
drawQuads cx = drawPrimitive' Quads red $ cx

               
drawQuadsColor:: Color3 GLdouble -> [Vertex3 GLfloat] -> IO()
drawQuadsColor c cx = drawPrimitive' Quads c $ cx 


{-|
             ↑
             |
        v0   ⟶   v1
                   
        ↑           |
        |    +      ↓  -> y

       v3    <—-    v2

-}
drawRectFill2d::Color3 GLdouble -> (GLfloat, GLfloat) -> IO()
drawRectFill2d c (w, h) = do
  drawQuadsColor c [vx0, vx1, vx2, vx3]
  where
    x0 = w/2
    y0 = h/2
    vx0 = Vertex3 (-x0) y0    0
    vx1 = Vertex3 x0    y0    0
    vx2 = Vertex3 x0    (-y0) 0
    vx3 = Vertex3 (-x0) (-y0) 0


{-|
    === draw circle with center and radius

    >drawCircle cen r = drawPrimitive LineLoop red $ circle cen r
-}
drawCircle'::Vertex3 GLfloat -> Double -> IO()
drawCircle' cen r = drawPrimitive' LineLoop red $ circle' cen r

                    
drawCircle2::Vertex3 GLfloat -> Double -> IO()
drawCircle2 cen r = drawPrimitive' LineLoop red $ circleN cen r 30
                    

drawCircleXYZ::Vertex3 GLfloat -> Double -> IO()
drawCircleXYZ cen r = do
                drawPrimitive' LineLoop red $ circleXY cen r
                drawPrimitive' LineLoop red $ circleXZ cen r
                drawPrimitive' LineLoop red $ circleYZ cen r

drawCircleXYZColor::Vertex3 GLfloat -> Double -> Color3 GLdouble -> IO()
drawCircleXYZColor cen r c = do
                drawPrimitive' LineLoop c $ circleXY cen r
--                drawPrimitive' LineLoop c $ circleXZ cen r
--                drawPrimitive' LineLoop c $ circleYZ cen r


{-|
    === draw circle with center , Color3, radius

    >drawCircleColor (Vertex3 0.1 0.2 0.3) red 0.5
-}
drawCircleColor::Vertex3 GLfloat -> Color3 GLdouble -> Double -> IO()
drawCircleColor cen c r = drawPrimitive' LineLoop c $ circle' cen r

{-|
    === Similar to drawCircleColor, but it can do more

    * draw two circles with different centers

    >mapM_ (drawCircleColor' red 0.5) [Vertex3 0.1 0.2 0.3, Vertex3 0.2 0.3 04]
-}
drawCircleColor'::Color3 GLdouble ->Double -> Vertex3 GLfloat -> IO()
drawCircleColor' c r cen = drawPrimitive' LineLoop c $ circle' cen r

{-|
    === Conic Parameter Equation
    gx <http://localhost/html/indexThebeautyofTorus.html Conic>
-}
conic::[(GLfloat, GLfloat, GLfloat)]
conic= [ let r' = r - rd*i in (d'*i, r'*sin(δ*k), r'*cos(δ*k)) | i <- [0..m], k <-[1..n]]
        where
            n = 20
            m = 10
            h = 0.1
            r = 0.05
            δ = 2*pi/n
            d' = h/m
            rd = r/m



-- | determinant of two dimension matrix
det2::(Num a)=>[[a]] -> a
det2 [[a, b], [c, d]] = a*d - b*c


--- | Sat Dec 22 20:59:28 2018
--- |
--- | find the inverse of matrix in Rational number
--- | it is not much difference from the Integer code
--- | change division from (div n m) => (n / m)
--- |
inverseR::[[Rational]]->[[Rational]]
inverseR m = if diag == 0 then [[]] else mb'
        where
            id = ident' $ length m
            -- argumented matrix [m] ++ [id]
            argm = zipWith(\x y -> x ++ y) m id
            -- argm =
            -- [[1 2 3 1 0 0]]
            -- [[4 5 6 0 1 0]]
            -- [[7 8 9 0 0 1]]
            mt = upperTri' argm
            -- mt =
            -- [[1, 2, 3 x x x]]
            -- [[   2, 2 x x x]]
            -- [[      1 x x x]]
            --
            -- If diag[onal] == 0 then it is single matrix
            diag = foldl(*) 1 [head x | x <- mt]
            ar = zipWith(\x y -> (replicate x 0) ++ y) [0..] mt
            -- ar =
            -- [[1 2 3 x x x]
            --  [0 2 2 x x x]
            --  [0 0 1 x x x]]
            pm = map(\x -> partList (length ar) x ) ar
            -- pm =
            -- [[[1 2 3] [x x x]]
            --  [[0 1 2] [x x x]]
            --  [[0 0 1] [x x x]]]
            m1 = map(\r -> head r) pm
            m2 = map(\r -> last r) pm
            -- m1 =
            -- [[1 2 3]
            --  [0 1 2]
            --  [0 0 1]]
            -- m2 =
            -- [[x x x]
            --  [x x x]
            --  [x x x]]
            m11= reverse $ map(\x -> reverse x) m1
            -- m11 =
            -- [[3 2 1]
            --  [2 1 0]
            --  [1 0 0]]
            -- [[1 0 0]
            --  [2 1 0]
            --  [3 2 1]]
            m22= reverse $ map(\x -> reverse x) m2
            -- m22 =
            -- [[x x x]
            --  [x x x]
            --  [x x x]]

            m3 = zipWith(\x y -> x ++ y) m11 m22
            m4 = upperTri' m3
            --m4'= map(\r -> map(\x -> divI x   $ toInteger (head r))
            -- Fri Dec 14 16:04:32 2018
            -- remove the division here
            m4'= map(\r -> map(\x -> x / (head r)) r) m4
            -- Not full inverse matrix here
            mm'= zipWith(\x y -> (replicate x 0) ++ y) [0..] m4'
            mm = map(\x -> partList (length mm') x) mm'
            m1'= map(\x -> head x) mm
            m2'= map(\x -> last x) mm
            ma'= map(\x -> reverse x) $ reverse m1'
            mb'= map(\x -> reverse x) $ reverse m2'







{-|
  === Inverse of two dimension matrix
  <http://localhost/pdf/inverse_matrix2.pdf Inverse_matrix_determinant>

    \[
        \begin{equation}
        \begin{aligned}
        A &= \begin{bmatrix}
            a & b \\
            c & d \\
            \end{bmatrix} \\
        A^{ -1} &= \frac{1}{ \det A }
                \begin{bmatrix}
                 d & -b \\
                 -c & a \\
                 \end{bmatrix}
        \end{aligned}
        \end{equation}
    \]
    * inverse should be used in general.

    * Or 'isInver' should be used for large matrix because 'isInver' is implemented
      in <http://localhost/pdf/gram_schmidt.pdf QR_Decompoisition>

    @
    isInver::(Fractional a, Ord a)=> [[a]] -> Bool
    isInver m = if len (filter(< 0.0001) cx) > 0 then False else True
    @

    * Following function is implemented in Gaussian Elimination

    * There is some Integer overflow issue, it only works for small matrix, e.g. 10 by 10

    @
    isInvertible::[[Integer]]->Bool
    @
-}
inv2::(Fractional a)=>[[a]] -> [[a]]
inv2 [[a, b], [c, d]] = (1/det2([[a, b], [c, d]])) *: [[d, (-b)], [(-c), a]]
    where
        -- scalar multiply 2x2 matrix
        (*:)::(Num a)=>a -> [[a]] -> [[a]]
        (*:) a cx = (map . map) (*a) cx
        dt = det2([[a, b], [c, d]])

data SegColinear = Colinear3 -- ^ If three pts are colinear => Colinear3
                   | Colinear4 -- ^ If four pts are colinear => Colinear4
                   | None deriving (Eq, Show)
{-|
    === If four points are colinear then return 'Colinear4'
    === If only three points are colinear then return 'Colinear3'
    === Else return 'None'

    >data SegColinear = Colinear3 -- ^ If three pts are colinear => Colinear3
    >                   | Colinear4 -- ^ If four pts are colinear => Colinear4
    >                   | None deriving (Eq, Show) -- ^ else => None

    The function uses 'isColinear'
-}
fourPtColinear::(Vertex3 GLfloat, Vertex3 GLfloat) -> (Vertex3 GLfloat, Vertex3 GLfloat) -> SegColinear
fourPtColinear  (p0, p1) (q0, q1) = if (is0 && is1) then Colinear4 else
                                            if (is0 || is1) || (is0' || is1') then Colinear3 else None
           where
                is0 = isColinear p0 q0 q1
                is1 = isColinear p1 q0 q1
                is0'= isColinear q0 p0 p1
                is1'= isColinear q1 p0 p1


{-|
    === Find the intersection of two lines, also see 'intersectSeg'
    ==== Assume that two endpoints of each segment are not overlapped.(segment has non-zero length)

    __NOTE__ This function is ONLY for two dimensions

    __Line__ extends infinitly in both direction


    * If both line intersect at one pt => return the pt
        1. Intersection can be on a segments or NOT on a segment 
    * If four points are colinear: return Nothing, see 'fourPtColinear'

    >let p0 = Vertex3 0 0 0
    >let p1 = Vertex3 1 0 0
    >let q0 = Vertex3 1 0 0
    >let q1 = Vertex3 2 0 0
    >intersectLine p0 p1 q0 q1
    >Nothing

    * Two dimensions determinant is used here

    * The Intersection of two line is __NOT__ necessary in their segments
    * If two lines are __parallel__ or __overlapped__ then return Nothing
    * Else return the intersection and \( s, t \)

    \[
        \text{Given four pts: \(p_0, p_1\) and \(q_0, q_1\)} \\
        \begin{aligned}
        v_0 &= p_1 - p_0 \quad v_0 \text{ is a vector} \\
        v_1 &= q_1 - q_0 \quad v_1 \text{ is a vector} \\
        A &= \begin{bmatrix}
             v_0 & v_1
             \end{bmatrix} \\
        \det A &= 0  \quad \text{they are linearly dependent }
        \end{aligned}
    \]

    <http://localhost/pdf/check_line_intersection.pdf Check Line Intersection>
    <http://localhost/pdf/intersectionLine.pdf intersect_line_PDF>

    @
    data Seg a = Seg a a
    fun::Seg GLflat -> Seg GLfloat -> Maybe(Vertex3 GLfloat, [[GLfloat]])
    fun (Seg (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1)) = Nothing
    @
    @
    intersectLine p0 p1 q0 q1
    (Just ((Vertex3 2.0 2.0 0), [[2.0],[2.0]]))= intersectLine
                                                (Vertex3 0 0 0)
                                                (Vertex3 1 1 0)
                                                (Vertex3 2 0 0)
                                                (Vertex3 2 1 0)
    @
-}
intersectLine::Vertex3 GLfloat ->
               Vertex3 GLfloat ->
               Vertex3 GLfloat ->
               Vertex3 GLfloat ->
               Maybe (Vertex3 GLfloat, [[GLfloat]])
intersectLine p0@(Vertex3 x0 y0 z0)
              p1@(Vertex3 x1 y1 z1)
              q0@(Vertex3 a0 b0 c0)
              q1@(Vertex3 a1 b1 c1)
              = if d == 0 || c4 == Colinear4 then Nothing else (Just (pt, st))
              where
--                is0 = isColinear p0 q0 q1
--                is1 = isColinear p1 q0 q1
--
                -- if c4 == Colinear4 then four pts are colinear
                c4 = fourPtColinear (p0, p1) (q0, q1)
                w1   = v2a $ p1 - p0  -- [[1, 2]]
                w2   = v2a $ q1 - q0  -- [[4, 5]]
                -- if d == 0 then two lines are parallel
                d    = det2 $ w1 ++ w2 -- det2 [[1, 2], [4, 5]]
                v01 = p1 -: p0 -- p₁ - p₀  f(t) = p₀ + s(p₁ - p₀)
                u01 = q1 -: q0 -- q₁ - q₀  f(s) = q₀ + t(q₁ - q₀)
                ma = [[ne (x1 - x0), (a1 - a0)],  -- [s]
                      [ne (y1 - y0), (b1 - b0)]]  -- [t]
                ivm= inv2 ma
                v  = [[x0 - a0],   -- p₀ - q₀
                      [y0 - b0]]
                -- solve s and t
                -- [s]
                -- [t]
                st = multiMat ivm v
                -- |  st = ivm *. v
                s  = (head . head) st -- st = [[s],[t]]
                -- t  = (last . last) st
                pt = p0 +: (s *: v01)
                ne = negate
                (*.) [[a, b], [c, d]] [[x], [y]]  = [[a*x + b*y], [c*x + d*y]]

                v2a (Vertex3 x y z) = [[x, y]]

{-| 
    === If two line parallel or four pts colinear => return Nothing
    === Else there is intersection pt, pt maybe on on segment or NOT on segment

    Sun Feb 17 15:32:59 2019 
    This function should replace 'intersectLine'
-} 
intersectLine2::Vertex3 GLfloat ->
                Vertex3 GLfloat ->
                Vertex3 GLfloat ->
                Vertex3 GLfloat ->
                Maybe (Vertex3 GLfloat, (GLfloat, GLfloat))
intersectLine2 p0@(Vertex3 x0 y0 z0)
               p1@(Vertex3 x1 y1 z1)
               q0@(Vertex3 a0 b0 c0)
               q1@(Vertex3 a1 b1 c1) 
                 | d == 0 || c4 == Colinear4  = Nothing 
                 | overLappedMaybe /= Nothing = overLappedMaybe
                 | otherwise                  = Just (pt, (s, t)) 
--                 | otherwise                  = (Just (pt, st)) 
                    where
                        -- if c4 == Colinear4 then four pts are colinear
                        c4 = fourPtColinear (p0, p1) (q0, q1)
                        w1   = v2a $ p1 - p0  -- [[1, 2]]
                        w2   = v2a $ q1 - q0  -- [[4, 5]]
                        -- if d == 0 then two lines are parallel
                        d    = det2 $ w1 ++ w2 -- det2 [[1, 2], [4, 5]]
                        v01 = p1 -: p0 -- p₁ - p₀  f(s) = p₀ + s(p₁ - p₀)
                        u01 = q1 -: q0 -- q₁ - q₀  f(t) = q₀ + t(q₁ - q₀)
                        ma = [[ne (x1 - x0), (a1 - a0)],  -- [s]
                              [ne (y1 - y0), (b1 - b0)]]  -- [t]
                        ivm= inv2 ma
                        v  = [[x0 - a0],   -- p₀ - q₀
                              [y0 - b0]]

                        -- If two segments are overlapped ONE endPt
                        overLappedMaybe = onePtOverlappedSeg (p0, p1) (q0, q1)
                        -- solve s and t
                        -- [s]
                        -- [t]
                        st = multiMat ivm v
                        -- |  st = ivm *. v
                        s  = (head . head) st -- st = [[s],[t]]
                        t  = (last . last) st -- st = [[s],[t]]
                        pt = p0 +: (s *: v01)
                        ne = negate
                        (*.) [[a, b], [c, d]] [[x], [y]]  = [[a*x + b*y], [c*x + d*y]]

                        v2a (Vertex3 x y z) = [[x, y]]


{-| 
    === Given two segments: \( (p_0, p_1), (q_0, q_1) \), find the overlapped endPt

    == Precondition: Four pts are NOT colinear \( \Rightarrow \) __any three pts__ are NOT colinear

    If two segments are overlapped at one endPt, return Maybe(Vertex3 GLfloat, GLfloat s, GLfloat t)
    else return Nothing

    == Four cases
    1. \( p_0 = q_0 \)
    2. \( p_0 = q_1 \)
    3. \( p_1 = q_0 \)
    4. \( p_1 = q_1 \)

    <http://localhost/html/indexConvexHullAlgorithm.html#onept_overlapped_segment onept_overlapped_segment>

-} 
onePtOverlappedSeg::(Vertex3 GLfloat, Vertex3 GLfloat) -> 
                    (Vertex3 GLfloat, Vertex3 GLfloat) -> 
                    Maybe (Vertex3 GLfloat, (GLfloat, GLfloat))
onePtOverlappedSeg (p0, p1) (q0, q1) | p0 == q0 = Just (p0, (0.0, 0.0)) -- (Vertex3, s, t) 
                                     | p0 == q1 = Just (p0, (0.0, 1.0)) -- (Vertex3, s, t) 
                                     | p1 == q0 = Just (p1, (1.0, 0.0)) -- (Vertex3, s, t) 
                                     | p1 == q1 = Just (p1, (1.0, 1.0)) -- (Vertex3, s, t) 
                                     | otherwise = Nothing

-- | Four Vertex3 in a list
--
intersectLine'::[Vertex3 GLfloat] ->
               Maybe (Vertex3 GLfloat, [[GLfloat]])
intersectLine' [p0@(Vertex3 x0 y0 z0)
               ,p1@(Vertex3 x1 y1 z1)
               ,q0@(Vertex3 a0 b0 c0)
               ,q1@(Vertex3 a1 b1 c1)
               ]
              = if is0 && is1 then Nothing else (Just (pt, st))
              where
                is0 = isColinear p0 q0 q1
                is1 = isColinear p1 q0 q1
                v01 = p1 -: p0 -- p₁ - p₀  f(t) = p₀ + s(p₁ - p₀)
                u01 = q1 -: q0 -- q₁ - q₀  f(s) = q₀ + t(q₁ - q₀)
                ma  = [[ne (x1 - x0), (a1 - a0)],  -- [s]
                      [ne (y1 - y0), (b1 - b0)]]  -- [t]
                ivm = inv2 ma
                v   = [[x0 - a0],   -- p₀ - q₀
                      [y0 - b0]]
                -- solve s and t
                -- [s]
                -- [t]
                st = multiMat ivm v
                -- |  st = ivm *. v
                s  = (head . head) st -- st = [[s],[t]]
                pt = p0 +: (s *: v01)
                ne = negate
                (*.) [[a, b], [c, d]] [[x], [y]]  = [[a*x + b*y], [c*x + d*y]]

-- | TODO: fix, the code only handles two dimension.
-- | Given a point: p0, line: q0 q1
-- | Compute the distance from p0 to line: q0 q1
-- |
pointToLine::Vertex3 GLfloat ->
             Vertex3 GLfloat ->
             Vertex3 GLfloat ->
             GLfloat
pointToLine p0 q0 q1 = if is then 0.0 else di
            where
                is = isColinear p0 q0 q1 -- if three pts are colinear, return zero
                nr = perpcw $ (q1 -: q0) -- normal of q0 -> q1
                p1 = p0 +: nr -- p0 +: t*normal where t = 1
                vx = fst $ fromJust $ intersectLine p0 p1 q0 q1  -- compute the insection of two lines

                -- square root can not be represented in Rational number in general,
                -- Convert: Vertex3 Rational => Vertex3 GLfloat
                vx'= fmap (realToFrac) vx
                di = dist p0 vx'


{-|
    === Find the intersection of two segments, 'intersectLine' or 'intersectSegNoEndPt'
    ==== Assume that two endpoints of each segment are not overlapped.(segment has non-zero length)

    * If two EndPts from different segments are over overlapped => then Maybe(Vertex3 endpt)

    __NOTE__ This function is ONLY for two dimensions

    __NOTE__ endpoins of different segments may be coincide

    > intersectSeg (p0, p1) (q0, q1)
    > Nothing
    >
    > intersectSeg (p0, p1) (q0, q1)
    > Vertex3 x y z

    The function is based on 'intersectLine'
    * If two segments are __parallel__ or __overlapped__ then return Nothing

    TODO: add test cases
-}
intersectSeg::(Vertex3 GLfloat, Vertex3 GLfloat)->(Vertex3 GLfloat, Vertex3 GLfloat)->Maybe (Vertex3 GLfloat)
intersectSeg (p0, p1) (q0, q1) = case is of
                                      Just x -> if (s ∈ [0.0, 1.0]) && (t ∈ [0.0, 1.0]) then (Just (fst x)) else Nothing
                                        where s = (head . head) $ snd x
                                              t = (last . last) $ snd x
                                      _      -> Nothing
               where
                is = intersectLine p0 p1 q0 q1

{-|
    === intersection excluding two EndPts, 'intersectLine' or 'intersectSeg'

    Sun Feb 17 19:24:22 2019

    There are some issues in endpts overlapped

    Deprecated, Should use 'intersectSegNoEndPt2'

    If four pts are colinear \( \Rightarrow \) Nothing

-}
intersectSegNoEndPt::(Vertex3 GLfloat, Vertex3 GLfloat)->(Vertex3 GLfloat, Vertex3 GLfloat)->Maybe (Vertex3 GLfloat)
intersectSegNoEndPt (p0, p1) (q0, q1) = case is of
                                      Just x -> if s /= 0.0 && s /= 1.0 && t /= 0.0 && t /= 1.0 && (s ∈ [0.0, 1.0]) && (t ∈ [0.0, 1.0]) then (Just (fst x)) else Nothing
                                        where s = (head . head) $ snd x
                                              t = (last . last) $ snd x
                                      _      -> Nothing
               where
                is = intersectLine p0 p1 q0 q1

{-| 
    === intersection excluding two EndPts, 'intersectLine' or 'intersectSeg'
    Sun Feb 17 19:26:05 2019 

    Fixed some bugs from 'intersectSegNoEndPt'
-} 
intersectSegNoEndPt2::(Vertex3 GLfloat, Vertex3 GLfloat)->(Vertex3 GLfloat, Vertex3 GLfloat)->Maybe (Vertex3 GLfloat)
intersectSegNoEndPt2 (p0, p1) (q0, q1) | overLapped /= Nothing = Nothing
                                       | is /= Nothing = if s ∈ [0.0, 1.0] && (t ∈ [0.0, 1.0]) then mj else Nothing
                                       | otherwise = Nothing
                                          where 
                                                is = intersectLine2 p0 p1 q0 q1
                                                s  = (fst . snd) $ fromJust is 
                                                t  = (snd . snd) $ fromJust is  
                                                mj = Just $ fst $ fromJust is 
                                                overLapped = onePtOverlappedSeg (p0, p1) (q0, q1)

{-|
    === If four pts are colinear return False 
    === If the intersection is within \( s \in [0.0, 1.0], t \in [0.0, 1.0] \) return True
    === Else return False
-}
isIntersectedSeg::(Vertex3 GLfloat, Vertex3 GLfloat)->(Vertex3 GLfloat, Vertex3 GLfloat)->Bool
isIntersectedSeg (p0, p1) (q0, q1) = case is of
                                      Just x -> (s ∈ [0.0, 1.0]) && (t ∈ [0.0, 1.0])
                                        where s = (head . head) $ snd x
                                              t = (last . last) $ snd x
                                      _      -> False
               where
                is = intersectLine p0 p1 q0 q1

{-|
    === intersect line, return Rational
-}
intersectLineR::Vertex3 GLfloat ->
                Vertex3 GLfloat ->
                Vertex3 GLfloat ->
                Vertex3 GLfloat ->
                Maybe (Vertex3 Rational, [[Rational]])
intersectLineR p0@(Vertex3 x0 y0 z0)
               p1@(Vertex3 x1 y1 z1)
               q0@(Vertex3 a0 b0 c0)
               q1@(Vertex3 a1 b1 c1)
               = if is0 && is1 then Nothing else (Just (pt, st))
               where
                is0 = isColinear p0 q0 q1
                is1 = isColinear p1 q0 q1
                v01 = fmap (toRational) $ p1 -: p0 -- p₁ - p₀  f(t) = p₀ + s(p₁ - p₀)
                u01 = q1 -: q0 -- q₁ - q₀  f(s) = q₀ + t(q₁ - q₀)
                ma = [[ne (x1 - x0), (a1 - a0)],  -- [s]
                      [ne (y1 - y0), (b1 - b0)]]  -- [t]
                ma'= toR ma
                ivm= invR ma'
                v'  = [[x0 - a0],   -- p₀ - q₀
                      [y0 - b0]]
                v = toR v'
                -- solve s and t
                -- [s]
                -- [t]
                st = ivm *. v
                s  = (head . head) st -- st = [[s],[t]]
                p0' = fmap (toRational) p0
                pt = p0' +> (s **: v01)
                ne = negate
                invR::[[Rational]] -> [[Rational]]
                invR [[a, b], [c, d]] = (1/detR([[a, b], [c, d]])) #: [[d, (-b)], [(-c), a]]

                (*.)::[[Rational]]->[[Rational]] -> [[Rational]]
                (*.) [[a, b], [c, d]] [[x], [y]]  = [[a*x + b*y], [c*x + d*y]]

                detR::[[Rational]] -> Rational
                detR [[a, b], [c, d]] = a*d - b*c
                (#:)::Rational->[[Rational]] -> [[Rational]]
                (#:) r cx = (fmap . fmap) (*r) cx

                (**:)::Rational -> Vector3 Rational -> Vector3 Rational
                (**:) r (Vector3 x y z) = Vector3 (r*x) (r*y) (r*z)

                toR m = (fmap . fmap) (toRational) m

                (+>)::Vertex3 Rational -> Vector3 Rational -> Vertex3 Rational
                (+>) (Vertex3 a b c) (Vector3 x y z) = Vertex3 (a + x) (b + y) (c + z)

{-|
 === Three points rotation order can be determinated by the \( \color{red}{\text{Right Hand Rule}} \)
 <http://localhost/html/indexConvexHullAlgorithm.html#rotate_dir Three_Points_Direction>

  If given three points in following order:

  p₀ = (1, 0)

  p₁ = (0, 0)

  p₂ = (1, 0)

  then vectors are computed in following:

  v10 = p₀ - p₁

  v12 = p₂ - p₁

 matrix can be formed as following:

 m = [v10, v12]

 \( \vec{v_{01}} \times \vec{v_{02}} \) in Right Hand Rule

 \[
  m = \begin{bmatrix}
      1 & 0 \\
      0 & 1 \\
      \end{bmatrix} \\
  \det m = 1 > 0 \\
 \]

 If the three points are collinear:

 \( \det M = 0 \)

 If the order of three points in clockwise order:

 \( \det M > 0 \)

 If the order of three points in counter clockwise order:

 \( \det M < 0 \)

-}
threePtDeterminant::(Fractional a)=>(Vertex3 a) ->(Vertex3 a)->(Vertex3 a)->a
threePtDeterminant p0 p1 p2 = det m
  where
    v10 = v2m $ p0 -: p1
    v12 = v2m $ p2 -: p1
    v2m (Vector3 x y z) = [x, y]
    m = [v10, v12]

{-|
 === Three points in Counter ClockWise order
 \(  \det M > 0 \)
-}
threePtCCW::(Fractional a, Ord a)=>(Vertex3 a)->(Vertex3 a)->(Vertex3 a)->Bool
threePtCCW p0 p1 p2 = threePtDeterminant p0 p1 p2 < 0

{-|
 === Three points in ClockWise order
 \( \det M < 0 \)
-}
threePtCW::(Fractional a, Ord a)=>(Vertex3 a)->(Vertex3 a)->(Vertex3 a)->Bool
threePtCW p0 p1 p2 = threePtDeterminant p0 p1 p2 > 0

{-|
    === Three points are collinear

\[
    \begin{aligned}
    p_0 &= (x_0, y_0) \\
    p_1 &= (x_1, y_1) \\
    p_2 &= (x_2, y_2) \\
    u   &= p_0 - p_1 \\
    u   &= (x_0 - x_1, y_0 - y_1) \\
    v   &= p_2 - p_1 \\
    v   &= (x_2 - x_1, y_2 - y_1) \\
    M   &= \begin{bmatrix}
           u & v
           \end{bmatrix} \\
    &\mbox{If three pts are colinear, then } \\
    \det M &= 0 \\
    \end{aligned}
\]
-}
threePtCollinear::(Fractional a, Ord a)=>(Vertex3 a)->(Vertex3 a)->(Vertex3 a)->Bool
threePtCollinear p0 p1 p2 = threePtDeterminant p0 p1 p2 == 0.0

{-|
    Given a point \( p_0 \), a line \(q_0, q_2 \)
    === Check whether a point \(p_0\) is on the left size of a line \( q_0, q_2 \), it is based on 'threePtDeterminant' with additional condtion: \(y_2 > y_0 \) or \( y_2 < y_0 \) since

    __NOTE__ y2 \(\neq\) y0 where given (Vertex3 x0 y0 z0) and (Vertex3 x2 y2 z2)

    ==== Assume \( p_0, p_2 \) are not __coincide__

    * If pt on the line, return False
    * If pt on the right side of the line, return False
    * Else return True

    \[
        \begin{aligned}
        p_1 &= (x_1, y_1) \\
        p_0 &= (x_0, y_0) \\
        p_2 &= (x_2, y_2) \\
        u &= \overrightarrow{p1 p0}  \\
        v &= \overrightarrow{p1 p2}  \\
        \end{aligned}
    \]

    if \( y_2 > y_0 \)
    form a determinant
    \[
        \begin{aligned}
         \det \begin{vmatrix}
                u & v
                \end{vmatrix} > 0
        \end{aligned}
    \]
    if \( y_2 < y_0 \)
    form a determinant
    \[
        \begin{aligned}
         \det \begin{vmatrix}
                u & v
                \end{vmatrix} < 0
        \end{aligned}
    \]

    If \( y_2 = y_0 \), compare \( x_0, x_2 \)

    === Check whether a point is below a line \(p_0, p_2\)
    if \( x_2 < x_0 \)
    \[
        \begin{aligned}
         \det \begin{vmatrix}
                u & v
                \end{vmatrix} > 0
        \end{aligned}
    \]

    if \( x_2 > x_0 \)
    \[
        \begin{aligned}
         \det \begin{vmatrix}
                u & v
                \end{vmatrix} < 0
        \end{aligned}
    \]


-}
ptOnLeftLine::(Fractional a, Ord a)=>(Vertex3 a) ->(Vertex3 a)->(Vertex3 a) -> Bool
ptOnLeftLine p0@(Vertex3 x0 y0 z0)
             p1@(Vertex3 x1 y1 z1)
             p2@(Vertex3 x2 y2 z2) = if y2 > y0 then (de > 0) == True
                                        else if y2 < y0 then (de > 0) == False else (de > 0) == False
             where
                de = threePtDeterminant p0 p1 p2

--ptOnUpLine::(Fractional a, Ord a)=>(Vertex3 a) ->(Vertex3 a)->(Vertex3 a) -> Bool
--

{-|
  === Check whether a point \(p_0\) is inside a triangle \( \triangle ABC \)

  * __NOTE__ the order of three pts: \(A, B, C\) does't matter. e.g CCW or CW
  * \(p_0\) can NOT be the same point as \(A, B, C\)
  * If \(p_0\) is collinear with AB, BC, or AC, then \(p_0\) is considered inside the \( \triangle ABC \)

  \(p_0\) is the point that is tested
  three points \(A, B, C\) forms a triangle

  if point p0 inside the triangle, return true

  The sum of three angles are in degree.

  >let p0 = Vertex3 0.1 0.1 0
  >let a  = Vertex3 1 0 0
  >let b  = Vertex3 0 0 0
  >let c  = Vertex3 0 1 0
  >
  >ptInsideTri p0 a b c
  >(True, 360.0)

  TODO: how to check whether a pt is inside a n-polygon 'ptInsidePolygon'
  <http://localhost/html/indexConvexHullAlgorithm.html#npolygon N-Polygon>

-}
ptInsideTri::Vertex3 GLfloat -> (Vertex3 GLfloat, Vertex3 GLfloat, Vertex3 GLfloat) -> (Bool, GLfloat)
ptInsideTri p0 (a, b, c) = (is, degree)
                           where
                             degree = dePerRad*rad
                             rad= acos (cosVex3 b p0 a) + acos (cosVex3 c p0 b) + acos (cosVex3 c p0 a)
                             is = abs(rad - 2*pi) < 0.001
                             dePerRad = 360/(2*pi)
{-|
Given a point p0 and three pts: q0, q1, q2,

Compute the intersection of line is perpendicular to the plane and passes point p0

If three pts (q0, q1, q2) are colinear, return Nothing

otherwise, return Just (Vertex3 GLfloat)
-}
perpPlane::(Vertex3 GLfloat) ->
           (Vertex3 GLfloat) ->
           (Vertex3 GLfloat) ->
           (Vertex3 GLfloat) ->
           Maybe (Vertex3 Rational)
perpPlane p0@(Vertex3 e0 e1 e2)
          q0@(Vertex3 m0 m1 m2)
          q1@(Vertex3 k0 k1 k2)
          q2@(Vertex3 d0 d1 d2)
          = if is then Nothing else Just pt
        where
            is = isColinear q0 q1 q2
            nr = fromJust $ normal3 q0 q1 q2
            v1 = q1 -: q0
            v2 = q2 -: q0
            qp = p0 -: q0
            vqp = (map . map) (toRational) $ veMat qp
            mat = zipWith3(\x y z -> x ++ y ++ z) (veMat v1) (veMat v2) (veMat $ neg nr)
            mat' = (map . map) (toRational) mat
            ima = inverseR mat'
            sth = multiMatR ima vqp -- sth =  [[s], [t], [h]]
            h  = (last . last) sth
            pt  = p0' `ad` (h `mu` nr') where
                                        p0' = toR p0 -- toRational
                                        mu x y = fmap(*x) y  -- t*v
                                        nr' = toR nr  -- toRational
                                        -- pt = p0 + v in affine space:)
                                        ad (Vertex3 x y z) (Vector3 a b c) = Vertex3 (x + a) (y + b) (z + c)

            -- | local functions
            veMat (Vector3 x y z) = [[x], [y], [z]]
            matVx [[x], [y], [z]] = (Vertex3 x y z)
            neg x = fmap (negate) x
            toR x = fmap (toRational) x

-- | Line intersects Plane
-- | Given Line: p0 p1 and Plane: q0 q1 q2
-- | Compute the intersection of line and plane
-- |
--  p1 = p0 +: h(p1 -: p0)
lineIntersectPlane::Vertex3 GLfloat ->
                    Vector3 GLfloat ->
                    Vertex3 GLfloat ->
                    Vertex3 GLfloat ->
                    Vertex3 GLfloat ->
                    Maybe (Vertex3 Rational)
lineIntersectPlane p0 ve q0 q1 q2 = if is then Nothing else Just pt
                where
                    is = isColinear q0 q1 q2
                    v1 = q1 -: q0 -- q0 -> q1
                    v2 = q2 -: q0 -- q0 -> q2
                    nr = ve
                    qp = p0 -: q0
                    vqp = (map . map) (toRational) $ veMat qp
                    mat = zipWith3(\x y z -> x ++ y ++ z) (veMat v1) (veMat v2) (veMat $ neg nr)
                    mat' = (map . map) (toRational) mat
                    ima = inverseR mat'
                    sth = multiMatR ima vqp -- sth =  [[s], [t], [h]]
                    h  = (last . last) sth
                    pt  = p0' `ad` (h `mu` nr') where
                                                p0' = toR p0 -- toRational
                                                mu x y = fmap(*x) y  -- t*v
                                                nr' = toR nr  -- toRational
                                                -- pt = p0 + v in affine space:)
                                                ad (Vertex3 x y z) (Vector3 a b c) = Vertex3 (x + a) (y + b) (z + c)

                    -- | local functions
                    veMat (Vector3 x y z) = [[x], [y], [z]]
                    matVx [[x], [y], [z]] = (Vertex3 x y z)
                    neg x = fmap (negate) x
                    toR x = fmap (toRational) x


{-|
 === Slow Algorithm 1 \( \color{red}{ \mathcal{O}(n^2)} \)
 1. find the a point \( p_0 \) which has the maximum y-axis value
 2. find the a point \( p' \) from \( p_0 \) so that the rest of all the points on one side of the segment \( \overline{p_0 p'} \)
 3. continua with the rest of points

 <http://localhost/html/indexConvexHullAlgorithm.html Slow_Algorithm>

 >p0 p1 p2 p3
 >(p0, p1) (p0, p2) (p0, p3)
 >(p1, p2) (p1, p3)
 >(p2, p3)

 === Naive Algorithm to compute the convex hull in \( \color{red}{\mathcal{O}(nh)} \) where \( \color{red}{n} \) is the number of points on the plane, \( \color{red}{h} \) is the number of points on the convex hull

 If all the points are around the convex hull, then the runtime will be \( \color{red}{\mathcal{O}(n^2)} \)

 @
 convexHull n pts
 n is the total of vertices
 pts is length pts

 p0 = Vertex3 0 0 0
 p1 = Vertex3 1 0 0
 p2 = Vertex3 0 1 0
 p3 = Vertex3 0.1 0.1 0
 pts= [p0, p1, p2, p3]
 n  = 4 = len pts
 pts = [p0, p1, p2, p3]
 exp= [[p2, p1],
       [p1, p0],
       [p0, p2]]
 @

 * __Algorithm__
 <http://localhost/html/indexConvexHullAlgorithm.html#convexhull ConvexHull>
 * Find a __top__ vertex: B(Vertex3 x y z) with maximum y-Axis value
 * Construct a new vertex: A(Vertex3 x+1 y z) = __top__(Vertex3 x + 1 y z) according to __top__(Vertex3 x y z)
 * Compute the angle \( \cos ∠ABC \) from three points: \(A, B, C\)
 * If there are only two points in pts for the list of Vertex3 e.g. p0 p1
 * then segments: \(\overline{AB} \) and \(\overline{BC} \) are drawn
 <http://localhost/html/indexConvexHullAlgorithm.html#three_pts_angle Three_pts_angle>

 n = length pts \(\Rightarrow\) n vertices \(\Rightarrow\) n edges. if there is two vertices, then \((p_0, p_1)\) and \((p_1, p_0)\)

 vec(top x1) and vec(top x2)

 \(\vec{v_1}\) = top -> x1 = x1 -: top

 \(\vec{v_2}\) = top -> x2 = x2 -: top

 * TODO: Handle the case where the three points are colinear.

 * TODO: If there are more than one point that have maximum y-axis, then choose the left most point
 <http://localhost/html/indexConvexHullAlgorithm.html#top_point Top_Point>

 \( \color{red}{TODO} \): use eliminate points technic to improve the algorithm
 <http://localhost/html/indexConvexHullAlgorithm.html#eliminate_points Eliminate_Points>

 It seems that the algorithm is still \( \color{red}{ \mathcal{O}(nh)} \)
 Given n vertices on a plane. There are \(h\) points on the convex hull.
 Assume \( \frac{1}{h} n \) vertices may be eliminated when the next point is found on the convex hull.
 The total number of steps are:
 \[
 \begin{aligned}
    s &= n + \frac{h-1}{h} n + \frac{h-2}{h} n + \dots + \frac{1}{h} n \\
    s &= n h (1 + 2 + 3 + \dots + h) \\
    s &= n h \frac{(1 + h)h}{2} \\
    s &= n \frac{1+ h}{2} \\
      &= \mathcal{O}(nh) \\
 \end{aligned}
 \]



 When the algo walks around the convex hull in counter clokcwise order.

 segment can be drawn from top point to previous point

 All the points can be removed if they are on the left side of the segment

 \( \color{red}{TODO} \) use other algo: check whether a point is on the left or right side of a segment

 The algo can be check with the determinant of two vectors 'threePtDeterminant'
 <http://localhost/html/indexConvexHullAlgorithm.html#rotate_dir Three_Points_Direction>

-}
convexHull::Int->
        [Vertex3 GLfloat] -> [[Vertex3 GLfloat]]
convexHull n pts = convexHull' n xi topx top pts
    where
        t1 (a, b, c) = a
        t2 (a, b, c) = b
        t3 (a, b, c) = c
        -- find the vertex that has maximum y value.
        ptsSort = qqsort cmp pts where cmp (Vertex3 x1 y1 z1) (Vertex3 x2 y2 z2) = y1 > y2

        -- find the largest angle
        -- angle: p0_p1_p2, vec: (p1, p2) and vec: (p1, p0)
        vexSort = mergeSortC cmp $ map(\p2 -> (cosVex3 p0 p1 p2, p1, p2)) cx  -- p1 to all the rest of pts
                        where
                            p1 = head ptsSort
                            p0 = addx1 p1 where addx1 (Vertex3 x y z) = Vertex3 (x+1) y z
                            cx = tail ptsSort
                            cmp c1 c2  = t1 c1 > t1 c2

        top = let t2 (a, b, c) = b in t2 $ head' vexSort
        topx = addx1 top where addx1 (Vertex3 x y z) = Vertex3 (x + 1) y z
        xi = top
        convexHull' n xi topx top ptsSort = if (n > 0) && xi /= x1 then (convexHull' (n - 1) xi top x1 ptsSort) ++ [[x1, top]] else [[x1, top]]
                            where
                                -- use merge sort here for y andthen x
                                -- choose the left most vertex
                                -- find the biggest dot product of vec(top x1) vec(top x2) => the smallest angle
                                cx = filter(\x -> x /= top) ptsSort -- remove top pt
                                vexSort = qqsort cmp $ map(\p -> (cosVex3 topx top p, top, p)) $ cx
                                cmp c1 c2  = t1 c1 < t1 c2 -- the largest angle for the smallest cos(angle)
                                x1 = t3 $ head vexSort


{-| 
    === Same as 'convexHull', except that it return [(Vertex3 GLfloat, Vertex3 GLfloat)]
-} 
convexHull2::Int->
        [Vertex3 GLfloat] -> [(Vertex3 GLfloat, Vertex3 GLfloat)]
convexHull2 n pts = convexHull' n xi topx top pts
    where
        t1 (a, b, c) = a
        t2 (a, b, c) = b
        t3 (a, b, c) = c
        -- find the vertex that has maximum y value.
        ptsSort = qqsort cmp pts where cmp (Vertex3 x1 y1 z1) (Vertex3 x2 y2 z2) = y1 > y2

        -- find the largest angle
        -- angle: p0_p1_p2, vec: (p1, p2) and vec: (p1, p0)
        vexSort = mergeSortC cmp $ map(\p2 -> (cosVex3 p0 p1 p2, p1, p2)) cx  -- p1 to all the rest of pts
                        where
                            p1 = head ptsSort
                            p0 = addx1 p1 where addx1 (Vertex3 x y z) = Vertex3 (x+1) y z
                            cx = tail ptsSort
                            cmp c1 c2  = t1 c1 > t1 c2

        top = let t2 (a, b, c) = b in t2 $ head' vexSort
        topx = addx1 top where addx1 (Vertex3 x y z) = Vertex3 (x + 1) y z
        xi = top
        convexHull' n xi topx top ptsSort = if (n > 0) && xi /= x1 then (convexHull' (n - 1) xi top x1 ptsSort) ++ [(x1, top)] else [(x1, top)]
                            where
                                -- use merge sort here for y andthen x
                                -- choose the left most vertex
                                -- find the biggest dot product of vec(top x1) vec(top x2) => the smallest angle
                                cx = filter(\x -> x /= top) ptsSort -- remove top pt
                                vexSort = qqsort cmp $ map(\p -> (cosVex3 topx top p, top, p)) $ cx
                                cmp c1 c2  = t1 c1 < t1 c2 -- the largest angle for the smallest cos(angle)
                                x1 = t3 $ head vexSort


{-| 
    === Connect all pts inside convex hull without crossing segments

    <http://localhost/html/indexConvexHullAlgorithm.html#convexhull_all_segments All_Segments_ConvexHull>

    @
    let vexList = [Vertex3 0.1 0.1 0.1, Vertex3 0.4 0.3 0.6, Vertex3 0.6 0.8 0.2]
    mapM_(\vx -> do 
                drawSegment green vx 
                threadDelay 500
                ) $ convexHullAllSeg vexList
    @
-} 
convexHullAllSeg::[Vertex3 GLfloat] -> [(Vertex3 GLfloat, Vertex3 GLfloat)]
convexHullAllSeg cx = segList hullList diffList
    where
        -- all the pts are on convex hull
        hullList = convexHull2 le cx where le = len cx 
        -- all the pts are NOT on the convex hull
        diffList = (L.\\) cx $ map fst hullList 

        -- connect all segments inside convex hull
        segList::[(Vertex3 GLfloat, Vertex3 GLfloat)] -> [Vertex3 GLfloat] -> [(Vertex3 GLfloat, Vertex3 GLfloat)]
        segList sl []     = sl
        segList sl (p:cx) = segList ((nonCrossSegmentNoEndPt sl p) ++ sl) cx

{-|
    === Check whether a pt is inside a polygon (assume the polygon is convex)
    === This is essentially a Covex Hull problem => use Convex Hull Algorithm

    TODO: Add test cases, never test it yet
-}
ptInsidePolygon::Vertex3 GLfloat -> [Vertex3 GLfloat] -> Bool
ptInsidePolygon p0 cx = not $ containPt p0 cxx
    where
        cx' = cx ++ [p0]
        sz = len cx'
        cxx = map(\x -> head x) $ convexHull sz cx'

{-|
    === Given a \(p_0\): Vertex3 and [Vertex3], check whether the list contain p0
-}
containPt::Vertex3 GLfloat -> [Vertex3 GLfloat] -> Bool
containPt p0 cx = S.member p0 s
    where
        s = S.fromList cx
{-|
    === Projection from \(u\) onto \(v\) in Vector3
    <http://localhost/pdf/projectionlatex.pdf projection>
-}
projv::(Vector3 GLfloat)->(Vector3 GLfloat)->(Vector3 GLfloat)
projv u v = w'
  where
    u' = veMat u
    v' = veMat v
    w  = projn u' v'
    w' = matVe w

{-|
   === Compute the norm of a vector
   \(v = (x, y, z)\)

   \(|v| = \sqrt{ x^2 + y^2 + z^2} \)
-}
nr::(Vector3 GLfloat)-> GLfloat
nr v = sqrt $ dot3ve v v

{-|
  === Normalize a vector
  e.g \( \|\vec{v}\| = 1 \)
-}
uv::(Vector3 GLfloat)->(Vector3 GLfloat)
uv v = fmap (/n) v
    where
        n = nr v
{-|
    === Rodrigue formula

    \(u\) rotates around \(v\) in angle \(\phi\) in right hand rule

    rejection = \(u - p\)

    gx <http://localhost/pdf/rotate_arbitrary_axis.pdf Rotation>
-}
rod u v θ = tv
  where
    p = projv u v
    w = u ⊗ v -- right hand rule
    re= u - p
    u'= ((cos θ) *: re) + (((sin θ) * (nr re)) *: (uv w))
    tv= p + u'

{-|
    === Compute an angle from three points: \(a, b, c \) with dot product
    >let a = Vertex3 1 0 0
    >let b = Vertex3 0 0 0
    >let c = Vertex3 1 0 0
    >cosVex3 a b c
    >0.0

    \( \vec{ba} = a - b \)

    \( \vec{bc} = c - b \)

    \[
        \begin{equation}
        \begin{aligned}
            \vec{ba} \circ \vec{bc} &= | \vec{ba} | | \vec{bc} | \cos{\angle ABC}  \\
            \cos{\angle ABC} &= \frac{ \vec{bc} \circ \vec{bc} }{| \vec{ba} | | \vec{bc}|} \\
        \end{aligned}
        \end{equation}
    \]

-}
cosVex3::Vertex3 GLfloat -> Vertex3 GLfloat -> Vertex3 GLfloat -> GLfloat
cosVex3 p0 p1 p2 = d/(n1*n2)
                    where
                        v10 = p0 -: p1
                        v12 = p2 -: p1
                        d = dot3ve v10 v12
                        n1 = nr v10
                        n2 = nr v12

{-|
    === Find all the segments DO NOT Cross some new segments
    Current algorithm is brute force

    __NOTE__ use __Convex Hull algo__ might be faster

    __NOTE__ EnPts are excluded, please see 'intersectSegNoEndPt'

    * Given a list of segments and a point

    <http://localhost/html/indexConvexHullAlgorithm.html#non_cross_segment non_cross_segment>

    <http://localhost/html/indexConvexHullAlgorithm.html#non_cross_segment_2 non_cross_segment_2>

    Given Segments \( [(B, E), (E, A), (A, D), (D, C)] \) pt: \(F\)

    return => \( [(A, F), (F, D), (F, C)] \)

    @
    let p0 = Vertex3 0 0 0
        p1 = Vertex3 0.5 0 0
        p2 = Vertex3 0 0.5 0
        q0 = Vertex3 1 1 0
        ls = [(p0, p1), (p0, p2)]
        exp= sort [(p0, q0), (p1, q0), (p2, q0)]
        in exp == (sort $ nonCrossSegmentNoEndPt ls q0)

    let p0 = Vertex3 0 0 0
        p1 = Vertex3 0.5 0 0
        p2 = Vertex3 0 0.5 0
        q0 = Vertex3 1 1 0
        ls = [(p0, p1), (p1, p2)]
        exp= sort [(p1, q0), (p2, q0)]
        in exp == (sort $ nonCrossSegmentNoEndPt ls q0)
    @

    remove all the segments with same vertex from old segments list
    then check all the new segments with the rest of old segments list

-}
nonCrossSegmentNoEndPt::[(Vertex3 GLfloat, Vertex3 GLfloat)] ->
                        Vertex3 GLfloat ->
                        [(Vertex3 GLfloat, Vertex3 GLfloat)]
nonCrossSegmentNoEndPt cx p = S.toList $ delSeg (S.fromList newSeg) badSeg
        where
            -- Cartesian product
            -- checkInter oldSeg newSeg = join $ map(\x -> map(\y -> (intersectSegNoEndPt x y) /= Nothing) oldSeg) newSeg
            badSeg = map(\x -> snd x) $ filter(\x -> fst x == True) $ checkInter oldSeg newSeg

            delSeg::Set (Vertex3 GLfloat, Vertex3 GLfloat) ->
                    [(Vertex3 GLfloat, Vertex3 GLfloat)] ->
                    Set (Vertex3 GLfloat, Vertex3 GLfloat)
            delSeg s []     = s
            delSeg s (b:bx) = delSeg (g s b) bx
                where
                    g s b = delete b s


            frm::[(Vertex3 GLfloat, Vertex3 GLfloat)] ->
                 [(Vertex3 GLfloat, Vertex3 GLfloat)] ->
                 [(Vertex3 GLfloat, Vertex3 GLfloat)]
            frm cx [] = cx
            frm cx (b:bx) = frm (g cx b) bx
                where
                    g cx b = filter(\x -> x /= b) cx

            checkInter oldSeg newSeg = join $ map(\nx -> map(\y -> ((intersectSegNoEndPt2 nx y) /= Nothing, nx)) oldSeg) newSeg
            -- Did not improve performance 
--            checkInter oldSeg newSeg = join $ map(\nx -> 
--                                map(\y -> ((intersectSegNoEndPt2 nx y) /= Nothing, nx)) (rmAdjacentSeg (fst nx) oldSeg)) newSeg
            oldSeg = cx         -- [(p0, p1)]
            newSeg = mkSeg cx p -- [(p0, p), (p1, p)]
            mkSeg cx p = map(\x -> (x, p)) $ fv cx -- [(p0, p1)], p => [(p0, p), (p1, p)]
            fv cx = unique $ join $ map(\x -> [fst x, snd x]) cx -- [(p0, p1), (p0, p2), (p1, p2)] => [p0, p1, p2]

{-| 
    === Remove all adjacent edges or adjacent segments from a list of segments/edges
-} 
rmAdjacentSeg::Vertex3 GLfloat -> 
               [(Vertex3 GLfloat, Vertex3 GLfloat)] -> 
               [(Vertex3 GLfloat, Vertex3 GLfloat)]
rmAdjacentSeg p0 cx = filter(\s -> fst s /= p0 && snd s /= p0) cx


{-| 
    === Affine Combination on three points
    == Draw all the points inside a triangle p0 p1 p2

    The number of steps is \( n = 10 \)

    Extend it to polygon

    <http://localhost/image/affine_triangle.png Affine_Triangle>
-} 
affineTri::Vertex3 GLfloat -> Vertex3 GLfloat -> Vertex3 GLfloat -> [Vertex3 GLfloat]
affineTri p0 p1 p2 = unique $ join $ map(\h -> map(\t -> if t + h <= 1 then mu (1 - h - t) p0 + (mu h p1) + (mu t p2) else p0 ) tt ) hh 
    where
        mu x v = (*x) <$> v

        hh::[Float]
        hh = map(\x ->x*del) [0..n]
            where
                del = 1/n;
                n = 10;
        tt = hh



{-| 
    === Draw all segments from Vertex3 a To Vertex3 b
-} 
drawSegmentFromTo::Color3 GLdouble -> [Vertex3 GLfloat] -> IO()
drawSegmentFromTo c cx = do
                            let n = length cx
                            let pair = join $ zipWith(\x y -> [x, y]) (init cx) (tail cx)
                            mapM_ (\x -> drawCircleColor x red 0.002) pair 
                            drawPrimitive' Lines c pair 
                            let one = head pair
                            let las = last pair
                            drawCircleColor one green 0.005 
                            drawCircleColor las blue 0.014 

{-| 
    === 2d grid on x-y-plane, z=0
    == draw grid

    >mapM_ (\row -> drawSegmentFromTo red row ) grid 
    >mapM_ (\row -> drawSegmentFromTo red row ) $ tran grid 
-} 
grid::[[Vertex3 GLfloat]]
grid =[[ let c = (C a b) in Vertex3 (re c) (im c) 0 | a <- aa] | b <- bb]
        where 
            n  = 10 
            fa = 1/(1.5*n)
            aa = map(\x -> fa * x) [-n..n]
            bb = map(\x -> fa * x) [-n..n]

grid2::(GLfloat -> GLfloat -> GLfloat) -> [[Vertex3 GLfloat]]
grid2 f =[[ Vertex3 x y (f x y) | x <- aa] | y <- bb]
        where 
            n  = 10 
            fa = 1/(1.5*n)
            aa = map(\x -> fa * x) [-n..n]
            bb = map(\x -> fa * x) [-n..n]

{-| 
    === Draw parameter equation.
    x = 2*u    \u -> 2*u        u [1..10]
    y = 3*v    \v -> 3*v        v [3..20]
    z = u + v  \(u, v) -> u + v
-} 
grid3::(GLfloat -> GLfloat -> GLfloat) -> (GLfloat -> GLfloat) -> (GLfloat -> GLfloat) -> [[Vertex3 GLfloat]]
grid3 f u v =[[ Vertex3 x y (f x y) | x <- aa] | y <- bb]
        where 
            n  = 20 
            fa = 1/n
            aa = map(\x -> u x) $ map(*fa) [-n..n]
            bb = map(\x -> v x) $ map(*fa) [-n..n]

drawParamSurf::(GLfloat -> GLfloat -> GLfloat) -> (GLfloat -> GLfloat) -> (GLfloat -> GLfloat) -> IO()
drawParamSurf f u v = do
                mapM_ (\row -> drawSegmentFromTo red row ) $ grid3 f u v
                mapM_ (\row -> drawSegmentFromTo blue row ) $ tran $ grid3 f u v


{-|

torus2::[(GLfloat, GLfloat, GLfloat)]
torus2= [ ( fx i k, 
           fy i k, 
           fz i k ) | i <- [1..n], k <-[1..n]]
        where 
            del = rf(2*pi/(n-1))
            n = 100 
            r = 0.2
            br = 0.3

            fx = \i k -> (br + r**cos(del*i))*cos(del*j)
            fy = \i k -> sin(rf del*i)
            fz = \i k -> (br + r*cos(rf del*i))*sin(rf del*j)

  i = [1..n], j = [1..n]
  x = outer + inner × cos(δ × i) × cos(δ × j)
  y = sin(δ × i)
  z = outer + inner × cos(δ × i) × sin(δ × j)

  Torus equation
  http://localhost/html/indexThebeautyofTorus.html
-}
torus2::[[Vertex3 GLfloat]]
torus2 =[[Vertex3 (fx i j) 
                  (fy i j) 
                  (fz i j) | i <- [1..n]] | j <-[1..n]]
        where 
            δ = rf(2*pi/(n-1))
            n = 40 
            r = 0.1
            br = 0.2

            fx = \i j -> (br + r*cos(δ*i))*cos(δ*j)
            fy = \i j -> sin(rf δ*i)
            fz = \i j -> (br + r*cos(rf δ*i))*sin(rf δ*j)


drawTorus2::IO()
drawTorus2 = do
  mapM_ (\row -> drawSegmentFromTo red row ) torus2
  mapM_ (\row -> drawSegmentFromTo blue row ) $ tran torus2


type Fx = Int -> Int -> GLfloat
type Fy = Int -> Int -> GLfloat
type Fz = Int -> Int -> GLfloat

drawParamSurface::Fx -> Fy -> Fz -> IO ()
drawParamSurface fx fy fz = do
  mapM_ (\row -> drawSegmentFromTo red row) ss
  mapM_ (\row -> drawSegmentFromTo blue row) $ tran ss
  where
    n = 40
    ss = [[Vertex3 (fx i j)
                   (fy i j)
                   (fz i j) | i <- [1..n]] | j <- [1..n]]

drawParamSurfaceN::Fx -> Fy -> Fz -> Int -> IO ()
drawParamSurfaceN fx fy fz n = do
  mapM_ (\row -> drawSegmentFromTo red row) ss
  mapM_ (\row -> drawSegmentFromTo blue row) $ tran ss
  where
    ss = [[Vertex3 (fx i j)
                   (fy i j)
                   (fz i j) | i <- [1..n]] | j <- [1..n]]    

{-| 
    === grid 2d with ratio: 

    >r = 1  => 1/(r*n) => 1/n
    >r = 2  => 1/(2*n) 
-} 
grid2Ratio::(GLfloat -> GLfloat -> GLfloat) -> GLfloat -> [[Vertex3 GLfloat]]
grid2Ratio f r =[[ Vertex3 x y (f x y) | x <- aa] | y <- bb]
        where 
            n  = 10 
            fa = 1/(r*n)
            aa = map(\x -> fa * x) [-n..n]
            bb = map(\x -> fa * x) [-n..n]

{-| 
    === list to 'Vertex3'

    >[1, 2, 3] => (1, 2, 3)
-} 
list3ToVertex3::[a] -> Vertex3 a  -- [1, 2, 3] => (1, 2, 3)
list3ToVertex3 [a, b, c] = Vertex3 a b c
                           
{-| 
    === 'Vertex3' to list

    >(1, 2, 3) => [1, 2, 3]
-} 
vertex3ToList::Vertex3 a -> [a]  -- (1, 2, 3) => [1, 2, 3]
vertex3ToList (Vertex3 a b c) = [a, b, c]
