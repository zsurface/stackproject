module X where
takeIndexBetweenInc::(Int, Int) -> [a] -> [a]
takeIndexBetweenInc (x0, x1) cx = take (x1 - x0 + 1) $ drop x0 cx
