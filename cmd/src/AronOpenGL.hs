module AronOpenGL where

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import qualified Graphics.UI.GLFW as G
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S 

-- import Graphics.UI.GLUT
-- import Graphics.UI.GLUT.Callbacks.Global
import AronModule  hiding (rw)
import AronGraphic hiding (dist)

{-| 
    === Vertex3 List, 25 vertices on x-y-plane only
-} 
vertexList = [
            Vertex3 0.9 0.8 0,
            Vertex3 0.5 0.1 0,
            Vertex3 0.7 0.1 0,
            Vertex3 0.2 0.4 0,
            Vertex3 0.4 0.29 0,

            Vertex3 0.9 0.7 0,
            Vertex3 0.71 0.1 0,
            Vertex3 0.2 0.9 0,
            Vertex3 0.23 0.3 0,
            Vertex3 0.5 0.5 0,

            Vertex3 0.1 0.9 0,
            Vertex3 0.2 0.31 0,
            Vertex3 0.471 0.21 0,
            Vertex3 0.442 0.34 0,
            Vertex3 0.2333 0.6 0,
            
            Vertex3 0.555 0.245 0,
            Vertex3 0.111 0.399 0,
            Vertex3 0.222 0.231 0,
            Vertex3 0.89 0.33 0,
            Vertex3 0.21 0.31 0,

            Vertex3 0.69 0.13 0,
            Vertex3 0.121 0.51 0,
            Vertex3 0.49 0.43 0,
            Vertex3 0.44 0.66 0,
            Vertex3 0.49 0.26 0 
            ]::[Vertex3 GLfloat]

{-| 
    === GLfloat pts
-} 
pts = [0.3, 
      0.1,
      0.4,
      0.2,
      0.32,

      0.19,
      0.21,
      0.39,
      0.19,
      0.09,
      0.29,
      0.239
      ]::[GLfloat]

_STEP = 1.0 
data Cam = Cam{alpha::Double, beta::Double, gramma::Double, dist::Double} deriving(Show)
data Step = Step{xx::Double, yy::Double, zz::Double, ww::Double} deriving(Show)

initCam = Cam{alpha=0.0, beta=0.0, gramma=0.0, dist = 0.0}
initStep = Step{xx=0.0, yy=0.0, zz=0.0, ww = 0.01}



{-| 
    === keyboard call back function

    >data Step = Step{xx::Double, yy::Double, zz::Double, ww::Double} deriving(Show)
-} 
keyBoardCallBack::IORef Step -> G.KeyCallback
keyBoardCallBack ref window key scanCode keyState modKeys = do
    pp "keyBoardCallBack in $b/haskelllib/AronOpenGL.hs"
    putStrLn $ "inside =>" ++ show keyState ++ " " ++ show key
    case keyState of
        G.KeyState'Pressed -> do 
            -- write Step{...} to ref
            if key == G.Key'Right then writeIORef ref Step{xx=_STEP,    yy =0.0,      zz = 0.0,    ww = 0.0}    else return ()
            if key == G.Key'Left  then writeIORef ref Step{xx=(-_STEP), yy =0.0,      zz = 0.0,    ww = 0.0}    else return ()
            if key == G.Key'Up    then writeIORef ref Step{xx=0.0,      yy =_STEP,    zz = 0.0,    ww = 0.0}    else return ()
            if key == G.Key'Down  then writeIORef ref Step{xx=0.0,      yy =(-_STEP), zz = 0.0,    ww = 0.0}    else return ()
            if key == G.Key'9     then writeIORef ref Step{xx=0.0,      yy =0.0,      zz = _STEP,  ww = 0.0}    else return ()
            if key == G.Key'0     then writeIORef ref Step{xx=0.0,      yy =0.0,      zz = -_STEP, ww = 0.0}    else return ()
            if key == G.Key'8     then writeIORef ref Step{xx=0.0,      yy =0.0,      zz = 0.0,    ww = _STEP}  else return ()
            if key == G.Key'7     then writeIORef ref Step{xx=0.0,      yy =0.0,      zz = 0.0,    ww = -_STEP} else return ()
            if key == G.Key'Space then writeIORef ref initStep                                                  else pp "Press No Down"
        G.KeyState'Released -> do
            if key == G.Key'Right then pp "rel Right" else pp "Press No Right"
            if key == G.Key'Left  then pp "rel left"  else pp "Press No Right"
            if key == G.Key'Up    then pp "rel up"    else pp "Release No Up"
            if key == G.Key'Down  then pp "rel Down"  else pp "Release No Down"
    when (key == G.Key'Escape && keyState == G.KeyState'Pressed)
        (G.setWindowShouldClose window True)

{-| 
    === Camera rotates around x y z axis

    * The function is not done yet.

    >data Cam = Cam{alpha::Double, beta::Double, gramma::Double, dist::Double} deriving(Show)
-} 
keyboardRot::IORef Cam -> IORef Step -> Double -> Double -> IO()
keyboardRot ref refStep w h = do
    cam <- readIORef ref
    step <- readIORef refStep   -- refStep is not modified actually.
    modifyIORef ref (rx (xx step)) 
    modifyIORef ref (ry (yy step)) 
    modifyIORef ref (rz (zz step)) 
    modifyIORef ref (rw (ww step))
    
    rotate ( beta cam)   $ ( Vector3 1 0 0 :: Vector3 GLdouble)
    rotate ( alpha cam)  $ ( Vector3 0 1 0 :: Vector3 GLdouble)
    rotate ( gramma cam) $ ( Vector3 0 0 1 :: Vector3 GLdouble)
    
    -- y' = y cos alpha
    -- z' = z cos gramma
    -- lookAt (Vertex3 1.1 + (gamma cam) 0 0) (Vertex3 0 0 0)  (Vector3 0 1 0)
    -- get viewport width and height for ratio
    -- let ratio = width/height
    -- perspective 60.0 ratio 0.5 4.0
    -- perspective (field of view) width/height zNear zFar
    -- lookAt eye_vertex center_vertex lookat_vector
    -- perspective 65.0 (w/h) 1.0 4.0
        where
            rx::Double -> Cam -> Cam
            rx d (Cam x y z w) = Cam{alpha = x + d, beta = y, gramma = z, dist = w}

            ry::Double -> Cam -> Cam
            ry d (Cam x y z w) = Cam{alpha = x, beta = y + d, gramma = z, dist = w}

            rz::Double -> Cam -> Cam
            rz d (Cam x y z w) = Cam{alpha = x, beta = y, gramma = z + d, dist = w}

            rw::Double -> Cam -> Cam
            rw d (Cam x y z w) = Cam{alpha = x, beta = y, gramma = z, dist = w + d}


{-| 
    === Draw Triangle
-} 
drawTriangle::IO()
drawTriangle = 
    preservingMatrix $ do
        translate (Vector3 0.2 0 0 :: Vector3 GLdouble)
        renderPrimitive Triangles $ do
            color  (Color3 1 0 0 :: Color3 GLdouble)
            vertex (Vertex3 (negate 0.6) (negate 0.4) 0 :: Vertex3 GLdouble)
            color  (Color3 0 1 0 :: Color3 GLdouble)
            vertex (Vertex3 0.6 (negate 0.4) 0 :: Vertex3 GLdouble)
            color  (Color3 0 0 1 :: Color3 GLdouble)
            vertex (Vertex3 0 0.6 0 :: Vertex3 GLdouble)

{-| 
    === Draw Triangle specified location and size

    >let v = Vector3 0.0 0 0
    >drawTriangle' v 0.1
-} 
drawTriangle'::Vector3 GLdouble -> GLdouble ->IO()
drawTriangle' v s = 
    preservingMatrix $ do
        translate v 
        renderPrimitive Triangles $ do
            color  (Color3 1 0 0 :: Color3 GLdouble)
            vertex (Vertex3 (negate s) (negate s) 0 :: Vertex3 GLdouble)
            color  (Color3 0 1 0 :: Color3 GLdouble)
            vertex (Vertex3 s (negate s) 0 :: Vertex3 GLdouble)
            color  (Color3 0 0 1 :: Color3 GLdouble)
            vertex (Vertex3 0 s 0 :: Vertex3 GLdouble)

bool :: Bool -> a -> a -> a
bool b falseRes trueRes = if b then trueRes else falseRes

unless' :: Monad m => m Bool -> m () -> m ()
unless' action falseAction = do
    b <- action
    unless b falseAction

maybe' :: Maybe a -> b -> (a -> b) -> b
maybe' m nothingRes f = case m of
    Nothing -> nothingRes
    Just x  -> f x
    
-- type ErrorCallback = Error -> String -> IO ()
errorCallback :: G.ErrorCallback
errorCallback err description = hPutStrLn stderr description

{-| 
    === Torus paremater equation center at (0, 0, 0)

    Torus equation: <http://xfido.com/html/indexThebeautyofTorus.html Torus> 

    * drawPrimitive Points red torus 
-} 
torus::[(GLfloat, GLfloat, GLfloat)]
torus= [ ( fx i k, 
           fy i k, 
           fz i k ) | i <- [1..n], k <-[1..n]]
        where 
            del = rf(2*pi/(n-1))
            n = 100 
            r = 0.2
            br = 0.3

            fx = \i k -> (br + r**cos(del*i))*cos(del*k)
            fy = \i k -> sin(rf del*i)
            fz = \i k -> (br + r*cos(rf del*i))*sin(rf del*k)

{-| 
    === Draw torus with small and large radius

    >mapM_ (\lo -> drawPrimitive LineLoop red lo) $ torusR 0.1 0.2 
-} 
torusR::GLfloat -> GLfloat -> [[(GLfloat, GLfloat, GLfloat)]]
torusR r br = [[((br + r*cos(del*i))*cos(del*k), 
          sin(rf del*i), 
          (br + r*cos(rf del*i))*sin(rf del*k) ) | i <- [1..n]] | k <-[1..n]]
            where 
                del = rf(2*pi/(n-1))
                n = 100 


{-| 
    === sphere rotates around y-axis

    === cut the sphere alone the xz-plane

    * The radius of the circle is \( r \cos \alpha \)

    * The circle on the xz-plane is 

    \[
        \begin{equation}
        \begin{aligned}
        x &= r \cos \alpha \sin \beta \\
        z &= r \cos \alpha \cos \beta \\
        \end{aligned}
        \end{equation}
    \]

    * The center of the circle from the center of sphere is \( r \cos \alpha \sin \beta \) 

    * Put all together
    \[
        \begin{equation}
        \begin{aligned}
        x &= r \cos \alpha \sin \beta \\
        y &= r \sin \beta \\
        z &= r \cos \alpha \cos \beta \\
        \end{aligned}
        \end{equation}
    \]
-} 
sphere::GLfloat -> [[(GLfloat, GLfloat, GLfloat)]]
sphere r = [[((r*cos(del*i))*cos(del*k), 
               sin(rf del*i), 
              (r*cos(rf del*i))*sin(rf del*k) ) | i <- [1..n]] | k <-[1..n]]
            where 
                del = rf(2*pi/(n-1))
                n = 40 

torus'::[[(GLfloat, GLfloat, GLfloat)]]
torus'= [[((br + r*cos(del*i))*cos(del*k), 
          sin(rf del*i), 
          (br + r*cos(rf del*i))*sin(rf del*k) ) | i <- [1..n]] | k <-[1..n]]
            where 
                del = rf(2*pi/(n-1))
                n = 100 
                r = 0.3
                br = 0.3

parabolic::[[(GLfloat, GLfloat, GLfloat)]]
parabolic =[[(x'*i, y'*j, 2*(x'*i)^2 + 3*(y'*j)^2) | i <- [-n..n]] | j <-[-n..n]]
            where 
                n = 20 
                del = rf(1/n);
                x' = del
                y' = del

-- x^4 + y^2 + z^6 = 6
surface1::[[(GLfloat, GLfloat, GLfloat)]]
surface1 =[[(u*d, v*d, (c - (u*d)^4 - (v*d)^2)**(1/6)) | u <- [-n..n]] | v <-[-n..n]]
            where 
                n = 160 
                d = rf(1/n);
                c = 0.1

surface2::[[(GLfloat, GLfloat, GLfloat)]]
surface2 =[[(u*d, v*d, -(c - (u*d)^4 - (v*d)^2)**(1/6)) | u <- [-n..n]] | v <-[-n..n]]
            where 
                n = 100 
                d = rf(1/n);
                c = 0.1

surface3::[[(GLfloat, GLfloat, GLfloat)]]
surface3 =[[let x' = x*d; y' = y*d in (x', y', 1 - (1-x')^2 - 100*(y' - x'^2)^2 ) | y <- [-n..n]] | x <-[-n..n]]
            where 
                n = 100 
                d = rf(1/n);
                c = 0.1

lightingInfo::IO()
lightingInfo  = do
    diffuse  (Light 0) $= lightDiffuse
    ambient  (Light 0) $= lightAmbient
    specular (Light 0) $= lightSpecular
    position (Light 0) $= lightPosition
    light    (Light 0) $= Enabled
    -- lighting           $= Enabled
    depthFunc          $= Just Lequal
    blend              $= Enabled
    lineSmooth         $= Enabled

{-| 
    === Generate random Vertex3
-} 
randomVertex::Integer -> IO [Vertex3 GLfloat]
randomVertex n = do 
                    ranl <- randomDouble (fromIntegral n) 
                    let ranlist = map (\x -> 1.5*x) ranl 
                    let vexList = fmap (\x -> x - 0.5) $ fmap realToFrac ranlist 
                    let vexTuple = map(\x -> tripleToVertex3 x ) $ filter(\x -> length x == 3) $ partList 3 vexList 
                                where 
                                    tripleToVertex3::[GLfloat] -> Vertex3 GLfloat 
                                    tripleToVertex3 [a, b, c] = Vertex3 a b 0.0 
                    return vexTuple
