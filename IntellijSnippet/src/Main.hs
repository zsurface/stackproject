
-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"
-- import Data.Set   -- collide with Data.List 
{-# LANGUAGE QuasiQuotes       #-}
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- {-# LANGUAGE QuasiQuotes       #-}
import Text.RawString.QQ (r)         -- Need QuasiQuotes too 

import qualified Text.Regex.TDFA as TD
import Text.Pretty.Simple 
import qualified NeatInterpolation as NI -- variable interpolation

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

-- snippet = "/Users/cat/myfile/bitbucket/snippets/snippet_test.hs"
snippet = "/Users/cat/myfile/bitbucket/snippets/snippet.hs"

--  <template name="jtestit" value="test it only;&#10;print(file);" description="java file extension, base, path" toReformat="false" toShortenFQNames="true">
--    <context>
--      <option name="JAVA_CODE" value="true" />
--    </context>
--  </template>

toStr = strictTextToStr
toText = strToStrictText

--hiddenForm3::String -> String -> String  
--hiddenForm3 k s = toStr [NI.text| 
--              <template name="${tk}" value="${ts}" description="java file extension, base, path" toReformat="false" toShortenFQNames="true">
--                <context>
--                  <option name="JAVA_CODE" value="true" />
--                </context>
--              </template>
--                |] 
--           where
--              ts = toText s
--              tk = toText k 

--hiddenxx::String -> String -> String  
--hiddenxx k s = toStr [NI.text|
--            <template ></template>
--            |] 
--           where
--              ts = toText s
--              tk = toText k 

-- KYE: Convert $b/snippet/snippet.hs to Intellij code template
snippetBlock::String -> String -> String
snippetBlock k s = [r| 
              <template name="|] <> k <>[r|" value="|]<> (escapeXML s) <> 
              [r|" description="java file extension, base, path" toReformat="false" toShortenFQNames="true"><context>
              <option name="JAVA_CODE" value="true" /></context></template>
              |]

javaFile::String -> Bool
javaFile s = containStr "\\*\\.java" s 

xmlBlock::[String] -> String
xmlBlock s = concatStr s "&#10;"

templatePath="/Users/cat/myfile/bitbucket/intellij/templates/Java.xml" 

xopen = "<templateSet group=\"Java\">"
xclose = "</templateSet>"
main = do 
        tlist <- readSnippet snippet
        pp tlist
        fl
        let javaList = filter(\x ->  javaFile $ head $ snd x) tlist
        let xmls = map(\x -> let ks = head $ fst x 
                                 ls = tail $ snd x
                             in snippetBlock ks $ xmlBlock ls 
                     ) javaList
        let cxmls = [xopen] ++ xmls ++ [xclose] 
        writeFileList templatePath cxmls
        pp $ "numbers of block=" ++ (show $ len tlist)
        pp $ "numbers of java block=" ++ (show $ len javaList)
        pp $ "write to =>" ++ templatePath


