{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_readLine (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/readLine/.stack-work/install/x86_64-osx/c663fe2d8cd423f77905142c2fecfb5e70d060126d511d0d1235f8b07023a3e4/8.10.3/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/readLine/.stack-work/install/x86_64-osx/c663fe2d8cd423f77905142c2fecfb5e70d060126d511d0d1235f8b07023a3e4/8.10.3/lib/x86_64-osx-ghc-8.10.3/readLine-0.1.0.0-EK16gljusOVJvIzfTxyAdc-readLine"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/readLine/.stack-work/install/x86_64-osx/c663fe2d8cd423f77905142c2fecfb5e70d060126d511d0d1235f8b07023a3e4/8.10.3/lib/x86_64-osx-ghc-8.10.3"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/readLine/.stack-work/install/x86_64-osx/c663fe2d8cd423f77905142c2fecfb5e70d060126d511d0d1235f8b07023a3e4/8.10.3/share/x86_64-osx-ghc-8.10.3/readLine-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/readLine/.stack-work/install/x86_64-osx/c663fe2d8cd423f77905142c2fecfb5e70d060126d511d0d1235f8b07023a3e4/8.10.3/libexec/x86_64-osx-ghc-8.10.3/readLine-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/readLine/.stack-work/install/x86_64-osx/c663fe2d8cd423f77905142c2fecfb5e70d060126d511d0d1235f8b07023a3e4/8.10.3/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "readLine_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "readLine_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "readLine_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "readLine_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "readLine_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "readLine_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
