{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_OpenGLConvexHull (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLConvexHull/.stack-work/install/x86_64-osx/8f04c4cafb31aaaf1d60efaf9097ca9686efa211c7d3bf41d01aad3b1a597b62/8.6.5/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLConvexHull/.stack-work/install/x86_64-osx/8f04c4cafb31aaaf1d60efaf9097ca9686efa211c7d3bf41d01aad3b1a597b62/8.6.5/lib/x86_64-osx-ghc-8.6.5/OpenGLConvexHull-0.1.0.0-HuH7fzq0tqI5HzaE16kMT2-OpenGLConvexHull"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLConvexHull/.stack-work/install/x86_64-osx/8f04c4cafb31aaaf1d60efaf9097ca9686efa211c7d3bf41d01aad3b1a597b62/8.6.5/lib/x86_64-osx-ghc-8.6.5"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLConvexHull/.stack-work/install/x86_64-osx/8f04c4cafb31aaaf1d60efaf9097ca9686efa211c7d3bf41d01aad3b1a597b62/8.6.5/share/x86_64-osx-ghc-8.6.5/OpenGLConvexHull-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLConvexHull/.stack-work/install/x86_64-osx/8f04c4cafb31aaaf1d60efaf9097ca9686efa211c7d3bf41d01aad3b1a597b62/8.6.5/libexec/x86_64-osx-ghc-8.6.5/OpenGLConvexHull-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/OpenGLConvexHull/.stack-work/install/x86_64-osx/8f04c4cafb31aaaf1d60efaf9097ca9686efa211c7d3bf41d01aad3b1a597b62/8.6.5/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "OpenGLConvexHull_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "OpenGLConvexHull_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "OpenGLConvexHull_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "OpenGLConvexHull_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "OpenGLConvexHull_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "OpenGLConvexHull_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
