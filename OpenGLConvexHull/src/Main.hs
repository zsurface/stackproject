{-# LANGUAGE DataKinds #-}
module Main where

import Control.Concurrent (threadDelay)
import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import qualified Graphics.UI.GLFW as G 
import Bindings.GLFW 

import System.Exit
import System.IO
import Control.Monad
import System.Random
import Data.Set(Set) 
import Data.IORef 
import Data.Maybe
import qualified Data.Set as S 
import Linear.V3
import Linear.V3(cross) 
import Linear.Vector
import Linear.Matrix
import Linear.Projection as P
import Linear.Metric(norm, signorm)

import Text.Regex
import AronModule
import AronGraphic
-- import AronHtml2


-- tiny utility functions, in the same spirit as 'maybe' or 'either'
-- makes the code a wee bit easier to read
-- compile: ghc -o glfw glfw.hs

--t1 (a, b, c) = a
--t2 (a, b, c) = b 
--t3 (a, b, c) = c 

c0 = Vertex3 0   0 0 
x0 = Vertex3 0.5   0 0 
y0 = Vertex3 0   0.5 0 
z0 = Vertex3 0     0 0.5 
xyz = Vertex3 0.5 0.5 0.5 

l3ToVx3::[GLfloat] -> Vertex3 GLfloat 
l3ToVx3 [a, b, c] = Vertex3 (a - 0.7) (b - 0.7) 0 

-- vertex3ToList::Vertex3 a -> [a]
-- vertex3ToList (Vertex3 a b c) = [a, b, c]


addx::(Num n)=> n -> (n, n, n) -> (n, n, n)
addx m (a, b, c) = (m + a, b, c)

addy::(Num n)=> n -> (n, n, n) -> (n, n, n)
addy m (a, b, c) = (a, m + b, c)

mx::(Num n)=> n -> (n, n, n) -> (n, n, n)
mx m (a, b, c) = (m, b, c)

my::(Num n)=> n -> (n, n, n) -> (n, n, n)
my m (a, b, c) = (a, m, c)

-- | mod for Double, 6.28 `fmod` 2 => 0.28
fmod::Double->Integer->Double
fmod  a n = a - du
            where     
                num = realToFrac (div (round a) n)
                du = realToFrac ((round num)*n)

vertexList = [
            (0.3, 0.4, 0),
            (0.6, 0.9, 0),
            (0.5, 0.2, 0),
            (0.8, 0.3, 0),
            (0.4, 0.6, 0),
            (0.8, 0.1, 0),
            (0.6, 0.3, 0),
            (0.9, 0.5, 0),
            (0.9, 0.4, 0)
            ]

--vertexList2 = [
--            Vertex3 0.9 0.8 0,
--            Vertex3 0.5 0.1 0,
--
--            Vertex3 0.7 0.1 0,
--            Vertex3 0.2 0.4 0,
--            Vertex3 0.9 0.7 0,
--
--            Vertex3 0.71 0.1 0,
--            Vertex3 0.2 0.9 0,
--            Vertex3 0.23 0.3 0,
--            
--            Vertex3 0.5 0.5 0,
--            Vertex3 0.1 0.9 0,
--            Vertex3 0.2 0.31 0,
--            
--            Vertex3 0.471 0.21 0,
--            Vertex3 0.442 0.34 0,
--            Vertex3 0.2333 0.6 0,
--            
--            Vertex3 0.555 0.245 0,
--            Vertex3 0.111 0.399 0,
--            Vertex3 0.222 0.231 0,
--
--
--            Vertex3 0.89 0.33 0,
--            Vertex3 0.21 0.31 0,
--            Vertex3 0.69 0.13 0,
--            Vertex3 0.121 0.51 0,
--            Vertex3 0.49 0.43 0,
--            Vertex3 0.44 0.66 0
--            ]
--
vertexList2 = [
--    Vertex3 0.38 0.41 0.44,
--    Vertex3 0.72 6.0e-2 0.23,
--    Vertex3 0.73 0.21 0.38,
--    Vertex3 0.74 0.34 7.0e-2
        Vertex3 0.16 0.43 0,
        Vertex3 0.18 0.16 0,
        Vertex3 0.28 0.85 0,
        Vertex3 0.95 0.82 0
    ]
--vertexList2 = [
--            Vertex3 0.9 0.8 0,
--            Vertex3 0.23 0.4 0,
--            Vertex3 0.2 0.31 0,
--            Vertex3 0.1 0.95 0
----            Vertex3 0.89 0.33 0,
----            Vertex3 0.44 0.66 0
--            ]
--

-- compute the insection of two line
-- compute the inverse of 2x2 matrix
-- solve [s, t]
--insertLine::Vertex3 a ->Vertex3 a ->Vertex3 a ->Vertex3 a -> Vertex3 a
--insertLine (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) (Vertex3 a0 b0 c0)(Vertex3 a1 b1 c1) =



mergeChunk::Int->[(GLfloat, GLfloat, GLfloat)]->[(GLfloat, GLfloat, GLfloat)]
mergeChunk n c = mergeList  (take n c)  (take n $ drop n c) 

nStep::Float
nStep = 40

sphere::[(GLfloat, GLfloat, GLfloat)]
sphere = [(cos(del*k)*cos(del*i), 
          _sin(del*k), 
          cos(rf del*k)*sin(rf del*i)) | k <- [1..n], i <-[1..n]]
            where 
                del = rf(2*pi/(n-1))
                n = nStep 

shear f = do
   m <-  (newMatrix RowMajor [1,f,0,0
                             ,0,1,0,0
                             ,0,0,1,0
                             ,0,0,0,1])
   multMatrix (m:: GLmatrix GLfloat)

poly::[Double]->[Double]->[Double]
poly [] _ = [] 
poly _ [] = [] 
poly xs sx = map(\s -> sum s) $ map(\x -> map(\(c, p) -> c*(x^p)) po) sx
            where
                po = zip xs [0..]

-- | Torus paremater equation center at (0, 0, 0)
-- | Torus equation: http://xfido.com/html/indexThebeautyofTorus.html 
torus::[(GLfloat, GLfloat, GLfloat)]
torus= [((br + r*cos(del*i))*cos(del*k), 
        sin(rf del*i), 
        (br + r*cos(rf del*i))*sin(rf del*k) ) | i <- [1..n], k <-[1..n]]
        where 
            del = rf(2*pi/(n-1))
            n = nStep 
            r = 0.2
            br = 0.3

slop::(GLfloat, GLfloat, GLfloat) -> (GLfloat, GLfloat, GLfloat) -> Double
slop (x1, y1, z1) (x2, y2, z2) = rf(y2 - y1) / rf(x2 - x1)

slop'::Vertex3 GLfloat -> Vertex3 GLfloat -> Float 
slop' (Vertex3 x1 y1 z1) (Vertex3 x2 y2 z2)  = rf(y2 - y1) / rf(x2 - x1) 

cosVec::Vertex3 GLfloat -> Vertex3 GLfloat -> Float
cosVec (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) = d/n 
                    where
                        vx = Vertex3 (-1) 0 0  -- norm lv = 1
                        v01= Vertex3 (x1 - x0) (y1 - y0) (z1 - z0)
                        -- TODO: should use Vector3?
                        dot (Vertex3 a0 b0 c0) (Vertex3 a1 b1 c1) = a0*a1 + b0*b1 + c0*c1 
                        d = dot vx v01 
                        norm v = sqrt $ dot v v 
                        -- n = if norm v01 > 0 then norm v01 else 1
                        n = norm v01

cosVec3::Vertex3 GLfloat -> Vertex3 GLfloat -> Vertex3 GLfloat -> Float
-- cosVec3 (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) (Vertex3 x2 y2 z2) = d/((norm v10)*(norm v12))
cosVec3 (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) (Vertex3 x2 y2 z2) = d/(n1*n2)
                    where
                        -- vector 1 -> 0 
                        v10 = Vertex3 (x0 - x1) (y0 - y1) (z0 - z1)
                        -- vector 1 -> 2 
                        v12 = Vertex3 (x2 - x1) (y2 - y1) (z2 - z1)
                        -- dot v10 v12
                        dot (Vertex3 a0 b0 c0) (Vertex3 a1 b1 c1) = a0*a1 + b0*b1 + c0*c1 
                        d = dot v10 v12
                        norm v = sqrt $ dot v v
                        n1 = norm v10
                        n2 = norm v12
--                        n1 = if norm v10 /= 0 then norm v10 else 1
--                        n2 = if norm v12 /= 0 then norm v12 else 1

det::Vertex3 GLfloat -> Vertex3 GLfloat -> Vertex3 GLfloat -> Float
det (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) (Vertex3 x2 y2 z2) = det' v10 v12
                    where
                        -- vector 1 -> 0 
                        v10 = Vertex3 (x0 - x1) (y0 - y1) (z0 - z1)
                        -- vector 1 -> 2 
                        v12 = Vertex3 (x2 - x1) (y2 - y1) (z2 - z1)
                        -- dot v10 v12
                        --  1 0 
                        --  0 1 
                        -- | => det > 0, left turn from +X-axis to +Y-axis (CCW)
                        det' (Vertex3 x0 y0 _) (Vertex3 x1 y1 _) = x0*y1 - y1*y0 


tryDrawCircle::IO()
tryDrawCircle = do
    preservingMatrix $ do
        forM_ list $ \x -> do
            rotate (x)$ (Vector3 1 1 1 :: Vector3 GLdouble)
            drawCircle' vx  0.04 
            where
                list = [ x | x <- let d = 360/100 in map (*d) [1..100]]
                vx = Vertex3 0.5 0.5 0


torus2::[(GLfloat, GLfloat, GLfloat)]
torus2= [((br + r*_cos(del*i))*_cos(del*k) + _cos(del*i), 
        _sin(del*i)*_cos(del*k), 
        (br + r*_cos(del*i))*_sin(del*k) ) | i <- [1..n], k <-[1..n]]
        where 
            del = 2*pi/(n-1)
            n = nStep 
            r = 0.1
            br = 0.2



--circle::[(GLfloat, GLfloat, GLfloat)]
--circle =[ let alpha = (pi2*n)/num in (r*_sin(alpha), r*_cos(alpha), 0) | n <- [1..num]]
--        where
--            num = 40 
--            r = 0.2 
--            pi2 = 2*pi::Float 


curve1::[(GLfloat, GLfloat, GLfloat)]
curve1 = [let x = d*k  in 
            (x, f x, 0) | k <- [a..b]] 
            where
                a = -300
                b = 300
                d = 0.01
                f x = x*x*x

pointList::[(GLfloat, GLfloat, GLfloat)]
pointList = [let x = d*k  in 
            (x, f x, 0) | k <- [a..b]] 
            where
                a = -300
                b = 300
                d = 0.01
                f x = x*x*x

data Seg a = Smk (a, a) deriving (Show)

beg::(Num a)=>Seg a -> a
beg (Smk (p0, p1)) = p0

end::(Num a)=>Seg a -> a
end (Smk (p0, p1)) = p1 

contains::Vertex3 a -> Seg (Vertex3 a) -> Bool
contains _ _ = undefined

isCol::Vertex3 a -> Seg (Vertex3 a) -> Bool
isCol _ _ = undefined

dst::Seg (Vertex3 a) -> a 
dst _ = undefined

getParameter::Vertex3 a -> Seg (Vertex3 a) -> (a, a) 
getParameter _ _ = undefined

isIntersected::Seg (Vertex3 a) -> Seg(Vertex3 a) -> Bool
isIntersected _ _ = undefined

intersect::Seg (Vertex3 a) -> Seg(Vertex3 a) -> Maybe (Vertex3 a)
intersect _ _ = undefined

extendSeg::Seg (Vertex3 a) -> a -> (Vertex3 a)
extendSeg _ _ = undefined


randomVertex3::Int-> IO [Vertex3 GLfloat]
randomVertex3 n = randomDouble n >>= \x -> return $ map(\w ->Vertex3 (head w) ((head . tail) w) (last w)) $ case (mod n 3) of 
                                                                            0 -> init $ partList 3 $ map (realToFrac) x
                                                                            _ -> partList 3 $ map (realToFrac) x

-- | line segements from points with LineStrip
-- | e.g. draw curve, line
renderCurve::[Vertex3 GLfloat]->IO()
renderCurve points = do 
    renderPrimitive LineStrip $ mapM_(\vx -> vertex $ vx) points


---- | Given a function f, interval (a, b), 
----   draw the curve from a to b
--drawCurve::(GLfloat -> GLfloat) -> (GLfloat, GLfloat) ->Color3 GLdouble  -> IO()
--drawCurve f (a, b) c = renderPrimitive LineStrip $ mapM_(\vx -> do 
--                                            color c 
--                                            vertex $ vx) $ curvePt f (a, b) 

toStr::(Show a)=>[a]->[String]
toStr [] = [] 
toStr xs = map(\x -> show x) xs 

bool :: Bool -> a -> a -> a
bool b falseRes trueRes = if b then trueRes else falseRes

unless' :: Monad m => m Bool -> m () -> m ()
unless' action falseAction = do
    b <- action
    unless b falseAction

maybe' :: Maybe a -> b -> (a -> b) -> b
maybe' m nothingRes f = case m of
    Nothing -> nothingRes
    Just x  -> f x
    
-- type ErrorCallback = Error -> String -> IO ()
errorCallback :: G.ErrorCallback
errorCallback err description = hPutStrLn stderr description


keyBoardCallBack :: IORef (Set G.Key) -> G.KeyCallback
keyBoardCallBack ref window key scanCode keyState modKeys = do
    putStrLn $ "keyBoardCallBack=>keyState" ++ show keyState ++ " " ++ "keyBoardCallBack=>key=" ++ show key
    case keyState of
        G.KeyState'Pressed -> modifyIORef ref (S.insert key) >> readIORef ref >>= \x -> print $ "inside keyBoardCallBack=> readIORef ref=>" ++ show x 
        G.KeyState'Released -> modifyIORef ref (S.delete key)
        _ -> return ()
    when (key == G.Key'Escape && keyState == G.KeyState'Pressed)
        (G.setWindowShouldClose window True)



readFileTo2dList::FilePath->String->IO[[String]]
readFileTo2dList fp s = do
        -- ls <- readFileToList fp 
        ls <- readFileLatin1ToList fp 
        let ls' = map(\x -> filter(\c -> (length.trim) x > 0) $ splitRegex (mkRegex s) x) $ filter(\x -> length x > 0) $ map(trim) ls 
        return ls'


mymain :: IO ()
mymain = do
  G.setErrorCallback (Just errorCallback)
  -- finally add double buffer
  successfulInit <- G.init
--  G.windowHint (G.WindowHint'DoubleBuffer True)
--  G.windowHint (G.WindowHint'ContextVersionMajor 3)
--  G.windowHint (G.WindowHint'ContextVersionMinor 2)
--  G.windowHint (G.WindowHint'OpenGLProfile G.OpenGLProfile'Core)
--  G.windowHint (G.WindowHint'OpenGLForwardCompat True)
  -- if init failed, we exit the program
  bool successfulInit exitFailure $ do
      mw <- G.createWindow 1000 1000 "Simple example, haskell style" Nothing Nothing
      maybe' mw (G.terminate >> exitFailure) $ \window -> do
          G.makeContextCurrent mw
          mainLoop window 
          G.destroyWindow window
          G.terminate
          exitSuccess
          
mainLoop :: G.Window -> IO ()
mainLoop w = unless' (G.windowShouldClose w) $ do
    -- writeToFile "/tmp/g1.x" $ toStr torus  
    lastFrame <- maybe 0 realToFrac <$> G.getTime

    (width, height) <- G.getFramebufferSize w
    let ratio = fromIntegral width / fromIntegral height
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    GL.clear [ColorBuffer, DepthBuffer]
    -- lighting info
    diffuse  (Light 0) $= lightDiffuse
    ambient  (Light 0) $= lightAmbient
    specular (Light 0) $= lightSpecular
    position (Light 0) $= lightPosition
    light    (Light 0) $= Enabled
    -- disable to show color
    -- lighting           $= Enabled
    depthFunc  $= Just Lequal
    blend          $= Enabled
    lineSmooth     $= Enabled
    -- end lighting info

    matrixMode $= Projection
    matrixMode $= Modelview 0
    loadIdentity
    let fovy = 60.0; aspect = 1.0; zNear = 2.0; zFar = (-2)
        in GM.perspective fovy aspect zNear zFar 
    --ortho (negate ratio) ratio (negate 1.0) 1.0 1.0 (negate 1.0)

    loadIdentity

    -- lookAt(Vertex3 pos of camera, Vertex3 point at, Vector up)
    -- GM.lookAt (Vertex3 0.1 0.1 0.5::Vertex3 GLdouble) (Vertex3 0 0 0:: Vertex3 GLdouble) (Vector3 0 1 0 :: Vector3 GLdouble)
    GM.lookAt (Vertex3 0 0 1::Vertex3 GLdouble) (Vertex3 0 0 0:: Vertex3 GLdouble) (Vector3 0 1 0 :: Vector3 GLdouble)

    ref <- newIORef S.empty
    G.setKeyCallback w (Just $ keyBoardCallBack ref)
    --render sphere
    renderCoordinates
    --axisTip
--    axisTipX
--    axisTipY
--    axisTipZ
    
    ranl <- randomDouble 300 
    writeToFile "./text/kkx.x" $ map show ranl
    ff "ranl" ranl
    let ranlist = map (\x -> 1.5*x) ranl 
    let vexList = fmap realToFrac ranlist 

    randSeg <- randomVertex3 60
    let ranx = map (show) randSeg
    writeToFile "./text/ran.x" ranx 
    ranTriple <- randomDouble 60 
    let rant = partList 3 ranTriple
    ff "rant" rant
    let rant' = (map . map) (show) rant
    ff "rant'" rant'
    -- mapM_ (\x -> write2dListToFile "/tmp/triple.x" " " x) rant'
    flist <- readFileTo2dList "./text/triple.x" " "
    ff "flist" flist 
    let nlist = (map . map) (\x -> read x ::Float) flist
    ff "nlist" nlist
    let nlist' = map(\x -> Vertex3 (head x) ((head . tail) x) (last x)) nlist
    let segList2 = partList 2 nlist'
    let segList = partList 2 randSeg
    -- let vexTuple = map(\x -> tuplify3 x ) $ filter(\x -> length x == 3) $ partList 3 vexList 
    -- [[1, 2, 3]] => [Vertex3 1 2 3]
    -- 
    let vexT = map(\x -> l3ToVx3 x ) $ filter(\x -> length x == 3) $ partList 3 vexList 
    let vexTuple' = vexT
    let stVex = fmap(\x -> show x) vexTuple'
    writeToFileAppend "./text/hu.x"  ["-----------------------------------------"] 
    writeToFileAppend "./text/hu.x" stVex 
    --let vexTuple' = vertexList2 
    let ylist = qqsort cmp vexTuple' where cmp (Vertex3 x1 y1 z1) (Vertex3 x2 y2 z2) = y1 > y2 

    -- problem in cosVec.. 
    

    -- draw a point on top of Y-axis
--    drawCircleColor (head ylist) green 0.02
    let sortVex = qqsort cmp $ map(\p -> (cosVec p0 p, p0, p)) cx 
                    where
                        p0 = head ylist
                        cx = tail ylist
                        cmp c1 c2  = t1 c1 > t1 c2 
                        t1 (a, b, c) = a
                        t2 (a, b, c) = b 
                        t3 (a, b, c) = c 

    let x0 = t2 $ head sortVex             
    let x1 = t3 $ head sortVex             
    let segment = [x0, x1]

    let drawArrow t p0 v1 = do 
                    let v1'= per v1 
                    let p1 = ray p0 t v1 
                    let p1' = ray (p0 +: ((-s) ∘ v1')) s v1' 

                    drawSegments green [p0, p1] 
                    drawSegments red [(p0 +: (k ∘ v1) +: ((-k) ∘ v1')), p0 +: (k ∘ v1) +: (k ∘ v1')] 
                    drawSegments blue [p0, (p0 +: (k ∘ v1) +: ((-k) ∘ v1'))] 
                    drawSegments blue [p0, (p0 +: (k ∘ v1) +: (k ∘ v1'))] 
                    where
                        per (Vector3 x y z) = Vector3 y (-x) z
                        k = 0.02
                        s = 0.2
        
    let v1 = Vector3 0.9 0.9 0
    let v1'= perpcw v1 
    let p0 = (Vertex3 (-0.5) (-0.5) 0) +: (0.5 ∘ v1')
    let p1 = ray p0 0.5 v1 
    let p1' = ray (p0 +: ((-0.1) ∘ v1')) 0.1 v1' 
--    mapM_(\c -> drawCircle' c 0.01) $ [p0] 

--    drawSegments green [p0, p1] 
--    drawSegments red [(p0 +: (0.05 ∘ v1) +: ((-0.05) ∘ v1')), p0 +: (0.05 ∘ v1) +: (0.05 ∘ v1')] 
--    drawSegments blue [p0, (p0 +: (0.05 ∘ v1) +: ((-0.05) ∘ v1'))] 
--    drawSegments blue [p0, (p0 +: (0.05 ∘ v1) +: ((0.05) ∘ v1'))] 

--    let p₀ = Vertex3 0.2 0.3 0; v = Vector3 0 1 0 
--        in drawArrow 0.6 p₀ v   
    let list = [p₀, p₁, q₀, q₁]
               where
                 p₀ = Vertex3 0 0 0
                 p₁ = Vertex3 1 1 0
                 q₀ = Vertex3 2 0 0
                 q₁ = Vertex3 2 1 0
    let p0 = Vertex3 0   0.35 0
    let p1 = Vertex3 0.5 0   0
    let q0 = Vertex3 0   0.5 0
    let q1 = Vertex3 0.5 0.5 0
    let ls = [p0, p1, q0, q1]
    let vst = intersectLine' $ (fmap . fmap) (/ 5) list 
    -- drawSegments red $ (fmap . fmap) (/ 5) list 
    -- drawSegments red ls 
    -- drawDot $ fmap (realToFrac) $ fst $ fromJust $ intersectLineR  p0 p1 q0 q1 
    let mab = intersectLineR  p0 p1 q0 q1
    let mab' = intersectLine  p0 p1 q0 q1
    pp $ show mab
    pp $ show mab'
    -- drawSegmentsArg p0 p1 q0 q1                   
--    drawCircleColor (fst $ fromJust vst) blue 0.05
--    pp $ "inter=" ++ (show vst)

--    let dis = let p₀ = Vertex3 0 0 0 
--                  q₀ = Vertex3 2 0 0
--                  q₁ = Vertex3 2 1 0
--                  in pointToLine p₀ q₀ q₁ 
--    pp $ "dis=" ++ (show dis)
--    let p₀ = Vertex3 0.5 0   0
--        p₁ = Vertex3 0  0.5  0
--        p₂ = Vertex3 0  0  0.5
--        nr = fromJust $ planeNormal p₀ p₁ p₂
--        in drawSegments blue [p₀, (p₀ +: (2.5 *: nr))]  
--    let p₀ = Vertex3 0.5 0   0
--        p₁ = Vertex3 0   0.5 0
--        p₂ = Vertex3 0   0 0.5
--        in drawPlane blue p₀ p₁ p₂
    
--    let p₀ = Vertex3 0.5 0   0
--        p₁ = Vertex3 0  0.5  0
--        in drawLines green [p₀, p₁]  
--    let p₀ = Vertex3 0.5 0   0
--        p₁ = Vertex3 0  0.5  0
--        in drawLines blue $ perpenLine (-0.5) 0.5 p₀ p₁;  
--    let p₀ = Vertex3 0.5 0   0
--        p₁ = Vertex3 0   0   0
--        in drawLines green $ perpenLine (-0.5) 0.5 p₀ p₁;  
--    let p₀ = Vertex3 0   0.5 0
--        p₁ = Vertex3 0   0   0
--        in drawLines red $ perpenLine (-0.5) 0.5 p₀ p₁;  
--    let list = [(Vertex3 0 0 0), (Vertex3 0.5 0 0), (Vertex3 0 0.5 0)]
--    mapM_ (drawDot) list
    let se = Smk (Vertex3 0 1 2, Vertex3 2 3 4)
    -- mapM_ (\x -> drawSegment' All green x) segList2 
    -- mapM_ (\x -> drawLines green x) segList 
    -- let l4 = partList 4 randSeg 
    -- mapM_ (\x -> drawLines blue x) l4
    let npt = let p0 = Vertex3 0.5 0.5 0.5
                  q0 = Vertex3 0.5   0 0 
                  q1 = Vertex3 0   0.5 0 
                  q2 = Vertex3 0     0 0.5 
                  in fmap realToFrac $ fromJust $ perpPlane p0 q0 q1 q2
    -- |  drawLines green [c0, npt]
    let x0 = Vertex3 0.5   0 0 
        y0 = Vertex3 0   0.5 0 
        z0 = Vertex3 0     0 0.5 
        in drawPlane red x0 y0 z0
    let ce = (Vertex3 0.0 0.0 0.0)
    let pp = drawSpherePt ce 0.5 
    -- | mapM_ drawDot pp
    -- | drawCircleVec 0.5 (Vertex3 0.0 0.0 0.0) (Vector3 1.0 1.0 1.0)
    -- | drawCircleVec 0.4 (Vertex3 0.0 0.0 0.0) (Vector3 1.0 (-1.0) 0.0)
    -- | drawCircleVec 0.3 (Vertex3 0.0 0.0 0.0) (Vector3 1.0 0.0 1.0)
    -- | drawCircleVec 0.2 (Vertex3 0.0 0.0 0.0) (Vector3 1.0 0.0 (-1.0))
    -- | drawCircleVec 0.1 (Vertex3 0.0 0.0 0.0) (Vector3 (1.0) 0.0 (1.0))
    -- drawCircleVec 0.5 (Vertex3 0.0 0.0 0.0) (Vector3 (1.0) (1.0) (0.5))
    -- | drawCircleVec 0.2 (Vertex3 0.0 0.0 0.0) (Vector3 (1.0) (1.0) 0.0)
    -- | drawCircleVec 0.3 (Vertex3 0.0 0.0 0.0) (Vector3 (1.0) (1.0) (0.5))

    let vec = Vector3 0.8 0.0 0.0
    drawLines green [c0, c0 +: vec]
    let vx  = multiMat (roty (-pi/6)) (veMat vec)
    drawLines blue [c0, c0 +: (matVe vx)]
    let v = Vector3 1 1 1 
    let u = Vector3 0.5 0.5 0
    let pp = [ v2x $ rod u v a | a <- let d = 2*pi/100 in map(*d) [1..100]]
    -- mapM_(\c -> drawCircleColor' blue 0.01 c) $ pp 
    let ls = let f x = x*x*x 
                 x0 = 0 
                 x1 = 1
                 c  = 0.5 
                 in tangentLine f (x0, x1) c 
    -- drawLines red $ map(\(x, y, z) -> Vertex3 x y z) ls 
    -- tryDrawCircle
--    renderCurve $ let f x = x*x*x in curvePt f (-1, 1)
--    renderCurve $ map(\(x, y, z) -> Vertex3 x y z) ls
    let f x = x*x*x 
    drawCurve f (0, 1) green
    drawSurface (\x y -> x**2 + y**2)
    drawTangentLine f (0.3 - 0.1, 0.3 + 0.1) 0.3 red 
    drawNormalLine f (0.3, 0.3 + 0.1) 0.3 green 
     
    G.swapBuffers w
    G.pollEvents
    mainLoop w 

main = mymain

