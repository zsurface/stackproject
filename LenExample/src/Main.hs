-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
{-# LANGUAGE TemplateHaskell #-}
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent
import Control.Lens hiding (set, (^.), (^=))
import Control.Lens.TH

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule hiding (getX, getY)

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

data Bike = Bike {_frontwheel::Int, _rearwheel::Int} deriving (Show)
makeLenses ''Bike

data User = User {name::String, age::Int} deriving (Show)

data NaiveLens s a = NaiveLens {view :: s -> a,
                                set :: a -> s -> s}

nameLens::NaiveLens User String
nameLens = NaiveLens name (\a s -> s {name = a})

ageLens::NaiveLens User Int
ageLens = NaiveLens age (\a s -> s {age = a})
--                       | ----------------|
--                               ↑
--                              User
--                   | – - -   Int - - - - |
--                       set:: a -> s -> a
--          
--     age :: User -> Int
--     name:: User -> String
--  ageLens :: Int -> User -> NaiveLens User Int
--       set :: (NaiveLens User Int) -> Int -> User -> User
--       set          ageLens           20     user
--                    |- -        ↑               |
--                                |-> ageLens = \a s -> s { age = a}
--                                  


data Point = Point{xx :: Double, yy :: Double} deriving (Show)

getX :: Point -> Double
getX p = xx p

getY :: Point -> Double
getY p = yy p

setX :: Double -> Point -> Point
setX xx' p = p {xx = xx'}

setY :: Double -> Point -> Point
setY yy' p = p { yy = yy'}


-- KEY: lens example good
-- https://www.haskellforall.com/2012/01/haskell-for-mainstream-programmers_28.html
--                       getter,      setter             
--   MyLens User Int = (User -> Int, Int -> User -> User) 
type MyLens a b  = (a -> b, b -> a -> a)

getL :: MyLens a b -> a -> b
getL (g, _) = g  -- getL (g, _) a = g a

setL :: MyLens a b -> b -> a -> a
setL (_, h) = h  --  setL (_, h) b a = h b a

modL :: MyLens a b -> (b -> b) -> a -> a
modL l f a = setL l (f (getL l a)) a

x'::MyLens Point Double
x' = (getX, setX)

y'::MyLens Point Double
y' = (getY, setY)

(^.)::a -> MyLens a b -> b
a ^. l = getL l a

(^=)::MyLens a b -> b -> a -> a
(l ^= b) a = setL l b a  -- (^=) = setL


main = do
        let user = User{name = "Bob", age = 18}
        print "Hello World"
        pp $ set nameLens "KK" user
        pp $ set ageLens 20 user
        argList <- getArgs
        pp $ length argList
        let point = Point {xx = 3.0, yy = 4.0}
        pp $ getL x' point  -- getL :: MyLens a b -> a -> b
        pp $ getL y' point
        pp $ modL x' (+1) point
        pp $ modL y' (+10) point
        pp $ point ^. x'
        pp $ point ^. y'
        pp $ (x' ^= 10.4) point
        pp "done!"
