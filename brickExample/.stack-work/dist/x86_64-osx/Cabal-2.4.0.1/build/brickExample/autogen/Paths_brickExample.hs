{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_brickExample (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/brickExample/.stack-work/install/x86_64-osx/622c876310f4588571b30d8bb60c7c1e82d734372539f9bc3380b7821606289e/8.6.5/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/brickExample/.stack-work/install/x86_64-osx/622c876310f4588571b30d8bb60c7c1e82d734372539f9bc3380b7821606289e/8.6.5/lib/x86_64-osx-ghc-8.6.5/brickExample-0.1.0.0-KIvQfgyEM5ZDKSMoLMILng-brickExample"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/brickExample/.stack-work/install/x86_64-osx/622c876310f4588571b30d8bb60c7c1e82d734372539f9bc3380b7821606289e/8.6.5/lib/x86_64-osx-ghc-8.6.5"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/brickExample/.stack-work/install/x86_64-osx/622c876310f4588571b30d8bb60c7c1e82d734372539f9bc3380b7821606289e/8.6.5/share/x86_64-osx-ghc-8.6.5/brickExample-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/brickExample/.stack-work/install/x86_64-osx/622c876310f4588571b30d8bb60c7c1e82d734372539f9bc3380b7821606289e/8.6.5/libexec/x86_64-osx-ghc-8.6.5/brickExample-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/brickExample/.stack-work/install/x86_64-osx/622c876310f4588571b30d8bb60c7c1e82d734372539f9bc3380b7821606289e/8.6.5/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "brickExample_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "brickExample_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "brickExample_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "brickExample_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "brickExample_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "brickExample_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
