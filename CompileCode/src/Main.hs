-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"


-- a b c ' ' e f
-- e f ' ' a b c
-- a b c => a b c
-- a b c ' ' => ' ' a b c
-- a b c ' ' d => d ' ' a b c
-- 0 1 2  3  4
-- 4 3 a  b  c   
-- a b c ' ' d ' ' e
-- e ' ' d ' ' a b c
-- 0  1  2  3  4 5 6
-- 6  5  4  3  2 1 0

        
runJava::FilePath -> IO ExitCode
runJava p = do
        home <- getEnv "HOME"
        pp home
        pwd <- getPwd
        let jar = home </> "myfile/bitbucket/javalib/jar/*"
        pp $ "jar=" ++ jar
        let slash = take 1 p
        let absPath = if slash == "/" then  p else pwd </> p
        let dir = takeDirectory absPath 
        let jclass = (dropExtension . takeFileName) absPath 
        let cmd = "java -cp " ++ jar ++ ":" ++ dir ++ " " ++ jclass
        pp cmd
        exCode <- sys cmd
        return exCode


-- zo - open
-- za - close

main = do 
        -- exCode <- compileJava "/Users/cat/myfile/bitbucket/java/try_kkf.java"
        argList <- getArgs
        if len argList == 1 then do
          let jfile = head argList
          exCode <- compileJava "/Users/cat/myfile/bitbucket/javalib/Aron.java"
          case exCode of
                  ExitSuccess -> print "Nice ExitSuccess"
                  ExitFailure x -> print "Failture"
          exCode <- runJava jfile
          case exCode of
                  ExitSuccess -> print "Nice ExitSuccess"
                  ExitFailure x -> print "Failture"
        else pp "Need one argument: absolute or current java file"
