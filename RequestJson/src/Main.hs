{-# LANGUAGE OverloadedStrings #-}
import Network.HTTP.Client
import Network.HTTP.Types.Status (statusCode)
import Data.Aeson (object, (.=), encode, decode)
import Data.Text (Text)
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process


  
import AronModule


-- KEY: http request, request json, insert code, insertcode

{-|

  data CommandService = CommandService{cmdServ::String, paramArg::String, inputData::String} deriving (GEN.Generic, Show)
     instance DA.FromJSON CommandService
     instance DA.ToJSON CommandService where
     toEncoding = DA.genericToEncoding DA.defaultOptions

   RequestJson alignment
   RequestJson commentcode   java 
   RequestJson uncommentcode java

   OK:
   RequestJson http://localhost:8080 commandservice alignment "#"
   [ "http://localhost:8080"
   , "commandservice"
   , "alignment"
   , "#"
   ]

   OK:
   RequestJson http://localhost:8080 commandservice alignment '#'
   [ "http://localhost:8080"
   , "commandservice"
   , "alignment"
   , "#"
   ]

   ERROR: Missing '#' in the argument list
   RequestJson http://localhost:8080 commandservice alignment #
                                                              ↑
                                                              |→ $j/Alignment.java  did not use the argument for now
   [ "http://localhost:8080"
   , "commandservice"
   , "alignment"
   ]
-}
main :: IO ()
main = do
       -- let host = "http://localhost:8081/" ++ "commandservice"
       argls <- getArgs

       logFileTmp argls
       -- pre argls
       -- let paraArg' = if len argls == 1 then head argls else ""
       let (url,cmdCall,cmdServ',paramArg') = case len argls of
                                                n | n == 3 -> (head argls, (head . tail) argls, last argls, "")
                                                  | n == 4 -> (head argls, (head . tail) argls, head $ drop 2 argls, last argls)
                                                  | otherwise -> error "ERROR: Invalid number of arguments"
 
       tmpfile <- getEnv "mytmp"   -- $HOME/myfile/bitbucket/tmp/x.x
       str <- readFileStr tmpfile
       let updateCodeBlock = UpdateCodeBlock{pid = 0, newcode = str , begt = 0, endt = 0}
       let commandService = CommandService{cmdServ = cmdServ', paramArg = paramArg', inputData = str}
       {-|
                                                        ↑                     ↑             ↑→ source code
                                                        |                     |
                                                        |                     | → haskell
                                                        |
                                                        | → [alignment, commandcode, uncommentcode etc]
       -}
       manager <- newManager defaultManagerSettings
       -- let requestObject = object[ "name" .= ("Michael" :: Text) , "age"  .= (30 :: Int)]
  
       -- initialRequest <- parseRequest "http://localhost:8081/commandservice"
       initialRequest <- parseRequest $ url + "/" + cmdCall
       -- let request = initialRequest { method = "POST", requestBody = RequestBodyLBS $ encode requestObject }
       -- let request = initialRequest { method = "POST", requestBody = RequestBodyLBS $ encode updateCodeBlock }
       let request = initialRequest { method = "POST", requestBody = RequestBodyLBS $ encode commandService }

       response <- httpLbs request manager
       -- putStrLn $ "The status code was: " ++ (show $ statusCode $ responseStatus response)
       let rep = responseBody response
       -- pre rep
       let may = decode rep :: Maybe ReplyCode
       let strout = case may of
                      Just s -> stdoutx s
                      Nothing -> "ERROR: nothing"
       putStr $ toStr strout
 where
   (+) = (++)
