-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

import AronModule 

import Codec.Picture( PixelRGBA8( .. ), writePng )
import Graphics.Rasterific
import Graphics.Rasterific.Texture
import Graphics.Text.TrueType( loadFontFile )
import Codec.Picture.Types

{-|
main :: IO ()
main = do

  let white = PixelRGBA8 255 255 255 255
      drawColor = PixelRGBA8 0 0x86 0xc1 255
      recColor = PixelRGBA8 0xFF 0x53 0x73 255
      img = renderDrawing 400 200 white $
         withTexture (uniformTexture drawColor) $ do
            fontErr <- loadFontFile "/System/Library/Fonts/SFCompactItalic.ttf"
            case fontErr of
              Left err -> putStrLn err
              Right font -> do
                fill $ circle (V2 0 0) 30
                stroke 4 JoinRound (CapRound, CapRound) $
                       circle (V2 400 200) 40
                withTexture (uniformTexture recColor) .
                       fill $ rectangle (V2 100 100) 200 100
                withTexture (uniformTexture recColor) . 
                       fill $ rectangle (V2 10 10) 50 50
                withTexture (uniformTexture $ PixelRGBA8 0 0 0 255) $
                  printTextAt font (PointSize 12) (V2 20 40)
                "Text"
  writePng "yourimage.png" img

-}
  
import Codec.Picture( PixelRGBA8( .. ), writePng )
import Graphics.Rasterific
import Graphics.Rasterific.Texture

{-|
 fun:: IO () -> IO()
 fun x = do
         xx
         xx
-}

-- KEY: print text, draw rectangle
main :: IO ()
main = do
  -- fontErr <- loadFontFile "test_fonts/DejaVuSans.ttf"
  fontErr <- loadFontFile "/System/Library/Fonts/SFCompactItalic.ttf"
  let img = let font = case fontErr of
                         Left err -> error err
                         Right fontx -> fontx
                x = 3
            in renderDrawing 1000 1000 (PixelRGBA8 255 255 255 255)
              . withTexture (uniformTexture $ PixelRGBA8 0 0 0 255) $ do
        
                  printTextAt font (PointSize 40) (V2 10 200) "Welcome to my Channel \x3304"
  
                  withTexture (uniformTexture $ PixelRGBA8 3 3 3 255) .
                      fill $ rectangle (V2 20 20) 50 50
  


  print (show $ imageWidth img)
  print (show $ imageHeight img)
  writePng "f.png" img
  putStrLn "Ok"                       
