-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
{-# LANGUAGE RankNTypes #-} 
-- {-# LANGUAGE OverloadedStrings #-}

-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import System.Console.ANSI

import qualified Text.Regex.TDFA as TD
import qualified Data.Text                 as TS

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}
import           Data.Int (Int64)
import           Database.SQLite.Simple
import           Database.SQLite.Simple.FromRow
import           Database.SQLite.Simple.FromField
import           Database.SQLite.Simple.ToField
import           Database.SQLite.Simple.Internal
import           Database.SQLite.Simple.Ok

import AronModule 

{-| 
    === Update to fix Non-ASCII char error in dot_bash_history_test (Wed Mar 11 15:16:58 2020) 

-} 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

-- filePath="/tmp/b.x"
-- filePath="/Users/cat/myfile/bitbucket/shell/dot_bash_history_test"
-- filePath="/Users/cat/myfile/bitbucket/testfile/test.txt"
 
-- The file is not used here, all the data are from sqlite3 database
filePath="/Users/cat/myfile/bitbucket/shell/dot_bash_history"

{-| 
    === help function
    * filter out lines
    
    > '^ls'
-} 
--filterPatter = ["^rm", "^ls", "^which", "^stack", "^#", "^gg", "^git",
--                "^spaceemacs"
--               ]

filterPatter = ["^rm", "^ls", "^which", "^#", "^gg", "^git",
                "^spaceemacs"
               ]

numTab = 2

helpme::IO()
helpme = do
          psTab 4 "cmd -r '^ls'                    "
          psTab 4 "cmd -r 'ls'                     " 
          psTab 4 "cmd -r 'ls'                     " 
          psTab 4 "cmd -s awk  => sorted and search" 
          psTab 4 "cmd awk grep => contain awk grep" 
          psTab 4 "sort unique trim                " 
          pre filterPatter

-- KEY: shell input, shell remove newline, shell no newline  
-- $HOME/myfile/bitbucket/cmake/OutputToShell/write_to_shell.c
toShell = "write_to_shell"


enter::Integer -> IO()
enter n = putStr $ box ++ "Enter num or String: "
        where
            box = "[" ++ (show n) ++ "]"

stopWatch::Integer  -> IO Integer
stopWatch old = do
                now <- timeNowSecond
                return $ now - old

loop::[String] -> [String] -> [String] -> IO()
loop alist cxx cx   = do
            if len alist > 0 
            -- input is number such as 0, 1, 2, .. 
            then do    
                 let h = head alist -- h is 0, 1, 2, ..
                 if isZeroPos h 
                 then do
                      let inx = strToInt h 
                      let cmd = cx !! inx 
                      runRawCmd toShell [cmd]  -- toShell = write_to_shell.c
                      return ()
                 else do  -- input is option such as -h 
                      case  alist of
                            ("-h":_)   -> helpme
                            ("-r":c:_) -> do 
                                          -- remove c from cx
                                          let lss  = filter( not . containStr c ) cx

                                          let tuple = tupleList lss 
                                          showTuple tuple 
                                          enter (len tuple)
                                          key <- getLineFlush
                                          let alist = splitSPC key
                                          loop alist cxx lss
                            ("-o":c:_) -> do 
                                          let lss  = filter( containStr c ) cxx 
                                          let tuple = tupleList lss 
                                          showTuple tuple 
                                          enter (len tuple)
                                          key <- getLineFlush
                                          let alist = splitSPC key
                                          loop alist cxx lss
                            ("-s":c:_) -> do  
                                          -- let sortedList = qqsort(\x y -> len x < len y) cx 
                                          let scoreList = matchScoreList c cx
                                          let sortedList = qqsort(\x y -> fst x < fst y) scoreList 
                                          let lss  = filter( containStr c ) $ map snd sortedList

                                          let tuple = tupleList lss 
                                          showTuple tuple 
                                          enter (len tuple)
                                          key <- getLineFlush
                                          let alist = splitSPC key
                                          loop alist cxx lss 
                            _          -> do                
                                          -- input is 'stack new'
                                          let c = concatStr alist " "  -- ["new", "stack"] => "new stack"
                                          let scoreList = matchScoreList c cx  -- [(2, "new stack")]
                                          let sortedList = qqsort(\x y -> fst x < fst y) scoreList
                                          -- let newList = filterList id alist cx
                                          let newList = filterList id alist $ snd <$> sortedList 
                                          let tuple = tupleList newList 
                                          showTuple tuple 
                                          enter (len tuple)
                                          key <- getLineFlush
                                          let alist = splitSPC key
                                          loop alist cxx newList 
            else do  
                 -- no input, reset the whole list
                 let tuple = tupleList cxx 
                 showTuple $ tuple 
                 enter (len tuple)
                 key <- getLineFlush
                 let alist = filterNonEmpty . splitSPC $ key
                 loop alist cxx cxx 

{-| 
    === Numbers commands
-} 
tupleList::[String] -> [(Integer, String)]
tupleList cx =  let n = len cx in reverse $ zip [0..] cx 

tupleStr::(Integer, String) -> String
tupleStr (a, b) =  (show a) ++ " -> " ++ b

showTuple::[(Integer, String)] -> IO()
showTuple cx = mapM_ (\x -> ((psTab numTab) . tupleStr) x ) cx 

sortedByMatchedStrIndex input cx = qqsort(\a b -> let la = matchIndex input a 
                                                      lb = matchIndex input b 
                                                  in la < lb ) cx 

                where
                    matchIndex q s = case matchAny q s of
                                       Just x -> fst x
                                       _      -> 10 
                                        
    
dbfile = "myfile/bitbucket/database/ShellHistory.db"
main = do 
        argList <- getArgs
        dbfileP <- getEnv "HOME" >>= \x -> return $ x </> dbfile
        cmdList <- queryShellHistory dbfileP
        -- pre cmdList
        -- ls <- readFileLatin1ToList filePath 
        -- read from ShellHistory table
        -- let nonList = containNonAsciiList ls
        -- If non-ascii is not filtered out, there is ERROR in Regex
        let cmdList' = filter(\x -> containAscii x) cmdList
        let nonList = containNonAsciiList cmdList' 
        -- check whether file contain non-ASCII char
        -- if null nonList then do
        if True then do
            -- let lss = unique $ filter (\x -> len x > 0) $ map trim ls
            let lss = unique $ filter (\x -> len x > 0) $ map trim cmdList' 
            let flist = filterList not filterPatter lss
            loop argList flist flist 
        else do
            pp $ "Error Contain Non Ascii Character"
            pre nonList
