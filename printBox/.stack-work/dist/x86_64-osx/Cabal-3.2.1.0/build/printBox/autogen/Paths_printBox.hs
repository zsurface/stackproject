{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_printBox (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/printBox/.stack-work/install/x86_64-osx/0ce9b9f7b19e12ee4f2645cafd1b4f881faa49becb098fa49ba8837fbe438ef3/8.10.4/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/printBox/.stack-work/install/x86_64-osx/0ce9b9f7b19e12ee4f2645cafd1b4f881faa49becb098fa49ba8837fbe438ef3/8.10.4/lib/x86_64-osx-ghc-8.10.4/printBox-0.1.0.0-9mD6bTjmapS1eNopVPaJ8c-printBox"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/printBox/.stack-work/install/x86_64-osx/0ce9b9f7b19e12ee4f2645cafd1b4f881faa49becb098fa49ba8837fbe438ef3/8.10.4/lib/x86_64-osx-ghc-8.10.4"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/printBox/.stack-work/install/x86_64-osx/0ce9b9f7b19e12ee4f2645cafd1b4f881faa49becb098fa49ba8837fbe438ef3/8.10.4/share/x86_64-osx-ghc-8.10.4/printBox-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/printBox/.stack-work/install/x86_64-osx/0ce9b9f7b19e12ee4f2645cafd1b4f881faa49becb098fa49ba8837fbe438ef3/8.10.4/libexec/x86_64-osx-ghc-8.10.4/printBox-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/printBox/.stack-work/install/x86_64-osx/0ce9b9f7b19e12ee4f2645cafd1b4f881faa49becb098fa49ba8837fbe438ef3/8.10.4/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "printBox_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "printBox_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "printBox_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "printBox_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "printBox_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "printBox_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
