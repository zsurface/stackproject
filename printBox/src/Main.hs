-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 
import System.Console.Pretty (Color (..), Style (..), bgColor, color, style, supportsPretty)
  
import qualified Data.ByteString.UTF8      as BSU
import qualified Data.ByteString           as BS  -- Strict ByteString
       
import qualified Text.Regex.TDFA as TD
import qualified Graphics.Text.Width as TW

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

cstr = colorfgStr

printBoxColor2'::Integer -> Integer -> [String] -> IO()
printBoxColor2' colorCode ntab cx = do
    psTab ntab (color colo top)
    mapM_ (\x -> do
              psTab ntab ((vl + x + (replicate (m - len x)) ' ') + vr)
              --            ↑                                                              ↑ 
              --            + -> vertical left '|'                                         + -> vertical right '|'
          ) cx
    psTab ntab (color colo bot)
  where
    -- len' = BS.length . BSU.fromString
    lt = len . trim 
    m = maxList $ map lt cx
    (+) = (++)
    top = "┌" + (concat $ replicate m "─") + "┐"
    bot = "└" + (concat $ replicate m "─") + "┘"
    -- rep' n c = concat $ replicate n c
    vl = color colo "│"
    -- vr = color colo ""
    vr = color colo "|"
    colo = White

{-|
-}
main = do 
        argList <- getArgs
        if len argList == 2 then do
          let ntab = strToInteger $ head argList
          let ls = lines $ last argList
          -- printBoxColor Cyan ntab ls
          printBoxColor2' 254 ntab ls
          else if len argList == 3 then do
          let ntab = strToInteger $ head argList
          let ls = (lines . head . init . tail)  argList
          let colorCode = strToInteger $ last argList
          printBoxColor2' colorCode ntab ls
          else printBox 4 ["Need two arguments",
                           "printBox 4 'dog'$'\\n''cat'",
                           "         ↑                  ",
                           "         |→ 4 tabs         ",
                           "                           ",
                           "                           ",
                           "Three arguments",
                           "printBox 4 'dog'$'\\n''cat' 200  ",
                           "         ↑                   ↑    ",
                           "         |→ 4 tabs           |    ",
                           "                             |→ (0-255) color code ",
                           "",
                           "printBox 4 'dog\\ncat' => DOES NOT WORK"
                          ]
