{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_haskellffi (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/haskellffi/.stack-work/install/x86_64-osx/1b8b6d818d73744014303694cd3c3c35b8fb48eb385653399a014413c4194c99/8.10.3/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/haskellffi/.stack-work/install/x86_64-osx/1b8b6d818d73744014303694cd3c3c35b8fb48eb385653399a014413c4194c99/8.10.3/lib/x86_64-osx-ghc-8.10.3/haskellffi-0.1.0.0-8N85jYu9EKl7GnJ1OWNRRZ-haskellffi"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/haskellffi/.stack-work/install/x86_64-osx/1b8b6d818d73744014303694cd3c3c35b8fb48eb385653399a014413c4194c99/8.10.3/lib/x86_64-osx-ghc-8.10.3"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/haskellffi/.stack-work/install/x86_64-osx/1b8b6d818d73744014303694cd3c3c35b8fb48eb385653399a014413c4194c99/8.10.3/share/x86_64-osx-ghc-8.10.3/haskellffi-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/haskellffi/.stack-work/install/x86_64-osx/1b8b6d818d73744014303694cd3c3c35b8fb48eb385653399a014413c4194c99/8.10.3/libexec/x86_64-osx-ghc-8.10.3/haskellffi-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/haskellffi/.stack-work/install/x86_64-osx/1b8b6d818d73744014303694cd3c3c35b8fb48eb385653399a014413c4194c99/8.10.3/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "haskellffi_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "haskellffi_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "haskellffi_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "haskellffi_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "haskellffi_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "haskellffi_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
