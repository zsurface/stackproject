-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent
import System.Timeout -- timeout

import qualified Text.Regex.TDFA as TD



import System.Timeout
import Criterion.Measurement
import System.IO.Unsafe
import System.Process
import Control.Exception
import System.IO
import System.IO.Error
import GHC.IO.Exception
import System.Exit
import Control.Concurrent.MVar
import Control.Concurrent
import System.CPUTime


import System.Timeout
import Criterion.Measurement
import System.IO.Unsafe
import System.Process

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule
import AronAlias

import Control.Concurrent
import Control.Exception

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

-- KEY: createProcess example, timeout example, process timeout example

timeoutIterate msec f x = do
    mvar <- newMVar x
    let loop = do
           x <- takeMVar mvar
           evaluate (f x) >>= putMVar mvar
           loop
    thread <- forkIO loop
    threadDelay msec
    u <- takeMVar mvar
    killThread thread
    return u


timeoutP :: Int -> IO a -> IO (Maybe a)
timeoutP t fun = timeout t $ fun

mytest :: Int -> IO String
mytest n = do
  x <- runOnExternalProgram $ n * 1000
  return $ x ++ ". Indeed."

mytest1 :: Int -> IO String
mytest1 n = do
  x <- runOnExternal $ n * 1000
  return $ x ++ ". mytest1."

    
runOnExternalProgram :: Int -> IO String
runOnExternalProgram n = 
    -- convert the input to a parameter of the external program
    let x = show $ n + 12
    in bracketOnError
        -- (createProcess (proc "sleep" [x]){std_in = CreatePipe
                                         -- ,std_out = CreatePipe
                                         -- ,std_err = Inherit})
        (createProcess (proc "/opt/local/bin/pdflatex" ["onepage.tex"]){  cwd = Just "/Users/cat/myfile/bitbucket/math"
                                                    ,std_in = CreatePipe
                                                    ,std_out = CreatePipe
                                                    ,std_err = Inherit})
        (\(Just inh, Just outh, _, pid) -> terminateProcess pid >> waitForProcess pid)

        (\(Just inh, Just outh, _, pid) -> do
          -- fork a thread to consume output
          output <- hGetContents outh
          outMVar <- newEmptyMVar
          forkIO $ evaluate (length output) >> putMVar outMVar ()

          -- no input in this case
          hClose inh

          -- wait on output
          takeMVar outMVar
          hClose outh

          -- wait for process
          ex <- waitForProcess pid

          case ex of
            ExitSuccess -> do
              -- convert the output as needed
              let verboseAnswer = "External program answered: " ++ output
              return verboseAnswer
            ExitFailure r ->
              ioError (mkIOError OtherError ("spawned process exit: " ++ show r) Nothing Nothing) )


runOnExternal:: Int -> IO String
runOnExternal n =
          let pdf = "/opt/local/bin/pdflatex"
              mathPath = "/Users/cat/myfile/bitbucket/math"
              sle = "/bin/sleep"
          in bracketOnError
             -- (createProcess(proc pdf ["try99.tex"]) { cwd = Just mathPath, std_in = CreatePipe, std_out = CreatePipe})
             (createProcess(proc sle ["20"]) { cwd = Just mathPath, std_in = CreatePipe, std_out = CreatePipe})
             (\(Just inh, Just outh, _, pid) -> terminateProcess pid >> waitForProcess pid)
             (\(Just inh, Just outh, _, pid) -> do
                 strout <- hGetContents outh
                 putStrLn "end 1"
                 hClose inh
                 -- hClose outh
                 ex <- waitForProcess pid
                 putStrLn "end 2"
                 case ex of
                   ExitSuccess -> do
                     putStrLn "sleep n sec"
                     mapM putStrLn $ lines strout
                     return "output from ex"
                   ExitFailure r -> ioError (mkIOError OtherError ("spawned process exit: " ++ show r) Nothing Nothing) )
 


main = do
        
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        let cmd = "pdflatex -halt-on-error onepage.tex"
        let pdf = "/opt/local/bin/pdflatex"
        -- (_, Just hOut, _, ph) <- createProcess (proc "/bin/ls"  ["/tmp"]) { cwd = Just "/tmp", std_out = CreatePipe}
        mathPath <- getEnv "m"
        -- (_, Just hOut, _, ph) <- createProcess (proc pdf  ["-halt-on-error", "onepage.tex"]) { cwd = Just mathPath, std_out = CreatePipe}
        
        -- timeout::Int -> IO a -> IO (Maybe a)
        -- (_, Just hOut, _, ph) <- createProcess (proc pdf ["onepage.tex"]) { cwd = Just mathPath, std_out = CreatePipe}
        -- mayout <- timeout 4000000 $ hGetContents hOut
        -- ec <- waitForProcess ph
        -- case mayout of
          -- Just out ->
            -- if ec == ExitSuccess
            -- then do
              -- putStrLn "END /bin/sleep 3 second"
              -- print "createProcess process ExitSuccess"
              -- mapM putStrLn $ lines out
              -- return ()
            -- else error $ "createProcess process failed:" ++ show ec
          -- _        -> putStrLn "mayout Nothing"

        mvar <- newEmptyMVar
          
        -- let loop = do
              -- evaluate (createProcess(proc pdf ["onepage.tex"]) { cwd = Just mathPath, std_in = CreatePipe, std_out = CreatePipe}) >>= \x -> x >>= putMVar mvar
              -- (Just inp, Just hOut, _, ph) <- takeMVar mvar

              -- strout <- hGetContents hOut
              -- putStrLn "beg 1"
              -- exx <- evaluate $ waitForProcess ph
              -- putStrLn "end 1"
              -- ec <- exx
              -- putStrLn "end 2"
              -- if ec == ExitSuccess
                -- then do
                -- putStrLn "sleep n sec"
                -- mapM putStrLn $ lines strout
                -- return ()
                -- else error $ "createProcess failed:" ++ show ec
            
              -- loop

        -- let loop = do
              -- evaluate (createProcess(proc pdf ["onepage.tex"]) { cwd = Just mathPath, std_in = CreatePipe, std_out = CreatePipe}) >>= \x -> x >>= putMVar mvar
              -- (Just inp, Just hOut, _, ph) <- takeMVar mvar

              -- strout <- hGetContents hOut
              -- putStrLn "beg 1"
              -- ec <- waitForProcess ph
              -- putStrLn "beg 2"
              -- if ec == ExitSuccess
                -- then do
                -- putStrLn "sleep n sec"
                -- mapM putStrLn $ lines strout
                -- return ()
                -- else error $ "createProcess failed:" ++ show ec
            
              -- loop    
        -- thread <- forkIO loop
        -- threadDelay 3000000
        -- killThread thread 
        -- print "OK"
            
        -- x <- timeout (1 * 4000000) $ mytest 10
        -- putStrLn $ show x

        y <- timeout 5000000 $ runOnExternalProgram 3
        putStrLn $ show y
        -- x1 <- timeoutP (1 * 4000000) $ mytest1 3
        -- putStrLn $ show x1
        
