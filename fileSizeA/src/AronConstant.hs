module AronConstant where

import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import qualified Graphics.UI.GLFW as G
import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.IORef 
import qualified Data.Set as S 

-- import Graphics.UI.GLUT
import Graphics.UI.GLUT.Callbacks.Global
import AronModule  
import AronGraphic 

p0 = Vertex3 0 0   0::Vertex3 GLfloat
p1 = Vertex3 0.5 0 0::Vertex3 GLfloat
p2 = Vertex3 0 0.5 0::Vertex3 GLfloat
p3 = Vertex3 0.5 0.5 0::Vertex3 GLfloat
