-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

-- BEG
-- KEY: ansi color, console color
import Rainbow
import System.Console.Pretty (Color (..), Style (..), bgColor, color, style, supportsPretty)
-- https://hackage.haskell.org/package/ansi-terminal-0.10.3/docs/System-Console-ANSI.html
import System.IO (hFlush, stdout)
import qualified System.Console.ANSI as AN
-- END


import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 
import AronDevLib

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close


-- KEY: See file:////Library/WebServer/Documents/xfido/pdf/unicode_box_drawing.pdf

-- printBox:: Integer -> [String] -> IO()
-- printBox n cx = do
    -- psTab n (color Green top)
    -- mapM_ (\x -> do
              -- psTab n ((vl + (color Yellow x) +  (rep (m - len x) ' ') + vr))
          -- ) cx
    -- psTab n (color Green bot)
  -- where
    -- m = maxList $ map len cx
    -- (+) = (++)
    -- top = "┌" + (rep' m "─") + "┐"
    -- bot = "└" + (rep' m "─") + "┘"
    -- rep = replicate
    -- rep' n c = concat $ replicate n c
    -- vl = color Green "│"
    -- vr = color Green "│"
    
-- ⎤
-- RIGHT SQUARE BRACKET UPPER CORNER
-- Unicode: U+23A4, UTF-8: E2 8E A4

-- ⎡
-- LEFT SQUARE BRACKET UPPER CORNER
-- Unicode: U+23A1, UTF-8: E2 8E A1

-- ⎦
-- RIGHT SQUARE BRACKET LOWER CORNER
-- Unicode: U+23A6, UTF-8: E2 8E A6
                 
-- ⎣
-- LEFT SQUARE BRACKET LOWER CORNER
-- Unicode: U+23A3, UTF-8: E2 8E A3

-- ∣
-- DIVIDES
-- Unicode: U+2223, UTF-8: E2 88 A3
                 
-- ↴
-- RIGHTWARDS ARROW WITH CORNER DOWNWARDS
-- Unicode: U+21B4, UTF-8: E2 86 B4

-- printBox1:: Integer -> [String] -> IO()
-- printBox n cx = do
    -- psTab n (color Green top)
    -- mapM_ (\x -> do
              -- psTab n ((vl + (color Yellow x) + (replicate (m - len x) ' ') + vr))
          -- ) cx
    -- psTab n (color Green bot)
  -- where
    -- m = maxList $ map len cx
    -- (+) = (++)
    -- top = "┌" + (concat $ replicate m "─") + "┐"
    -- bot = "└" + (concat $ replicate m "─") + "┘"
    -- rep' n c = concat $ replicate n c
    -- vl = color Green "│"
    -- vr = color Green "│"
     

main = do 
        print "Hello World"
        let ls = ["dog", "Proceed", "Concrete", "Neascus"]
        printBox 4 ls
