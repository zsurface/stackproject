{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_createpipe (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/createpipe/.stack-work/install/x86_64-osx/6e06b755f6b184f86519062d90cda6a3a307acb46baff716277bf144ab543b12/8.10.4/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/createpipe/.stack-work/install/x86_64-osx/6e06b755f6b184f86519062d90cda6a3a307acb46baff716277bf144ab543b12/8.10.4/lib/x86_64-osx-ghc-8.10.4/createpipe-0.1.0.0-EGEgU6a1gsjGzebLEDWvOS-createpipe"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/createpipe/.stack-work/install/x86_64-osx/6e06b755f6b184f86519062d90cda6a3a307acb46baff716277bf144ab543b12/8.10.4/lib/x86_64-osx-ghc-8.10.4"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/createpipe/.stack-work/install/x86_64-osx/6e06b755f6b184f86519062d90cda6a3a307acb46baff716277bf144ab543b12/8.10.4/share/x86_64-osx-ghc-8.10.4/createpipe-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/createpipe/.stack-work/install/x86_64-osx/6e06b755f6b184f86519062d90cda6a3a307acb46baff716277bf144ab543b12/8.10.4/libexec/x86_64-osx-ghc-8.10.4/createpipe-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/createpipe/.stack-work/install/x86_64-osx/6e06b755f6b184f86519062d90cda6a3a307acb46baff716277bf144ab543b12/8.10.4/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "createpipe_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "createpipe_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "createpipe_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "createpipe_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "createpipe_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "createpipe_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
