{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_haskell_stm (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/haskell-stm/.stack-work/install/x86_64-osx/c663fe2d8cd423f77905142c2fecfb5e70d060126d511d0d1235f8b07023a3e4/8.10.3/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/haskell-stm/.stack-work/install/x86_64-osx/c663fe2d8cd423f77905142c2fecfb5e70d060126d511d0d1235f8b07023a3e4/8.10.3/lib/x86_64-osx-ghc-8.10.3/haskell-stm-0.1.0.0-27q9EBNKk289Ia8kiTTBmu-haskell-stm"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/haskell-stm/.stack-work/install/x86_64-osx/c663fe2d8cd423f77905142c2fecfb5e70d060126d511d0d1235f8b07023a3e4/8.10.3/lib/x86_64-osx-ghc-8.10.3"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/haskell-stm/.stack-work/install/x86_64-osx/c663fe2d8cd423f77905142c2fecfb5e70d060126d511d0d1235f8b07023a3e4/8.10.3/share/x86_64-osx-ghc-8.10.3/haskell-stm-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/haskell-stm/.stack-work/install/x86_64-osx/c663fe2d8cd423f77905142c2fecfb5e70d060126d511d0d1235f8b07023a3e4/8.10.3/libexec/x86_64-osx-ghc-8.10.3/haskell-stm-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/haskell-stm/.stack-work/install/x86_64-osx/c663fe2d8cd423f77905142c2fecfb5e70d060126d511d0d1235f8b07023a3e4/8.10.3/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "haskell_stm_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "haskell_stm_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "haskell_stm_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "haskell_stm_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "haskell_stm_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "haskell_stm_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
