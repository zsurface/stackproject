-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD
import GHC.Conc 
import Control.Monad.STM
import Control.Concurrent.STM.TVar

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close



-- KEY: TVar, stm, atomically, software transaction memory
runSimulation :: IO ()
runSimulation = do
  -- Start with 0
  wallet <- newTVarIO 0
  cashRegister <- newTVarIO 100

  -- Get paid at the same time
  _ <- forkIO $ getPaid wallet

  -- Try to make a donut-money transaction
  atomically $ do
    myCash <- readTVar wallet
    -- Check if I have enough money
    check $ myCash >= donutPrice
    -- Subtract the amount from my wallet
    writeTVar wallet (myCash - donutPrice)
    storeCash <- readTVar cashRegister
    -- Add the amount to the cash register
    writeTVar cashRegister (storeCash + donutPrice)

  myFinal <- readTVarIO wallet
  print $ "I have: $" ++ show myFinal

  storeFinal <- readTVarIO cashRegister
  print $ "Cash register: $" ++ show storeFinal
 where
  donutPrice = 3

-- Put $1 in my wallet every second
getPaid :: TVar Int -> IO ()
getPaid wallet = forever $ do
  threadDelay $ 1000 * 1000
  atomically $ modifyTVar wallet (+ 1)
  atomically $ writeTVar wallet 1
  print "I earned a dollar"

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        pp "done!"
        runSimulation
        pp "nice"
