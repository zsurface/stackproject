{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RankNTypes #-}

import AronSimple
import Data.List
import Control.Monad
import Control.Applicative
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import Data.Typeable
import AronDevLib
import Language.Haskell.Interpreter


import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 


(≡) ::forall a b c.(Typeable a, Num a, Typeable b, Num b, Integral c) => a -> b -> c
(≡) x y = floor(x ÷ y)
          
(%:) ::forall a b c.(Typeable a, Num a, Typeable b, Num b, Integral c) => a -> b -> c
(%:) = (≡)

       
{-|
(×)::forall a b c. (Typeable a, Num a, Typeable b, Num b, Fractional c) => a -> b -> c
(×) x y = 3
  where
    x' = case cast x of
            Just (x::Float) -> rf x
            _             -> case cast x of
                                Just (x::Integer) -> rf x
                                _                 -> case cast x of
                                                       Just (x::Double) -> x
                                                       _               -> case cast x of
                                                                            Just (x::Int) -> rf x
                                                                            _                -> error "Unknown type"
    y' = case cast y of
            Just (y::Int) -> rf y
            _             -> case cast y of
                                Just (y::Integer) -> rf y
                                _                 -> case cast y of
                                                       Just (y::Float) -> y
                                                       _               -> case cast y of
                                                                            Just (y::Double) -> rf y
                                                                            _                -> error "Unknown type"

    rf = realToFrac

-}


errorString :: InterpreterError -> String
errorString (WontCompile es) = intercalate "\n" (header : map unbox es)
  where
    header = "ERROR: Won't compile:"
    unbox (GhcError e) = e
errorString e = show e

say::String -> Interpreter()
say = liftIO . putStrLn

emptyLine::Interpreter()
emptyLine = say ""


testHint::[String] -> String -> Interpreter()
testHint xs str = do
        loadModules ["/Users/cat/myfile/bitbucket/stackproject/HelloSimple/src/AronSimple.hs"]
        setImportsQ [("Prelude", Nothing), ("Data.Map", Just "M"), ("AronSimple", Nothing)]        
        let stmts = xs
        forM_ stmts $ \s -> do
          say $ " " ++ s
          runStmt s        
        emptyLine


        let expr3 = str
        -- let expr3 = "\\(x, y) -> x + 1 + y"
        fun <- interpret expr3 (as :: (Int, Int) -> Int)
        say $ show $ fun (1, 2)

        -- liftIO $ drawCurve (\x -> x*x) (0.0, 1.0) red
          
        emptyLine
            
main = do
        print "hk"
        let n = 4::Int
        let m = 3::Int
        let p = 5::Integer
        let q = 6::Integer
        {- print $ (÷) (rf p) (rf n) -}
        {- print $ (÷) p q -}
        print $ "ok"
        let a1 = 3::Int
        let a2 = 4::Integer
        let c1 = 5::Float
        let d1 = 6::Double
        
        print $ a1 ÷ a2
        print $ a2 ÷ a1
        print $ c1 ÷ a1
        print $ a1 ÷ c1
        print $ c1 ÷ a2
        print $ a2 ÷ c1
        print $ a1 ÷ d1
        print $ d1 ÷ a1
        
        print $ show $ a1 ÷ (0.3::Float)
        print "Typeable"
        print $ (4.3::Double) ÷ (0.3::Float)
        print $ (4.3::Float) ÷ (0.3::Double)
        print $ (3::Int) ÷ (0.3::Double)
        print $ (3::Int) ÷ (0.3::Float)
        print $ sin $ (0.3::Float) ÷ (3::Int)
        print $ floor(a2 ÷ a1)
        print $ (≡) a2 d1
        print $ a2 %: d1
        print $ (3::Int) %: (4.0::Float)
        let n = 3::Int
            m = 4.3::Float
            q = 3.4::Double
            δ = 1.0::Float
          in print $ δ * sin(m ÷ n)
        {- print $ a1 ÷ 4 -}
        let e1 = 3::Int
        let e2 = 4::Float
        print $ e1 × e2
        print $ sin $ (×) e1 ((e2 ÷ e2)::Double)
        print "hi"
                
        strls <- readFileList "/tmp/tmpfile.txt"
        pp strls

        {-|
        input <- getLine

        mapM_ (\exp -> do
                  r <- runInterpreter $ testHint [input] exp
                  case r of
                    Left err -> putStrLn $ errorString err
                    Right () -> return ()
              ) strls
        -}
        print "ok"




          
        -- let maytag = (n ÷ m)::Float
        -- let t = maytag ÷ n
        -- print t
       
  where
    rf = realToFrac
