{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_MandelbrotSet (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/MandelbrotSet/.stack-work/install/x86_64-osx/331f1b663a6384d6f190995b886f93ecb2d949ea5bf138d05ce609786a846a3a/8.10.4/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/MandelbrotSet/.stack-work/install/x86_64-osx/331f1b663a6384d6f190995b886f93ecb2d949ea5bf138d05ce609786a846a3a/8.10.4/lib/x86_64-osx-ghc-8.10.4/MandelbrotSet-0.1.0.0-FVIBNU5EfgHKvYfeWH5W17-MandelbrotSet"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/MandelbrotSet/.stack-work/install/x86_64-osx/331f1b663a6384d6f190995b886f93ecb2d949ea5bf138d05ce609786a846a3a/8.10.4/lib/x86_64-osx-ghc-8.10.4"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/MandelbrotSet/.stack-work/install/x86_64-osx/331f1b663a6384d6f190995b886f93ecb2d949ea5bf138d05ce609786a846a3a/8.10.4/share/x86_64-osx-ghc-8.10.4/MandelbrotSet-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/MandelbrotSet/.stack-work/install/x86_64-osx/331f1b663a6384d6f190995b886f93ecb2d949ea5bf138d05ce609786a846a3a/8.10.4/libexec/x86_64-osx-ghc-8.10.4/MandelbrotSet-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/MandelbrotSet/.stack-work/install/x86_64-osx/331f1b663a6384d6f190995b886f93ecb2d949ea5bf138d05ce609786a846a3a/8.10.4/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "MandelbrotSet_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "MandelbrotSet_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "MandelbrotSet_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "MandelbrotSet_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "MandelbrotSet_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "MandelbrotSet_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
