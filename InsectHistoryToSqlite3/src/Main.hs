-- {-# LANGUAGE OverloadedStrings #-}
-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import           Data.Int (Int64)
import           Database.SQLite.Simple
import           Database.SQLite.Simple.FromRow
import           Database.SQLite.Simple.FromField
import           Database.SQLite.Simple.ToField
import           Database.SQLite.Simple.Internal
import           Database.SQLite.Simple.Ok
--import qualified Data.Text as TS    -- TS.Text
--import qualified Text.Regex.TDFA as TD

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

--data ShellHistory = ShellHistory 
--  { shId :: Int64
--  , shcmd :: TS.Text
--  } deriving (Eq,Read,Show)
--
---- | import Database.SQLite.Simple.FromRow
---- | two fields: shId, shcmd
--instance FromRow ShellHistory where
--   fromRow = ShellHistory <$> field <*> field
--
---- | import Database.SQLite.Simple.ToRow
--instance ToRow ShellHistory where
--   toRow (ShellHistory _shId shcmd) = toRow (Only shcmd)

hisdb  = "myfile/bitbucket/database/ShellHistory.db"
-- hisFile = "/Users/cat/myfile/bitbucket/shell/dot_bash_history_test"
bashHis = "myfile/bitbucket/shell/dot_bash_history"
 
main = do 
        now1 <- timeNowSecond
        hisdbP <- getEnv "HOME" >>= \x -> return $ x </>  hisdb
        bashHisP <- getEnv "HOME" >>= \x -> return $ x </>  bashHis
        insertShellHistory bashHisP hisdbP
        now2 <- timeNowSecond
        let sec = now2 - now1
        pre $ "Time Second=" ++ show sec 
        pp "done!"
