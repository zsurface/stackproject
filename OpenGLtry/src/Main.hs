{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE CPP #-}
-- {-# LANGUAGE CPP #-}
module Main where
import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import qualified Graphics.UI.GLFW as G
import Graphics.Rendering.OpenGL.GLU.Matrix as GM
import Graphics.Rendering.OpenGL 
import Graphics.Rendering.OpenGL.GL.CoordTrans
-- import qualified Graphics.Rendering.FTGL as FTGL
import qualified Graphics.UI.GLUT as GLUT
import Graphics.GL.Types
import System.Exit
import System.IO
import Control.Monad
import Data.Set(Set) 
import Data.IORef 
import Data.Int
import Data.Maybe
import qualified Data.Set as S
import qualified Data.List as DL
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import Data.Typeable
import qualified Text.Printf as PR
import Language.Haskell.Interpreter

import GHC.Float.RealFracMethods

import System.Directory
import System.Environment
import System.Exit
import System.IO
import Control.Concurrent

import Data.StateVar
import Foreign.ForeignPtr
import Foreign.Marshal.Alloc
import Foreign.Marshal.Array
import Foreign.Marshal.Utils
import Foreign.Ptr
import Foreign.Storable


-- Hidden modules
-- import Graphics.Rendering.OpenGL.GL.Capability
-- import Graphics.Rendering.OpenGL.GL.Exception
-- import Graphics.Rendering.OpenGL.GL.MatrixComponent
-- import Graphics.Rendering.OpenGL.GL.PeekPoke
-- import Graphics.Rendering.OpenGL.GL.QueryUtils
-- import Graphics.Rendering.OpenGL.GL.Texturing.TextureUnit
-- import Graphics.Rendering.OpenGL.GLU.ErrorsInternal
import Graphics.GL
import AronModule hiding (rw)
import AronGraphic hiding (dist)
import AronOpenGL
import AronDevLib

import qualified Data.Vector as VU

{-|

   | -------------------------------------------------------------------------------- 
   | compile: run.sh  
   | ghc -i/Users/cat/myfile/bitbucket/haskelllib -o file file.hs
   | 
   | KEY: keyboard example, keypress example, modifyIORef example,
   |
   | Tuesday, 09 November 2021 11:56 PST
   |
   | TODO: Combine Cam{..} and Step{..} in one function with Keyboard input
   |     Current issue: try to implement orthogonal projective with key one press
   |                    but Step{..} and Cam{..} are different type class.
   |
   | mainLoop w refCam refStep refCount lssVex
   | keyboardRot refCam refStep (fromIntegral width) (fromIntegral height)

   @
   data Cam = Cam{alpha::Double, beta::Double, gramma::Double, dist::Double} deriving(Show)

   data Step = Step{xx::Double, yy::Double, zz::Double, ww::Double} deriving(Show)
   initCam = Cam{alpha=0.0, beta=0.0, gramma=0.0, dist = 0.0}
   initStep = Step{xx=0.0, yy=0.0, zz=0.0, ww = 0.01}
   @
-}
  
tmpfile = "/tmp/tmpfile.txt"


convexPts::IO [Vertex3 GLfloat]
convexPts = return cx
    where
      cx = [
              Vertex3 0.1   0.1  0
             ,Vertex3 0.2   0.6  0
             ,Vertex3 0.88  0.9  0
             ,Vertex3 0.80  1.0  0
             ,Vertex3 0.25  0.34 0
             ,Vertex3 0.12  0.8  0
             ,Vertex3 1.3   0.12 0
            ]

pointSet:: [Vertex3 GLfloat]
pointSet = cx
    where
      cx = [
              Vertex3 1.3   0.12 0
             ,Vertex3 0.2   0.6  0
             ,Vertex3 0.80  1.0  0
             ,Vertex3 0.25  0.34 0
             ,Vertex3 0.5   0.23 0
             ,Vertex3 0.88  0.9  0
             ,Vertex3 0.1   0.1  0
             ,Vertex3 0.12  0.8  0
            ]

data Tri = Tri {a_::Vertex3 GLfloat, b_::Vertex3 GLfloat, c_::Vertex3 GLfloat} deriving(Show)

data TriX = TriX{arr_ :: [Vertex3 GLfloat]} deriving (Show)

sortTriX::TriX -> TriX
sortTriX t = TriX {arr_ = qqsort cmpVex s1}
  where
    s1 = arr_ t

helpme::IO()
helpme = do
  let (+) = (++)
  b <- en "b"
  -- AronModule.clear
  let ls = ["file => " + b + "/tmp/draw.x",
            "PlotGeometry -h => help     ",
            "                            ",
            "point                       ",
            "0.1 0.1 0.1                 ",
            "endpoint                    ",
            "                            ",
            "Support primitives:         ",
            "point, segment and triangle "
           ]
  printBox 4 ls

type Vex3 = Vertex3 GLfloat
type V3d = Vertex3 GLdouble
  
{-|   
renderText :: GLUT.Font -> String -> IO ()
renderText font str = do
    GL.scale (1/64 :: GL.GLdouble) (1/64) 1
    GLUT.renderString GLUT.Roman str
-}    
mymain :: IO ()
mymain = do
    successfulInit <- G.init
    G.windowHint (G.WindowHint'DoubleBuffer True)
    -- if init failed, we exit the program
    bool successfulInit exitFailure $ do
        mw <- G.createWindow 1000 1000 "PlotGeometry" Nothing Nothing
        maybe' mw (G.terminate >> exitFailure) $ \window -> do
            G.makeContextCurrent mw
            ref <- newIORef initCam 
            refStep <- newIORef initStep
            refGlobal <- newIORef initGlobal
            globalRef <- readIORef refGlobal
            writeIORef refGlobal globalRef

            -- sphere
            -- randomPts <- randomVertex (10*3)
            randomPts <- convexPts
            writeIORef refGlobal $ setDrawPts   globalRef spherePtsX
            globalRef2 <- readIORef refGlobal
            writeIORef refGlobal $ setRandomPts globalRef2 randomPts

            refFrame <- (timeNowMilli >>= \x -> newIORef FrameCount{frameTime = x, frameCount = 1, frameIndex = 0})
    

            let cx' = []
            mainLoop window ref refStep refGlobal refFrame cx'
            G.destroyWindow window
            G.terminate
            exitSuccess
                   
xor::Bool -> Bool -> Bool
xor True True = False
xor True False = True
xor False True = True
xor False False = False

{-|
   x  y  z
   0  1  0       
   1  0  0   XOR
  ---------
   1  1  0

   x  y  z
   0  1  0
   0  1  0   XOR
  ---------
   0  0  0

   x  y  z
   0  1  0
   0  0  1   XOR
  ---------
   0  1  1

-}
flipAxis::XYZAxis -> XYZAxis -> XYZAxis
flipAxis axisOld axisNew | x' = XYZAxis{xa = xor x x', ya = False,    za = False}
                         | y' = XYZAxis{xa = False,    ya = xor y y', za = False}
                         | z' = XYZAxis{xa = False,    ya = False,    za = xor z z'}
                         | otherwise = XYZAxis{xa = False, ya = False, za = False}
  where
    x = xa axisOld
    y = ya axisOld
    z = za axisOld
    x' = xa axisNew
    y' = ya axisNew
    z' = za axisNew
  
xAxis::XYZAxis
xAxis = XYZAxis{xa = True, ya = False, za = False}
  
yAxis::XYZAxis
yAxis = XYZAxis{xa = False, ya = True, za = False}
  
zAxis::XYZAxis
zAxis = XYZAxis{xa = False, ya = False, za = True}

initXYZAxis::XYZAxis
initXYZAxis = XYZAxis{xa = False, ya = False, za = False}


initGlobal::GlobalRef
initGlobal = GlobalRef{
             str_ = "" 
             ,cursor_ = (0.0, 0.0) 
             ,xyzAxis_ = initXYZAxis 
             ,mousePressed_ = (False, (0.0, 0.0))
             ,drawRectX_ = (Vertex3 (-0.2) (-0.2) (0.0::GLfloat), Vertex3 0.2 0.2 (0.0::GLfloat)) 
             ,tranDrawRectX_ = Vector3 0.0 0.0 (0.0::GLdouble)
             ,fovDegree_ = 100.0
             ,drawPts_ = [[Vertex3 0.0 0.0 0.0]]
             }


{-|
    KEY: 
    NOTE: USED
-}
keyBoardCallBack2::IORef Step -> IORef GlobalRef -> G.KeyCallback
keyBoardCallBack2 refStep refGlobalRef window key scanCode keyState modKeys = do
    pp "keyBoardCallBack in $b/haskelllib/AronOpenGL.hs"
    putStrLn $ "inside =>" ++ show keyState ++ " " ++ show key
    globalRef <- readIORef refGlobalRef
    let axisOld = xyzAxis_ globalRef
    let fovOld = fovDegree_ globalRef
    logFileG ["fovOld=" ++ show fovOld]
    case keyState of
        G.KeyState'Pressed -> do 
            -- write Step{...} to ref
            case key of
              k | k == G.Key'Right -> writeIORef refStep Step{xx=_STEP,    yy =0.0,      zz = 0.0,    ww = 0.0}
                | k == G.Key'Left  -> writeIORef refStep Step{xx=(-_STEP), yy =0.0,      zz = 0.0,    ww = 0.0}
                | k == G.Key'Up    -> writeIORef refStep Step{xx=0.0,      yy =_STEP,    zz = 0.0,    ww = 0.0}
                | k == G.Key'Down  -> writeIORef refStep Step{xx=0.0,      yy =(-_STEP), zz = 0.0,    ww = 0.0}
                | k == G.Key'9     -> writeIORef refStep Step{xx=0.0,      yy =0.0,      zz = _STEP,  ww = 0.0}
                | k == G.Key'0     -> writeIORef refStep Step{xx=0.0,      yy =0.0,      zz = -_STEP, ww = 0.0}
                | k == G.Key'8     -> writeIORef refStep Step{xx=0.0,      yy =0.0,      zz = 0.0,    ww = _STEP}
                | k == G.Key'7     -> writeIORef refStep Step{xx=0.0,      yy =0.0,      zz = 0.0,    ww = -_STEP}
                
                | k == G.Key'X     -> writeIORef refGlobalRef $ setXYZAxis globalRef $ flipAxis axisOld xAxis
                --                                  ↑  
                --                                  + -> Update Coord to YZ-plane

                | k == G.Key'Y     -> writeIORef refGlobalRef $ setXYZAxis globalRef $ flipAxis axisOld yAxis
                --                                  ↑ 
                --                                  + -> Update Coord to XZ-plane

                | k == G.Key'Z     -> writeIORef refGlobalRef $ setXYZAxis globalRef $ flipAxis axisOld zAxis
                --                                  ↑ 
                --                                  + -> Update Coord to XY-plane

                -- zoom out
                | k == G.Key'O     -> writeIORef refGlobalRef $ setFOVDegree globalRef $ fovOld + 5.0
                --                                  ↑ 
                --                                  + -> Update Coord to XY-plane
                -- zoom in 
                | k == G.Key'I     -> writeIORef refGlobalRef $ setFOVDegree globalRef $ fovOld - 5.0
                --                                  ↑ 
                --                                  + -> Update Coord to XY-plane
  
                -- TODO: In orthogonal projective status, 
                | k == G.Key'Space -> writeIORef refStep initStep
                | otherwise -> pp "Unknown Key Press"
        G.KeyState'Released -> do
            if key == G.Key'Right then pp "Release Key => Right" else pp "Press No Right"
            if key == G.Key'Left  then pp "Release Key => left"  else pp "Press No Right"
            if key == G.Key'Up    then pp "Release Key => up"    else pp "Release No Up"
            if key == G.Key'Down  then pp "Release Key => Down"  else pp "Release No Down"
        _   -> pp "Unknow keyState"
    when (key == G.Key'Escape && keyState == G.KeyState'Pressed)
        (G.setWindowShouldClose window True)

{-|
   === KEY: points set for sphere
   The function from AronGraphic.hs spherePts
-}
spherePtsX::[[Vertex3 GLfloat]]
spherePtsX = geneParamSurface fx fy fz n
    where
        n = 20::Int
        δ = (2*pi)/(rf(n-1)) :: Float
        r = 0.1
        br = 0.2
        σ = 1/rf(n-1)

        fx::Int -> Int -> GLfloat
        fx i j = let i' = rf i
                     j' = rf j
                     α  = δ*i'
                     β  = δ*j'
                 in r*cos(α)*cos(β)
        fy::Int -> Int -> GLfloat
        fy i j = let i' = rf i
                     j' = rf j
                     α  = δ*i'
                     β  = δ*j'
                 in r*cos(α)*sin(β)
        
        fz::Int -> Int -> GLfloat
        fz i j = let i' = rf i
                     j' = rf j
                     α  = δ*i'
                     β  = δ*j'
                 in r*sin(α)

  
d2f::Vector3 GLdouble -> Vector3 GLfloat
d2f(Vector3 x y z) = Vector3 x' y' z'
    where
        x' = rf x
        y' = rf y 
        z' = rf z 

{-|
    === KEY: check whether point (x, y) is inside the rectangle

    @ 
                    | upLeftY
         upLeftX -> v +----------+ 
                                 |
                                 |
                      +          + 
                                 ^ <-  downRightX 
                                 |
                              downRightY                                    
                                                                             +- - -> translate Vector3
                                 + -> upLeft                                 |
                                 |                                           |
                                 |                     + -> downRight        |
                                 |                     |                     |                    +-> cursor pos
                                 ↓                     ↓                     ↓                    ↓       
    @

-}
isPtInsideRectTran::G.Window ->(Vertex3 GLfloat, Vertex3 GLfloat) -> Vector3 GLdouble -> IO Bool
isPtInsideRectTran w (p0, p1) (Vector3 a b c) = do
    cursorPos <- getCursorPosf w  -- IO (GLfloat, GLfloat)
    let tvec = Vector3 a (-b) c
    isPtInsideRect w (p0 +: (d2f tvec), p1 +: (d2f tvec)) cursorPos 

isPtInsideRect::G.Window -> (Vertex3 GLfloat, Vertex3 GLfloat) -> (GLfloat, GLfloat) -> IO Bool
isPtInsideRect w (Vertex3 x0 y0 z0, Vertex3 x1 y1 z1) (x, y) = do
        let ndcWidth = 4.0
        (winWidth, winHeight) <- G.getWindowSize w
        let (winW, winH) = (rf winWidth, rf winHeight)
        -- let (w, h) = (rf width, rf height)
        --
        --                 + -> Shift to the Right in half window 
        --                 ↓ 
        let upLeftX = winW / 2 + x0 * (winW / ndcWidth)
        let upLeftY = winH / 2 + y0 * (winH / ndcWidth)
        let downRightX = winW / 2 + x1 * (winW / ndcWidth)
        let downRightY = winH / 2 + y1 * (winH / ndcWidth)
        if upLeftX <= x && x <= downRightX && upLeftY <= y && y <= downRightY
            then return True
            else return False

setStr::GlobalRef -> String -> GlobalRef
setStr gf s = GlobalRef{ 
                str_ = s, 
                cursor_ = cursor, 
                xyzAxis_ = xyzaxis, 
                mousePressed_ = mp, 
                drawRectX_ = vx,
                tranDrawRectX_ = td,
                fovDegree_ = fov,
                drawPts_ = drawPts,
                randomPts_ = randomPts
                }
  where
    xyzaxis = xyzAxis_ gf
    cursor = cursor_ gf
    mp = mousePressed_ gf
    vx = drawRectX_ gf
    td = tranDrawRectX_ gf
    fov= fovDegree_ gf
    drawPts = drawPts_ gf
    randomPts = randomPts_ gf


setDrawPts::GlobalRef -> [[Vertex3 GLfloat]] -> GlobalRef
setDrawPts gf cx = GlobalRef{ 
                str_ = str,
                cursor_ = cursor, 
                xyzAxis_ = xyzaxis, 
                mousePressed_ = mp, 
                drawRectX_ = vx,
                tranDrawRectX_ = td,
                fovDegree_ = fov,
                drawPts_ = cx,
                randomPts_ = rts
                }
  where
    str = str_ gf
    xyzaxis = xyzAxis_ gf
    cursor = cursor_ gf
    mp = mousePressed_ gf
    vx = drawRectX_ gf
    td = tranDrawRectX_ gf
    fov= fovDegree_ gf
    rts= randomPts_ gf

setRandomPts::GlobalRef -> [Vertex3 GLfloat] -> GlobalRef
setRandomPts gf rpt = GlobalRef{ 
                str_ = str,
                cursor_ = cursor, 
                xyzAxis_ = xyzaxis, 
                mousePressed_ = mp, 
                drawRectX_ = vx,
                tranDrawRectX_ = td,
                fovDegree_ = fov,
                drawPts_ = dp,
                randomPts_ = rpt
                }
  where
    str = str_ gf
    xyzaxis = xyzAxis_ gf
    cursor = cursor_ gf
    mp = mousePressed_ gf
    vx = drawRectX_ gf
    td = tranDrawRectX_ gf
    fov= fovDegree_ gf
    dp = drawPts_ gf
  

{-|
    KEY: getter for fovDegree_ 
    NOTE: zoom in, zoom out
-}
getFOVDegree::IORef GlobalRef -> IO GLdouble 
getFOVDegree ioGlobalRef = readIORef ioGlobalRef >>= return . fovDegree_ 

{-|
    KEY: getter for setFOVDegree 

    NOTE: zoom in, zoom out
-}
setFOVDegree::GlobalRef -> GLdouble -> GlobalRef
setFOVDegree gf fov = GlobalRef{
                  str_ = s, 
                  cursor_ = g, 
                  xyzAxis_ = xyzaxis, 
                  mousePressed_ = mp, 
                  drawRectX_ = vx,
                  tranDrawRectX_ = td,
                  fovDegree_ = fov,
                  drawPts_ = drawPts,
                  randomPts_ = randomPts
                  }
  where
    s = str_ gf
    g = cursor_ gf
    xyzaxis = xyzAxis_ gf
    mp = mousePressed_ gf
    vx = drawRectX_ gf
    td = tranDrawRectX_ gf
    drawPts = drawPts_ gf
    randomPts = randomPts_ gf

{-|
    KEY: getter for str_ 
-}
getStr::IORef GlobalRef -> IO String
getStr ioGlobalRef = readIORef ioGlobalRef >>= return . str_

    
{-|
    KEY: setter for xyzAxis_ 
-}
setXYZAxis::GlobalRef -> XYZAxis -> GlobalRef
setXYZAxis gf xyzAxis = GlobalRef{
                  str_ = s, 
                  cursor_ = g, 
                  xyzAxis_ = xyzAxis, 
                  mousePressed_ = mp, 
                  drawRectX_ = vx,
                  tranDrawRectX_ = td,
                  fovDegree_ = fov,
                  drawPts_ = drawPts,
                  randomPts_ = randomPts
                  }
  where
    s = str_ gf
    g = cursor_ gf
    mp = mousePressed_ gf
    vx = drawRectX_ gf
    td = tranDrawRectX_ gf
    fov = fovDegree_ gf
    drawPts = drawPts_ gf
    randomPts = randomPts_ gf
  
{-|
    KEY: getter for xyzAxis_
-}
getXYZAxis::IORef GlobalRef -> IO XYZAxis
getXYZAxis ioGlobalRef = readIORef ioGlobalRef >>= return . xyzAxis_

{-|
    KEY: getter for drawPts_
-}
getDrawPts::IORef GlobalRef -> IO [[Vertex3 GLfloat]]
getDrawPts ioGlobalRef = readIORef ioGlobalRef >>= return . drawPts_
  
getRandomPts::IORef GlobalRef -> IO [Vertex3 GLfloat]
getRandomPts ioGlobalRef = readIORef ioGlobalRef >>= return . randomPts_
  
--setCursor::GlobalRef -> (GLfloat, GLfloat) -> GlobalRef
--setCursor gf pos = GlobalRef{str_ = s, cursor_ = pos, xyzAxis_ = xyz}
--    where
--      s = str_ gf
--      xyz = xyzAxis_ gf

{-|
    KEY: setter for cursor_ 
-}
setCursor::(GLfloat, GLfloat) -> GlobalRef -> GlobalRef
setCursor pos gf = GlobalRef{
                    str_ = s, 
                    cursor_ = pos, 
                    xyzAxis_ = xyz, 
                    mousePressed_ = mp, 
                    drawRectX_ = vx,
                    tranDrawRectX_ = td,
                    fovDegree_ = fov,
                    drawPts_ = drawPts,
                    randomPts_ = randomPts
                    }
    where
      s = str_ gf
      xyz = xyzAxis_ gf
      mp = mousePressed_ gf
      vx = drawRectX_ gf
      td = tranDrawRectX_ gf
      fov = fovDegree_ gf
      drawPts = drawPts_ gf
      randomPts = randomPts_ gf
--setMousePressed::Bool -> GlobalRef -> GlobalRef
--setMousePressed b gf = GlobalRef{str_ = s, cursor_ = cursor, xyzAxis_ = xyz, mousePressed_ = b} 
--    where
--      s = str_ gf
--      xyz = xyzAxis_ gf
--      cursor = cursor_ gf

{-|
    KEY: setter for mousePressed_ 
-}
setMousePressed::(Bool, (GLfloat, GLfloat)) -> GlobalRef -> GlobalRef
setMousePressed (b, mpos) gf = GlobalRef{
                    str_ = s, 
                    cursor_ = cursor, 
                    xyzAxis_ = xyz, 
                    mousePressed_ = (b, mpos), 
                    drawRectX_ = vx,
                    tranDrawRectX_ = td,
                    fovDegree_ = fov,
                    drawPts_ = drawPts,
                    randomPts_ = randomPts
                    } 
    where
      s = str_ gf
      xyz = xyzAxis_ gf
      cursor = cursor_ gf
      vx = drawRectX_ gf
      td = tranDrawRectX_ gf
      fov= fovDegree_ gf
      drawPts = drawPts_ gf
      randomPts = randomPts_ gf

{-|
    KEY: 
-}
getCursorPosf::G.Window -> IO (GLfloat, GLfloat)
getCursorPosf w = G.getCursorPos w >>= \(x, y) -> return $ (rf x, rf y) :: IO (GLfloat, GLfloat)

{-|
    KEY: 
-}
getMousePressed::IORef GlobalRef -> IO (Bool, (GLfloat, GLfloat)) 
getMousePressed ioGlobalRef = readIORef ioGlobalRef >>= return . mousePressed_

{-|
    KEY: 
-}
getCursor::IORef GlobalRef -> IO (GLfloat, GLfloat)
getCursor ioGlobalRef = readIORef ioGlobalRef >>= return . cursor_

{-|
    KEY: 
-}
getDrawRectX::IORef GlobalRef -> IO (Vertex3 GLfloat, Vertex3 GLfloat)
getDrawRectX ioGlobalRef = readIORef ioGlobalRef >>= return . drawRectX_ 

{-|
    KEY: 
-}
setDrawRectX::(Vertex3 GLfloat, Vertex3 GLfloat) -> GlobalRef -> GlobalRef
setDrawRectX vex gf = GlobalRef{
                    str_ = s, 
                    cursor_ = cursor, 
                    xyzAxis_ = xyz, 
                    mousePressed_ = mp, 
                    drawRectX_ = vex,
                    tranDrawRectX_ = td,
                    fovDegree_ = fov,
                    drawPts_ = drawPts
                    }
    where
      s = str_ gf
      xyz = xyzAxis_ gf
      cursor = cursor_ gf
      mp = mousePressed_ gf
      td = tranDrawRectX_ gf
      fov = fovDegree_ gf
      drawPts = drawPts_ gf

  
getTranVecDrawRectX::IORef GlobalRef -> IO (Vector3 GLdouble)
getTranVecDrawRectX ioGlobalRef = readIORef ioGlobalRef >>= return . tranDrawRectX_

{-|
    KEY:

    t0 pressed
       (x0, y0) 

    t1 released
       (x1, y1) 
-}
mouseCallback:: IORef GlobalRef -> G.MouseButtonCallback
mouseCallback globalRef window but butState mk = do
  case butState of
    G.MouseButtonState'Pressed -> do
      case but of
        v | v == G.MouseButton'1 -> do
              (fbw, fbh) <- G.getFramebufferSize window 
              pos <- G.getCursorPos window >>= \(x, y) -> return $ (rf x, rf y) :: IO (GLfloat, GLfloat)
              ws <- G.getWindowSize window
              let str = PR.printf "cx=%.2f cy=%.2f wx=%d wy=%d bx=%d by=%d" (fst pos) (snd pos) (fst ws) (snd ws) fbw fbh:: String

              gRef <- readIORef globalRef
              -- ↑ 
              -- +---------------------------+
              --                             ↓ 
              writeIORef globalRef $ setStr gRef str
              -- newGlobalRef <- readIORef globalRef >>= return . setCursor pos 
              readIORef globalRef >>= return . setCursor pos >>= \x -> writeIORef globalRef $ setMousePressed (True, pos) x 
              --  ↑ 
              --  +--------------------------------------------------+
              --                                                     ↓  
              -- writeIORef globalRef $ setMousePressed (True, pos) newGlobalRef 

              pp str
          | otherwise            -> pp "No button pressed"
    G.MouseButtonState'Released -> do
      -- pos <- G.getCursorPos window >>= \(x, y) -> return $ (rf x, rf y) :: IO (GLfloat, GLfloat)
      let pos = (0.0, 0.0)
      readIORef globalRef >>= \x -> writeIORef globalRef $ setMousePressed (False, pos) x 
      pp "Button Released"
  
{-| 
    === KEY: screen coordinates-system to graphic coordinates-system

    @
        (x, y) <- getCursorPosf w = G.getCursorPos w >>= \(x, y) -> return $ (rf x, rf y)
        screen Coordinates-System

        [winW = 1000, winH = 1000]
        topLeft
         ↓ (0,0) 
         + - - - ->  winW
         |
         |
         |
         v winH

        Scale XY-axes to [0, 1.0]
        (winW, winH) <- G.getWindowSize w >>= \(u, v) -> return (rf u, rf v)

        (x/winW, y/winH)

         topLeft
         ↓ (0,0)
         + - - - ->  1.0 
         |
         |
         |
         v 1.0 


         Move (0,0) => (0, 0.5) 
         (x/winW, y/winH - 0.5)
         topLeft
         ↓
         + -     ->  1.0 
         |                 
 (0,0)   + - - - -> 
         |
         v 1.0 

         Flip Y-Axis
         (x/winW, (0.5 - y/winH))

         ^
         | 
         |
         + - - - ->
         |
         |

         ↑ 
         bottomLeft
         
         Move (0,0) to (0, 0.5), 0.5 on X-axis
         (0.5, 0.5) => (0.0, 0.0)

         (x/winW - 0.5,  0.5 - y/winH)

                 ^
                 |
                 | 
                 | (0,0)
          -------+------->
                 |
                 |
                 |
                 

         Test 1, x = winW/2, y=winH/2 => (0, 0)       ✓ 
         Test 2, x = winW,   y=winH   => (0.5, -0.5)  ✓ 
         Test 3, x = (1/4)winW, y= (1/4) winH => (0.25 - 0.5, 0.5 - 0.25) = (-0.25, 0.25)  ✓

    @
-} 
screenCSToGraphicCS::G.Window -> (GLfloat, GLfloat) -> IO (Vertex3 GLfloat)
screenCSToGraphicCS ws (wpx, hpx) = do
    let ndcWidth = 4.0
    (winW, winH) <- G.getWindowSize ws >>= \(u, v) -> return (rf u, rf v)
    let cenx = ndcWidth/2.0
    let ceny = ndcWidth/2.0
    -- [0, ndcWidth] - (ndcWidth/2)
    -- x: [0, 2] - 1 => [-1, 1], move (0, 0) => (1, 0) 
    -- y: [0, 2] - 1 => [-1, 1], flip Y-axis => -1*[-1, 1] => [1, -1] 
    let x' = ndcWidth/winW
    let y' = ndcWidth/winH
    return $ Vertex3 (x' * wpx - cenx) (negate(y' * hpx - ceny)) 0.0

drawConvexHull::[Vertex3 GLfloat] -> IO()
drawConvexHull pts = do
    let n = len pts
    let ls = convexHull n pts
    mapM_ (\x -> drawDot x) pts
    mapM_ (\x -> drawSegmentWithEndPt red x) ls

-- listTupeToList::[(Vertex3 GLfloat, Vertex3 GLfloat)] -> [[Vertex3 GLfloat]]
-- listTupeToList cx = map (\x -> ((fromJust . head) cx, (fromJust . last) cx) ) cx

-- sort vertex
cmpVex::Vertex3 GLfloat -> Vertex3 GLfloat -> Bool
cmpVex (Vertex3 x0 y0 z0) (Vertex3 x1 y1 z1) = x0 /= x1 ? x0 < x1 $ (y0 /= y1 ? y0 < y1 $ (z0 /= z1 ? z0 < z1 $ True))

seg::Vertex3 GLfloat -> Vertex3 GLfloat -> [Vertex3 GLfloat]
seg v0 v1 = cmpVex v0 v1 ? [v0, v1] $ [v1, v0]

mapSortVex::[[Vertex3 GLfloat]] -> [[Vertex3 GLfloat]]
mapSortVex cx = map (\lv -> qqsort (\a b -> cmpVex a b) lv) cx

sortVex::[Vertex3 GLfloat] -> [Vertex3 GLfloat]
sortVex = qqsort cmpVex

#if  0
    URL: xfido.com/image/add_remove_segment.svg
#endif
  
{-|
  Intersection between one segment and a set of segments

  Slow code

  Implement the algorithm:
  < https://xfido.com/image/triangulation.png >
  < http://localhost/html/indexIntersectionbetweenonesegmentandmanysegmentsin2dplan.html URL>
-}
oneSegIntersectList::[Vertex3 GLfloat] -> [[Vertex3 GLfloat]] -> Bool
oneSegIntersectList [] _ = False
oneSegIntersectList _ [] = False
oneSegIntersectList s ls = sm > 0
   where
     v0 = head s
     v1 = last s
     lt = map (\[x, y] -> intersectSegNoEndPt2 (x, y) (v0, v1)) ls
     sm = sum $ map (\x -> x /= Nothing ? 1 $ 0) lt

#if 0
  remove one segment from segls first
#endif
  
anyInter = oneSegIntersectList
removeIntersectSeg [x, y, z] v segs = if | interSeg (v, x) (y, z) /= Nothing && not (anyInter [v, x] segs) -> (sortVex [y, z], sortVex [x, v], mapSortVex [[v, x, z],[v, x, y]])
                                         | interSeg (v, y) (x, z) /= Nothing && not (anyInter [v, y] segs) -> (sortVex [x, z], sortVex [y, v], mapSortVex [[v, y, z],[v, x, y]])
                                         | interSeg (v, z) (x, y) /= Nothing && not (anyInter [v, z] segs) -> (sortVex [x, y], sortVex [z, v], mapSortVex [[v, y, z],[v, x, z]])
                                         | otherwise                                               -> error $ "ERROR to remove segment" ++ show ([x, y, z]) ++ "<->" ++ show v
  where
    interSeg = intersectSegNoEndPt2


    

mainLoop :: G.Window ->
            IORef Cam ->
            IORef Step ->
            IORef GlobalRef ->
            IORef FrameCount ->
            [[Vertex3 GLfloat]]-> IO ()
mainLoop w refCam refStep refGlobal refCount lssVex = unless' (G.windowShouldClose w) $ do
    (width, height) <- G.getFramebufferSize w
    viewport $= (Position 0 0, Size (fromIntegral width) (fromIntegral height))
    GL.clear [ColorBuffer, DepthBuffer]

    G.setKeyCallback w (Just $ keyBoardCallBack2 refStep refGlobal)    -- AronOpenGL
    G.setMouseButtonCallback w (Just $ mouseCallback refGlobal)  -- mouse event
    -- lightingInfo
    loadIdentity  -- glLoadIdentity

    step <- readIORef refStep
    xyzAxis <- getXYZAxis refGlobal
    fovNew <- getFOVDegree refGlobal
    case xyzAxis of
      --                                 +---> YZ-plane
      --                                 ↓
      var | xa var -> GL.lookAt (Vertex3 1.0 0 0::Vertex3 GLdouble) (Vertex3 0 0 0::Vertex3 GLdouble) (Vector3 0 1 0::Vector3 GLdouble)
  
          --                               +---> XZ-plane
          --                               ↓
          | ya var -> GL.lookAt (Vertex3 0 1.0 0::Vertex3 GLdouble) (Vertex3 0 0 0::Vertex3 GLdouble) (Vector3 1 0 0::Vector3 GLdouble)

          --                                 +---> XY-plane
          --                                 ↓
          | za var -> do 
            -- Graphics.Rendering.OpenGL.GLU.Matrix perspective :: GLdouble -> GLdouble -> GLdouble -> GLdouble -> IO ()

            -- glFrustum describes a perspective matrix that produces a perspective projection. (left,bottom,-near) and (right,top,-near) specify the points on the near clipping plane that
            -- are mapped to the lower left and upper right corners of the window, respectively, assuming that the eye is located at (0, 0, 0). -far specifies the location of the far clipping
            -- plane. Both near and far must be positive. The corresponding matrix is

            let zf = 0.5 
            -- perspective (field of view) width/height zNear zFar
            -- perspective 90 1.0 zf (zf + 4.0) 
            perspective 126.934 1.0 zf (zf + 4.0) 
            -- zoom in, zoom out
            -- perspective fovNew 1.0 zf (zf + 4.0) 
            -- GL.lookAt (Vertex3 0 4 1.0::Vertex3 GLdouble) (Vertex3 0 4 0::Vertex3 GLdouble) (Vector3 0 1 0::Vector3 GLdouble)
            GL.lookAt (Vertex3 0 0 1.0::Vertex3 GLdouble) (Vertex3 0 0 0::Vertex3 GLdouble) (Vector3 0 1 0::Vector3 GLdouble)

          | otherwise -> do
              GM.lookAt (Vertex3 0.2 0.2 0.2::Vertex3 GLdouble) (Vertex3 0 0 0::Vertex3 GLdouble) (Vector3 0 1 0::Vector3 GLdouble)
              keyboardRot refCam refStep (fromIntegral width) (fromIntegral height)

    -- AronOpenGL.hs
    -- delta refStep to modify Cam{xx, yy, zz} in degree
    -- keyboardRot => rotate around {x-axis, y-axis, y-axis} in some degrees
    -- keyboardRot refCam refStep (fromIntegral width) (fromIntegral height)
    renderCoordinates
    -- show3dStr "1234" red 0.8

    curStr <- getStr refGlobal 
    logFileG ["str_=" ++ (show curStr)]

    vx <- getDrawRectX refGlobal  -- (p0, p1) => (upLeft, bottomRight)

    cursorPos <- getCursor refGlobal

    (isPressed, (mx0, my0))<- getMousePressed refGlobal 
    let pos = (mx0, my0) 
    logFileG ["isPressed=" ++ (show isPressed) ++ " pos=" ++ (show pos)]

    -- TODO: add isPtInsideRect here
    tranVec <- getTranVecDrawRectX refGlobal 
    (p0, p1) <- getDrawRectX refGlobal
    -- isInside <- isPtInsideRect w (p0 +: (d2f tvec'), p1 +: (d2f tvec')) (x, y)
    isInTran <- isPtInsideRectTran w (p0, p1) tranVec

    vex <- getDrawRectX refGlobal

    rls <- randomFloat 10
    let cx = [Vertex3 0.1 0.1 0, Vertex3 0.2 0.1 0, Vertex3 0.2 (-0.1) 0, Vertex3 (-0.1) (-0.1) 0]

    ls3 <- (en "b" >>= \x -> readGLScript ("/tmp/draw.x"))

    pre ls3

    (index, isNext, currRef) <- readRefFrame2 refCount 20

    cylinderPath <- getDrawPts refGlobal
    randomPts <- getRandomPts refGlobal
    -- pre cylinderPath
    pp $ "index=" ++ (show index)

    let fn = "/tmp/c5.x"
    fb <- fExist fn
    when fb $ do
      ls11 <- readFile2d "/tmp/c5.x" :: IO [[Float]]
      let m1 = foldl max 0.0 $ join ls11
      let m = 2.0
      -- pre ls11
      let lsv = map (\s -> let x = (head s)/m; y = (last s)/m in Vertex3 x y 0.0) ls11
      -- pre lsv

      -- mapM_ drawDot lsv

      let cur = (500, 500::GLfloat)
      curVex <- screenCSToGraphicCS w cur
      logFileG ["GraphicCS=" ++ show curVex ++ " cursorCS=" ++ show cur]

      let cur1 = (600, 600::GLfloat)
      curVex1 <- screenCSToGraphicCS w cur1
      logFileG ["GraphicCS=" ++ show curVex1 ++ " cursorCS=" ++ show cur1]

      let cur2 = (400, 600::GLfloat)
      curVex2 <- screenCSToGraphicCS w cur2
      logFileG ["GraphicCS=" ++ show curVex2 ++ " cursorCS=" ++ show cur2]

#if 1
    when True $ do
      let a1 = Vertex3 0.0 0.0 0
      let b1 = Vertex3 1.0 1.0 0
      let c1 = Vertex3 1.0 0.0 0
      let d1 = Vertex3 0.0 1.0 0
      let b = oneSegIntersectList [a1, b1] [[c1, d1]]
      fw "oneSegIntersectList 0"
      print $ "b 0 =>" ++ (show $ b == True)
#endif

#if 1
    when True $ do
      let e0 = Vertex3 2.0 0.0 0      
      let e1 = Vertex3 0.0 2.0 0      

      let a1 = Vertex3 0.0 0.0 0
      let b1 = Vertex3 0.999 0.999 0
      let c1 = Vertex3 1.0 0.0 0
      let d1 = Vertex3 0.0 1.0 0
      let b = oneSegIntersectList [a1, b1] [[e0, e1]]
      fw "oneSegIntersectList 1"
      print $ "b 1 =>" ++ (show $ b == False)
#endif
#if 1
    when True $ do
      let a1 = Vertex3 0.0 0.0 0
      let b1 = Vertex3 1.0 1.0 0
      
      let e0 = Vertex3 2.0 0.0 0      
      let e1 = Vertex3 0.0 2.0 0      

      let c1 = Vertex3 1.0 0.0 0
      let d1 = Vertex3 0.0 1.0 0
      let b = oneSegIntersectList [a1, b1] [[e0, e1]]
      fw "oneSegIntersectList 2"
      print $ "b 2 =>" ++ (show $ b == True)
#endif
#if 1
    when True $ do
      let a1 = Vertex3 0.0 0.0 0
      let b1 = Vertex3 0.5 0.2 0
      
      let e0 = Vertex3 2.0 0.0 0      
      let e1 = Vertex3 0.0 2.0 0      

      let c1 = Vertex3 1.0 0.0 0
      let d1 = Vertex3 0.0 1.0 0
      let b = oneSegIntersectList [a1, b1] [[e0, e1], [c1, d1]]
      fw "oneSegIntersectList 3"
      print $ "b 3 =>" ++ (show $ b == False)
#endif

#if 0   

            p2
     
    

            p0              p1

           p1 => p2 rotate around p0 in counter-clockwise
    
#endif

    when True $ do
      let p0 = Vertex3 0.0 0.0 0
      let p1 = Vertex3 0.5 0.0 0
      let p2 = Vertex3 0.0 0.5 0
      -- Check rotation counter-clockwise or clockwise
      -- p0 -> p1 -> p2
      let v01 = p1 -: p0  -- vector p0 -> p1
      let v02 = p2 -: p0  -- vector p0 -> p2
      
      let angle = angleVectCCW v01 v02
      fw "det 0"
      print $ "radian angle  =" ++ (show angle)
      print $ "degree angle  =" ++ (show $ radianToDegree angle)
  
    when True $ do
      let p0 = Vertex3 0.0 0.0 0
      let p1 = Vertex3 0.5 0.0 0
      let p2 = Vertex3 0.0 (-0.5) 0
      -- Check rotation counter-clockwise or clockwise
      -- p0 -> p1 -> p2
      let v01 = p1 -: p0  -- vector p0 -> p1
      let v02 = p2 -: p0  -- vector p0 -> p2
      let angle = angleVectCCW v01 v02

      fw "det 1"
      print $ "radian angle  =" ++ (show angle)
      print $ "degree angle  =" ++ (show $ radianToDegree angle)

    when True $ do
      let p0 = Vertex3 0.0    0.0 0
      let p1 = Vertex3 0.5    0.0 0
      let p2 = Vertex3 (-0.5) 0   0
      -- Check rotation counter-clockwise or clockwise
      -- p0 -> p1 -> p2
      let v01 = p1 -: p0  -- vector p0 -> p1
      let v02 = p2 -: p0  -- vector p0 -> p2
      let angle = angleVectCCW v01 v02

      fw "det 2"
      print $ "radian angle  =" ++ (show angle)
      print $ "degree angle  =" ++ (show $ radianToDegree angle)

    when True $ do
      fw "state var"
      p <- malloc :: IO (Ptr Int)
      let v = makeStateVarFromPtr p
      v $= 11
      v $~ (+20)
      n <- Data.StateVar.get v
      print $ "First n=" ++ show n 
      pp "OK"
      v $~ (+100)
      n <- Data.StateVar.get v
      print $ "Second n=" ++ show n 

    G.swapBuffers w
    G.pollEvents
    mainLoop w refCam refStep refGlobal refCount lssVex

main = do
  argList <- getArgs
  if len argList > 0 then do
    case head argList of
      "-h" -> helpme
      _    -> do
        print $ "Wrong option => " ++ (head argList) ++ ", -h => Help"
  else mymain

makeStateVarFromPtr :: Storable a => Ptr a -> StateVar a
makeStateVarFromPtr p = makeStateVar (peek p) (poke p)

{-|
   KEY: Angle between v0 and v1, from v0 to v1 in Counter-Clockwise
-}
angleVectCCW::Vector3 GLfloat -> Vector3 GLfloat -> GLfloat
angleVectCCW v0 v1 = de < 0 ? 2*pi - angle $ angle
  where
    angle = acos $ d/(n1*n2)
    d = dot3ve v0 v1
    n1 = nr v0
    n2 = nr v1
    de = detVect v0 v1
    detVect::Vector3 GLfloat -> Vector3 GLfloat -> GLfloat
    detVect (Vector3 x0 y0 _) (Vector3 x1 y1 _) = det [[x0, y0], [x1, y1]]
