-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

data JMethod = JMethod{
                    method::String,
                    params::[(String, String)]
                } deriving(Eq, Show)
                      
{-| 
    === parse java method with dirty hack.

    * Valid form:
    > "int method(int n) {"
    > "int method(int n, List<String> ls ) {"
    > "int method() {"

    * Output:
    @
        /**
        *
        * @n
        * @ls
        */
    @

    * Invalid form:
    > "int method(int n) "

    * TODO: Add @return
-} 
parseMethod::String -> String 
parseMethod s = ret 
    where
        ls = splitStr "[[:space:]]+" s 
        lss = splitStrChar  "[()]" s 
        lsm = filter (\x -> (len . trim) x > 0) lss 
        jm = case (len lsm) of 
                  2           -> let mName = last $ filter(\x -> (len.trim) x > 0) $ splitStr "[[:space:]]+" $ head lsm 
                                 in JMethod{method = mName, params = []}
                  3           -> let mName = last $ filter(\x -> (len.trim) x > 0) $ splitStr "[[:space:]]+" $ head lsm 
                                     par = splitStrChar "[,]" $ (head . init . tail) lsm 
                                     ss  = map(\x -> splitStr "[[:space:]]+" x) par
                                     sss = map (\r -> filter (\x -> (len.trim) x > 0) r) ss 
                                     tls = map(\r -> (concat $ init r, last r)) sss
                                 in JMethod {method = mName, params = tls}
                  otherwise   -> error "error input" 
        ret = concat $ ["/**\n"] ++ (map(\x -> "* @" ++ (snd x) ++ "\n") (params jm)) ++ ["*/\n"]


-- str = "public static List<String> recurveDir(String fname, Integer num, List< String > ls) {"

main = do 
        argList <- getArgs
        if len argList == 1 then let s = parseMethod $ head argList in putStrLn s 
                            else print "need one argument"
--        pp $ length argList
--        let pam = parseMethod str
--        pw "me" pam 
--        let doc = parseMethod str
--        putStrLn doc
