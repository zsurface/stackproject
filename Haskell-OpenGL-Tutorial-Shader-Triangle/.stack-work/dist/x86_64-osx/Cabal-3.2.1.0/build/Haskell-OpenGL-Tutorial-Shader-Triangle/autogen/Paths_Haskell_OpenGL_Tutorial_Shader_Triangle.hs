{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_Haskell_OpenGL_Tutorial_Shader_Triangle (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/Haskell-OpenGL-Tutorial-Shader-Triangle/.stack-work/install/x86_64-osx/f0946b90f078913af346a363037ab90a3d44e0136e69c7156f73c8f5801ed467/8.10.4/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/Haskell-OpenGL-Tutorial-Shader-Triangle/.stack-work/install/x86_64-osx/f0946b90f078913af346a363037ab90a3d44e0136e69c7156f73c8f5801ed467/8.10.4/lib/x86_64-osx-ghc-8.10.4/Haskell-OpenGL-Tutorial-Shader-Triangle-0.1.0.0-D6WSDwzTOS73ed4IOvQ9Dv-Haskell-OpenGL-Tutorial-Shader-Triangle"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/Haskell-OpenGL-Tutorial-Shader-Triangle/.stack-work/install/x86_64-osx/f0946b90f078913af346a363037ab90a3d44e0136e69c7156f73c8f5801ed467/8.10.4/lib/x86_64-osx-ghc-8.10.4"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/Haskell-OpenGL-Tutorial-Shader-Triangle/.stack-work/install/x86_64-osx/f0946b90f078913af346a363037ab90a3d44e0136e69c7156f73c8f5801ed467/8.10.4/share/x86_64-osx-ghc-8.10.4/Haskell-OpenGL-Tutorial-Shader-Triangle-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/Haskell-OpenGL-Tutorial-Shader-Triangle/.stack-work/install/x86_64-osx/f0946b90f078913af346a363037ab90a3d44e0136e69c7156f73c8f5801ed467/8.10.4/libexec/x86_64-osx-ghc-8.10.4/Haskell-OpenGL-Tutorial-Shader-Triangle-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/Haskell-OpenGL-Tutorial-Shader-Triangle/.stack-work/install/x86_64-osx/f0946b90f078913af346a363037ab90a3d44e0136e69c7156f73c8f5801ed467/8.10.4/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "Haskell_OpenGL_Tutorial_Shader_Triangle_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "Haskell_OpenGL_Tutorial_Shader_Triangle_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "Haskell_OpenGL_Tutorial_Shader_Triangle_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "Haskell_OpenGL_Tutorial_Shader_Triangle_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "Haskell_OpenGL_Tutorial_Shader_Triangle_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "Haskell_OpenGL_Tutorial_Shader_Triangle_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
