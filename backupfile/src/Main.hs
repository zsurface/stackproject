-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

bmfile h = h </> "Library/Application Support/Firefox/Profiles/sk75a0xs.default-release-1/places.sqlite"

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

{-|
    === KEY: backup file

    @
    Backup file:
    places.sqlite ⟹  places.sqlite_2022-09-14_00_11_07_081807_PDT

    backup "/tmp/x.txt"  "/tmp" => "/tmp/x.txt_2022-09-14_00_11_07_081807_PDT
    @
-}
backup::FilePath -> FilePath -> IO () 
backup dbFile newDir = do
    s <- dateStr >>= \x -> return $ concatStr (splitSPC x) "_"
    let s' = replaceRegex (mkRegex ":|\\.") s "_"
    home <- getEnv "HOME"
    let newName = (takeName dbFile) ++ "_" ++ s'
    let dbFile' = "\"" ++ dbFile ++ "\""
    b <- fileExistA newDir 
    when (not b) $ do 
        mkdir newDir
    cp dbFile (newDir </> newName)

main = do 
        s <- dateStr >>= \x -> return $ concatStr (splitSPC x) "_"
        let s' = replaceRegex (mkRegex ":|\\.") s "_"
        pp s' 
        let backupDir = "dbbackup"
        home <- getEnv "HOME"
        let path = bmfile home
        backup path backupDir
        pp "done!"
