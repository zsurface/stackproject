module Main where

import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix hiding (combine)
import System.IO
import Language.Haskell.Exts
import Control.Monad
import AronModule


fun::Int -> String -> Int
fun x y = (fun2 3) + (fun3 "3" 2)


fun2::(Num a)=> a -> Int
fun2 x = 3

fun3::String -> Int -> Int
fun3 x y = 3


--  ParseFailed SrcLoc String

{-}
funName::Module -> String
funName Module _ _ _ _ e = case e of
                               (x:cx) -> case x of
                                          TypeSig _ name _  -> case name of
                                                                  (xx:cxx) -> case xx of
                                                                                Ident _ nn -> nn
                                                                                _          -> ""
                                                                  _        -> ""
                                          _                -> ""
                               _      -> ""
-}

funName::Module SrcSpanInfo -> [String]
funName modu = case modu of
            Module a b c d e -> case e of
                           (x:cx) -> map   (\e -> case e of
                                                  TypeSig _ name _ -> case name of
                                                                          (xx:cxx) -> case xx of
                                                                                        Ident _ nn -> "TypeSig=" ++ nn
                                                                                        _          -> []
                                                                          _        -> []

                                                  FunBind _ match  -> case match of  -- [Match]
                                                                        lsm -> join $ map (\c -> case c of
                                                                                               Match _ na _ _ _ -> case na of
                                                                                                                     Ident _ s -> "FunBin=" ++ s
                                                                                                                     _         -> ""
                                                                                               Match _ _ _ x _  -> "xx=" ++ (show x)
                                                                                                                     
                                                                                               Match _ _ _ _ mb -> case mb of
                                                                                                                     Just x -> "mb=" ++ (show x)
                                                                                                                     _      -> ""
                                                                                               _          -> "nono=" ++ (show c)
                                                                                          ) lsm
                                                                        _      -> ""
                                                  PatBind _ _ rhs _ -> case rhs of
                                                                           UnGuardedRhs _ exp -> "exp="
                                                                           _                -> ""
                                                  _                -> "yy=" ++ (show e)
                                                                      
                                                  
                                          ) (x:cx)

                           _                -> []
            _                -> []
  
main = do
  -- result <- parseFile "src/Main.hs"
  bitbucket <- getEnv "b"
  -- result <- parseFile $ bitbucket </> "testfile/AronModule.hs"
  result <- parseFile $ bitbucket </> "testfile/Main.hs"
  case result of
    ParseOk m -> pre m
    _         -> error $ show result
  let ls = case result of
            ParseOk m -> funName m
            _         -> error $ show result
  pre ls
  pp $ len $ filter (\x -> (len . trim) x > 0) ls
  print "ok"
