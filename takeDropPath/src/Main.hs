-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}

import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close

main = do
        fw "takePathEnd"
        
        pp $ takePathEnd 3 "dog/cat/pig" == "dog/cat/pig"
        pp $ takePathEnd 2 "dog/cat/pig" == "cat/pig"
        pp $ takePathEnd 1 "dog/cat/pig" == "pig"
        pp $ takePathEnd 0 "dog/cat/pig" == ""
        pp $ takePathEnd (negate 1) "dog/cat/pig" == ""


        fw "dropPath"
        pp $ dropPath 0 "dog/cat/pig" == "dog/cat/pig"
        pp $ dropPath 1 "dog/cat/pig" == "cat/pig"
        pp $ dropPath 2 "dog/cat/pig" == "pig"
        pp $ dropPath 3 "dog/cat/pig" == ""
        pp $ dropPath 4 "dog/cat/pig" == ""

        fw "takePath"
        pp $ takePath 0 "dog/cat/pig" == ""
        pp $ takePath 1 "dog/cat/pig" == "dog"
        pp $ takePath 2 "dog/cat/pig" == "dog/cat"
        pp $ takePath 3 "dog/cat/pig" == "dog/cat/pig"
        pp $ takePath 4 "dog/cat/pig" == "dog/cat/pig"



        argList <- getArgs
        if len argList > 1
          then do
               let n = strToInteger $ head argList
               let s = head $ tail argList
               let ls = splitRegex (mkRegex "/") s
               let rest = takeEnd n ls
               let path = concatStr rest "/"
               putStrLn path
          else do
               putStrLn $ "takePath 2 " ++ "/dog/cat/pig" ++ " => cat/pig "


