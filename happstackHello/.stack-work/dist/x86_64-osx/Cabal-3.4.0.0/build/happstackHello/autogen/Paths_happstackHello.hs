{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
{-# OPTIONS_GHC -Wno-missing-safe-haskell-mode #-}
module Paths_happstackHello (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/happstackHello/.stack-work/install/x86_64-osx/cc7880cea6fc5800fa89e098c88567e256ee5131d48837a756a21655979c42d6/9.0.1/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/happstackHello/.stack-work/install/x86_64-osx/cc7880cea6fc5800fa89e098c88567e256ee5131d48837a756a21655979c42d6/9.0.1/lib/x86_64-osx-ghc-9.0.1/happstackHello-0.1.0.0-JYY6u3IxwyJ4Edbuuu0SGa-happstackHello"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/happstackHello/.stack-work/install/x86_64-osx/cc7880cea6fc5800fa89e098c88567e256ee5131d48837a756a21655979c42d6/9.0.1/lib/x86_64-osx-ghc-9.0.1"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/happstackHello/.stack-work/install/x86_64-osx/cc7880cea6fc5800fa89e098c88567e256ee5131d48837a756a21655979c42d6/9.0.1/share/x86_64-osx-ghc-9.0.1/happstackHello-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/happstackHello/.stack-work/install/x86_64-osx/cc7880cea6fc5800fa89e098c88567e256ee5131d48837a756a21655979c42d6/9.0.1/libexec/x86_64-osx-ghc-9.0.1/happstackHello-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/happstackHello/.stack-work/install/x86_64-osx/cc7880cea6fc5800fa89e098c88567e256ee5131d48837a756a21655979c42d6/9.0.1/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "happstackHello_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "happstackHello_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "happstackHello_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "happstackHello_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "happstackHello_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "happstackHello_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
