-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD
import AronModule 

import qualified Data.ByteString as BS
import System.IO
import System.Process

-- KEY: create pipe, create pipe with data, pipe data
main :: IO ()
main = do
  (h_read, h_write) <- createPipe
  let msg = "233"
  let ls = ["dog", "cat", "fox\n", "pig", "barista"]
  let ln = foldl (+) 0 $ map len ls
  let ss = unlines ls
  BS.hPut h_write $ toSBS ss
  hFlush h_write
  BS.hGet h_read ln >>= print
