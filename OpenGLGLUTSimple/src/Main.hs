module Main where

{-|
import Data.List
import Data.IORef
import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT
import System.IO

main :: IO ()
main = do
  (_, _) <- getArgsAndInitialize
  -- initialDisplayMode $= [DoubleBuffered]
  createWindow "OpenGLGLUTSimple"

  
  displayCallback $= display
  mainLoop

display = do
  clearColor $= Color4 1 0 0 1
  clear [ColorBuffer]
-}

-- KEY: OpenGL GLUT, OpenGL simple, simple GLUT
import Graphics.UI.GLUT 
import Graphics.Rendering.OpenGL

main = do
  getArgsAndInitialize
  createAWindow "points"
  mainLoop

createAWindow windowName = do
  createWindow windowName
  displayCallback $= display

display = do 
  clear [ColorBuffer]
  currentColor $= Color4 1 0 0 1
  renderPrimitive Points $ vertex (Vertex3 (0.1::GLfloat) 0.5 0)
  flush
