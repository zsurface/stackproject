#version 150

#ifdef GL_ES
precision mediump float;
#endif


uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_time;
in  float time;

// Plot a line on Y using a value between 0.0-1.0
float plot(vec2 st) {    
    return smoothstep(0.02, 0.0, abs(st.y - st.x));
}

float lineSegment(vec2 p, vec2 a, vec2 b) {
    float thickness = 1.0/500.0;

    vec2 pa = p - a;
    vec2 ba = b - a;

    float h = clamp( dot(pa,ba)/dot(ba,ba), 0.0, 1.0 );
    float idk = length(pa - ba*h);
    return smoothstep(0.0, thickness, idk);
}

float circle(vec2 p, vec2 c, float radius){
    float len = length(p - c);
    float thickness = 0.001;
    return smoothstep(0.0, thickness, abs(len - radius)); 
}

float drawPixel(vec2 coord, vec2 v){
    float radius = 0.001;
    float len = length(coord - v);
    float thickness = 0.001;
    return smoothstep(0.0, thickness, abs(len - radius)); 
}

float drawCurve(vec2 coord, float r){
    // f(x) = x*x
    float x = coord.x - 0.5;
    float len = length(coord.y - r*x*x);
    float thickness = 0.01;
    return smoothstep(0.0, thickness, len); 
}

in vec2 fragCoord;
out vec4 fragColor;

void main() {
    vec2  iResolution = vec2(1024, 1024);
	vec2 st = fragCoord.xy/iResolution;

    float y = st.x;

    vec3 color = vec3(y);

    // Plot a line
    float pct = plot(st);
    // color = (1.0-pct)*color+pct*vec3(1.0,1.0,0.0);
    color = color + pct*vec3(0, 1.0, 0.0);

    vec2 v0 = vec2(0.5, 0.5);
    vec2 v1 = vec2(0.1, 0.6);
    float c = lineSegment(fragCoord, v0, v1);

    vec2 cen = vec2(0.3, 0.3);
    vec2 cen1 = vec2(0.3 + 0.2*sin(time), 0.3);
    float radius = 0.2;
    // float co = circle(fragCoord, cen1, radius);
    // float co = drawPixel(fragCoord, cen1);
    
    float co = drawCurve(fragCoord, abs(sin(time)));
    fragColor = vec4(co, 1, 1, 0.5);
}
