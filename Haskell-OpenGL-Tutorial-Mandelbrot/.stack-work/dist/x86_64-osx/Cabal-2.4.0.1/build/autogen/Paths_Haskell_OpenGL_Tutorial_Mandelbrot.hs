{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_Haskell_OpenGL_Tutorial_Mandelbrot (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/Users/aaa/myfile/bitbucket/stackproject/Haskell-OpenGL-Tutorial-Mandelbrot/.stack-work/install/x86_64-osx/080f9f41b4f5a57a5270aa3ccdf501901e595c6484965950ef6edd78e6081564/8.6.5/bin"
libdir     = "/Users/aaa/myfile/bitbucket/stackproject/Haskell-OpenGL-Tutorial-Mandelbrot/.stack-work/install/x86_64-osx/080f9f41b4f5a57a5270aa3ccdf501901e595c6484965950ef6edd78e6081564/8.6.5/lib/x86_64-osx-ghc-8.6.5/Haskell-OpenGL-Tutorial-Mandelbrot-0.1.0.0-HgTQYnTQL55LJrZuhhzaXW"
dynlibdir  = "/Users/aaa/myfile/bitbucket/stackproject/Haskell-OpenGL-Tutorial-Mandelbrot/.stack-work/install/x86_64-osx/080f9f41b4f5a57a5270aa3ccdf501901e595c6484965950ef6edd78e6081564/8.6.5/lib/x86_64-osx-ghc-8.6.5"
datadir    = "/Users/aaa/myfile/bitbucket/stackproject/Haskell-OpenGL-Tutorial-Mandelbrot/.stack-work/install/x86_64-osx/080f9f41b4f5a57a5270aa3ccdf501901e595c6484965950ef6edd78e6081564/8.6.5/share/x86_64-osx-ghc-8.6.5/Haskell-OpenGL-Tutorial-Mandelbrot-0.1.0.0"
libexecdir = "/Users/aaa/myfile/bitbucket/stackproject/Haskell-OpenGL-Tutorial-Mandelbrot/.stack-work/install/x86_64-osx/080f9f41b4f5a57a5270aa3ccdf501901e595c6484965950ef6edd78e6081564/8.6.5/libexec/x86_64-osx-ghc-8.6.5/Haskell-OpenGL-Tutorial-Mandelbrot-0.1.0.0"
sysconfdir = "/Users/aaa/myfile/bitbucket/stackproject/Haskell-OpenGL-Tutorial-Mandelbrot/.stack-work/install/x86_64-osx/080f9f41b4f5a57a5270aa3ccdf501901e595c6484965950ef6edd78e6081564/8.6.5/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "Haskell_OpenGL_Tutorial_Mandelbrot_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "Haskell_OpenGL_Tutorial_Mandelbrot_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "Haskell_OpenGL_Tutorial_Mandelbrot_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "Haskell_OpenGL_Tutorial_Mandelbrot_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "Haskell_OpenGL_Tutorial_Mandelbrot_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "Haskell_OpenGL_Tutorial_Mandelbrot_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
