-- {{{ begin_fold
-- script
-- #!/usr/bin/env runhaskell -i/Users/cat/myfile/bitbucket/haskelllib
-- {-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE DuplicateRecordFields #-} 
-- import Turtle
-- echo "turtle"

-- import Data.Set   -- collide with Data.List 
import Control.Monad
import Data.Char
import Data.Typeable (typeOf) -- runtime type checker, typeOf "k"
import qualified Data.List as L
import Data.List.Split
import Data.Time
import Data.Time.Clock.POSIX
import System.Directory
import System.Environment
import System.Exit
import System.FilePath.Posix
import System.IO
import System.Posix.Files
import System.Posix.Unistd
import System.Process
import Text.Read
import Text.Regex
import Text.Regex.Base
import Text.Regex.Base.RegexLike
import Text.Regex.Posix
import Data.IORef 
import Control.Monad (unless, when)
import Control.Concurrent 

import qualified Text.Regex.TDFA as TD

--import Data.Array

-- import Graphics.Rendering.OpenGL as GL 
-- import Graphics.Rendering.OpenGL.GLU.Matrix as GM  
-- import qualified Graphics.UI.GLFW as G
-- import Data.Set(Set) 
-- import qualified Data.Set as S 

--if (length argList) == 2 
--then case head argList of 
--    "svg" -> run cmd >> run "ls" >>= \x -> pp x 
--            where 
--                cmd = "pwd" 
--    "png" ->run cmd >> run ("ls " ++ fn) >>= \x -> pp x  
--            where 
--                cmd = "pwd" 
--    _     -> print "more arg" 
--else print "Need more arguments" 

--    takeFileName gives "file.ext"
--    takeDirectory gives "/directory"
--    takeExtension gives ".ext"
--    dropExtension gives "/directory/file"
--    takeBaseName gives "file"
--    "/directory" </> "file.ext".
--    "/directory/file" <.> "ext".
--    "/directory/file.txt" -<.> "ext".
-- |  end_fold ,}}}
import Data.Array
import Data.Array.IO
import Data.Array.MArray
import AronModule 

p1 = "/Users/cat/myfile/bitbucket/testfile/test.tex"

-- zo - open
-- za - close


data Slice x = Slice {
    readVals :: IO [x],
    setVals :: [x] -> IO (),
    lengthBacking :: Int
    -- backing :: IOArray Int x -- tell Haskell which MArray to use
}

data Dummy x = Dummy{arr::IOArray Int x}

type MyArray x = [Int] -> Slice x

allocMyArray :: Int -> x -> IO (MyArray x)
allocMyArray len start = do
    arr <- newArray (0, len - 1) start
    let d = Dummy arr
    return $ \ns -> Slice (mapM (readArray arr) ns) 
                          (sequence_ . zipWith (writeArray arr) ns)
                          len
                        --   arr -- see above

lenMyArray :: MyArray x -> Int
lenMyArray f = lengthBacking (f[])

(&=) :: Slice x -> x -> IO ()
lr &= val = setVals lr (repeat val)

readMyArray :: MyArray x -> [Int] -> IO [x]
readMyArray f i = readVals (f i)

createArrInt::(Int, Int) -> IO (IOArray Int Int)
createArrInt (a, b) = do
    arr <- newArray(a, b) 0 
    return arr

readArrInt::(IOArray Int Int) ->Int -> IO Int
readArrInt arr n = readArray arr n

writeArrInt::(IOArray Int Int) -> Int -> Int -> IO()
writeArrInt arr n x = writeArray arr n x

type AArray = IO(IOArray Int Int)

main = do 
        print "Hello World"
        argList <- getArgs
        pp $ length argList
        myarr <- allocMyArray 10 "hi"
        s <-  readMyArray myarr [1]
        pp s

        arr1 <- newArray (0, 10) 9 :: IO(IOArray Int Int)
        n <- readArray arr1 1
        pp n

        arr2 <- createArrInt (0, 5)
        m <- readArrInt arr2 0
        writeArrInt arr2 0 0
        writeArrInt arr2 1 1
        let k = 0
        i <- readArrInt arr2 k
        j <- readArrInt arr2 (k+1)
        writeArrInt arr2 (k+2) (i + j)

        mapM (\x -> writeArray arr2 x x) [0..5]
        pw "m" m
        m1 <- readArrInt arr2 3
        pw "m1" m1
        pp "done!"

        

        v1 <- newArray (0, 10) 1 :: AArray
        v2 <- newArray (0, 10) 2 :: AArray    
        -- mapM (a-> b -> m b)
        mapM (\x -> do 
                      a <- readArray v1 x
                      b <- readArray v2 x
                      writeArray v1 x (a + b)
              ) [0..10]
        pp "done"


