{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

import Data.Text (Text)
import Text.RawString.QQ (r)
import NeatInterpolation 
import AronModule

--rQuote :: Text -> Text
--rQuote myVariable = [r|line one
--line two
--line tree
-- ${ myVariable }
--line five|]
--
--neatQuote :: Text -> Text
--neatQuote myVariable = [text|line one
--line two
--line tree
-- $myVariable
--line five|]
--
--rText, neatText :: Text
--rText    = rQuote    "line four"
--neatText = neatQuote "line four"

fun name = [text|this_could_be_'${name}'_long_identifier|]
fun1 name = [text|this_could_be_'${name}'_long_identifier|]

s2t = strToStrictText

main = do 
        print "dog"
--        let var = fun "cat"
--        let v2 = fun1 (s2t $ show 3)
--        print v2
--        print var
        print "pig"
        
