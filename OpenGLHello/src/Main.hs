{-# LANGUAGE DataKinds #-}
module Main where

import Control.Concurrent (threadDelay)
import Control.Monad (unless, when)
import Graphics.Rendering.OpenGL as GL 
import Graphics.Rendering.OpenGL.GLU.Matrix as GM
       
-- http://hackage.haskell.org/package/FTGL-2.1/docs/Graphics-Rendering-FTGL.html
-- KEY: render font "Hello"
import qualified Graphics.Rendering.FTGL as FT
       
import qualified Graphics.UI.GLFW as G 
import Bindings.GLFW 

import System.Exit
import System.IO
import Control.Monad
import System.Random
import Data.Set(Set) 
import Data.IORef 
import Data.Maybe
import qualified Data.Set as S 
import Linear.V3
import Linear.V3(cross) 
import Linear.Vector
import Linear.Matrix
import Linear.Projection as P
import Linear.Metric(norm, signorm)
import Text.Regex
import System.FilePath ((</>))
       
import AronModule hiding(clear)
import AronGraphic


import Data.Word
import Data.IORef
import Data.Vector.Storable

import Foreign.Ptr
import Foreign.ForeignPtr
import Control.Monad
import System.IO

import Graphics.UI.GLUT
import Graphics.Rendering.OpenGL.GL.ReadCopyPixels

import Codec.Picture.Types -- from the JuicyPixels librar
import Codec.Picture.Png  

main :: IO ()
main = do
  _ <- getArgsAndInitialize 
  window  <- createWindow "Hello World"
  saveimg <- once (saveImage window)
  displayCallback $= display window saveimg
  mainLoop

-- we don't want to re-create the PNG file every frame
once :: IO () -> IO (IO ())
once action = do
  ref <- newIORef True
  return $ do
    b <- readIORef ref
    when b $ do
      writeIORef ref False
      action

display :: Window -> IO () -> DisplayCallback
display window saveimg = do 
  GL.clear [ ColorBuffer ]
  
  -- draw something:
  renderPrimitive Lines $ do
    vertex $ Vertex2 (-0.5) (-0.4 :: Double)
    vertex $ Vertex2 ( 0.7) ( 0.3 :: Double)  
  flush  
  saveimg

-- read pixels, convert them to first a Vector then a JuicyPixel image
-- and save the image as a PNG file
saveImage :: Window -> IO ()
saveImage window = do
  currentWindow $= Just window
  Size w h <- get windowSize
  let npixels = fromIntegral (w*h) :: Int
      nbytes  = 3*npixels
  fptr <- mallocForeignPtrArray nbytes :: IO (ForeignPtr Word8)
  withForeignPtr fptr $ \ptr -> do
    let pdata = PixelData RGB UnsignedByte ptr :: PixelData Word8
    readPixels (Position 0 0) (Size w h) pdata
  let fptr' = castForeignPtr fptr :: ForeignPtr (PixelBaseComponent PixelRGB8)
  let imgdata = unsafeFromForeignPtr0 fptr' npixels :: Vector (PixelBaseComponent PixelRGB8)
  let image = Image (fromIntegral w) (fromIntegral h) imgdata :: Image PixelRGB8
  writePng "test.png" image
